package com.haui.adidasshoes.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.haui.adidasshoes.web.rest.TestUtil;

public class TrackOrderUserTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TrackOrderUser.class);
        TrackOrderUser trackOrderUser1 = new TrackOrderUser();
        trackOrderUser1.setId(1L);
        TrackOrderUser trackOrderUser2 = new TrackOrderUser();
        trackOrderUser2.setId(trackOrderUser1.getId());
        assertThat(trackOrderUser1).isEqualTo(trackOrderUser2);
        trackOrderUser2.setId(2L);
        assertThat(trackOrderUser1).isNotEqualTo(trackOrderUser2);
        trackOrderUser1.setId(null);
        assertThat(trackOrderUser1).isNotEqualTo(trackOrderUser2);
    }
}
