package com.haui.adidasshoes.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.haui.adidasshoes.web.rest.TestUtil;

public class ListProductTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ListProduct.class);
        ListProduct listProduct1 = new ListProduct();
        listProduct1.setId(1L);
        ListProduct listProduct2 = new ListProduct();
        listProduct2.setId(listProduct1.getId());
        assertThat(listProduct1).isEqualTo(listProduct2);
        listProduct2.setId(2L);
        assertThat(listProduct1).isNotEqualTo(listProduct2);
        listProduct1.setId(null);
        assertThat(listProduct1).isNotEqualTo(listProduct2);
    }
}
