package com.haui.adidasshoes.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.haui.adidasshoes.web.rest.TestUtil;

public class DetailProductTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DetailProduct.class);
        DetailProduct detailProduct1 = new DetailProduct();
        detailProduct1.setId(1L);
        DetailProduct detailProduct2 = new DetailProduct();
        detailProduct2.setId(detailProduct1.getId());
        assertThat(detailProduct1).isEqualTo(detailProduct2);
        detailProduct2.setId(2L);
        assertThat(detailProduct1).isNotEqualTo(detailProduct2);
        detailProduct1.setId(null);
        assertThat(detailProduct1).isNotEqualTo(detailProduct2);
    }
}
