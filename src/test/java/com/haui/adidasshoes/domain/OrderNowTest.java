package com.haui.adidasshoes.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.haui.adidasshoes.web.rest.TestUtil;

public class OrderNowTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrderNow.class);
        OrderNow orderNow1 = new OrderNow();
        orderNow1.setId(1L);
        OrderNow orderNow2 = new OrderNow();
        orderNow2.setId(orderNow1.getId());
        assertThat(orderNow1).isEqualTo(orderNow2);
        orderNow2.setId(2L);
        assertThat(orderNow1).isNotEqualTo(orderNow2);
        orderNow1.setId(null);
        assertThat(orderNow1).isNotEqualTo(orderNow2);
    }
}
