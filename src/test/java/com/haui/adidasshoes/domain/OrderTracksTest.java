package com.haui.adidasshoes.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.haui.adidasshoes.web.rest.TestUtil;

public class OrderTracksTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrderTracks.class);
        OrderTracks orderTracks1 = new OrderTracks();
        orderTracks1.setId(1L);
        OrderTracks orderTracks2 = new OrderTracks();
        orderTracks2.setId(orderTracks1.getId());
        assertThat(orderTracks1).isEqualTo(orderTracks2);
        orderTracks2.setId(2L);
        assertThat(orderTracks1).isNotEqualTo(orderTracks2);
        orderTracks1.setId(null);
        assertThat(orderTracks1).isNotEqualTo(orderTracks2);
    }
}
