package com.haui.adidasshoes.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.haui.adidasshoes.web.rest.TestUtil;

public class ListCartTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ListCart.class);
        ListCart listCart1 = new ListCart();
        listCart1.setId(1L);
        ListCart listCart2 = new ListCart();
        listCart2.setId(listCart1.getId());
        assertThat(listCart1).isEqualTo(listCart2);
        listCart2.setId(2L);
        assertThat(listCart1).isNotEqualTo(listCart2);
        listCart1.setId(null);
        assertThat(listCart1).isNotEqualTo(listCart2);
    }
}
