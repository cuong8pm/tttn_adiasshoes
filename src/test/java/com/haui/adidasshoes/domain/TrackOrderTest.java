package com.haui.adidasshoes.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.haui.adidasshoes.web.rest.TestUtil;

public class TrackOrderTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TrackOrder.class);
        TrackOrder trackOrder1 = new TrackOrder();
        trackOrder1.setId(1L);
        TrackOrder trackOrder2 = new TrackOrder();
        trackOrder2.setId(trackOrder1.getId());
        assertThat(trackOrder1).isEqualTo(trackOrder2);
        trackOrder2.setId(2L);
        assertThat(trackOrder1).isNotEqualTo(trackOrder2);
        trackOrder1.setId(null);
        assertThat(trackOrder1).isNotEqualTo(trackOrder2);
    }
}
