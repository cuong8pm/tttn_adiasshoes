package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.HauiAdidasShoesApp;
import com.haui.adidasshoes.domain.DetailProduct;
import com.haui.adidasshoes.repository.DetailProductRepository;
import com.haui.adidasshoes.service.DetailProductService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DetailProductResource} REST controller.
 */
@SpringBootTest(classes = HauiAdidasShoesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class DetailProductResourceIT {

    @Autowired
    private DetailProductRepository detailProductRepository;

    @Autowired
    private DetailProductService detailProductService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDetailProductMockMvc;

    private DetailProduct detailProduct;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DetailProduct createEntity(EntityManager em) {
        DetailProduct detailProduct = new DetailProduct();
        return detailProduct;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DetailProduct createUpdatedEntity(EntityManager em) {
        DetailProduct detailProduct = new DetailProduct();
        return detailProduct;
    }

    @BeforeEach
    public void initTest() {
        detailProduct = createEntity(em);
    }

    @Test
    @Transactional
    public void createDetailProduct() throws Exception {
        int databaseSizeBeforeCreate = detailProductRepository.findAll().size();
        // Create the DetailProduct
        restDetailProductMockMvc.perform(post("/api/detail-products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(detailProduct)))
            .andExpect(status().isCreated());

        // Validate the DetailProduct in the database
        List<DetailProduct> detailProductList = detailProductRepository.findAll();
        assertThat(detailProductList).hasSize(databaseSizeBeforeCreate + 1);
        DetailProduct testDetailProduct = detailProductList.get(detailProductList.size() - 1);
    }

    @Test
    @Transactional
    public void createDetailProductWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = detailProductRepository.findAll().size();

        // Create the DetailProduct with an existing ID
        detailProduct.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDetailProductMockMvc.perform(post("/api/detail-products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(detailProduct)))
            .andExpect(status().isBadRequest());

        // Validate the DetailProduct in the database
        List<DetailProduct> detailProductList = detailProductRepository.findAll();
        assertThat(detailProductList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDetailProducts() throws Exception {
        // Initialize the database
        detailProductRepository.saveAndFlush(detailProduct);

        // Get all the detailProductList
        restDetailProductMockMvc.perform(get("/api/detail-products?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(detailProduct.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getDetailProduct() throws Exception {
        // Initialize the database
        detailProductRepository.saveAndFlush(detailProduct);

        // Get the detailProduct
        restDetailProductMockMvc.perform(get("/api/detail-products/{id}", detailProduct.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(detailProduct.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingDetailProduct() throws Exception {
        // Get the detailProduct
        restDetailProductMockMvc.perform(get("/api/detail-products/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDetailProduct() throws Exception {
        // Initialize the database
        detailProductService.save(detailProduct);

        int databaseSizeBeforeUpdate = detailProductRepository.findAll().size();

        // Update the detailProduct
        DetailProduct updatedDetailProduct = detailProductRepository.findById(detailProduct.getId()).get();
        // Disconnect from session so that the updates on updatedDetailProduct are not directly saved in db
        em.detach(updatedDetailProduct);

        restDetailProductMockMvc.perform(put("/api/detail-products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedDetailProduct)))
            .andExpect(status().isOk());

        // Validate the DetailProduct in the database
        List<DetailProduct> detailProductList = detailProductRepository.findAll();
        assertThat(detailProductList).hasSize(databaseSizeBeforeUpdate);
        DetailProduct testDetailProduct = detailProductList.get(detailProductList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingDetailProduct() throws Exception {
        int databaseSizeBeforeUpdate = detailProductRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDetailProductMockMvc.perform(put("/api/detail-products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(detailProduct)))
            .andExpect(status().isBadRequest());

        // Validate the DetailProduct in the database
        List<DetailProduct> detailProductList = detailProductRepository.findAll();
        assertThat(detailProductList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDetailProduct() throws Exception {
        // Initialize the database
        detailProductService.save(detailProduct);

        int databaseSizeBeforeDelete = detailProductRepository.findAll().size();

        // Delete the detailProduct
        restDetailProductMockMvc.perform(delete("/api/detail-products/{id}", detailProduct.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DetailProduct> detailProductList = detailProductRepository.findAll();
        assertThat(detailProductList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
