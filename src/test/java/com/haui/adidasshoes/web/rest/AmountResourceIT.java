package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.HauiAdidasShoesApp;
import com.haui.adidasshoes.domain.Amount;
import com.haui.adidasshoes.domain.Product;
import com.haui.adidasshoes.domain.Size;
import com.haui.adidasshoes.repository.AmountRepository;
import com.haui.adidasshoes.service.AmountService;
import com.haui.adidasshoes.service.dto.AmountCriteria;
import com.haui.adidasshoes.service.AmountQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AmountResource} REST controller.
 */
@SpringBootTest(classes = HauiAdidasShoesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AmountResourceIT {

    private static final Integer DEFAULT_TOTAL = 1;
    private static final Integer UPDATED_TOTAL = 2;
    private static final Integer SMALLER_TOTAL = 1 - 1;

    private static final Integer DEFAULT_INVENTORY = 1;
    private static final Integer UPDATED_INVENTORY = 2;
    private static final Integer SMALLER_INVENTORY = 1 - 1;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_SOLD_PRODUCT = 1;
    private static final Integer UPDATED_SOLD_PRODUCT = 2;
    private static final Integer SMALLER_SOLD_PRODUCT = 1 - 1;

    @Autowired
    private AmountRepository amountRepository;

    @Autowired
    private AmountService amountService;

    @Autowired
    private AmountQueryService amountQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAmountMockMvc;

    private Amount amount;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Amount createEntity(EntityManager em) {
        Amount amount = new Amount()
            .total(DEFAULT_TOTAL)
            .inventory(DEFAULT_INVENTORY)
            .description(DEFAULT_DESCRIPTION)
            .soldProduct(DEFAULT_SOLD_PRODUCT);
        return amount;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Amount createUpdatedEntity(EntityManager em) {
        Amount amount = new Amount()
            .total(UPDATED_TOTAL)
            .inventory(UPDATED_INVENTORY)
            .description(UPDATED_DESCRIPTION)
            .soldProduct(UPDATED_SOLD_PRODUCT);
        return amount;
    }

    @BeforeEach
    public void initTest() {
        amount = createEntity(em);
    }

    @Test
    @Transactional
    public void createAmount() throws Exception {
        int databaseSizeBeforeCreate = amountRepository.findAll().size();
        // Create the Amount
        restAmountMockMvc.perform(post("/api/amounts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(amount)))
            .andExpect(status().isCreated());

        // Validate the Amount in the database
        List<Amount> amountList = amountRepository.findAll();
        assertThat(amountList).hasSize(databaseSizeBeforeCreate + 1);
        Amount testAmount = amountList.get(amountList.size() - 1);
        assertThat(testAmount.getTotal()).isEqualTo(DEFAULT_TOTAL);
        assertThat(testAmount.getInventory()).isEqualTo(DEFAULT_INVENTORY);
        assertThat(testAmount.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testAmount.getSoldProduct()).isEqualTo(DEFAULT_SOLD_PRODUCT);
    }

    @Test
    @Transactional
    public void createAmountWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = amountRepository.findAll().size();

        // Create the Amount with an existing ID
        amount.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAmountMockMvc.perform(post("/api/amounts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(amount)))
            .andExpect(status().isBadRequest());

        // Validate the Amount in the database
        List<Amount> amountList = amountRepository.findAll();
        assertThat(amountList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAmounts() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList
        restAmountMockMvc.perform(get("/api/amounts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(amount.getId().intValue())))
            .andExpect(jsonPath("$.[*].total").value(hasItem(DEFAULT_TOTAL)))
            .andExpect(jsonPath("$.[*].inventory").value(hasItem(DEFAULT_INVENTORY)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].soldProduct").value(hasItem(DEFAULT_SOLD_PRODUCT)));
    }
    
    @Test
    @Transactional
    public void getAmount() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get the amount
        restAmountMockMvc.perform(get("/api/amounts/{id}", amount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(amount.getId().intValue()))
            .andExpect(jsonPath("$.total").value(DEFAULT_TOTAL))
            .andExpect(jsonPath("$.inventory").value(DEFAULT_INVENTORY))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.soldProduct").value(DEFAULT_SOLD_PRODUCT));
    }


    @Test
    @Transactional
    public void getAmountsByIdFiltering() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        Long id = amount.getId();

        defaultAmountShouldBeFound("id.equals=" + id);
        defaultAmountShouldNotBeFound("id.notEquals=" + id);

        defaultAmountShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultAmountShouldNotBeFound("id.greaterThan=" + id);

        defaultAmountShouldBeFound("id.lessThanOrEqual=" + id);
        defaultAmountShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllAmountsByTotalIsEqualToSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where total equals to DEFAULT_TOTAL
        defaultAmountShouldBeFound("total.equals=" + DEFAULT_TOTAL);

        // Get all the amountList where total equals to UPDATED_TOTAL
        defaultAmountShouldNotBeFound("total.equals=" + UPDATED_TOTAL);
    }

    @Test
    @Transactional
    public void getAllAmountsByTotalIsNotEqualToSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where total not equals to DEFAULT_TOTAL
        defaultAmountShouldNotBeFound("total.notEquals=" + DEFAULT_TOTAL);

        // Get all the amountList where total not equals to UPDATED_TOTAL
        defaultAmountShouldBeFound("total.notEquals=" + UPDATED_TOTAL);
    }

    @Test
    @Transactional
    public void getAllAmountsByTotalIsInShouldWork() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where total in DEFAULT_TOTAL or UPDATED_TOTAL
        defaultAmountShouldBeFound("total.in=" + DEFAULT_TOTAL + "," + UPDATED_TOTAL);

        // Get all the amountList where total equals to UPDATED_TOTAL
        defaultAmountShouldNotBeFound("total.in=" + UPDATED_TOTAL);
    }

    @Test
    @Transactional
    public void getAllAmountsByTotalIsNullOrNotNull() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where total is not null
        defaultAmountShouldBeFound("total.specified=true");

        // Get all the amountList where total is null
        defaultAmountShouldNotBeFound("total.specified=false");
    }

    @Test
    @Transactional
    public void getAllAmountsByTotalIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where total is greater than or equal to DEFAULT_TOTAL
        defaultAmountShouldBeFound("total.greaterThanOrEqual=" + DEFAULT_TOTAL);

        // Get all the amountList where total is greater than or equal to UPDATED_TOTAL
        defaultAmountShouldNotBeFound("total.greaterThanOrEqual=" + UPDATED_TOTAL);
    }

    @Test
    @Transactional
    public void getAllAmountsByTotalIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where total is less than or equal to DEFAULT_TOTAL
        defaultAmountShouldBeFound("total.lessThanOrEqual=" + DEFAULT_TOTAL);

        // Get all the amountList where total is less than or equal to SMALLER_TOTAL
        defaultAmountShouldNotBeFound("total.lessThanOrEqual=" + SMALLER_TOTAL);
    }

    @Test
    @Transactional
    public void getAllAmountsByTotalIsLessThanSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where total is less than DEFAULT_TOTAL
        defaultAmountShouldNotBeFound("total.lessThan=" + DEFAULT_TOTAL);

        // Get all the amountList where total is less than UPDATED_TOTAL
        defaultAmountShouldBeFound("total.lessThan=" + UPDATED_TOTAL);
    }

    @Test
    @Transactional
    public void getAllAmountsByTotalIsGreaterThanSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where total is greater than DEFAULT_TOTAL
        defaultAmountShouldNotBeFound("total.greaterThan=" + DEFAULT_TOTAL);

        // Get all the amountList where total is greater than SMALLER_TOTAL
        defaultAmountShouldBeFound("total.greaterThan=" + SMALLER_TOTAL);
    }


    @Test
    @Transactional
    public void getAllAmountsByInventoryIsEqualToSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where inventory equals to DEFAULT_INVENTORY
        defaultAmountShouldBeFound("inventory.equals=" + DEFAULT_INVENTORY);

        // Get all the amountList where inventory equals to UPDATED_INVENTORY
        defaultAmountShouldNotBeFound("inventory.equals=" + UPDATED_INVENTORY);
    }

    @Test
    @Transactional
    public void getAllAmountsByInventoryIsNotEqualToSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where inventory not equals to DEFAULT_INVENTORY
        defaultAmountShouldNotBeFound("inventory.notEquals=" + DEFAULT_INVENTORY);

        // Get all the amountList where inventory not equals to UPDATED_INVENTORY
        defaultAmountShouldBeFound("inventory.notEquals=" + UPDATED_INVENTORY);
    }

    @Test
    @Transactional
    public void getAllAmountsByInventoryIsInShouldWork() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where inventory in DEFAULT_INVENTORY or UPDATED_INVENTORY
        defaultAmountShouldBeFound("inventory.in=" + DEFAULT_INVENTORY + "," + UPDATED_INVENTORY);

        // Get all the amountList where inventory equals to UPDATED_INVENTORY
        defaultAmountShouldNotBeFound("inventory.in=" + UPDATED_INVENTORY);
    }

    @Test
    @Transactional
    public void getAllAmountsByInventoryIsNullOrNotNull() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where inventory is not null
        defaultAmountShouldBeFound("inventory.specified=true");

        // Get all the amountList where inventory is null
        defaultAmountShouldNotBeFound("inventory.specified=false");
    }

    @Test
    @Transactional
    public void getAllAmountsByInventoryIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where inventory is greater than or equal to DEFAULT_INVENTORY
        defaultAmountShouldBeFound("inventory.greaterThanOrEqual=" + DEFAULT_INVENTORY);

        // Get all the amountList where inventory is greater than or equal to UPDATED_INVENTORY
        defaultAmountShouldNotBeFound("inventory.greaterThanOrEqual=" + UPDATED_INVENTORY);
    }

    @Test
    @Transactional
    public void getAllAmountsByInventoryIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where inventory is less than or equal to DEFAULT_INVENTORY
        defaultAmountShouldBeFound("inventory.lessThanOrEqual=" + DEFAULT_INVENTORY);

        // Get all the amountList where inventory is less than or equal to SMALLER_INVENTORY
        defaultAmountShouldNotBeFound("inventory.lessThanOrEqual=" + SMALLER_INVENTORY);
    }

    @Test
    @Transactional
    public void getAllAmountsByInventoryIsLessThanSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where inventory is less than DEFAULT_INVENTORY
        defaultAmountShouldNotBeFound("inventory.lessThan=" + DEFAULT_INVENTORY);

        // Get all the amountList where inventory is less than UPDATED_INVENTORY
        defaultAmountShouldBeFound("inventory.lessThan=" + UPDATED_INVENTORY);
    }

    @Test
    @Transactional
    public void getAllAmountsByInventoryIsGreaterThanSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where inventory is greater than DEFAULT_INVENTORY
        defaultAmountShouldNotBeFound("inventory.greaterThan=" + DEFAULT_INVENTORY);

        // Get all the amountList where inventory is greater than SMALLER_INVENTORY
        defaultAmountShouldBeFound("inventory.greaterThan=" + SMALLER_INVENTORY);
    }


    @Test
    @Transactional
    public void getAllAmountsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where description equals to DEFAULT_DESCRIPTION
        defaultAmountShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the amountList where description equals to UPDATED_DESCRIPTION
        defaultAmountShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllAmountsByDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where description not equals to DEFAULT_DESCRIPTION
        defaultAmountShouldNotBeFound("description.notEquals=" + DEFAULT_DESCRIPTION);

        // Get all the amountList where description not equals to UPDATED_DESCRIPTION
        defaultAmountShouldBeFound("description.notEquals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllAmountsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultAmountShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the amountList where description equals to UPDATED_DESCRIPTION
        defaultAmountShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllAmountsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where description is not null
        defaultAmountShouldBeFound("description.specified=true");

        // Get all the amountList where description is null
        defaultAmountShouldNotBeFound("description.specified=false");
    }
                @Test
    @Transactional
    public void getAllAmountsByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where description contains DEFAULT_DESCRIPTION
        defaultAmountShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the amountList where description contains UPDATED_DESCRIPTION
        defaultAmountShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllAmountsByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where description does not contain DEFAULT_DESCRIPTION
        defaultAmountShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the amountList where description does not contain UPDATED_DESCRIPTION
        defaultAmountShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }


    @Test
    @Transactional
    public void getAllAmountsBySoldProductIsEqualToSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where soldProduct equals to DEFAULT_SOLD_PRODUCT
        defaultAmountShouldBeFound("soldProduct.equals=" + DEFAULT_SOLD_PRODUCT);

        // Get all the amountList where soldProduct equals to UPDATED_SOLD_PRODUCT
        defaultAmountShouldNotBeFound("soldProduct.equals=" + UPDATED_SOLD_PRODUCT);
    }

    @Test
    @Transactional
    public void getAllAmountsBySoldProductIsNotEqualToSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where soldProduct not equals to DEFAULT_SOLD_PRODUCT
        defaultAmountShouldNotBeFound("soldProduct.notEquals=" + DEFAULT_SOLD_PRODUCT);

        // Get all the amountList where soldProduct not equals to UPDATED_SOLD_PRODUCT
        defaultAmountShouldBeFound("soldProduct.notEquals=" + UPDATED_SOLD_PRODUCT);
    }

    @Test
    @Transactional
    public void getAllAmountsBySoldProductIsInShouldWork() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where soldProduct in DEFAULT_SOLD_PRODUCT or UPDATED_SOLD_PRODUCT
        defaultAmountShouldBeFound("soldProduct.in=" + DEFAULT_SOLD_PRODUCT + "," + UPDATED_SOLD_PRODUCT);

        // Get all the amountList where soldProduct equals to UPDATED_SOLD_PRODUCT
        defaultAmountShouldNotBeFound("soldProduct.in=" + UPDATED_SOLD_PRODUCT);
    }

    @Test
    @Transactional
    public void getAllAmountsBySoldProductIsNullOrNotNull() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where soldProduct is not null
        defaultAmountShouldBeFound("soldProduct.specified=true");

        // Get all the amountList where soldProduct is null
        defaultAmountShouldNotBeFound("soldProduct.specified=false");
    }

    @Test
    @Transactional
    public void getAllAmountsBySoldProductIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where soldProduct is greater than or equal to DEFAULT_SOLD_PRODUCT
        defaultAmountShouldBeFound("soldProduct.greaterThanOrEqual=" + DEFAULT_SOLD_PRODUCT);

        // Get all the amountList where soldProduct is greater than or equal to UPDATED_SOLD_PRODUCT
        defaultAmountShouldNotBeFound("soldProduct.greaterThanOrEqual=" + UPDATED_SOLD_PRODUCT);
    }

    @Test
    @Transactional
    public void getAllAmountsBySoldProductIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where soldProduct is less than or equal to DEFAULT_SOLD_PRODUCT
        defaultAmountShouldBeFound("soldProduct.lessThanOrEqual=" + DEFAULT_SOLD_PRODUCT);

        // Get all the amountList where soldProduct is less than or equal to SMALLER_SOLD_PRODUCT
        defaultAmountShouldNotBeFound("soldProduct.lessThanOrEqual=" + SMALLER_SOLD_PRODUCT);
    }

    @Test
    @Transactional
    public void getAllAmountsBySoldProductIsLessThanSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where soldProduct is less than DEFAULT_SOLD_PRODUCT
        defaultAmountShouldNotBeFound("soldProduct.lessThan=" + DEFAULT_SOLD_PRODUCT);

        // Get all the amountList where soldProduct is less than UPDATED_SOLD_PRODUCT
        defaultAmountShouldBeFound("soldProduct.lessThan=" + UPDATED_SOLD_PRODUCT);
    }

    @Test
    @Transactional
    public void getAllAmountsBySoldProductIsGreaterThanSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);

        // Get all the amountList where soldProduct is greater than DEFAULT_SOLD_PRODUCT
        defaultAmountShouldNotBeFound("soldProduct.greaterThan=" + DEFAULT_SOLD_PRODUCT);

        // Get all the amountList where soldProduct is greater than SMALLER_SOLD_PRODUCT
        defaultAmountShouldBeFound("soldProduct.greaterThan=" + SMALLER_SOLD_PRODUCT);
    }


    @Test
    @Transactional
    public void getAllAmountsByProductIsEqualToSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);
        Product product = ProductResourceIT.createEntity(em);
        em.persist(product);
        em.flush();
        amount.setProduct(product);
        amountRepository.saveAndFlush(amount);
        Long productId = product.getId();

        // Get all the amountList where product equals to productId
        defaultAmountShouldBeFound("productId.equals=" + productId);

        // Get all the amountList where product equals to productId + 1
        defaultAmountShouldNotBeFound("productId.equals=" + (productId + 1));
    }


    @Test
    @Transactional
    public void getAllAmountsBySizeIsEqualToSomething() throws Exception {
        // Initialize the database
        amountRepository.saveAndFlush(amount);
        Size size = SizeResourceIT.createEntity(em);
        em.persist(size);
        em.flush();
        amount.setSize(size);
        amountRepository.saveAndFlush(amount);
        Long sizeId = size.getId();

        // Get all the amountList where size equals to sizeId
        defaultAmountShouldBeFound("sizeId.equals=" + sizeId);

        // Get all the amountList where size equals to sizeId + 1
        defaultAmountShouldNotBeFound("sizeId.equals=" + (sizeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAmountShouldBeFound(String filter) throws Exception {
        restAmountMockMvc.perform(get("/api/amounts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(amount.getId().intValue())))
            .andExpect(jsonPath("$.[*].total").value(hasItem(DEFAULT_TOTAL)))
            .andExpect(jsonPath("$.[*].inventory").value(hasItem(DEFAULT_INVENTORY)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].soldProduct").value(hasItem(DEFAULT_SOLD_PRODUCT)));

        // Check, that the count call also returns 1
        restAmountMockMvc.perform(get("/api/amounts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAmountShouldNotBeFound(String filter) throws Exception {
        restAmountMockMvc.perform(get("/api/amounts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAmountMockMvc.perform(get("/api/amounts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingAmount() throws Exception {
        // Get the amount
        restAmountMockMvc.perform(get("/api/amounts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAmount() throws Exception {
        // Initialize the database
        amountService.save(amount);

        int databaseSizeBeforeUpdate = amountRepository.findAll().size();

        // Update the amount
        Amount updatedAmount = amountRepository.findById(amount.getId()).get();
        // Disconnect from session so that the updates on updatedAmount are not directly saved in db
        em.detach(updatedAmount);
        updatedAmount
            .total(UPDATED_TOTAL)
            .inventory(UPDATED_INVENTORY)
            .description(UPDATED_DESCRIPTION)
            .soldProduct(UPDATED_SOLD_PRODUCT);

        restAmountMockMvc.perform(put("/api/amounts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAmount)))
            .andExpect(status().isOk());

        // Validate the Amount in the database
        List<Amount> amountList = amountRepository.findAll();
        assertThat(amountList).hasSize(databaseSizeBeforeUpdate);
        Amount testAmount = amountList.get(amountList.size() - 1);
        assertThat(testAmount.getTotal()).isEqualTo(UPDATED_TOTAL);
        assertThat(testAmount.getInventory()).isEqualTo(UPDATED_INVENTORY);
        assertThat(testAmount.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testAmount.getSoldProduct()).isEqualTo(UPDATED_SOLD_PRODUCT);
    }

    @Test
    @Transactional
    public void updateNonExistingAmount() throws Exception {
        int databaseSizeBeforeUpdate = amountRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAmountMockMvc.perform(put("/api/amounts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(amount)))
            .andExpect(status().isBadRequest());

        // Validate the Amount in the database
        List<Amount> amountList = amountRepository.findAll();
        assertThat(amountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAmount() throws Exception {
        // Initialize the database
        amountService.save(amount);

        int databaseSizeBeforeDelete = amountRepository.findAll().size();

        // Delete the amount
        restAmountMockMvc.perform(delete("/api/amounts/{id}", amount.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Amount> amountList = amountRepository.findAll();
        assertThat(amountList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
