package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.HauiAdidasShoesApp;
import com.haui.adidasshoes.domain.ListCart;
import com.haui.adidasshoes.repository.ListCartRepository;
import com.haui.adidasshoes.service.ListCartService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ListCartResource} REST controller.
 */
@SpringBootTest(classes = HauiAdidasShoesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ListCartResourceIT {

    private static final Integer DEFAULT_AMOUNT = 1;
    private static final Integer UPDATED_AMOUNT = 2;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private ListCartRepository listCartRepository;

    @Autowired
    private ListCartService listCartService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restListCartMockMvc;

    private ListCart listCart;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ListCart createEntity(EntityManager em) {
        ListCart listCart = new ListCart()
            .amount(DEFAULT_AMOUNT)
            .description(DEFAULT_DESCRIPTION);
        return listCart;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ListCart createUpdatedEntity(EntityManager em) {
        ListCart listCart = new ListCart()
            .amount(UPDATED_AMOUNT)
            .description(UPDATED_DESCRIPTION);
        return listCart;
    }

    @BeforeEach
    public void initTest() {
        listCart = createEntity(em);
    }

    @Test
    @Transactional
    public void createListCart() throws Exception {
        int databaseSizeBeforeCreate = listCartRepository.findAll().size();
        // Create the ListCart
        restListCartMockMvc.perform(post("/api/list-carts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(listCart)))
            .andExpect(status().isCreated());

        // Validate the ListCart in the database
        List<ListCart> listCartList = listCartRepository.findAll();
        assertThat(listCartList).hasSize(databaseSizeBeforeCreate + 1);
        ListCart testListCart = listCartList.get(listCartList.size() - 1);
        assertThat(testListCart.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testListCart.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createListCartWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = listCartRepository.findAll().size();

        // Create the ListCart with an existing ID
        listCart.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restListCartMockMvc.perform(post("/api/list-carts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(listCart)))
            .andExpect(status().isBadRequest());

        // Validate the ListCart in the database
        List<ListCart> listCartList = listCartRepository.findAll();
        assertThat(listCartList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllListCarts() throws Exception {
        // Initialize the database
        listCartRepository.saveAndFlush(listCart);

        // Get all the listCartList
        restListCartMockMvc.perform(get("/api/list-carts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(listCart.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }
    
    @Test
    @Transactional
    public void getListCart() throws Exception {
        // Initialize the database
        listCartRepository.saveAndFlush(listCart);

        // Get the listCart
        restListCartMockMvc.perform(get("/api/list-carts/{id}", listCart.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(listCart.getId().intValue()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }
    @Test
    @Transactional
    public void getNonExistingListCart() throws Exception {
        // Get the listCart
        restListCartMockMvc.perform(get("/api/list-carts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateListCart() throws Exception {
        // Initialize the database
        listCartService.save(listCart);

        int databaseSizeBeforeUpdate = listCartRepository.findAll().size();

        // Update the listCart
        ListCart updatedListCart = listCartRepository.findById(listCart.getId()).get();
        // Disconnect from session so that the updates on updatedListCart are not directly saved in db
        em.detach(updatedListCart);
        updatedListCart
            .amount(UPDATED_AMOUNT)
            .description(UPDATED_DESCRIPTION);

        restListCartMockMvc.perform(put("/api/list-carts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedListCart)))
            .andExpect(status().isOk());

        // Validate the ListCart in the database
        List<ListCart> listCartList = listCartRepository.findAll();
        assertThat(listCartList).hasSize(databaseSizeBeforeUpdate);
        ListCart testListCart = listCartList.get(listCartList.size() - 1);
        assertThat(testListCart.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testListCart.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingListCart() throws Exception {
        int databaseSizeBeforeUpdate = listCartRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restListCartMockMvc.perform(put("/api/list-carts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(listCart)))
            .andExpect(status().isBadRequest());

        // Validate the ListCart in the database
        List<ListCart> listCartList = listCartRepository.findAll();
        assertThat(listCartList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteListCart() throws Exception {
        // Initialize the database
        listCartService.save(listCart);

        int databaseSizeBeforeDelete = listCartRepository.findAll().size();

        // Delete the listCart
        restListCartMockMvc.perform(delete("/api/list-carts/{id}", listCart.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ListCart> listCartList = listCartRepository.findAll();
        assertThat(listCartList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
