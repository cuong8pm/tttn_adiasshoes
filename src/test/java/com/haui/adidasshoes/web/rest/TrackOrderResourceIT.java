package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.HauiAdidasShoesApp;
import com.haui.adidasshoes.domain.TrackOrder;
import com.haui.adidasshoes.domain.Orders;
import com.haui.adidasshoes.repository.TrackOrderRepository;
import com.haui.adidasshoes.service.TrackOrderService;
import com.haui.adidasshoes.service.dto.TrackOrderCriteria;
import com.haui.adidasshoes.service.TrackOrderQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TrackOrderResource} REST controller.
 */
@SpringBootTest(classes = HauiAdidasShoesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class TrackOrderResourceIT {

    private static final Boolean DEFAULT_ORDER_CONFIRM = false;
    private static final Boolean UPDATED_ORDER_CONFIRM = true;

    private static final Boolean DEFAULT_WAITTING_PRODUCT = false;
    private static final Boolean UPDATED_WAITTING_PRODUCT = true;

    private static final Boolean DEFAULT_SHIPPING = false;
    private static final Boolean UPDATED_SHIPPING = true;

    private static final Boolean DEFAULT_DELIVERED = false;
    private static final Boolean UPDATED_DELIVERED = true;

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    @Autowired
    private TrackOrderRepository trackOrderRepository;

    @Autowired
    private TrackOrderService trackOrderService;

    @Autowired
    private TrackOrderQueryService trackOrderQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTrackOrderMockMvc;

    private TrackOrder trackOrder;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TrackOrder createEntity(EntityManager em) {
        TrackOrder trackOrder = new TrackOrder()
            .orderConfirm(DEFAULT_ORDER_CONFIRM)
            .waittingProduct(DEFAULT_WAITTING_PRODUCT)
            .shipping(DEFAULT_SHIPPING)
            .delivered(DEFAULT_DELIVERED)
            .status(DEFAULT_STATUS);
        return trackOrder;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TrackOrder createUpdatedEntity(EntityManager em) {
        TrackOrder trackOrder = new TrackOrder()
            .orderConfirm(UPDATED_ORDER_CONFIRM)
            .waittingProduct(UPDATED_WAITTING_PRODUCT)
            .shipping(UPDATED_SHIPPING)
            .delivered(UPDATED_DELIVERED)
            .status(UPDATED_STATUS);
        return trackOrder;
    }

    @BeforeEach
    public void initTest() {
        trackOrder = createEntity(em);
    }

    @Test
    @Transactional
    public void createTrackOrder() throws Exception {
        int databaseSizeBeforeCreate = trackOrderRepository.findAll().size();
        // Create the TrackOrder
        restTrackOrderMockMvc.perform(post("/api/track-orders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(trackOrder)))
            .andExpect(status().isCreated());

        // Validate the TrackOrder in the database
        List<TrackOrder> trackOrderList = trackOrderRepository.findAll();
        assertThat(trackOrderList).hasSize(databaseSizeBeforeCreate + 1);
        TrackOrder testTrackOrder = trackOrderList.get(trackOrderList.size() - 1);
        assertThat(testTrackOrder.isOrderConfirm()).isEqualTo(DEFAULT_ORDER_CONFIRM);
        assertThat(testTrackOrder.isWaittingProduct()).isEqualTo(DEFAULT_WAITTING_PRODUCT);
        assertThat(testTrackOrder.isShipping()).isEqualTo(DEFAULT_SHIPPING);
        assertThat(testTrackOrder.isDelivered()).isEqualTo(DEFAULT_DELIVERED);
        assertThat(testTrackOrder.isStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createTrackOrderWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = trackOrderRepository.findAll().size();

        // Create the TrackOrder with an existing ID
        trackOrder.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTrackOrderMockMvc.perform(post("/api/track-orders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(trackOrder)))
            .andExpect(status().isBadRequest());

        // Validate the TrackOrder in the database
        List<TrackOrder> trackOrderList = trackOrderRepository.findAll();
        assertThat(trackOrderList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTrackOrders() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get all the trackOrderList
        restTrackOrderMockMvc.perform(get("/api/track-orders?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(trackOrder.getId().intValue())))
            .andExpect(jsonPath("$.[*].orderConfirm").value(hasItem(DEFAULT_ORDER_CONFIRM.booleanValue())))
            .andExpect(jsonPath("$.[*].waittingProduct").value(hasItem(DEFAULT_WAITTING_PRODUCT.booleanValue())))
            .andExpect(jsonPath("$.[*].shipping").value(hasItem(DEFAULT_SHIPPING.booleanValue())))
            .andExpect(jsonPath("$.[*].delivered").value(hasItem(DEFAULT_DELIVERED.booleanValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getTrackOrder() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get the trackOrder
        restTrackOrderMockMvc.perform(get("/api/track-orders/{id}", trackOrder.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(trackOrder.getId().intValue()))
            .andExpect(jsonPath("$.orderConfirm").value(DEFAULT_ORDER_CONFIRM.booleanValue()))
            .andExpect(jsonPath("$.waittingProduct").value(DEFAULT_WAITTING_PRODUCT.booleanValue()))
            .andExpect(jsonPath("$.shipping").value(DEFAULT_SHIPPING.booleanValue()))
            .andExpect(jsonPath("$.delivered").value(DEFAULT_DELIVERED.booleanValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()));
    }


    @Test
    @Transactional
    public void getTrackOrdersByIdFiltering() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        Long id = trackOrder.getId();

        defaultTrackOrderShouldBeFound("id.equals=" + id);
        defaultTrackOrderShouldNotBeFound("id.notEquals=" + id);

        defaultTrackOrderShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTrackOrderShouldNotBeFound("id.greaterThan=" + id);

        defaultTrackOrderShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTrackOrderShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllTrackOrdersByOrderConfirmIsEqualToSomething() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get all the trackOrderList where orderConfirm equals to DEFAULT_ORDER_CONFIRM
        defaultTrackOrderShouldBeFound("orderConfirm.equals=" + DEFAULT_ORDER_CONFIRM);

        // Get all the trackOrderList where orderConfirm equals to UPDATED_ORDER_CONFIRM
        defaultTrackOrderShouldNotBeFound("orderConfirm.equals=" + UPDATED_ORDER_CONFIRM);
    }

    @Test
    @Transactional
    public void getAllTrackOrdersByOrderConfirmIsNotEqualToSomething() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get all the trackOrderList where orderConfirm not equals to DEFAULT_ORDER_CONFIRM
        defaultTrackOrderShouldNotBeFound("orderConfirm.notEquals=" + DEFAULT_ORDER_CONFIRM);

        // Get all the trackOrderList where orderConfirm not equals to UPDATED_ORDER_CONFIRM
        defaultTrackOrderShouldBeFound("orderConfirm.notEquals=" + UPDATED_ORDER_CONFIRM);
    }

    @Test
    @Transactional
    public void getAllTrackOrdersByOrderConfirmIsInShouldWork() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get all the trackOrderList where orderConfirm in DEFAULT_ORDER_CONFIRM or UPDATED_ORDER_CONFIRM
        defaultTrackOrderShouldBeFound("orderConfirm.in=" + DEFAULT_ORDER_CONFIRM + "," + UPDATED_ORDER_CONFIRM);

        // Get all the trackOrderList where orderConfirm equals to UPDATED_ORDER_CONFIRM
        defaultTrackOrderShouldNotBeFound("orderConfirm.in=" + UPDATED_ORDER_CONFIRM);
    }

    @Test
    @Transactional
    public void getAllTrackOrdersByOrderConfirmIsNullOrNotNull() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get all the trackOrderList where orderConfirm is not null
        defaultTrackOrderShouldBeFound("orderConfirm.specified=true");

        // Get all the trackOrderList where orderConfirm is null
        defaultTrackOrderShouldNotBeFound("orderConfirm.specified=false");
    }

    @Test
    @Transactional
    public void getAllTrackOrdersByWaittingProductIsEqualToSomething() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get all the trackOrderList where waittingProduct equals to DEFAULT_WAITTING_PRODUCT
        defaultTrackOrderShouldBeFound("waittingProduct.equals=" + DEFAULT_WAITTING_PRODUCT);

        // Get all the trackOrderList where waittingProduct equals to UPDATED_WAITTING_PRODUCT
        defaultTrackOrderShouldNotBeFound("waittingProduct.equals=" + UPDATED_WAITTING_PRODUCT);
    }

    @Test
    @Transactional
    public void getAllTrackOrdersByWaittingProductIsNotEqualToSomething() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get all the trackOrderList where waittingProduct not equals to DEFAULT_WAITTING_PRODUCT
        defaultTrackOrderShouldNotBeFound("waittingProduct.notEquals=" + DEFAULT_WAITTING_PRODUCT);

        // Get all the trackOrderList where waittingProduct not equals to UPDATED_WAITTING_PRODUCT
        defaultTrackOrderShouldBeFound("waittingProduct.notEquals=" + UPDATED_WAITTING_PRODUCT);
    }

    @Test
    @Transactional
    public void getAllTrackOrdersByWaittingProductIsInShouldWork() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get all the trackOrderList where waittingProduct in DEFAULT_WAITTING_PRODUCT or UPDATED_WAITTING_PRODUCT
        defaultTrackOrderShouldBeFound("waittingProduct.in=" + DEFAULT_WAITTING_PRODUCT + "," + UPDATED_WAITTING_PRODUCT);

        // Get all the trackOrderList where waittingProduct equals to UPDATED_WAITTING_PRODUCT
        defaultTrackOrderShouldNotBeFound("waittingProduct.in=" + UPDATED_WAITTING_PRODUCT);
    }

    @Test
    @Transactional
    public void getAllTrackOrdersByWaittingProductIsNullOrNotNull() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get all the trackOrderList where waittingProduct is not null
        defaultTrackOrderShouldBeFound("waittingProduct.specified=true");

        // Get all the trackOrderList where waittingProduct is null
        defaultTrackOrderShouldNotBeFound("waittingProduct.specified=false");
    }

    @Test
    @Transactional
    public void getAllTrackOrdersByShippingIsEqualToSomething() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get all the trackOrderList where shipping equals to DEFAULT_SHIPPING
        defaultTrackOrderShouldBeFound("shipping.equals=" + DEFAULT_SHIPPING);

        // Get all the trackOrderList where shipping equals to UPDATED_SHIPPING
        defaultTrackOrderShouldNotBeFound("shipping.equals=" + UPDATED_SHIPPING);
    }

    @Test
    @Transactional
    public void getAllTrackOrdersByShippingIsNotEqualToSomething() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get all the trackOrderList where shipping not equals to DEFAULT_SHIPPING
        defaultTrackOrderShouldNotBeFound("shipping.notEquals=" + DEFAULT_SHIPPING);

        // Get all the trackOrderList where shipping not equals to UPDATED_SHIPPING
        defaultTrackOrderShouldBeFound("shipping.notEquals=" + UPDATED_SHIPPING);
    }

    @Test
    @Transactional
    public void getAllTrackOrdersByShippingIsInShouldWork() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get all the trackOrderList where shipping in DEFAULT_SHIPPING or UPDATED_SHIPPING
        defaultTrackOrderShouldBeFound("shipping.in=" + DEFAULT_SHIPPING + "," + UPDATED_SHIPPING);

        // Get all the trackOrderList where shipping equals to UPDATED_SHIPPING
        defaultTrackOrderShouldNotBeFound("shipping.in=" + UPDATED_SHIPPING);
    }

    @Test
    @Transactional
    public void getAllTrackOrdersByShippingIsNullOrNotNull() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get all the trackOrderList where shipping is not null
        defaultTrackOrderShouldBeFound("shipping.specified=true");

        // Get all the trackOrderList where shipping is null
        defaultTrackOrderShouldNotBeFound("shipping.specified=false");
    }

    @Test
    @Transactional
    public void getAllTrackOrdersByDeliveredIsEqualToSomething() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get all the trackOrderList where delivered equals to DEFAULT_DELIVERED
        defaultTrackOrderShouldBeFound("delivered.equals=" + DEFAULT_DELIVERED);

        // Get all the trackOrderList where delivered equals to UPDATED_DELIVERED
        defaultTrackOrderShouldNotBeFound("delivered.equals=" + UPDATED_DELIVERED);
    }

    @Test
    @Transactional
    public void getAllTrackOrdersByDeliveredIsNotEqualToSomething() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get all the trackOrderList where delivered not equals to DEFAULT_DELIVERED
        defaultTrackOrderShouldNotBeFound("delivered.notEquals=" + DEFAULT_DELIVERED);

        // Get all the trackOrderList where delivered not equals to UPDATED_DELIVERED
        defaultTrackOrderShouldBeFound("delivered.notEquals=" + UPDATED_DELIVERED);
    }

    @Test
    @Transactional
    public void getAllTrackOrdersByDeliveredIsInShouldWork() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get all the trackOrderList where delivered in DEFAULT_DELIVERED or UPDATED_DELIVERED
        defaultTrackOrderShouldBeFound("delivered.in=" + DEFAULT_DELIVERED + "," + UPDATED_DELIVERED);

        // Get all the trackOrderList where delivered equals to UPDATED_DELIVERED
        defaultTrackOrderShouldNotBeFound("delivered.in=" + UPDATED_DELIVERED);
    }

    @Test
    @Transactional
    public void getAllTrackOrdersByDeliveredIsNullOrNotNull() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get all the trackOrderList where delivered is not null
        defaultTrackOrderShouldBeFound("delivered.specified=true");

        // Get all the trackOrderList where delivered is null
        defaultTrackOrderShouldNotBeFound("delivered.specified=false");
    }

    @Test
    @Transactional
    public void getAllTrackOrdersByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get all the trackOrderList where status equals to DEFAULT_STATUS
        defaultTrackOrderShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the trackOrderList where status equals to UPDATED_STATUS
        defaultTrackOrderShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllTrackOrdersByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get all the trackOrderList where status not equals to DEFAULT_STATUS
        defaultTrackOrderShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the trackOrderList where status not equals to UPDATED_STATUS
        defaultTrackOrderShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllTrackOrdersByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get all the trackOrderList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultTrackOrderShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the trackOrderList where status equals to UPDATED_STATUS
        defaultTrackOrderShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllTrackOrdersByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);

        // Get all the trackOrderList where status is not null
        defaultTrackOrderShouldBeFound("status.specified=true");

        // Get all the trackOrderList where status is null
        defaultTrackOrderShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    public void getAllTrackOrdersByOrdersIsEqualToSomething() throws Exception {
        // Initialize the database
        trackOrderRepository.saveAndFlush(trackOrder);
        Orders orders = OrdersResourceIT.createEntity(em);
        em.persist(orders);
        em.flush();
        trackOrder.setOrders(orders);
        trackOrderRepository.saveAndFlush(trackOrder);
        Long ordersId = orders.getId();

        // Get all the trackOrderList where orders equals to ordersId
        defaultTrackOrderShouldBeFound("ordersId.equals=" + ordersId);

        // Get all the trackOrderList where orders equals to ordersId + 1
        defaultTrackOrderShouldNotBeFound("ordersId.equals=" + (ordersId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTrackOrderShouldBeFound(String filter) throws Exception {
        restTrackOrderMockMvc.perform(get("/api/track-orders?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(trackOrder.getId().intValue())))
            .andExpect(jsonPath("$.[*].orderConfirm").value(hasItem(DEFAULT_ORDER_CONFIRM.booleanValue())))
            .andExpect(jsonPath("$.[*].waittingProduct").value(hasItem(DEFAULT_WAITTING_PRODUCT.booleanValue())))
            .andExpect(jsonPath("$.[*].shipping").value(hasItem(DEFAULT_SHIPPING.booleanValue())))
            .andExpect(jsonPath("$.[*].delivered").value(hasItem(DEFAULT_DELIVERED.booleanValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())));

        // Check, that the count call also returns 1
        restTrackOrderMockMvc.perform(get("/api/track-orders/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTrackOrderShouldNotBeFound(String filter) throws Exception {
        restTrackOrderMockMvc.perform(get("/api/track-orders?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTrackOrderMockMvc.perform(get("/api/track-orders/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingTrackOrder() throws Exception {
        // Get the trackOrder
        restTrackOrderMockMvc.perform(get("/api/track-orders/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTrackOrder() throws Exception {
        // Initialize the database
        trackOrderService.save(trackOrder);

        int databaseSizeBeforeUpdate = trackOrderRepository.findAll().size();

        // Update the trackOrder
        TrackOrder updatedTrackOrder = trackOrderRepository.findById(trackOrder.getId()).get();
        // Disconnect from session so that the updates on updatedTrackOrder are not directly saved in db
        em.detach(updatedTrackOrder);
        updatedTrackOrder
            .orderConfirm(UPDATED_ORDER_CONFIRM)
            .waittingProduct(UPDATED_WAITTING_PRODUCT)
            .shipping(UPDATED_SHIPPING)
            .delivered(UPDATED_DELIVERED)
            .status(UPDATED_STATUS);

        restTrackOrderMockMvc.perform(put("/api/track-orders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedTrackOrder)))
            .andExpect(status().isOk());

        // Validate the TrackOrder in the database
        List<TrackOrder> trackOrderList = trackOrderRepository.findAll();
        assertThat(trackOrderList).hasSize(databaseSizeBeforeUpdate);
        TrackOrder testTrackOrder = trackOrderList.get(trackOrderList.size() - 1);
        assertThat(testTrackOrder.isOrderConfirm()).isEqualTo(UPDATED_ORDER_CONFIRM);
        assertThat(testTrackOrder.isWaittingProduct()).isEqualTo(UPDATED_WAITTING_PRODUCT);
        assertThat(testTrackOrder.isShipping()).isEqualTo(UPDATED_SHIPPING);
        assertThat(testTrackOrder.isDelivered()).isEqualTo(UPDATED_DELIVERED);
        assertThat(testTrackOrder.isStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingTrackOrder() throws Exception {
        int databaseSizeBeforeUpdate = trackOrderRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTrackOrderMockMvc.perform(put("/api/track-orders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(trackOrder)))
            .andExpect(status().isBadRequest());

        // Validate the TrackOrder in the database
        List<TrackOrder> trackOrderList = trackOrderRepository.findAll();
        assertThat(trackOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTrackOrder() throws Exception {
        // Initialize the database
        trackOrderService.save(trackOrder);

        int databaseSizeBeforeDelete = trackOrderRepository.findAll().size();

        // Delete the trackOrder
        restTrackOrderMockMvc.perform(delete("/api/track-orders/{id}", trackOrder.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TrackOrder> trackOrderList = trackOrderRepository.findAll();
        assertThat(trackOrderList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
