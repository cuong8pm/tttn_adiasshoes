package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.HauiAdidasShoesApp;
import com.haui.adidasshoes.domain.TrackOrderUser;
import com.haui.adidasshoes.repository.TrackOrderUserRepository;
import com.haui.adidasshoes.service.TrackOrderUserService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TrackOrderUserResource} REST controller.
 */
@SpringBootTest(classes = HauiAdidasShoesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class TrackOrderUserResourceIT {

    @Autowired
    private TrackOrderUserRepository trackOrderUserRepository;

    @Autowired
    private TrackOrderUserService trackOrderUserService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTrackOrderUserMockMvc;

    private TrackOrderUser trackOrderUser;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TrackOrderUser createEntity(EntityManager em) {
        TrackOrderUser trackOrderUser = new TrackOrderUser();
        return trackOrderUser;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TrackOrderUser createUpdatedEntity(EntityManager em) {
        TrackOrderUser trackOrderUser = new TrackOrderUser();
        return trackOrderUser;
    }

    @BeforeEach
    public void initTest() {
        trackOrderUser = createEntity(em);
    }

    @Test
    @Transactional
    public void createTrackOrderUser() throws Exception {
        int databaseSizeBeforeCreate = trackOrderUserRepository.findAll().size();
        // Create the TrackOrderUser
        restTrackOrderUserMockMvc.perform(post("/api/track-order-users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(trackOrderUser)))
            .andExpect(status().isCreated());

        // Validate the TrackOrderUser in the database
        List<TrackOrderUser> trackOrderUserList = trackOrderUserRepository.findAll();
        assertThat(trackOrderUserList).hasSize(databaseSizeBeforeCreate + 1);
        TrackOrderUser testTrackOrderUser = trackOrderUserList.get(trackOrderUserList.size() - 1);
    }

    @Test
    @Transactional
    public void createTrackOrderUserWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = trackOrderUserRepository.findAll().size();

        // Create the TrackOrderUser with an existing ID
        trackOrderUser.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTrackOrderUserMockMvc.perform(post("/api/track-order-users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(trackOrderUser)))
            .andExpect(status().isBadRequest());

        // Validate the TrackOrderUser in the database
        List<TrackOrderUser> trackOrderUserList = trackOrderUserRepository.findAll();
        assertThat(trackOrderUserList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTrackOrderUsers() throws Exception {
        // Initialize the database
        trackOrderUserRepository.saveAndFlush(trackOrderUser);

        // Get all the trackOrderUserList
        restTrackOrderUserMockMvc.perform(get("/api/track-order-users?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(trackOrderUser.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getTrackOrderUser() throws Exception {
        // Initialize the database
        trackOrderUserRepository.saveAndFlush(trackOrderUser);

        // Get the trackOrderUser
        restTrackOrderUserMockMvc.perform(get("/api/track-order-users/{id}", trackOrderUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(trackOrderUser.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingTrackOrderUser() throws Exception {
        // Get the trackOrderUser
        restTrackOrderUserMockMvc.perform(get("/api/track-order-users/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTrackOrderUser() throws Exception {
        // Initialize the database
        trackOrderUserService.save(trackOrderUser);

        int databaseSizeBeforeUpdate = trackOrderUserRepository.findAll().size();

        // Update the trackOrderUser
        TrackOrderUser updatedTrackOrderUser = trackOrderUserRepository.findById(trackOrderUser.getId()).get();
        // Disconnect from session so that the updates on updatedTrackOrderUser are not directly saved in db
        em.detach(updatedTrackOrderUser);

        restTrackOrderUserMockMvc.perform(put("/api/track-order-users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedTrackOrderUser)))
            .andExpect(status().isOk());

        // Validate the TrackOrderUser in the database
        List<TrackOrderUser> trackOrderUserList = trackOrderUserRepository.findAll();
        assertThat(trackOrderUserList).hasSize(databaseSizeBeforeUpdate);
        TrackOrderUser testTrackOrderUser = trackOrderUserList.get(trackOrderUserList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingTrackOrderUser() throws Exception {
        int databaseSizeBeforeUpdate = trackOrderUserRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTrackOrderUserMockMvc.perform(put("/api/track-order-users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(trackOrderUser)))
            .andExpect(status().isBadRequest());

        // Validate the TrackOrderUser in the database
        List<TrackOrderUser> trackOrderUserList = trackOrderUserRepository.findAll();
        assertThat(trackOrderUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTrackOrderUser() throws Exception {
        // Initialize the database
        trackOrderUserService.save(trackOrderUser);

        int databaseSizeBeforeDelete = trackOrderUserRepository.findAll().size();

        // Delete the trackOrderUser
        restTrackOrderUserMockMvc.perform(delete("/api/track-order-users/{id}", trackOrderUser.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TrackOrderUser> trackOrderUserList = trackOrderUserRepository.findAll();
        assertThat(trackOrderUserList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
