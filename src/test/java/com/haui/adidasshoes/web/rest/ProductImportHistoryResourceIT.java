package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.HauiAdidasShoesApp;
import com.haui.adidasshoes.domain.ProductImportHistory;
import com.haui.adidasshoes.domain.Amount;
import com.haui.adidasshoes.repository.ProductImportHistoryRepository;
import com.haui.adidasshoes.service.ProductImportHistoryService;
import com.haui.adidasshoes.service.dto.ProductImportHistoryCriteria;
import com.haui.adidasshoes.service.ProductImportHistoryQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ProductImportHistoryResource} REST controller.
 */
@SpringBootTest(classes = HauiAdidasShoesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ProductImportHistoryResourceIT {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private ProductImportHistoryRepository productImportHistoryRepository;

    @Autowired
    private ProductImportHistoryService productImportHistoryService;

    @Autowired
    private ProductImportHistoryQueryService productImportHistoryQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restProductImportHistoryMockMvc;

    private ProductImportHistory productImportHistory;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductImportHistory createEntity(EntityManager em) {
        ProductImportHistory productImportHistory = new ProductImportHistory()
            .description(DEFAULT_DESCRIPTION);
        return productImportHistory;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductImportHistory createUpdatedEntity(EntityManager em) {
        ProductImportHistory productImportHistory = new ProductImportHistory()
            .description(UPDATED_DESCRIPTION);
        return productImportHistory;
    }

    @BeforeEach
    public void initTest() {
        productImportHistory = createEntity(em);
    }

    @Test
    @Transactional
    public void createProductImportHistory() throws Exception {
        int databaseSizeBeforeCreate = productImportHistoryRepository.findAll().size();
        // Create the ProductImportHistory
        restProductImportHistoryMockMvc.perform(post("/api/product-import-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productImportHistory)))
            .andExpect(status().isCreated());

        // Validate the ProductImportHistory in the database
        List<ProductImportHistory> productImportHistoryList = productImportHistoryRepository.findAll();
        assertThat(productImportHistoryList).hasSize(databaseSizeBeforeCreate + 1);
        ProductImportHistory testProductImportHistory = productImportHistoryList.get(productImportHistoryList.size() - 1);
        assertThat(testProductImportHistory.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createProductImportHistoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = productImportHistoryRepository.findAll().size();

        // Create the ProductImportHistory with an existing ID
        productImportHistory.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProductImportHistoryMockMvc.perform(post("/api/product-import-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productImportHistory)))
            .andExpect(status().isBadRequest());

        // Validate the ProductImportHistory in the database
        List<ProductImportHistory> productImportHistoryList = productImportHistoryRepository.findAll();
        assertThat(productImportHistoryList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllProductImportHistories() throws Exception {
        // Initialize the database
        productImportHistoryRepository.saveAndFlush(productImportHistory);

        // Get all the productImportHistoryList
        restProductImportHistoryMockMvc.perform(get("/api/product-import-histories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(productImportHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }
    
    @Test
    @Transactional
    public void getProductImportHistory() throws Exception {
        // Initialize the database
        productImportHistoryRepository.saveAndFlush(productImportHistory);

        // Get the productImportHistory
        restProductImportHistoryMockMvc.perform(get("/api/product-import-histories/{id}", productImportHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(productImportHistory.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }


    @Test
    @Transactional
    public void getProductImportHistoriesByIdFiltering() throws Exception {
        // Initialize the database
        productImportHistoryRepository.saveAndFlush(productImportHistory);

        Long id = productImportHistory.getId();

        defaultProductImportHistoryShouldBeFound("id.equals=" + id);
        defaultProductImportHistoryShouldNotBeFound("id.notEquals=" + id);

        defaultProductImportHistoryShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultProductImportHistoryShouldNotBeFound("id.greaterThan=" + id);

        defaultProductImportHistoryShouldBeFound("id.lessThanOrEqual=" + id);
        defaultProductImportHistoryShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllProductImportHistoriesByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        productImportHistoryRepository.saveAndFlush(productImportHistory);

        // Get all the productImportHistoryList where description equals to DEFAULT_DESCRIPTION
        defaultProductImportHistoryShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the productImportHistoryList where description equals to UPDATED_DESCRIPTION
        defaultProductImportHistoryShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllProductImportHistoriesByDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        productImportHistoryRepository.saveAndFlush(productImportHistory);

        // Get all the productImportHistoryList where description not equals to DEFAULT_DESCRIPTION
        defaultProductImportHistoryShouldNotBeFound("description.notEquals=" + DEFAULT_DESCRIPTION);

        // Get all the productImportHistoryList where description not equals to UPDATED_DESCRIPTION
        defaultProductImportHistoryShouldBeFound("description.notEquals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllProductImportHistoriesByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        productImportHistoryRepository.saveAndFlush(productImportHistory);

        // Get all the productImportHistoryList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultProductImportHistoryShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the productImportHistoryList where description equals to UPDATED_DESCRIPTION
        defaultProductImportHistoryShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllProductImportHistoriesByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        productImportHistoryRepository.saveAndFlush(productImportHistory);

        // Get all the productImportHistoryList where description is not null
        defaultProductImportHistoryShouldBeFound("description.specified=true");

        // Get all the productImportHistoryList where description is null
        defaultProductImportHistoryShouldNotBeFound("description.specified=false");
    }
                @Test
    @Transactional
    public void getAllProductImportHistoriesByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        productImportHistoryRepository.saveAndFlush(productImportHistory);

        // Get all the productImportHistoryList where description contains DEFAULT_DESCRIPTION
        defaultProductImportHistoryShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the productImportHistoryList where description contains UPDATED_DESCRIPTION
        defaultProductImportHistoryShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllProductImportHistoriesByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        productImportHistoryRepository.saveAndFlush(productImportHistory);

        // Get all the productImportHistoryList where description does not contain DEFAULT_DESCRIPTION
        defaultProductImportHistoryShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the productImportHistoryList where description does not contain UPDATED_DESCRIPTION
        defaultProductImportHistoryShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }


    @Test
    @Transactional
    public void getAllProductImportHistoriesByAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        productImportHistoryRepository.saveAndFlush(productImportHistory);
        Amount amount = AmountResourceIT.createEntity(em);
        em.persist(amount);
        em.flush();
        productImportHistory.setAmount(amount);
        productImportHistoryRepository.saveAndFlush(productImportHistory);
        Long amountId = amount.getId();

        // Get all the productImportHistoryList where amount equals to amountId
        defaultProductImportHistoryShouldBeFound("amountId.equals=" + amountId);

        // Get all the productImportHistoryList where amount equals to amountId + 1
        defaultProductImportHistoryShouldNotBeFound("amountId.equals=" + (amountId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProductImportHistoryShouldBeFound(String filter) throws Exception {
        restProductImportHistoryMockMvc.perform(get("/api/product-import-histories?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(productImportHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));

        // Check, that the count call also returns 1
        restProductImportHistoryMockMvc.perform(get("/api/product-import-histories/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProductImportHistoryShouldNotBeFound(String filter) throws Exception {
        restProductImportHistoryMockMvc.perform(get("/api/product-import-histories?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProductImportHistoryMockMvc.perform(get("/api/product-import-histories/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingProductImportHistory() throws Exception {
        // Get the productImportHistory
        restProductImportHistoryMockMvc.perform(get("/api/product-import-histories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProductImportHistory() throws Exception {
        // Initialize the database
        productImportHistoryService.save(productImportHistory);

        int databaseSizeBeforeUpdate = productImportHistoryRepository.findAll().size();

        // Update the productImportHistory
        ProductImportHistory updatedProductImportHistory = productImportHistoryRepository.findById(productImportHistory.getId()).get();
        // Disconnect from session so that the updates on updatedProductImportHistory are not directly saved in db
        em.detach(updatedProductImportHistory);
        updatedProductImportHistory
            .description(UPDATED_DESCRIPTION);

        restProductImportHistoryMockMvc.perform(put("/api/product-import-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedProductImportHistory)))
            .andExpect(status().isOk());

        // Validate the ProductImportHistory in the database
        List<ProductImportHistory> productImportHistoryList = productImportHistoryRepository.findAll();
        assertThat(productImportHistoryList).hasSize(databaseSizeBeforeUpdate);
        ProductImportHistory testProductImportHistory = productImportHistoryList.get(productImportHistoryList.size() - 1);
        assertThat(testProductImportHistory.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingProductImportHistory() throws Exception {
        int databaseSizeBeforeUpdate = productImportHistoryRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProductImportHistoryMockMvc.perform(put("/api/product-import-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productImportHistory)))
            .andExpect(status().isBadRequest());

        // Validate the ProductImportHistory in the database
        List<ProductImportHistory> productImportHistoryList = productImportHistoryRepository.findAll();
        assertThat(productImportHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProductImportHistory() throws Exception {
        // Initialize the database
        productImportHistoryService.save(productImportHistory);

        int databaseSizeBeforeDelete = productImportHistoryRepository.findAll().size();

        // Delete the productImportHistory
        restProductImportHistoryMockMvc.perform(delete("/api/product-import-histories/{id}", productImportHistory.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ProductImportHistory> productImportHistoryList = productImportHistoryRepository.findAll();
        assertThat(productImportHistoryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
