package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.HauiAdidasShoesApp;
import com.haui.adidasshoes.domain.Reviews;
import com.haui.adidasshoes.domain.OrderProduct;
import com.haui.adidasshoes.repository.ReviewsRepository;
import com.haui.adidasshoes.service.ReviewsService;
import com.haui.adidasshoes.service.dto.ReviewsCriteria;
import com.haui.adidasshoes.service.ReviewsQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ReviewsResource} REST controller.
 */
@SpringBootTest(classes = HauiAdidasShoesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ReviewsResourceIT {

    private static final Integer DEFAULT_RATING = 1;
    private static final Integer UPDATED_RATING = 2;
    private static final Integer SMALLER_RATING = 1 - 1;

    private static final String DEFAULT_DECRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DECRIPTION = "BBBBBBBBBB";

    @Autowired
    private ReviewsRepository reviewsRepository;

    @Autowired
    private ReviewsService reviewsService;

    @Autowired
    private ReviewsQueryService reviewsQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restReviewsMockMvc;

    private Reviews reviews;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Reviews createEntity(EntityManager em) {
        Reviews reviews = new Reviews()
            .rating(DEFAULT_RATING)
            .decription(DEFAULT_DECRIPTION);
        return reviews;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Reviews createUpdatedEntity(EntityManager em) {
        Reviews reviews = new Reviews()
            .rating(UPDATED_RATING)
            .decription(UPDATED_DECRIPTION);
        return reviews;
    }

    @BeforeEach
    public void initTest() {
        reviews = createEntity(em);
    }

    @Test
    @Transactional
    public void createReviews() throws Exception {
        int databaseSizeBeforeCreate = reviewsRepository.findAll().size();
        // Create the Reviews
        restReviewsMockMvc.perform(post("/api/reviews")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(reviews)))
            .andExpect(status().isCreated());

        // Validate the Reviews in the database
        List<Reviews> reviewsList = reviewsRepository.findAll();
        assertThat(reviewsList).hasSize(databaseSizeBeforeCreate + 1);
        Reviews testReviews = reviewsList.get(reviewsList.size() - 1);
        assertThat(testReviews.getRating()).isEqualTo(DEFAULT_RATING);
        assertThat(testReviews.getDecription()).isEqualTo(DEFAULT_DECRIPTION);
    }

    @Test
    @Transactional
    public void createReviewsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = reviewsRepository.findAll().size();

        // Create the Reviews with an existing ID
        reviews.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restReviewsMockMvc.perform(post("/api/reviews")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(reviews)))
            .andExpect(status().isBadRequest());

        // Validate the Reviews in the database
        List<Reviews> reviewsList = reviewsRepository.findAll();
        assertThat(reviewsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllReviews() throws Exception {
        // Initialize the database
        reviewsRepository.saveAndFlush(reviews);

        // Get all the reviewsList
        restReviewsMockMvc.perform(get("/api/reviews?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(reviews.getId().intValue())))
            .andExpect(jsonPath("$.[*].rating").value(hasItem(DEFAULT_RATING)))
            .andExpect(jsonPath("$.[*].decription").value(hasItem(DEFAULT_DECRIPTION)));
    }
    
    @Test
    @Transactional
    public void getReviews() throws Exception {
        // Initialize the database
        reviewsRepository.saveAndFlush(reviews);

        // Get the reviews
        restReviewsMockMvc.perform(get("/api/reviews/{id}", reviews.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(reviews.getId().intValue()))
            .andExpect(jsonPath("$.rating").value(DEFAULT_RATING))
            .andExpect(jsonPath("$.decription").value(DEFAULT_DECRIPTION));
    }


    @Test
    @Transactional
    public void getReviewsByIdFiltering() throws Exception {
        // Initialize the database
        reviewsRepository.saveAndFlush(reviews);

        Long id = reviews.getId();

        defaultReviewsShouldBeFound("id.equals=" + id);
        defaultReviewsShouldNotBeFound("id.notEquals=" + id);

        defaultReviewsShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultReviewsShouldNotBeFound("id.greaterThan=" + id);

        defaultReviewsShouldBeFound("id.lessThanOrEqual=" + id);
        defaultReviewsShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllReviewsByRatingIsEqualToSomething() throws Exception {
        // Initialize the database
        reviewsRepository.saveAndFlush(reviews);

        // Get all the reviewsList where rating equals to DEFAULT_RATING
        defaultReviewsShouldBeFound("rating.equals=" + DEFAULT_RATING);

        // Get all the reviewsList where rating equals to UPDATED_RATING
        defaultReviewsShouldNotBeFound("rating.equals=" + UPDATED_RATING);
    }

    @Test
    @Transactional
    public void getAllReviewsByRatingIsNotEqualToSomething() throws Exception {
        // Initialize the database
        reviewsRepository.saveAndFlush(reviews);

        // Get all the reviewsList where rating not equals to DEFAULT_RATING
        defaultReviewsShouldNotBeFound("rating.notEquals=" + DEFAULT_RATING);

        // Get all the reviewsList where rating not equals to UPDATED_RATING
        defaultReviewsShouldBeFound("rating.notEquals=" + UPDATED_RATING);
    }

    @Test
    @Transactional
    public void getAllReviewsByRatingIsInShouldWork() throws Exception {
        // Initialize the database
        reviewsRepository.saveAndFlush(reviews);

        // Get all the reviewsList where rating in DEFAULT_RATING or UPDATED_RATING
        defaultReviewsShouldBeFound("rating.in=" + DEFAULT_RATING + "," + UPDATED_RATING);

        // Get all the reviewsList where rating equals to UPDATED_RATING
        defaultReviewsShouldNotBeFound("rating.in=" + UPDATED_RATING);
    }

    @Test
    @Transactional
    public void getAllReviewsByRatingIsNullOrNotNull() throws Exception {
        // Initialize the database
        reviewsRepository.saveAndFlush(reviews);

        // Get all the reviewsList where rating is not null
        defaultReviewsShouldBeFound("rating.specified=true");

        // Get all the reviewsList where rating is null
        defaultReviewsShouldNotBeFound("rating.specified=false");
    }

    @Test
    @Transactional
    public void getAllReviewsByRatingIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        reviewsRepository.saveAndFlush(reviews);

        // Get all the reviewsList where rating is greater than or equal to DEFAULT_RATING
        defaultReviewsShouldBeFound("rating.greaterThanOrEqual=" + DEFAULT_RATING);

        // Get all the reviewsList where rating is greater than or equal to UPDATED_RATING
        defaultReviewsShouldNotBeFound("rating.greaterThanOrEqual=" + UPDATED_RATING);
    }

    @Test
    @Transactional
    public void getAllReviewsByRatingIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        reviewsRepository.saveAndFlush(reviews);

        // Get all the reviewsList where rating is less than or equal to DEFAULT_RATING
        defaultReviewsShouldBeFound("rating.lessThanOrEqual=" + DEFAULT_RATING);

        // Get all the reviewsList where rating is less than or equal to SMALLER_RATING
        defaultReviewsShouldNotBeFound("rating.lessThanOrEqual=" + SMALLER_RATING);
    }

    @Test
    @Transactional
    public void getAllReviewsByRatingIsLessThanSomething() throws Exception {
        // Initialize the database
        reviewsRepository.saveAndFlush(reviews);

        // Get all the reviewsList where rating is less than DEFAULT_RATING
        defaultReviewsShouldNotBeFound("rating.lessThan=" + DEFAULT_RATING);

        // Get all the reviewsList where rating is less than UPDATED_RATING
        defaultReviewsShouldBeFound("rating.lessThan=" + UPDATED_RATING);
    }

    @Test
    @Transactional
    public void getAllReviewsByRatingIsGreaterThanSomething() throws Exception {
        // Initialize the database
        reviewsRepository.saveAndFlush(reviews);

        // Get all the reviewsList where rating is greater than DEFAULT_RATING
        defaultReviewsShouldNotBeFound("rating.greaterThan=" + DEFAULT_RATING);

        // Get all the reviewsList where rating is greater than SMALLER_RATING
        defaultReviewsShouldBeFound("rating.greaterThan=" + SMALLER_RATING);
    }


    @Test
    @Transactional
    public void getAllReviewsByDecriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        reviewsRepository.saveAndFlush(reviews);

        // Get all the reviewsList where decription equals to DEFAULT_DECRIPTION
        defaultReviewsShouldBeFound("decription.equals=" + DEFAULT_DECRIPTION);

        // Get all the reviewsList where decription equals to UPDATED_DECRIPTION
        defaultReviewsShouldNotBeFound("decription.equals=" + UPDATED_DECRIPTION);
    }

    @Test
    @Transactional
    public void getAllReviewsByDecriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        reviewsRepository.saveAndFlush(reviews);

        // Get all the reviewsList where decription not equals to DEFAULT_DECRIPTION
        defaultReviewsShouldNotBeFound("decription.notEquals=" + DEFAULT_DECRIPTION);

        // Get all the reviewsList where decription not equals to UPDATED_DECRIPTION
        defaultReviewsShouldBeFound("decription.notEquals=" + UPDATED_DECRIPTION);
    }

    @Test
    @Transactional
    public void getAllReviewsByDecriptionIsInShouldWork() throws Exception {
        // Initialize the database
        reviewsRepository.saveAndFlush(reviews);

        // Get all the reviewsList where decription in DEFAULT_DECRIPTION or UPDATED_DECRIPTION
        defaultReviewsShouldBeFound("decription.in=" + DEFAULT_DECRIPTION + "," + UPDATED_DECRIPTION);

        // Get all the reviewsList where decription equals to UPDATED_DECRIPTION
        defaultReviewsShouldNotBeFound("decription.in=" + UPDATED_DECRIPTION);
    }

    @Test
    @Transactional
    public void getAllReviewsByDecriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        reviewsRepository.saveAndFlush(reviews);

        // Get all the reviewsList where decription is not null
        defaultReviewsShouldBeFound("decription.specified=true");

        // Get all the reviewsList where decription is null
        defaultReviewsShouldNotBeFound("decription.specified=false");
    }
                @Test
    @Transactional
    public void getAllReviewsByDecriptionContainsSomething() throws Exception {
        // Initialize the database
        reviewsRepository.saveAndFlush(reviews);

        // Get all the reviewsList where decription contains DEFAULT_DECRIPTION
        defaultReviewsShouldBeFound("decription.contains=" + DEFAULT_DECRIPTION);

        // Get all the reviewsList where decription contains UPDATED_DECRIPTION
        defaultReviewsShouldNotBeFound("decription.contains=" + UPDATED_DECRIPTION);
    }

    @Test
    @Transactional
    public void getAllReviewsByDecriptionNotContainsSomething() throws Exception {
        // Initialize the database
        reviewsRepository.saveAndFlush(reviews);

        // Get all the reviewsList where decription does not contain DEFAULT_DECRIPTION
        defaultReviewsShouldNotBeFound("decription.doesNotContain=" + DEFAULT_DECRIPTION);

        // Get all the reviewsList where decription does not contain UPDATED_DECRIPTION
        defaultReviewsShouldBeFound("decription.doesNotContain=" + UPDATED_DECRIPTION);
    }


    @Test
    @Transactional
    public void getAllReviewsByOrderProductIsEqualToSomething() throws Exception {
        // Initialize the database
        reviewsRepository.saveAndFlush(reviews);
        OrderProduct orderProduct = OrderProductResourceIT.createEntity(em);
        em.persist(orderProduct);
        em.flush();
        reviews.setOrderProduct(orderProduct);
        reviewsRepository.saveAndFlush(reviews);
        Long orderProductId = orderProduct.getId();

        // Get all the reviewsList where orderProduct equals to orderProductId
        defaultReviewsShouldBeFound("orderProductId.equals=" + orderProductId);

        // Get all the reviewsList where orderProduct equals to orderProductId + 1
        defaultReviewsShouldNotBeFound("orderProductId.equals=" + (orderProductId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultReviewsShouldBeFound(String filter) throws Exception {
        restReviewsMockMvc.perform(get("/api/reviews?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(reviews.getId().intValue())))
            .andExpect(jsonPath("$.[*].rating").value(hasItem(DEFAULT_RATING)))
            .andExpect(jsonPath("$.[*].decription").value(hasItem(DEFAULT_DECRIPTION)));

        // Check, that the count call also returns 1
        restReviewsMockMvc.perform(get("/api/reviews/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultReviewsShouldNotBeFound(String filter) throws Exception {
        restReviewsMockMvc.perform(get("/api/reviews?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restReviewsMockMvc.perform(get("/api/reviews/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingReviews() throws Exception {
        // Get the reviews
        restReviewsMockMvc.perform(get("/api/reviews/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateReviews() throws Exception {
        // Initialize the database
        reviewsService.save(reviews);

        int databaseSizeBeforeUpdate = reviewsRepository.findAll().size();

        // Update the reviews
        Reviews updatedReviews = reviewsRepository.findById(reviews.getId()).get();
        // Disconnect from session so that the updates on updatedReviews are not directly saved in db
        em.detach(updatedReviews);
        updatedReviews
            .rating(UPDATED_RATING)
            .decription(UPDATED_DECRIPTION);

        restReviewsMockMvc.perform(put("/api/reviews")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedReviews)))
            .andExpect(status().isOk());

        // Validate the Reviews in the database
        List<Reviews> reviewsList = reviewsRepository.findAll();
        assertThat(reviewsList).hasSize(databaseSizeBeforeUpdate);
        Reviews testReviews = reviewsList.get(reviewsList.size() - 1);
        assertThat(testReviews.getRating()).isEqualTo(UPDATED_RATING);
        assertThat(testReviews.getDecription()).isEqualTo(UPDATED_DECRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingReviews() throws Exception {
        int databaseSizeBeforeUpdate = reviewsRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restReviewsMockMvc.perform(put("/api/reviews")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(reviews)))
            .andExpect(status().isBadRequest());

        // Validate the Reviews in the database
        List<Reviews> reviewsList = reviewsRepository.findAll();
        assertThat(reviewsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteReviews() throws Exception {
        // Initialize the database
        reviewsService.save(reviews);

        int databaseSizeBeforeDelete = reviewsRepository.findAll().size();

        // Delete the reviews
        restReviewsMockMvc.perform(delete("/api/reviews/{id}", reviews.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Reviews> reviewsList = reviewsRepository.findAll();
        assertThat(reviewsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
