package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.HauiAdidasShoesApp;
import com.haui.adidasshoes.domain.Pay;
import com.haui.adidasshoes.repository.PayRepository;
import com.haui.adidasshoes.service.PayService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PayResource} REST controller.
 */
@SpringBootTest(classes = HauiAdidasShoesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class PayResourceIT {

    @Autowired
    private PayRepository payRepository;

    @Autowired
    private PayService payService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPayMockMvc;

    private Pay pay;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pay createEntity(EntityManager em) {
        Pay pay = new Pay();
        return pay;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pay createUpdatedEntity(EntityManager em) {
        Pay pay = new Pay();
        return pay;
    }

    @BeforeEach
    public void initTest() {
        pay = createEntity(em);
    }

    @Test
    @Transactional
    public void createPay() throws Exception {
        int databaseSizeBeforeCreate = payRepository.findAll().size();
        // Create the Pay
        restPayMockMvc.perform(post("/api/pays")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pay)))
            .andExpect(status().isCreated());

        // Validate the Pay in the database
        List<Pay> payList = payRepository.findAll();
        assertThat(payList).hasSize(databaseSizeBeforeCreate + 1);
        Pay testPay = payList.get(payList.size() - 1);
    }

    @Test
    @Transactional
    public void createPayWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = payRepository.findAll().size();

        // Create the Pay with an existing ID
        pay.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPayMockMvc.perform(post("/api/pays")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pay)))
            .andExpect(status().isBadRequest());

        // Validate the Pay in the database
        List<Pay> payList = payRepository.findAll();
        assertThat(payList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPays() throws Exception {
        // Initialize the database
        payRepository.saveAndFlush(pay);

        // Get all the payList
        restPayMockMvc.perform(get("/api/pays?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pay.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getPay() throws Exception {
        // Initialize the database
        payRepository.saveAndFlush(pay);

        // Get the pay
        restPayMockMvc.perform(get("/api/pays/{id}", pay.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(pay.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingPay() throws Exception {
        // Get the pay
        restPayMockMvc.perform(get("/api/pays/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePay() throws Exception {
        // Initialize the database
        payService.save(pay);

        int databaseSizeBeforeUpdate = payRepository.findAll().size();

        // Update the pay
        Pay updatedPay = payRepository.findById(pay.getId()).get();
        // Disconnect from session so that the updates on updatedPay are not directly saved in db
        em.detach(updatedPay);

        restPayMockMvc.perform(put("/api/pays")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedPay)))
            .andExpect(status().isOk());

        // Validate the Pay in the database
        List<Pay> payList = payRepository.findAll();
        assertThat(payList).hasSize(databaseSizeBeforeUpdate);
        Pay testPay = payList.get(payList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingPay() throws Exception {
        int databaseSizeBeforeUpdate = payRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPayMockMvc.perform(put("/api/pays")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pay)))
            .andExpect(status().isBadRequest());

        // Validate the Pay in the database
        List<Pay> payList = payRepository.findAll();
        assertThat(payList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePay() throws Exception {
        // Initialize the database
        payService.save(pay);

        int databaseSizeBeforeDelete = payRepository.findAll().size();

        // Delete the pay
        restPayMockMvc.perform(delete("/api/pays/{id}", pay.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Pay> payList = payRepository.findAll();
        assertThat(payList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
