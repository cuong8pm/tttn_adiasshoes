package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.HauiAdidasShoesApp;
import com.haui.adidasshoes.domain.OrderTracks;
import com.haui.adidasshoes.repository.OrderTracksRepository;
import com.haui.adidasshoes.service.OrderTracksService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OrderTracksResource} REST controller.
 */
@SpringBootTest(classes = HauiAdidasShoesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class OrderTracksResourceIT {

    @Autowired
    private OrderTracksRepository orderTracksRepository;

    @Autowired
    private OrderTracksService orderTracksService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOrderTracksMockMvc;

    private OrderTracks orderTracks;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderTracks createEntity(EntityManager em) {
        OrderTracks orderTracks = new OrderTracks();
        return orderTracks;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderTracks createUpdatedEntity(EntityManager em) {
        OrderTracks orderTracks = new OrderTracks();
        return orderTracks;
    }

    @BeforeEach
    public void initTest() {
        orderTracks = createEntity(em);
    }

    @Test
    @Transactional
    public void createOrderTracks() throws Exception {
        int databaseSizeBeforeCreate = orderTracksRepository.findAll().size();
        // Create the OrderTracks
        restOrderTracksMockMvc.perform(post("/api/order-tracks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(orderTracks)))
            .andExpect(status().isCreated());

        // Validate the OrderTracks in the database
        List<OrderTracks> orderTracksList = orderTracksRepository.findAll();
        assertThat(orderTracksList).hasSize(databaseSizeBeforeCreate + 1);
        OrderTracks testOrderTracks = orderTracksList.get(orderTracksList.size() - 1);
    }

    @Test
    @Transactional
    public void createOrderTracksWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = orderTracksRepository.findAll().size();

        // Create the OrderTracks with an existing ID
        orderTracks.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrderTracksMockMvc.perform(post("/api/order-tracks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(orderTracks)))
            .andExpect(status().isBadRequest());

        // Validate the OrderTracks in the database
        List<OrderTracks> orderTracksList = orderTracksRepository.findAll();
        assertThat(orderTracksList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllOrderTracks() throws Exception {
        // Initialize the database
        orderTracksRepository.saveAndFlush(orderTracks);

        // Get all the orderTracksList
        restOrderTracksMockMvc.perform(get("/api/order-tracks?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orderTracks.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getOrderTracks() throws Exception {
        // Initialize the database
        orderTracksRepository.saveAndFlush(orderTracks);

        // Get the orderTracks
        restOrderTracksMockMvc.perform(get("/api/order-tracks/{id}", orderTracks.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(orderTracks.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingOrderTracks() throws Exception {
        // Get the orderTracks
        restOrderTracksMockMvc.perform(get("/api/order-tracks/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrderTracks() throws Exception {
        // Initialize the database
        orderTracksService.save(orderTracks);

        int databaseSizeBeforeUpdate = orderTracksRepository.findAll().size();

        // Update the orderTracks
        OrderTracks updatedOrderTracks = orderTracksRepository.findById(orderTracks.getId()).get();
        // Disconnect from session so that the updates on updatedOrderTracks are not directly saved in db
        em.detach(updatedOrderTracks);

        restOrderTracksMockMvc.perform(put("/api/order-tracks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedOrderTracks)))
            .andExpect(status().isOk());

        // Validate the OrderTracks in the database
        List<OrderTracks> orderTracksList = orderTracksRepository.findAll();
        assertThat(orderTracksList).hasSize(databaseSizeBeforeUpdate);
        OrderTracks testOrderTracks = orderTracksList.get(orderTracksList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingOrderTracks() throws Exception {
        int databaseSizeBeforeUpdate = orderTracksRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrderTracksMockMvc.perform(put("/api/order-tracks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(orderTracks)))
            .andExpect(status().isBadRequest());

        // Validate the OrderTracks in the database
        List<OrderTracks> orderTracksList = orderTracksRepository.findAll();
        assertThat(orderTracksList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOrderTracks() throws Exception {
        // Initialize the database
        orderTracksService.save(orderTracks);

        int databaseSizeBeforeDelete = orderTracksRepository.findAll().size();

        // Delete the orderTracks
        restOrderTracksMockMvc.perform(delete("/api/order-tracks/{id}", orderTracks.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OrderTracks> orderTracksList = orderTracksRepository.findAll();
        assertThat(orderTracksList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
