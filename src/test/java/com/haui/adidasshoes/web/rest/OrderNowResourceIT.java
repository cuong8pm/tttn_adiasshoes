package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.HauiAdidasShoesApp;
import com.haui.adidasshoes.domain.OrderNow;
import com.haui.adidasshoes.repository.OrderNowRepository;
import com.haui.adidasshoes.service.OrderNowService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OrderNowResource} REST controller.
 */
@SpringBootTest(classes = HauiAdidasShoesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class OrderNowResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ORDER_CONFIRM = false;
    private static final Boolean UPDATED_ORDER_CONFIRM = true;

    private static final Boolean DEFAULT_DELIVERED = false;
    private static final Boolean UPDATED_DELIVERED = true;

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    private static final Double DEFAULT_TOTAL_ORDER_PRICE = 1D;
    private static final Double UPDATED_TOTAL_ORDER_PRICE = 2D;

    @Autowired
    private OrderNowRepository orderNowRepository;

    @Autowired
    private OrderNowService orderNowService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOrderNowMockMvc;

    private OrderNow orderNow;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderNow createEntity(EntityManager em) {
        OrderNow orderNow = new OrderNow()
            .name(DEFAULT_NAME)
            .phone(DEFAULT_PHONE)
            .address(DEFAULT_ADDRESS)
            .orderConfirm(DEFAULT_ORDER_CONFIRM)
            .delivered(DEFAULT_DELIVERED)
            .status(DEFAULT_STATUS)
            .totalOrderPrice(DEFAULT_TOTAL_ORDER_PRICE);
        return orderNow;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderNow createUpdatedEntity(EntityManager em) {
        OrderNow orderNow = new OrderNow()
            .name(UPDATED_NAME)
            .phone(UPDATED_PHONE)
            .address(UPDATED_ADDRESS)
            .orderConfirm(UPDATED_ORDER_CONFIRM)
            .delivered(UPDATED_DELIVERED)
            .status(UPDATED_STATUS)
            .totalOrderPrice(UPDATED_TOTAL_ORDER_PRICE);
        return orderNow;
    }

    @BeforeEach
    public void initTest() {
        orderNow = createEntity(em);
    }

    @Test
    @Transactional
    public void createOrderNow() throws Exception {
        int databaseSizeBeforeCreate = orderNowRepository.findAll().size();
        // Create the OrderNow
        restOrderNowMockMvc.perform(post("/api/order-nows")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(orderNow)))
            .andExpect(status().isCreated());

        // Validate the OrderNow in the database
        List<OrderNow> orderNowList = orderNowRepository.findAll();
        assertThat(orderNowList).hasSize(databaseSizeBeforeCreate + 1);
        OrderNow testOrderNow = orderNowList.get(orderNowList.size() - 1);
        assertThat(testOrderNow.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testOrderNow.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testOrderNow.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testOrderNow.isOrderConfirm()).isEqualTo(DEFAULT_ORDER_CONFIRM);
        assertThat(testOrderNow.isDelivered()).isEqualTo(DEFAULT_DELIVERED);
        assertThat(testOrderNow.isStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testOrderNow.getTotalOrderPrice()).isEqualTo(DEFAULT_TOTAL_ORDER_PRICE);
    }

    @Test
    @Transactional
    public void createOrderNowWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = orderNowRepository.findAll().size();

        // Create the OrderNow with an existing ID
        orderNow.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrderNowMockMvc.perform(post("/api/order-nows")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(orderNow)))
            .andExpect(status().isBadRequest());

        // Validate the OrderNow in the database
        List<OrderNow> orderNowList = orderNowRepository.findAll();
        assertThat(orderNowList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllOrderNows() throws Exception {
        // Initialize the database
        orderNowRepository.saveAndFlush(orderNow);

        // Get all the orderNowList
        restOrderNowMockMvc.perform(get("/api/order-nows?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orderNow.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].orderConfirm").value(hasItem(DEFAULT_ORDER_CONFIRM.booleanValue())))
            .andExpect(jsonPath("$.[*].delivered").value(hasItem(DEFAULT_DELIVERED.booleanValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())))
            .andExpect(jsonPath("$.[*].totalOrderPrice").value(hasItem(DEFAULT_TOTAL_ORDER_PRICE.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getOrderNow() throws Exception {
        // Initialize the database
        orderNowRepository.saveAndFlush(orderNow);

        // Get the orderNow
        restOrderNowMockMvc.perform(get("/api/order-nows/{id}", orderNow.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(orderNow.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.orderConfirm").value(DEFAULT_ORDER_CONFIRM.booleanValue()))
            .andExpect(jsonPath("$.delivered").value(DEFAULT_DELIVERED.booleanValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()))
            .andExpect(jsonPath("$.totalOrderPrice").value(DEFAULT_TOTAL_ORDER_PRICE.doubleValue()));
    }
    @Test
    @Transactional
    public void getNonExistingOrderNow() throws Exception {
        // Get the orderNow
        restOrderNowMockMvc.perform(get("/api/order-nows/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrderNow() throws Exception {
        // Initialize the database
        orderNowService.save(orderNow);

        int databaseSizeBeforeUpdate = orderNowRepository.findAll().size();

        // Update the orderNow
        OrderNow updatedOrderNow = orderNowRepository.findById(orderNow.getId()).get();
        // Disconnect from session so that the updates on updatedOrderNow are not directly saved in db
        em.detach(updatedOrderNow);
        updatedOrderNow
            .name(UPDATED_NAME)
            .phone(UPDATED_PHONE)
            .address(UPDATED_ADDRESS)
            .orderConfirm(UPDATED_ORDER_CONFIRM)
            .delivered(UPDATED_DELIVERED)
            .status(UPDATED_STATUS)
            .totalOrderPrice(UPDATED_TOTAL_ORDER_PRICE);

        restOrderNowMockMvc.perform(put("/api/order-nows")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedOrderNow)))
            .andExpect(status().isOk());

        // Validate the OrderNow in the database
        List<OrderNow> orderNowList = orderNowRepository.findAll();
        assertThat(orderNowList).hasSize(databaseSizeBeforeUpdate);
        OrderNow testOrderNow = orderNowList.get(orderNowList.size() - 1);
        assertThat(testOrderNow.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testOrderNow.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testOrderNow.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testOrderNow.isOrderConfirm()).isEqualTo(UPDATED_ORDER_CONFIRM);
        assertThat(testOrderNow.isDelivered()).isEqualTo(UPDATED_DELIVERED);
        assertThat(testOrderNow.isStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testOrderNow.getTotalOrderPrice()).isEqualTo(UPDATED_TOTAL_ORDER_PRICE);
    }

    @Test
    @Transactional
    public void updateNonExistingOrderNow() throws Exception {
        int databaseSizeBeforeUpdate = orderNowRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrderNowMockMvc.perform(put("/api/order-nows")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(orderNow)))
            .andExpect(status().isBadRequest());

        // Validate the OrderNow in the database
        List<OrderNow> orderNowList = orderNowRepository.findAll();
        assertThat(orderNowList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOrderNow() throws Exception {
        // Initialize the database
        orderNowService.save(orderNow);

        int databaseSizeBeforeDelete = orderNowRepository.findAll().size();

        // Delete the orderNow
        restOrderNowMockMvc.perform(delete("/api/order-nows/{id}", orderNow.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OrderNow> orderNowList = orderNowRepository.findAll();
        assertThat(orderNowList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
