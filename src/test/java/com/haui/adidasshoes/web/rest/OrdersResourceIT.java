package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.HauiAdidasShoesApp;
import com.haui.adidasshoes.domain.Orders;
import com.haui.adidasshoes.domain.User;
import com.haui.adidasshoes.repository.OrdersRepository;
import com.haui.adidasshoes.service.OrdersService;
import com.haui.adidasshoes.service.dto.OrdersCriteria;
import com.haui.adidasshoes.service.OrdersQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OrdersResource} REST controller.
 */
@SpringBootTest(classes = HauiAdidasShoesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class OrdersResourceIT {

    private static final Boolean DEFAULT_CONFIRM = false;
    private static final Boolean UPDATED_CONFIRM = true;

    private static final String DEFAULT_DESCRIPSTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPSTION = "BBBBBBBBBB";

    private static final Double DEFAULT_TOTAL_ORDER_PRICE = 1D;
    private static final Double UPDATED_TOTAL_ORDER_PRICE = 2D;
    private static final Double SMALLER_TOTAL_ORDER_PRICE = 1D - 1D;

    private static final LocalDate DEFAULT_ORDER_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ORDER_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_ORDER_DATE = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DELIVERY_DATE_FROM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DELIVERY_DATE_FROM = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DELIVERY_DATE_FROM = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DELIVERY_DATE_TO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DELIVERY_DATE_TO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DELIVERY_DATE_TO = LocalDate.ofEpochDay(-1L);

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private OrdersQueryService ordersQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOrdersMockMvc;

    private Orders orders;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Orders createEntity(EntityManager em) {
        Orders orders = new Orders()
            .confirm(DEFAULT_CONFIRM)
            .descripstion(DEFAULT_DESCRIPSTION)
            .totalOrderPrice(DEFAULT_TOTAL_ORDER_PRICE)
            .orderDate(DEFAULT_ORDER_DATE)
            .deliveryDateFrom(DEFAULT_DELIVERY_DATE_FROM)
            .deliveryDateTo(DEFAULT_DELIVERY_DATE_TO);
        return orders;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Orders createUpdatedEntity(EntityManager em) {
        Orders orders = new Orders()
            .confirm(UPDATED_CONFIRM)
            .descripstion(UPDATED_DESCRIPSTION)
            .totalOrderPrice(UPDATED_TOTAL_ORDER_PRICE)
            .orderDate(UPDATED_ORDER_DATE)
            .deliveryDateFrom(UPDATED_DELIVERY_DATE_FROM)
            .deliveryDateTo(UPDATED_DELIVERY_DATE_TO);
        return orders;
    }

    @BeforeEach
    public void initTest() {
        orders = createEntity(em);
    }

    @Test
    @Transactional
    public void createOrders() throws Exception {
        int databaseSizeBeforeCreate = ordersRepository.findAll().size();
        // Create the Orders
        restOrdersMockMvc.perform(post("/api/orders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(orders)))
            .andExpect(status().isCreated());

        // Validate the Orders in the database
        List<Orders> ordersList = ordersRepository.findAll();
        assertThat(ordersList).hasSize(databaseSizeBeforeCreate + 1);
        Orders testOrders = ordersList.get(ordersList.size() - 1);
        assertThat(testOrders.isConfirm()).isEqualTo(DEFAULT_CONFIRM);
        assertThat(testOrders.getDescripstion()).isEqualTo(DEFAULT_DESCRIPSTION);
        assertThat(testOrders.getTotalOrderPrice()).isEqualTo(DEFAULT_TOTAL_ORDER_PRICE);
        assertThat(testOrders.getOrderDate()).isEqualTo(DEFAULT_ORDER_DATE);
        assertThat(testOrders.getDeliveryDateFrom()).isEqualTo(DEFAULT_DELIVERY_DATE_FROM);
        assertThat(testOrders.getDeliveryDateTo()).isEqualTo(DEFAULT_DELIVERY_DATE_TO);
    }

    @Test
    @Transactional
    public void createOrdersWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ordersRepository.findAll().size();

        // Create the Orders with an existing ID
        orders.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrdersMockMvc.perform(post("/api/orders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(orders)))
            .andExpect(status().isBadRequest());

        // Validate the Orders in the database
        List<Orders> ordersList = ordersRepository.findAll();
        assertThat(ordersList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllOrders() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList
        restOrdersMockMvc.perform(get("/api/orders?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orders.getId().intValue())))
            .andExpect(jsonPath("$.[*].confirm").value(hasItem(DEFAULT_CONFIRM.booleanValue())))
            .andExpect(jsonPath("$.[*].descripstion").value(hasItem(DEFAULT_DESCRIPSTION)))
            .andExpect(jsonPath("$.[*].totalOrderPrice").value(hasItem(DEFAULT_TOTAL_ORDER_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].orderDate").value(hasItem(DEFAULT_ORDER_DATE.toString())))
            .andExpect(jsonPath("$.[*].deliveryDateFrom").value(hasItem(DEFAULT_DELIVERY_DATE_FROM.toString())))
            .andExpect(jsonPath("$.[*].deliveryDateTo").value(hasItem(DEFAULT_DELIVERY_DATE_TO.toString())));
    }
    
    @Test
    @Transactional
    public void getOrders() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get the orders
        restOrdersMockMvc.perform(get("/api/orders/{id}", orders.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(orders.getId().intValue()))
            .andExpect(jsonPath("$.confirm").value(DEFAULT_CONFIRM.booleanValue()))
            .andExpect(jsonPath("$.descripstion").value(DEFAULT_DESCRIPSTION))
            .andExpect(jsonPath("$.totalOrderPrice").value(DEFAULT_TOTAL_ORDER_PRICE.doubleValue()))
            .andExpect(jsonPath("$.orderDate").value(DEFAULT_ORDER_DATE.toString()))
            .andExpect(jsonPath("$.deliveryDateFrom").value(DEFAULT_DELIVERY_DATE_FROM.toString()))
            .andExpect(jsonPath("$.deliveryDateTo").value(DEFAULT_DELIVERY_DATE_TO.toString()));
    }


    @Test
    @Transactional
    public void getOrdersByIdFiltering() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        Long id = orders.getId();

        defaultOrdersShouldBeFound("id.equals=" + id);
        defaultOrdersShouldNotBeFound("id.notEquals=" + id);

        defaultOrdersShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultOrdersShouldNotBeFound("id.greaterThan=" + id);

        defaultOrdersShouldBeFound("id.lessThanOrEqual=" + id);
        defaultOrdersShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllOrdersByConfirmIsEqualToSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where confirm equals to DEFAULT_CONFIRM
        defaultOrdersShouldBeFound("confirm.equals=" + DEFAULT_CONFIRM);

        // Get all the ordersList where confirm equals to UPDATED_CONFIRM
        defaultOrdersShouldNotBeFound("confirm.equals=" + UPDATED_CONFIRM);
    }

    @Test
    @Transactional
    public void getAllOrdersByConfirmIsNotEqualToSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where confirm not equals to DEFAULT_CONFIRM
        defaultOrdersShouldNotBeFound("confirm.notEquals=" + DEFAULT_CONFIRM);

        // Get all the ordersList where confirm not equals to UPDATED_CONFIRM
        defaultOrdersShouldBeFound("confirm.notEquals=" + UPDATED_CONFIRM);
    }

    @Test
    @Transactional
    public void getAllOrdersByConfirmIsInShouldWork() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where confirm in DEFAULT_CONFIRM or UPDATED_CONFIRM
        defaultOrdersShouldBeFound("confirm.in=" + DEFAULT_CONFIRM + "," + UPDATED_CONFIRM);

        // Get all the ordersList where confirm equals to UPDATED_CONFIRM
        defaultOrdersShouldNotBeFound("confirm.in=" + UPDATED_CONFIRM);
    }

    @Test
    @Transactional
    public void getAllOrdersByConfirmIsNullOrNotNull() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where confirm is not null
        defaultOrdersShouldBeFound("confirm.specified=true");

        // Get all the ordersList where confirm is null
        defaultOrdersShouldNotBeFound("confirm.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrdersByDescripstionIsEqualToSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where descripstion equals to DEFAULT_DESCRIPSTION
        defaultOrdersShouldBeFound("descripstion.equals=" + DEFAULT_DESCRIPSTION);

        // Get all the ordersList where descripstion equals to UPDATED_DESCRIPSTION
        defaultOrdersShouldNotBeFound("descripstion.equals=" + UPDATED_DESCRIPSTION);
    }

    @Test
    @Transactional
    public void getAllOrdersByDescripstionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where descripstion not equals to DEFAULT_DESCRIPSTION
        defaultOrdersShouldNotBeFound("descripstion.notEquals=" + DEFAULT_DESCRIPSTION);

        // Get all the ordersList where descripstion not equals to UPDATED_DESCRIPSTION
        defaultOrdersShouldBeFound("descripstion.notEquals=" + UPDATED_DESCRIPSTION);
    }

    @Test
    @Transactional
    public void getAllOrdersByDescripstionIsInShouldWork() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where descripstion in DEFAULT_DESCRIPSTION or UPDATED_DESCRIPSTION
        defaultOrdersShouldBeFound("descripstion.in=" + DEFAULT_DESCRIPSTION + "," + UPDATED_DESCRIPSTION);

        // Get all the ordersList where descripstion equals to UPDATED_DESCRIPSTION
        defaultOrdersShouldNotBeFound("descripstion.in=" + UPDATED_DESCRIPSTION);
    }

    @Test
    @Transactional
    public void getAllOrdersByDescripstionIsNullOrNotNull() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where descripstion is not null
        defaultOrdersShouldBeFound("descripstion.specified=true");

        // Get all the ordersList where descripstion is null
        defaultOrdersShouldNotBeFound("descripstion.specified=false");
    }
                @Test
    @Transactional
    public void getAllOrdersByDescripstionContainsSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where descripstion contains DEFAULT_DESCRIPSTION
        defaultOrdersShouldBeFound("descripstion.contains=" + DEFAULT_DESCRIPSTION);

        // Get all the ordersList where descripstion contains UPDATED_DESCRIPSTION
        defaultOrdersShouldNotBeFound("descripstion.contains=" + UPDATED_DESCRIPSTION);
    }

    @Test
    @Transactional
    public void getAllOrdersByDescripstionNotContainsSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where descripstion does not contain DEFAULT_DESCRIPSTION
        defaultOrdersShouldNotBeFound("descripstion.doesNotContain=" + DEFAULT_DESCRIPSTION);

        // Get all the ordersList where descripstion does not contain UPDATED_DESCRIPSTION
        defaultOrdersShouldBeFound("descripstion.doesNotContain=" + UPDATED_DESCRIPSTION);
    }


    @Test
    @Transactional
    public void getAllOrdersByTotalOrderPriceIsEqualToSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where totalOrderPrice equals to DEFAULT_TOTAL_ORDER_PRICE
        defaultOrdersShouldBeFound("totalOrderPrice.equals=" + DEFAULT_TOTAL_ORDER_PRICE);

        // Get all the ordersList where totalOrderPrice equals to UPDATED_TOTAL_ORDER_PRICE
        defaultOrdersShouldNotBeFound("totalOrderPrice.equals=" + UPDATED_TOTAL_ORDER_PRICE);
    }

    @Test
    @Transactional
    public void getAllOrdersByTotalOrderPriceIsNotEqualToSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where totalOrderPrice not equals to DEFAULT_TOTAL_ORDER_PRICE
        defaultOrdersShouldNotBeFound("totalOrderPrice.notEquals=" + DEFAULT_TOTAL_ORDER_PRICE);

        // Get all the ordersList where totalOrderPrice not equals to UPDATED_TOTAL_ORDER_PRICE
        defaultOrdersShouldBeFound("totalOrderPrice.notEquals=" + UPDATED_TOTAL_ORDER_PRICE);
    }

    @Test
    @Transactional
    public void getAllOrdersByTotalOrderPriceIsInShouldWork() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where totalOrderPrice in DEFAULT_TOTAL_ORDER_PRICE or UPDATED_TOTAL_ORDER_PRICE
        defaultOrdersShouldBeFound("totalOrderPrice.in=" + DEFAULT_TOTAL_ORDER_PRICE + "," + UPDATED_TOTAL_ORDER_PRICE);

        // Get all the ordersList where totalOrderPrice equals to UPDATED_TOTAL_ORDER_PRICE
        defaultOrdersShouldNotBeFound("totalOrderPrice.in=" + UPDATED_TOTAL_ORDER_PRICE);
    }

    @Test
    @Transactional
    public void getAllOrdersByTotalOrderPriceIsNullOrNotNull() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where totalOrderPrice is not null
        defaultOrdersShouldBeFound("totalOrderPrice.specified=true");

        // Get all the ordersList where totalOrderPrice is null
        defaultOrdersShouldNotBeFound("totalOrderPrice.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrdersByTotalOrderPriceIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where totalOrderPrice is greater than or equal to DEFAULT_TOTAL_ORDER_PRICE
        defaultOrdersShouldBeFound("totalOrderPrice.greaterThanOrEqual=" + DEFAULT_TOTAL_ORDER_PRICE);

        // Get all the ordersList where totalOrderPrice is greater than or equal to UPDATED_TOTAL_ORDER_PRICE
        defaultOrdersShouldNotBeFound("totalOrderPrice.greaterThanOrEqual=" + UPDATED_TOTAL_ORDER_PRICE);
    }

    @Test
    @Transactional
    public void getAllOrdersByTotalOrderPriceIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where totalOrderPrice is less than or equal to DEFAULT_TOTAL_ORDER_PRICE
        defaultOrdersShouldBeFound("totalOrderPrice.lessThanOrEqual=" + DEFAULT_TOTAL_ORDER_PRICE);

        // Get all the ordersList where totalOrderPrice is less than or equal to SMALLER_TOTAL_ORDER_PRICE
        defaultOrdersShouldNotBeFound("totalOrderPrice.lessThanOrEqual=" + SMALLER_TOTAL_ORDER_PRICE);
    }

    @Test
    @Transactional
    public void getAllOrdersByTotalOrderPriceIsLessThanSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where totalOrderPrice is less than DEFAULT_TOTAL_ORDER_PRICE
        defaultOrdersShouldNotBeFound("totalOrderPrice.lessThan=" + DEFAULT_TOTAL_ORDER_PRICE);

        // Get all the ordersList where totalOrderPrice is less than UPDATED_TOTAL_ORDER_PRICE
        defaultOrdersShouldBeFound("totalOrderPrice.lessThan=" + UPDATED_TOTAL_ORDER_PRICE);
    }

    @Test
    @Transactional
    public void getAllOrdersByTotalOrderPriceIsGreaterThanSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where totalOrderPrice is greater than DEFAULT_TOTAL_ORDER_PRICE
        defaultOrdersShouldNotBeFound("totalOrderPrice.greaterThan=" + DEFAULT_TOTAL_ORDER_PRICE);

        // Get all the ordersList where totalOrderPrice is greater than SMALLER_TOTAL_ORDER_PRICE
        defaultOrdersShouldBeFound("totalOrderPrice.greaterThan=" + SMALLER_TOTAL_ORDER_PRICE);
    }


    @Test
    @Transactional
    public void getAllOrdersByOrderDateIsEqualToSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where orderDate equals to DEFAULT_ORDER_DATE
        defaultOrdersShouldBeFound("orderDate.equals=" + DEFAULT_ORDER_DATE);

        // Get all the ordersList where orderDate equals to UPDATED_ORDER_DATE
        defaultOrdersShouldNotBeFound("orderDate.equals=" + UPDATED_ORDER_DATE);
    }

    @Test
    @Transactional
    public void getAllOrdersByOrderDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where orderDate not equals to DEFAULT_ORDER_DATE
        defaultOrdersShouldNotBeFound("orderDate.notEquals=" + DEFAULT_ORDER_DATE);

        // Get all the ordersList where orderDate not equals to UPDATED_ORDER_DATE
        defaultOrdersShouldBeFound("orderDate.notEquals=" + UPDATED_ORDER_DATE);
    }

    @Test
    @Transactional
    public void getAllOrdersByOrderDateIsInShouldWork() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where orderDate in DEFAULT_ORDER_DATE or UPDATED_ORDER_DATE
        defaultOrdersShouldBeFound("orderDate.in=" + DEFAULT_ORDER_DATE + "," + UPDATED_ORDER_DATE);

        // Get all the ordersList where orderDate equals to UPDATED_ORDER_DATE
        defaultOrdersShouldNotBeFound("orderDate.in=" + UPDATED_ORDER_DATE);
    }

    @Test
    @Transactional
    public void getAllOrdersByOrderDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where orderDate is not null
        defaultOrdersShouldBeFound("orderDate.specified=true");

        // Get all the ordersList where orderDate is null
        defaultOrdersShouldNotBeFound("orderDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrdersByOrderDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where orderDate is greater than or equal to DEFAULT_ORDER_DATE
        defaultOrdersShouldBeFound("orderDate.greaterThanOrEqual=" + DEFAULT_ORDER_DATE);

        // Get all the ordersList where orderDate is greater than or equal to UPDATED_ORDER_DATE
        defaultOrdersShouldNotBeFound("orderDate.greaterThanOrEqual=" + UPDATED_ORDER_DATE);
    }

    @Test
    @Transactional
    public void getAllOrdersByOrderDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where orderDate is less than or equal to DEFAULT_ORDER_DATE
        defaultOrdersShouldBeFound("orderDate.lessThanOrEqual=" + DEFAULT_ORDER_DATE);

        // Get all the ordersList where orderDate is less than or equal to SMALLER_ORDER_DATE
        defaultOrdersShouldNotBeFound("orderDate.lessThanOrEqual=" + SMALLER_ORDER_DATE);
    }

    @Test
    @Transactional
    public void getAllOrdersByOrderDateIsLessThanSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where orderDate is less than DEFAULT_ORDER_DATE
        defaultOrdersShouldNotBeFound("orderDate.lessThan=" + DEFAULT_ORDER_DATE);

        // Get all the ordersList where orderDate is less than UPDATED_ORDER_DATE
        defaultOrdersShouldBeFound("orderDate.lessThan=" + UPDATED_ORDER_DATE);
    }

    @Test
    @Transactional
    public void getAllOrdersByOrderDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where orderDate is greater than DEFAULT_ORDER_DATE
        defaultOrdersShouldNotBeFound("orderDate.greaterThan=" + DEFAULT_ORDER_DATE);

        // Get all the ordersList where orderDate is greater than SMALLER_ORDER_DATE
        defaultOrdersShouldBeFound("orderDate.greaterThan=" + SMALLER_ORDER_DATE);
    }


    @Test
    @Transactional
    public void getAllOrdersByDeliveryDateFromIsEqualToSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where deliveryDateFrom equals to DEFAULT_DELIVERY_DATE_FROM
        defaultOrdersShouldBeFound("deliveryDateFrom.equals=" + DEFAULT_DELIVERY_DATE_FROM);

        // Get all the ordersList where deliveryDateFrom equals to UPDATED_DELIVERY_DATE_FROM
        defaultOrdersShouldNotBeFound("deliveryDateFrom.equals=" + UPDATED_DELIVERY_DATE_FROM);
    }

    @Test
    @Transactional
    public void getAllOrdersByDeliveryDateFromIsNotEqualToSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where deliveryDateFrom not equals to DEFAULT_DELIVERY_DATE_FROM
        defaultOrdersShouldNotBeFound("deliveryDateFrom.notEquals=" + DEFAULT_DELIVERY_DATE_FROM);

        // Get all the ordersList where deliveryDateFrom not equals to UPDATED_DELIVERY_DATE_FROM
        defaultOrdersShouldBeFound("deliveryDateFrom.notEquals=" + UPDATED_DELIVERY_DATE_FROM);
    }

    @Test
    @Transactional
    public void getAllOrdersByDeliveryDateFromIsInShouldWork() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where deliveryDateFrom in DEFAULT_DELIVERY_DATE_FROM or UPDATED_DELIVERY_DATE_FROM
        defaultOrdersShouldBeFound("deliveryDateFrom.in=" + DEFAULT_DELIVERY_DATE_FROM + "," + UPDATED_DELIVERY_DATE_FROM);

        // Get all the ordersList where deliveryDateFrom equals to UPDATED_DELIVERY_DATE_FROM
        defaultOrdersShouldNotBeFound("deliveryDateFrom.in=" + UPDATED_DELIVERY_DATE_FROM);
    }

    @Test
    @Transactional
    public void getAllOrdersByDeliveryDateFromIsNullOrNotNull() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where deliveryDateFrom is not null
        defaultOrdersShouldBeFound("deliveryDateFrom.specified=true");

        // Get all the ordersList where deliveryDateFrom is null
        defaultOrdersShouldNotBeFound("deliveryDateFrom.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrdersByDeliveryDateFromIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where deliveryDateFrom is greater than or equal to DEFAULT_DELIVERY_DATE_FROM
        defaultOrdersShouldBeFound("deliveryDateFrom.greaterThanOrEqual=" + DEFAULT_DELIVERY_DATE_FROM);

        // Get all the ordersList where deliveryDateFrom is greater than or equal to UPDATED_DELIVERY_DATE_FROM
        defaultOrdersShouldNotBeFound("deliveryDateFrom.greaterThanOrEqual=" + UPDATED_DELIVERY_DATE_FROM);
    }

    @Test
    @Transactional
    public void getAllOrdersByDeliveryDateFromIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where deliveryDateFrom is less than or equal to DEFAULT_DELIVERY_DATE_FROM
        defaultOrdersShouldBeFound("deliveryDateFrom.lessThanOrEqual=" + DEFAULT_DELIVERY_DATE_FROM);

        // Get all the ordersList where deliveryDateFrom is less than or equal to SMALLER_DELIVERY_DATE_FROM
        defaultOrdersShouldNotBeFound("deliveryDateFrom.lessThanOrEqual=" + SMALLER_DELIVERY_DATE_FROM);
    }

    @Test
    @Transactional
    public void getAllOrdersByDeliveryDateFromIsLessThanSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where deliveryDateFrom is less than DEFAULT_DELIVERY_DATE_FROM
        defaultOrdersShouldNotBeFound("deliveryDateFrom.lessThan=" + DEFAULT_DELIVERY_DATE_FROM);

        // Get all the ordersList where deliveryDateFrom is less than UPDATED_DELIVERY_DATE_FROM
        defaultOrdersShouldBeFound("deliveryDateFrom.lessThan=" + UPDATED_DELIVERY_DATE_FROM);
    }

    @Test
    @Transactional
    public void getAllOrdersByDeliveryDateFromIsGreaterThanSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where deliveryDateFrom is greater than DEFAULT_DELIVERY_DATE_FROM
        defaultOrdersShouldNotBeFound("deliveryDateFrom.greaterThan=" + DEFAULT_DELIVERY_DATE_FROM);

        // Get all the ordersList where deliveryDateFrom is greater than SMALLER_DELIVERY_DATE_FROM
        defaultOrdersShouldBeFound("deliveryDateFrom.greaterThan=" + SMALLER_DELIVERY_DATE_FROM);
    }


    @Test
    @Transactional
    public void getAllOrdersByDeliveryDateToIsEqualToSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where deliveryDateTo equals to DEFAULT_DELIVERY_DATE_TO
        defaultOrdersShouldBeFound("deliveryDateTo.equals=" + DEFAULT_DELIVERY_DATE_TO);

        // Get all the ordersList where deliveryDateTo equals to UPDATED_DELIVERY_DATE_TO
        defaultOrdersShouldNotBeFound("deliveryDateTo.equals=" + UPDATED_DELIVERY_DATE_TO);
    }

    @Test
    @Transactional
    public void getAllOrdersByDeliveryDateToIsNotEqualToSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where deliveryDateTo not equals to DEFAULT_DELIVERY_DATE_TO
        defaultOrdersShouldNotBeFound("deliveryDateTo.notEquals=" + DEFAULT_DELIVERY_DATE_TO);

        // Get all the ordersList where deliveryDateTo not equals to UPDATED_DELIVERY_DATE_TO
        defaultOrdersShouldBeFound("deliveryDateTo.notEquals=" + UPDATED_DELIVERY_DATE_TO);
    }

    @Test
    @Transactional
    public void getAllOrdersByDeliveryDateToIsInShouldWork() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where deliveryDateTo in DEFAULT_DELIVERY_DATE_TO or UPDATED_DELIVERY_DATE_TO
        defaultOrdersShouldBeFound("deliveryDateTo.in=" + DEFAULT_DELIVERY_DATE_TO + "," + UPDATED_DELIVERY_DATE_TO);

        // Get all the ordersList where deliveryDateTo equals to UPDATED_DELIVERY_DATE_TO
        defaultOrdersShouldNotBeFound("deliveryDateTo.in=" + UPDATED_DELIVERY_DATE_TO);
    }

    @Test
    @Transactional
    public void getAllOrdersByDeliveryDateToIsNullOrNotNull() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where deliveryDateTo is not null
        defaultOrdersShouldBeFound("deliveryDateTo.specified=true");

        // Get all the ordersList where deliveryDateTo is null
        defaultOrdersShouldNotBeFound("deliveryDateTo.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrdersByDeliveryDateToIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where deliveryDateTo is greater than or equal to DEFAULT_DELIVERY_DATE_TO
        defaultOrdersShouldBeFound("deliveryDateTo.greaterThanOrEqual=" + DEFAULT_DELIVERY_DATE_TO);

        // Get all the ordersList where deliveryDateTo is greater than or equal to UPDATED_DELIVERY_DATE_TO
        defaultOrdersShouldNotBeFound("deliveryDateTo.greaterThanOrEqual=" + UPDATED_DELIVERY_DATE_TO);
    }

    @Test
    @Transactional
    public void getAllOrdersByDeliveryDateToIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where deliveryDateTo is less than or equal to DEFAULT_DELIVERY_DATE_TO
        defaultOrdersShouldBeFound("deliveryDateTo.lessThanOrEqual=" + DEFAULT_DELIVERY_DATE_TO);

        // Get all the ordersList where deliveryDateTo is less than or equal to SMALLER_DELIVERY_DATE_TO
        defaultOrdersShouldNotBeFound("deliveryDateTo.lessThanOrEqual=" + SMALLER_DELIVERY_DATE_TO);
    }

    @Test
    @Transactional
    public void getAllOrdersByDeliveryDateToIsLessThanSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where deliveryDateTo is less than DEFAULT_DELIVERY_DATE_TO
        defaultOrdersShouldNotBeFound("deliveryDateTo.lessThan=" + DEFAULT_DELIVERY_DATE_TO);

        // Get all the ordersList where deliveryDateTo is less than UPDATED_DELIVERY_DATE_TO
        defaultOrdersShouldBeFound("deliveryDateTo.lessThan=" + UPDATED_DELIVERY_DATE_TO);
    }

    @Test
    @Transactional
    public void getAllOrdersByDeliveryDateToIsGreaterThanSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);

        // Get all the ordersList where deliveryDateTo is greater than DEFAULT_DELIVERY_DATE_TO
        defaultOrdersShouldNotBeFound("deliveryDateTo.greaterThan=" + DEFAULT_DELIVERY_DATE_TO);

        // Get all the ordersList where deliveryDateTo is greater than SMALLER_DELIVERY_DATE_TO
        defaultOrdersShouldBeFound("deliveryDateTo.greaterThan=" + SMALLER_DELIVERY_DATE_TO);
    }


    @Test
    @Transactional
    public void getAllOrdersByUserIsEqualToSomething() throws Exception {
        // Initialize the database
        ordersRepository.saveAndFlush(orders);
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        orders.setUser(user);
        ordersRepository.saveAndFlush(orders);
        Long userId = user.getId();

        // Get all the ordersList where user equals to userId
        defaultOrdersShouldBeFound("userId.equals=" + userId);

        // Get all the ordersList where user equals to userId + 1
        defaultOrdersShouldNotBeFound("userId.equals=" + (userId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultOrdersShouldBeFound(String filter) throws Exception {
        restOrdersMockMvc.perform(get("/api/orders?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orders.getId().intValue())))
            .andExpect(jsonPath("$.[*].confirm").value(hasItem(DEFAULT_CONFIRM.booleanValue())))
            .andExpect(jsonPath("$.[*].descripstion").value(hasItem(DEFAULT_DESCRIPSTION)))
            .andExpect(jsonPath("$.[*].totalOrderPrice").value(hasItem(DEFAULT_TOTAL_ORDER_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].orderDate").value(hasItem(DEFAULT_ORDER_DATE.toString())))
            .andExpect(jsonPath("$.[*].deliveryDateFrom").value(hasItem(DEFAULT_DELIVERY_DATE_FROM.toString())))
            .andExpect(jsonPath("$.[*].deliveryDateTo").value(hasItem(DEFAULT_DELIVERY_DATE_TO.toString())));

        // Check, that the count call also returns 1
        restOrdersMockMvc.perform(get("/api/orders/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultOrdersShouldNotBeFound(String filter) throws Exception {
        restOrdersMockMvc.perform(get("/api/orders?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restOrdersMockMvc.perform(get("/api/orders/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingOrders() throws Exception {
        // Get the orders
        restOrdersMockMvc.perform(get("/api/orders/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrders() throws Exception {
        // Initialize the database
        ordersService.save(orders);

        int databaseSizeBeforeUpdate = ordersRepository.findAll().size();

        // Update the orders
        Orders updatedOrders = ordersRepository.findById(orders.getId()).get();
        // Disconnect from session so that the updates on updatedOrders are not directly saved in db
        em.detach(updatedOrders);
        updatedOrders
            .confirm(UPDATED_CONFIRM)
            .descripstion(UPDATED_DESCRIPSTION)
            .totalOrderPrice(UPDATED_TOTAL_ORDER_PRICE)
            .orderDate(UPDATED_ORDER_DATE)
            .deliveryDateFrom(UPDATED_DELIVERY_DATE_FROM)
            .deliveryDateTo(UPDATED_DELIVERY_DATE_TO);

        restOrdersMockMvc.perform(put("/api/orders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedOrders)))
            .andExpect(status().isOk());

        // Validate the Orders in the database
        List<Orders> ordersList = ordersRepository.findAll();
        assertThat(ordersList).hasSize(databaseSizeBeforeUpdate);
        Orders testOrders = ordersList.get(ordersList.size() - 1);
        assertThat(testOrders.isConfirm()).isEqualTo(UPDATED_CONFIRM);
        assertThat(testOrders.getDescripstion()).isEqualTo(UPDATED_DESCRIPSTION);
        assertThat(testOrders.getTotalOrderPrice()).isEqualTo(UPDATED_TOTAL_ORDER_PRICE);
        assertThat(testOrders.getOrderDate()).isEqualTo(UPDATED_ORDER_DATE);
        assertThat(testOrders.getDeliveryDateFrom()).isEqualTo(UPDATED_DELIVERY_DATE_FROM);
        assertThat(testOrders.getDeliveryDateTo()).isEqualTo(UPDATED_DELIVERY_DATE_TO);
    }

    @Test
    @Transactional
    public void updateNonExistingOrders() throws Exception {
        int databaseSizeBeforeUpdate = ordersRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrdersMockMvc.perform(put("/api/orders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(orders)))
            .andExpect(status().isBadRequest());

        // Validate the Orders in the database
        List<Orders> ordersList = ordersRepository.findAll();
        assertThat(ordersList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOrders() throws Exception {
        // Initialize the database
        ordersService.save(orders);

        int databaseSizeBeforeDelete = ordersRepository.findAll().size();

        // Delete the orders
        restOrdersMockMvc.perform(delete("/api/orders/{id}", orders.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Orders> ordersList = ordersRepository.findAll();
        assertThat(ordersList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
