package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.HauiAdidasShoesApp;
import com.haui.adidasshoes.domain.OrderProduct;
import com.haui.adidasshoes.domain.Orders;
import com.haui.adidasshoes.domain.ListCart;
import com.haui.adidasshoes.repository.OrderProductRepository;
import com.haui.adidasshoes.service.OrderProductService;
import com.haui.adidasshoes.service.dto.OrderProductCriteria;
import com.haui.adidasshoes.service.OrderProductQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OrderProductResource} REST controller.
 */
@SpringBootTest(classes = HauiAdidasShoesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class OrderProductResourceIT {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private OrderProductRepository orderProductRepository;

    @Autowired
    private OrderProductService orderProductService;

    @Autowired
    private OrderProductQueryService orderProductQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOrderProductMockMvc;

    private OrderProduct orderProduct;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderProduct createEntity(EntityManager em) {
        OrderProduct orderProduct = new OrderProduct()
            .description(DEFAULT_DESCRIPTION);
        return orderProduct;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderProduct createUpdatedEntity(EntityManager em) {
        OrderProduct orderProduct = new OrderProduct()
            .description(UPDATED_DESCRIPTION);
        return orderProduct;
    }

    @BeforeEach
    public void initTest() {
        orderProduct = createEntity(em);
    }

    @Test
    @Transactional
    public void createOrderProduct() throws Exception {
        int databaseSizeBeforeCreate = orderProductRepository.findAll().size();
        // Create the OrderProduct
        restOrderProductMockMvc.perform(post("/api/order-products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(orderProduct)))
            .andExpect(status().isCreated());

        // Validate the OrderProduct in the database
        List<OrderProduct> orderProductList = orderProductRepository.findAll();
        assertThat(orderProductList).hasSize(databaseSizeBeforeCreate + 1);
        OrderProduct testOrderProduct = orderProductList.get(orderProductList.size() - 1);
        assertThat(testOrderProduct.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createOrderProductWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = orderProductRepository.findAll().size();

        // Create the OrderProduct with an existing ID
        orderProduct.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrderProductMockMvc.perform(post("/api/order-products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(orderProduct)))
            .andExpect(status().isBadRequest());

        // Validate the OrderProduct in the database
        List<OrderProduct> orderProductList = orderProductRepository.findAll();
        assertThat(orderProductList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllOrderProducts() throws Exception {
        // Initialize the database
        orderProductRepository.saveAndFlush(orderProduct);

        // Get all the orderProductList
        restOrderProductMockMvc.perform(get("/api/order-products?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orderProduct.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }
    
    @Test
    @Transactional
    public void getOrderProduct() throws Exception {
        // Initialize the database
        orderProductRepository.saveAndFlush(orderProduct);

        // Get the orderProduct
        restOrderProductMockMvc.perform(get("/api/order-products/{id}", orderProduct.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(orderProduct.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }


    @Test
    @Transactional
    public void getOrderProductsByIdFiltering() throws Exception {
        // Initialize the database
        orderProductRepository.saveAndFlush(orderProduct);

        Long id = orderProduct.getId();

        defaultOrderProductShouldBeFound("id.equals=" + id);
        defaultOrderProductShouldNotBeFound("id.notEquals=" + id);

        defaultOrderProductShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultOrderProductShouldNotBeFound("id.greaterThan=" + id);

        defaultOrderProductShouldBeFound("id.lessThanOrEqual=" + id);
        defaultOrderProductShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllOrderProductsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        orderProductRepository.saveAndFlush(orderProduct);

        // Get all the orderProductList where description equals to DEFAULT_DESCRIPTION
        defaultOrderProductShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the orderProductList where description equals to UPDATED_DESCRIPTION
        defaultOrderProductShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllOrderProductsByDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        orderProductRepository.saveAndFlush(orderProduct);

        // Get all the orderProductList where description not equals to DEFAULT_DESCRIPTION
        defaultOrderProductShouldNotBeFound("description.notEquals=" + DEFAULT_DESCRIPTION);

        // Get all the orderProductList where description not equals to UPDATED_DESCRIPTION
        defaultOrderProductShouldBeFound("description.notEquals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllOrderProductsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        orderProductRepository.saveAndFlush(orderProduct);

        // Get all the orderProductList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultOrderProductShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the orderProductList where description equals to UPDATED_DESCRIPTION
        defaultOrderProductShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllOrderProductsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        orderProductRepository.saveAndFlush(orderProduct);

        // Get all the orderProductList where description is not null
        defaultOrderProductShouldBeFound("description.specified=true");

        // Get all the orderProductList where description is null
        defaultOrderProductShouldNotBeFound("description.specified=false");
    }
                @Test
    @Transactional
    public void getAllOrderProductsByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        orderProductRepository.saveAndFlush(orderProduct);

        // Get all the orderProductList where description contains DEFAULT_DESCRIPTION
        defaultOrderProductShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the orderProductList where description contains UPDATED_DESCRIPTION
        defaultOrderProductShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllOrderProductsByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        orderProductRepository.saveAndFlush(orderProduct);

        // Get all the orderProductList where description does not contain DEFAULT_DESCRIPTION
        defaultOrderProductShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the orderProductList where description does not contain UPDATED_DESCRIPTION
        defaultOrderProductShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }


    @Test
    @Transactional
    public void getAllOrderProductsByOrdersIsEqualToSomething() throws Exception {
        // Initialize the database
        orderProductRepository.saveAndFlush(orderProduct);
        Orders orders = OrdersResourceIT.createEntity(em);
        em.persist(orders);
        em.flush();
        orderProduct.setOrders(orders);
        orderProductRepository.saveAndFlush(orderProduct);
        Long ordersId = orders.getId();

        // Get all the orderProductList where orders equals to ordersId
        defaultOrderProductShouldBeFound("ordersId.equals=" + ordersId);

        // Get all the orderProductList where orders equals to ordersId + 1
        defaultOrderProductShouldNotBeFound("ordersId.equals=" + (ordersId + 1));
    }


    @Test
    @Transactional
    public void getAllOrderProductsByListCartIsEqualToSomething() throws Exception {
        // Initialize the database
        orderProductRepository.saveAndFlush(orderProduct);
        ListCart listCart = ListCartResourceIT.createEntity(em);
        em.persist(listCart);
        em.flush();
        orderProduct.setListCart(listCart);
        orderProductRepository.saveAndFlush(orderProduct);
        Long listCartId = listCart.getId();

        // Get all the orderProductList where listCart equals to listCartId
        defaultOrderProductShouldBeFound("listCartId.equals=" + listCartId);

        // Get all the orderProductList where listCart equals to listCartId + 1
        defaultOrderProductShouldNotBeFound("listCartId.equals=" + (listCartId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultOrderProductShouldBeFound(String filter) throws Exception {
        restOrderProductMockMvc.perform(get("/api/order-products?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orderProduct.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));

        // Check, that the count call also returns 1
        restOrderProductMockMvc.perform(get("/api/order-products/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultOrderProductShouldNotBeFound(String filter) throws Exception {
        restOrderProductMockMvc.perform(get("/api/order-products?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restOrderProductMockMvc.perform(get("/api/order-products/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingOrderProduct() throws Exception {
        // Get the orderProduct
        restOrderProductMockMvc.perform(get("/api/order-products/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrderProduct() throws Exception {
        // Initialize the database
        orderProductService.save(orderProduct);

        int databaseSizeBeforeUpdate = orderProductRepository.findAll().size();

        // Update the orderProduct
        OrderProduct updatedOrderProduct = orderProductRepository.findById(orderProduct.getId()).get();
        // Disconnect from session so that the updates on updatedOrderProduct are not directly saved in db
        em.detach(updatedOrderProduct);
        updatedOrderProduct
            .description(UPDATED_DESCRIPTION);

        restOrderProductMockMvc.perform(put("/api/order-products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedOrderProduct)))
            .andExpect(status().isOk());

        // Validate the OrderProduct in the database
        List<OrderProduct> orderProductList = orderProductRepository.findAll();
        assertThat(orderProductList).hasSize(databaseSizeBeforeUpdate);
        OrderProduct testOrderProduct = orderProductList.get(orderProductList.size() - 1);
        assertThat(testOrderProduct.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingOrderProduct() throws Exception {
        int databaseSizeBeforeUpdate = orderProductRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrderProductMockMvc.perform(put("/api/order-products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(orderProduct)))
            .andExpect(status().isBadRequest());

        // Validate the OrderProduct in the database
        List<OrderProduct> orderProductList = orderProductRepository.findAll();
        assertThat(orderProductList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOrderProduct() throws Exception {
        // Initialize the database
        orderProductService.save(orderProduct);

        int databaseSizeBeforeDelete = orderProductRepository.findAll().size();

        // Delete the orderProduct
        restOrderProductMockMvc.perform(delete("/api/order-products/{id}", orderProduct.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OrderProduct> orderProductList = orderProductRepository.findAll();
        assertThat(orderProductList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
