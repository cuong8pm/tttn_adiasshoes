package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.HauiAdidasShoesApp;
import com.haui.adidasshoes.domain.Size;
import com.haui.adidasshoes.repository.SizeRepository;
import com.haui.adidasshoes.service.SizeService;
import com.haui.adidasshoes.service.dto.SizeCriteria;
import com.haui.adidasshoes.service.SizeQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SizeResource} REST controller.
 */
@SpringBootTest(classes = HauiAdidasShoesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class SizeResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPSTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPSTION = "BBBBBBBBBB";

    @Autowired
    private SizeRepository sizeRepository;

    @Autowired
    private SizeService sizeService;

    @Autowired
    private SizeQueryService sizeQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSizeMockMvc;

    private Size size;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Size createEntity(EntityManager em) {
        Size size = new Size()
            .name(DEFAULT_NAME)
            .descripstion(DEFAULT_DESCRIPSTION);
        return size;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Size createUpdatedEntity(EntityManager em) {
        Size size = new Size()
            .name(UPDATED_NAME)
            .descripstion(UPDATED_DESCRIPSTION);
        return size;
    }

    @BeforeEach
    public void initTest() {
        size = createEntity(em);
    }

    @Test
    @Transactional
    public void createSize() throws Exception {
        int databaseSizeBeforeCreate = sizeRepository.findAll().size();
        // Create the Size
        restSizeMockMvc.perform(post("/api/sizes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(size)))
            .andExpect(status().isCreated());

        // Validate the Size in the database
        List<Size> sizeList = sizeRepository.findAll();
        assertThat(sizeList).hasSize(databaseSizeBeforeCreate + 1);
        Size testSize = sizeList.get(sizeList.size() - 1);
        assertThat(testSize.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSize.getDescripstion()).isEqualTo(DEFAULT_DESCRIPSTION);
    }

    @Test
    @Transactional
    public void createSizeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sizeRepository.findAll().size();

        // Create the Size with an existing ID
        size.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSizeMockMvc.perform(post("/api/sizes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(size)))
            .andExpect(status().isBadRequest());

        // Validate the Size in the database
        List<Size> sizeList = sizeRepository.findAll();
        assertThat(sizeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllSizes() throws Exception {
        // Initialize the database
        sizeRepository.saveAndFlush(size);

        // Get all the sizeList
        restSizeMockMvc.perform(get("/api/sizes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(size.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].descripstion").value(hasItem(DEFAULT_DESCRIPSTION)));
    }
    
    @Test
    @Transactional
    public void getSize() throws Exception {
        // Initialize the database
        sizeRepository.saveAndFlush(size);

        // Get the size
        restSizeMockMvc.perform(get("/api/sizes/{id}", size.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(size.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.descripstion").value(DEFAULT_DESCRIPSTION));
    }


    @Test
    @Transactional
    public void getSizesByIdFiltering() throws Exception {
        // Initialize the database
        sizeRepository.saveAndFlush(size);

        Long id = size.getId();

        defaultSizeShouldBeFound("id.equals=" + id);
        defaultSizeShouldNotBeFound("id.notEquals=" + id);

        defaultSizeShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultSizeShouldNotBeFound("id.greaterThan=" + id);

        defaultSizeShouldBeFound("id.lessThanOrEqual=" + id);
        defaultSizeShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllSizesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        sizeRepository.saveAndFlush(size);

        // Get all the sizeList where name equals to DEFAULT_NAME
        defaultSizeShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the sizeList where name equals to UPDATED_NAME
        defaultSizeShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllSizesByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        sizeRepository.saveAndFlush(size);

        // Get all the sizeList where name not equals to DEFAULT_NAME
        defaultSizeShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the sizeList where name not equals to UPDATED_NAME
        defaultSizeShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllSizesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        sizeRepository.saveAndFlush(size);

        // Get all the sizeList where name in DEFAULT_NAME or UPDATED_NAME
        defaultSizeShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the sizeList where name equals to UPDATED_NAME
        defaultSizeShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllSizesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        sizeRepository.saveAndFlush(size);

        // Get all the sizeList where name is not null
        defaultSizeShouldBeFound("name.specified=true");

        // Get all the sizeList where name is null
        defaultSizeShouldNotBeFound("name.specified=false");
    }
                @Test
    @Transactional
    public void getAllSizesByNameContainsSomething() throws Exception {
        // Initialize the database
        sizeRepository.saveAndFlush(size);

        // Get all the sizeList where name contains DEFAULT_NAME
        defaultSizeShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the sizeList where name contains UPDATED_NAME
        defaultSizeShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllSizesByNameNotContainsSomething() throws Exception {
        // Initialize the database
        sizeRepository.saveAndFlush(size);

        // Get all the sizeList where name does not contain DEFAULT_NAME
        defaultSizeShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the sizeList where name does not contain UPDATED_NAME
        defaultSizeShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }


    @Test
    @Transactional
    public void getAllSizesByDescripstionIsEqualToSomething() throws Exception {
        // Initialize the database
        sizeRepository.saveAndFlush(size);

        // Get all the sizeList where descripstion equals to DEFAULT_DESCRIPSTION
        defaultSizeShouldBeFound("descripstion.equals=" + DEFAULT_DESCRIPSTION);

        // Get all the sizeList where descripstion equals to UPDATED_DESCRIPSTION
        defaultSizeShouldNotBeFound("descripstion.equals=" + UPDATED_DESCRIPSTION);
    }

    @Test
    @Transactional
    public void getAllSizesByDescripstionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        sizeRepository.saveAndFlush(size);

        // Get all the sizeList where descripstion not equals to DEFAULT_DESCRIPSTION
        defaultSizeShouldNotBeFound("descripstion.notEquals=" + DEFAULT_DESCRIPSTION);

        // Get all the sizeList where descripstion not equals to UPDATED_DESCRIPSTION
        defaultSizeShouldBeFound("descripstion.notEquals=" + UPDATED_DESCRIPSTION);
    }

    @Test
    @Transactional
    public void getAllSizesByDescripstionIsInShouldWork() throws Exception {
        // Initialize the database
        sizeRepository.saveAndFlush(size);

        // Get all the sizeList where descripstion in DEFAULT_DESCRIPSTION or UPDATED_DESCRIPSTION
        defaultSizeShouldBeFound("descripstion.in=" + DEFAULT_DESCRIPSTION + "," + UPDATED_DESCRIPSTION);

        // Get all the sizeList where descripstion equals to UPDATED_DESCRIPSTION
        defaultSizeShouldNotBeFound("descripstion.in=" + UPDATED_DESCRIPSTION);
    }

    @Test
    @Transactional
    public void getAllSizesByDescripstionIsNullOrNotNull() throws Exception {
        // Initialize the database
        sizeRepository.saveAndFlush(size);

        // Get all the sizeList where descripstion is not null
        defaultSizeShouldBeFound("descripstion.specified=true");

        // Get all the sizeList where descripstion is null
        defaultSizeShouldNotBeFound("descripstion.specified=false");
    }
                @Test
    @Transactional
    public void getAllSizesByDescripstionContainsSomething() throws Exception {
        // Initialize the database
        sizeRepository.saveAndFlush(size);

        // Get all the sizeList where descripstion contains DEFAULT_DESCRIPSTION
        defaultSizeShouldBeFound("descripstion.contains=" + DEFAULT_DESCRIPSTION);

        // Get all the sizeList where descripstion contains UPDATED_DESCRIPSTION
        defaultSizeShouldNotBeFound("descripstion.contains=" + UPDATED_DESCRIPSTION);
    }

    @Test
    @Transactional
    public void getAllSizesByDescripstionNotContainsSomething() throws Exception {
        // Initialize the database
        sizeRepository.saveAndFlush(size);

        // Get all the sizeList where descripstion does not contain DEFAULT_DESCRIPSTION
        defaultSizeShouldNotBeFound("descripstion.doesNotContain=" + DEFAULT_DESCRIPSTION);

        // Get all the sizeList where descripstion does not contain UPDATED_DESCRIPSTION
        defaultSizeShouldBeFound("descripstion.doesNotContain=" + UPDATED_DESCRIPSTION);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSizeShouldBeFound(String filter) throws Exception {
        restSizeMockMvc.perform(get("/api/sizes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(size.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].descripstion").value(hasItem(DEFAULT_DESCRIPSTION)));

        // Check, that the count call also returns 1
        restSizeMockMvc.perform(get("/api/sizes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSizeShouldNotBeFound(String filter) throws Exception {
        restSizeMockMvc.perform(get("/api/sizes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSizeMockMvc.perform(get("/api/sizes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingSize() throws Exception {
        // Get the size
        restSizeMockMvc.perform(get("/api/sizes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSize() throws Exception {
        // Initialize the database
        sizeService.save(size);

        int databaseSizeBeforeUpdate = sizeRepository.findAll().size();

        // Update the size
        Size updatedSize = sizeRepository.findById(size.getId()).get();
        // Disconnect from session so that the updates on updatedSize are not directly saved in db
        em.detach(updatedSize);
        updatedSize
            .name(UPDATED_NAME)
            .descripstion(UPDATED_DESCRIPSTION);

        restSizeMockMvc.perform(put("/api/sizes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedSize)))
            .andExpect(status().isOk());

        // Validate the Size in the database
        List<Size> sizeList = sizeRepository.findAll();
        assertThat(sizeList).hasSize(databaseSizeBeforeUpdate);
        Size testSize = sizeList.get(sizeList.size() - 1);
        assertThat(testSize.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSize.getDescripstion()).isEqualTo(UPDATED_DESCRIPSTION);
    }

    @Test
    @Transactional
    public void updateNonExistingSize() throws Exception {
        int databaseSizeBeforeUpdate = sizeRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSizeMockMvc.perform(put("/api/sizes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(size)))
            .andExpect(status().isBadRequest());

        // Validate the Size in the database
        List<Size> sizeList = sizeRepository.findAll();
        assertThat(sizeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSize() throws Exception {
        // Initialize the database
        sizeService.save(size);

        int databaseSizeBeforeDelete = sizeRepository.findAll().size();

        // Delete the size
        restSizeMockMvc.perform(delete("/api/sizes/{id}", size.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Size> sizeList = sizeRepository.findAll();
        assertThat(sizeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
