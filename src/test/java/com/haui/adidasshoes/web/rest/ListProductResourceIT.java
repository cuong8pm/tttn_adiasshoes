package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.HauiAdidasShoesApp;
import com.haui.adidasshoes.domain.ListProduct;
import com.haui.adidasshoes.repository.ListProductRepository;
import com.haui.adidasshoes.service.ListProductService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ListProductResource} REST controller.
 */
@SpringBootTest(classes = HauiAdidasShoesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ListProductResourceIT {

    @Autowired
    private ListProductRepository listProductRepository;

    @Autowired
    private ListProductService listProductService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restListProductMockMvc;

    private ListProduct listProduct;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ListProduct createEntity(EntityManager em) {
        ListProduct listProduct = new ListProduct();
        return listProduct;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ListProduct createUpdatedEntity(EntityManager em) {
        ListProduct listProduct = new ListProduct();
        return listProduct;
    }

    @BeforeEach
    public void initTest() {
        listProduct = createEntity(em);
    }

    @Test
    @Transactional
    public void createListProduct() throws Exception {
        int databaseSizeBeforeCreate = listProductRepository.findAll().size();
        // Create the ListProduct
        restListProductMockMvc.perform(post("/api/list-products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(listProduct)))
            .andExpect(status().isCreated());

        // Validate the ListProduct in the database
        List<ListProduct> listProductList = listProductRepository.findAll();
        assertThat(listProductList).hasSize(databaseSizeBeforeCreate + 1);
        ListProduct testListProduct = listProductList.get(listProductList.size() - 1);
    }

    @Test
    @Transactional
    public void createListProductWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = listProductRepository.findAll().size();

        // Create the ListProduct with an existing ID
        listProduct.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restListProductMockMvc.perform(post("/api/list-products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(listProduct)))
            .andExpect(status().isBadRequest());

        // Validate the ListProduct in the database
        List<ListProduct> listProductList = listProductRepository.findAll();
        assertThat(listProductList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllListProducts() throws Exception {
        // Initialize the database
        listProductRepository.saveAndFlush(listProduct);

        // Get all the listProductList
        restListProductMockMvc.perform(get("/api/list-products?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(listProduct.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getListProduct() throws Exception {
        // Initialize the database
        listProductRepository.saveAndFlush(listProduct);

        // Get the listProduct
        restListProductMockMvc.perform(get("/api/list-products/{id}", listProduct.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(listProduct.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingListProduct() throws Exception {
        // Get the listProduct
        restListProductMockMvc.perform(get("/api/list-products/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateListProduct() throws Exception {
        // Initialize the database
        listProductService.save(listProduct);

        int databaseSizeBeforeUpdate = listProductRepository.findAll().size();

        // Update the listProduct
        ListProduct updatedListProduct = listProductRepository.findById(listProduct.getId()).get();
        // Disconnect from session so that the updates on updatedListProduct are not directly saved in db
        em.detach(updatedListProduct);

        restListProductMockMvc.perform(put("/api/list-products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedListProduct)))
            .andExpect(status().isOk());

        // Validate the ListProduct in the database
        List<ListProduct> listProductList = listProductRepository.findAll();
        assertThat(listProductList).hasSize(databaseSizeBeforeUpdate);
        ListProduct testListProduct = listProductList.get(listProductList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingListProduct() throws Exception {
        int databaseSizeBeforeUpdate = listProductRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restListProductMockMvc.perform(put("/api/list-products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(listProduct)))
            .andExpect(status().isBadRequest());

        // Validate the ListProduct in the database
        List<ListProduct> listProductList = listProductRepository.findAll();
        assertThat(listProductList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteListProduct() throws Exception {
        // Initialize the database
        listProductService.save(listProduct);

        int databaseSizeBeforeDelete = listProductRepository.findAll().size();

        // Delete the listProduct
        restListProductMockMvc.perform(delete("/api/list-products/{id}", listProduct.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ListProduct> listProductList = listProductRepository.findAll();
        assertThat(listProductList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
