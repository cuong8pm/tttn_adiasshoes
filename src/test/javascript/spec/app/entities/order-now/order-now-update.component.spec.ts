import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { HauiAdidasShoesTestModule } from '../../../test.module';
import { OrderNowUpdateComponent } from 'app/entities/order-now/order-now-update.component';
import { OrderNowService } from 'app/entities/order-now/order-now.service';
import { OrderNow } from 'app/shared/model/order-now.model';

describe('Component Tests', () => {
  describe('OrderNow Management Update Component', () => {
    let comp: OrderNowUpdateComponent;
    let fixture: ComponentFixture<OrderNowUpdateComponent>;
    let service: OrderNowService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HauiAdidasShoesTestModule],
        declarations: [OrderNowUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(OrderNowUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OrderNowUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OrderNowService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new OrderNow(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new OrderNow();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
