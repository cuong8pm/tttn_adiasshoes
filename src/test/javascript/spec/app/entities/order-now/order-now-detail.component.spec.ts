import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HauiAdidasShoesTestModule } from '../../../test.module';
import { OrderNowDetailComponent } from 'app/entities/order-now/order-now-detail.component';
import { OrderNow } from 'app/shared/model/order-now.model';

describe('Component Tests', () => {
  describe('OrderNow Management Detail Component', () => {
    let comp: OrderNowDetailComponent;
    let fixture: ComponentFixture<OrderNowDetailComponent>;
    const route = ({ data: of({ orderNow: new OrderNow(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HauiAdidasShoesTestModule],
        declarations: [OrderNowDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(OrderNowDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OrderNowDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load orderNow on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.orderNow).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
