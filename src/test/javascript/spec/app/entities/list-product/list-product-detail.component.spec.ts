import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HauiAdidasShoesTestModule } from '../../../test.module';
import { ListProductDetailComponent } from 'app/entities/list-product/list-product-detail.component';
import { ListProduct } from 'app/shared/model/list-product.model';

describe('Component Tests', () => {
  describe('ListProduct Management Detail Component', () => {
    let comp: ListProductDetailComponent;
    let fixture: ComponentFixture<ListProductDetailComponent>;
    const route = ({ data: of({ listProduct: new ListProduct(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HauiAdidasShoesTestModule],
        declarations: [ListProductDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ListProductDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ListProductDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load listProduct on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.listProduct).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
