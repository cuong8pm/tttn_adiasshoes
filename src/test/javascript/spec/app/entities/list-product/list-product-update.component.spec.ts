import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { HauiAdidasShoesTestModule } from '../../../test.module';
import { ListProductUpdateComponent } from 'app/entities/list-product/list-product-update.component';
import { ListProductService } from 'app/entities/list-product/list-product.service';
import { ListProduct } from 'app/shared/model/list-product.model';

describe('Component Tests', () => {
  describe('ListProduct Management Update Component', () => {
    let comp: ListProductUpdateComponent;
    let fixture: ComponentFixture<ListProductUpdateComponent>;
    let service: ListProductService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HauiAdidasShoesTestModule],
        declarations: [ListProductUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ListProductUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ListProductUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ListProductService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ListProduct(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ListProduct();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
