import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { HauiAdidasShoesTestModule } from '../../../test.module';
import { SizeUpdateComponent } from 'app/entities/size/size-update.component';
import { SizeService } from 'app/entities/size/size.service';
import { Size } from 'app/shared/model/size.model';

describe('Component Tests', () => {
  describe('Size Management Update Component', () => {
    let comp: SizeUpdateComponent;
    let fixture: ComponentFixture<SizeUpdateComponent>;
    let service: SizeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HauiAdidasShoesTestModule],
        declarations: [SizeUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(SizeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SizeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SizeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Size(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Size();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
