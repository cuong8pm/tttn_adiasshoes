import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { HauiAdidasShoesTestModule } from '../../../test.module';
import { DetailProductUpdateComponent } from 'app/entities/detail-product/detail-product-update.component';
import { DetailProductService } from 'app/entities/detail-product/detail-product.service';
import { DetailProduct } from 'app/shared/model/detail-product.model';

describe('Component Tests', () => {
  describe('DetailProduct Management Update Component', () => {
    let comp: DetailProductUpdateComponent;
    let fixture: ComponentFixture<DetailProductUpdateComponent>;
    let service: DetailProductService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HauiAdidasShoesTestModule],
        declarations: [DetailProductUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(DetailProductUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DetailProductUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DetailProductService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new DetailProduct(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new DetailProduct();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
