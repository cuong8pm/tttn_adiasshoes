import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HauiAdidasShoesTestModule } from '../../../test.module';
import { DetailProductDetailComponent } from 'app/entities/detail-product/detail-product-detail.component';
import { DetailProduct } from 'app/shared/model/detail-product.model';

describe('Component Tests', () => {
  describe('DetailProduct Management Detail Component', () => {
    let comp: DetailProductDetailComponent;
    let fixture: ComponentFixture<DetailProductDetailComponent>;
    const route = ({ data: of({ detailProduct: new DetailProduct(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HauiAdidasShoesTestModule],
        declarations: [DetailProductDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(DetailProductDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DetailProductDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load detailProduct on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.detailProduct).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
