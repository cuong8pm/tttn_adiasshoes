import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HauiAdidasShoesTestModule } from '../../../test.module';
import { TrackOrderUserDetailComponent } from 'app/entities/track-order-user/track-order-user-detail.component';
import { TrackOrderUser } from 'app/shared/model/track-order-user.model';

describe('Component Tests', () => {
  describe('TrackOrderUser Management Detail Component', () => {
    let comp: TrackOrderUserDetailComponent;
    let fixture: ComponentFixture<TrackOrderUserDetailComponent>;
    const route = ({ data: of({ trackOrderUser: new TrackOrderUser(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HauiAdidasShoesTestModule],
        declarations: [TrackOrderUserDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(TrackOrderUserDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TrackOrderUserDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load trackOrderUser on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.trackOrderUser).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
