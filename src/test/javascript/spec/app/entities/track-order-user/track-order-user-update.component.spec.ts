import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { HauiAdidasShoesTestModule } from '../../../test.module';
import { TrackOrderUserUpdateComponent } from 'app/entities/track-order-user/track-order-user-update.component';
import { TrackOrderUserService } from 'app/entities/track-order-user/track-order-user.service';
import { TrackOrderUser } from 'app/shared/model/track-order-user.model';

describe('Component Tests', () => {
  describe('TrackOrderUser Management Update Component', () => {
    let comp: TrackOrderUserUpdateComponent;
    let fixture: ComponentFixture<TrackOrderUserUpdateComponent>;
    let service: TrackOrderUserService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HauiAdidasShoesTestModule],
        declarations: [TrackOrderUserUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(TrackOrderUserUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TrackOrderUserUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TrackOrderUserService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TrackOrderUser(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TrackOrderUser();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
