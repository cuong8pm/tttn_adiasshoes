import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HauiAdidasShoesTestModule } from '../../../test.module';
import { TrackOrderDetailComponent } from 'app/entities/track-order/track-order-detail.component';
import { TrackOrder } from 'app/shared/model/track-order.model';

describe('Component Tests', () => {
  describe('TrackOrder Management Detail Component', () => {
    let comp: TrackOrderDetailComponent;
    let fixture: ComponentFixture<TrackOrderDetailComponent>;
    const route = ({ data: of({ trackOrder: new TrackOrder(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HauiAdidasShoesTestModule],
        declarations: [TrackOrderDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(TrackOrderDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TrackOrderDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load trackOrder on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.trackOrder).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
