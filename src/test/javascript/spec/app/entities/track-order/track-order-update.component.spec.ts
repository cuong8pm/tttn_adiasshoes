import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { HauiAdidasShoesTestModule } from '../../../test.module';
import { TrackOrderUpdateComponent } from 'app/entities/track-order/track-order-update.component';
import { TrackOrderService } from 'app/entities/track-order/track-order.service';
import { TrackOrder } from 'app/shared/model/track-order.model';

describe('Component Tests', () => {
  describe('TrackOrder Management Update Component', () => {
    let comp: TrackOrderUpdateComponent;
    let fixture: ComponentFixture<TrackOrderUpdateComponent>;
    let service: TrackOrderService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HauiAdidasShoesTestModule],
        declarations: [TrackOrderUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(TrackOrderUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TrackOrderUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TrackOrderService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TrackOrder(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TrackOrder();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
