import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, convertToParamMap } from '@angular/router';

import { HauiAdidasShoesTestModule } from '../../../test.module';
import { TrackOrderComponent } from 'app/entities/track-order/track-order.component';
import { TrackOrderService } from 'app/entities/track-order/track-order.service';
import { TrackOrder } from 'app/shared/model/track-order.model';

describe('Component Tests', () => {
  describe('TrackOrder Management Component', () => {
    let comp: TrackOrderComponent;
    let fixture: ComponentFixture<TrackOrderComponent>;
    let service: TrackOrderService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HauiAdidasShoesTestModule],
        declarations: [TrackOrderComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: of({
                defaultSort: 'id,asc',
              }),
              queryParamMap: of(
                convertToParamMap({
                  page: '1',
                  size: '1',
                  sort: 'id,desc',
                })
              ),
            },
          },
        ],
      })
        .overrideTemplate(TrackOrderComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TrackOrderComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TrackOrderService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new TrackOrder(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.trackOrders && comp.trackOrders[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new TrackOrder(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.trackOrders && comp.trackOrders[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      comp.ngOnInit();
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // INIT
      comp.ngOnInit();

      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
