import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { HauiAdidasShoesTestModule } from '../../../test.module';
import { OrderTracksUpdateComponent } from 'app/entities/order-tracks/order-tracks-update.component';
import { OrderTracksService } from 'app/entities/order-tracks/order-tracks.service';
import { OrderTracks } from 'app/shared/model/order-tracks.model';

describe('Component Tests', () => {
  describe('OrderTracks Management Update Component', () => {
    let comp: OrderTracksUpdateComponent;
    let fixture: ComponentFixture<OrderTracksUpdateComponent>;
    let service: OrderTracksService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HauiAdidasShoesTestModule],
        declarations: [OrderTracksUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(OrderTracksUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OrderTracksUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OrderTracksService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new OrderTracks(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new OrderTracks();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
