import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HauiAdidasShoesTestModule } from '../../../test.module';
import { OrderTracksDetailComponent } from 'app/entities/order-tracks/order-tracks-detail.component';
import { OrderTracks } from 'app/shared/model/order-tracks.model';

describe('Component Tests', () => {
  describe('OrderTracks Management Detail Component', () => {
    let comp: OrderTracksDetailComponent;
    let fixture: ComponentFixture<OrderTracksDetailComponent>;
    const route = ({ data: of({ orderTracks: new OrderTracks(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HauiAdidasShoesTestModule],
        declarations: [OrderTracksDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(OrderTracksDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OrderTracksDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load orderTracks on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.orderTracks).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
