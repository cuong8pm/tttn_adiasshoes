import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HauiAdidasShoesTestModule } from '../../../test.module';
import { ListCartDetailComponent } from 'app/entities/list-cart/list-cart-detail.component';
import { ListCart } from 'app/shared/model/list-cart.model';

describe('Component Tests', () => {
  describe('ListCart Management Detail Component', () => {
    let comp: ListCartDetailComponent;
    let fixture: ComponentFixture<ListCartDetailComponent>;
    const route = ({ data: of({ listCart: new ListCart(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HauiAdidasShoesTestModule],
        declarations: [ListCartDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ListCartDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ListCartDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load listCart on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.listCart).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
