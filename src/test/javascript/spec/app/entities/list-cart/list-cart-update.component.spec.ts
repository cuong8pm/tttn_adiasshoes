import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { HauiAdidasShoesTestModule } from '../../../test.module';
import { ListCartUpdateComponent } from 'app/entities/list-cart/list-cart-update.component';
import { ListCartService } from 'app/entities/list-cart/list-cart.service';
import { ListCart } from 'app/shared/model/list-cart.model';

describe('Component Tests', () => {
  describe('ListCart Management Update Component', () => {
    let comp: ListCartUpdateComponent;
    let fixture: ComponentFixture<ListCartUpdateComponent>;
    let service: ListCartService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HauiAdidasShoesTestModule],
        declarations: [ListCartUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ListCartUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ListCartUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ListCartService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ListCart(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ListCart();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
