// import { Search } from './../../shared/util/request-util';
// import { ListProduct } from './../../shared/model/list-product.model';
import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { SessionStorageService } from 'ngx-webstorage';

import { VERSION } from 'app/app.constants';
import { LANGUAGES } from 'app/core/language/language.constants';
import { AccountService } from 'app/core/auth/account.service';
import { LoginModalService } from 'app/core/login/login-modal.service';
import { LoginService } from 'app/core/login/login.service';
import { ProfileService } from 'app/layouts/profiles/profile.service';
import { Account } from 'app/core/user/account.model';
import { ListCartService } from 'app/entities/list-cart/list-cart.service';
import { HttpResponse } from '@angular/common/http';
import { ISize } from 'app/shared/model/size.model';
// import { SearchCondition } from './../../shared/model/searchCondition.model';

@Component({
  selector: 'jhi-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['navbar.scss'],
})
export class NavbarComponent implements OnInit {
  inProduction?: boolean;
  isNavbarCollapsed = true;
  languages = LANGUAGES;
  swaggerEnabled?: boolean;
  version: string;
  name?: string;
  totalCart?: Number | any[] = 0;
  account!: Account;

  constructor(
    private loginService: LoginService,
    private listCartService: ListCartService,
    private languageService: JhiLanguageService,
    private sessionStorage: SessionStorageService,
    private accountService: AccountService,
    private loginModalService: LoginModalService,
    private profileService: ProfileService,
    private router: Router // private searchCondition : SearchCondition
  ) {
    this.version = VERSION ? (VERSION.toLowerCase().startsWith('v') ? VERSION : 'v' + VERSION) : '';
  }

  ngOnInit(): void {
    this.profileService.getProfileInfo().subscribe(profileInfo => {
      this.inProduction = profileInfo.inProduction;
      this.swaggerEnabled = profileInfo.swaggerEnabled;
    });
    this.accountService.identity().subscribe(account => {
      if (account) {
        this.account = account;
      }
    });
    this.listCartService
      .getTotalCarts({ username: this.account.login })
      .subscribe((res: HttpResponse<Number | undefined>) => (this.totalCart = res.body || []));
  }

  changeLanguage(languageKey: string): void {
    this.sessionStorage.store('locale', languageKey);
    this.languageService.changeLanguage(languageKey);
  }

  collapseNavbar(): void {
    this.isNavbarCollapsed = true;
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  logout(): void {
    this.collapseNavbar();
    this.loginService.logout();
    this.router.navigate(['']);
  }

  toggleNavbar(): void {
    this.isNavbarCollapsed = !this.isNavbarCollapsed;
  }

  getImageUrl(): string {
    return this.isAuthenticated() ? this.accountService.getImageUrl() : '';
  }

  searchProduct(e: KeyboardEvent): void {
    if (e.key === 'Enter') {
      this.router.navigate(['/list-product'], { queryParams: { name: this.name } });
    }
  }
  listCart(): void {
    this.router.navigate(['/list-cart'], { queryParams: {} });
  }
}
