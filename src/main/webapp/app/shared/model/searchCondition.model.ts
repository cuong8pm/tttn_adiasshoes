export interface ISearchCondition {
  name?: string;
  type?: number;
  fromPrice?: number;
  toPrice?: number;
  isAsc?: boolean;
}

export class SearchCondition implements ISearchCondition {
  constructor(public name?: string, public type?: number, public fromPrice?: number, public toPrice?: number, public isAsc?: boolean) {
    this.isAsc = this.isAsc || true;
  }
}
