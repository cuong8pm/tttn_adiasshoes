import { ICategory } from 'app/shared/model/category.model';

export interface IProduct {
  id?: number;
  name?: string;
  imageContentType?: string;
  image?: any;
  price?: number;
  title?: string;
  description?: string;
  hot?: boolean;
  category?: ICategory;
}

export class Product implements IProduct {
  constructor(
    public id?: number,
    public name?: string,
    public imageContentType?: string,
    public image?: any,
    public price?: number,
    public title?: string,
    public description?: string,
    public hot?: boolean,
    public category?: ICategory
  ) {
    this.hot = this.hot || false;
  }
}
