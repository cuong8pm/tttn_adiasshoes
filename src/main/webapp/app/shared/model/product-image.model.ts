import { IProduct } from 'app/shared/model/product.model';

export interface IProductImage {
  id?: number;
  imageContentType?: string;
  image?: any;
  description?: string;
  product?: IProduct;
}

export class ProductImage implements IProductImage {
  constructor(
    public id?: number,
    public imageContentType?: string,
    public image?: any,
    public description?: string,
    public product?: IProduct
  ) {}
}
