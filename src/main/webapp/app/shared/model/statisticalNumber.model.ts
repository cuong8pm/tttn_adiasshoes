export interface IStatisticalNumber {
  orderNew?: number;
  orderNowNew?: number;
  revenue?: number;
  product?: number;
}

export class StatisticalNumber implements IStatisticalNumber {
  constructor(public orderNew?: number, public orderNowNew?: number, public revenue?: number, public product?: number) {}
}
