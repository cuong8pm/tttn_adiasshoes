import { IProduct } from 'app/shared/model/product.model';
import { ISize } from 'app/shared/model/size.model';

export interface IAmount {
  id?: number;
  total?: number;
  inventory?: number;
  description?: string;
  soldProduct?: number;
  product?: IProduct;
  size?: ISize;
}

export class Amount implements IAmount {
  constructor(
    public id?: number,
    public total?: number,
    public inventory?: number,
    public description?: string,
    public soldProduct?: number,
    public product?: IProduct,
    public size?: ISize
  ) {}
}
