import { IUser } from 'app/core/user/user.model';
import { IProduct } from 'app/shared/model/product.model';
import { ISize } from 'app/shared/model/size.model';

export interface IListCart {
  id?: number;
  amount?: number;
  checkReview?: boolean;
  checkDisplay?: boolean;
  description?: string;
  user?: IUser;
  product?: IProduct;
  size?: ISize;
}

export class ListCart implements IListCart {
  constructor(
    public id?: number,
    public amount?: number,
    public checkReview?: boolean,
    public checkDisplay?: boolean,
    public description?: string,
    public user?: IUser,
    public product?: IProduct,
    public size?: ISize
  ) {}
}
