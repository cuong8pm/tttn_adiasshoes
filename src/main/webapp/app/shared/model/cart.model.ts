import { IUser } from '../../core/user/user.model';
import { IProduct } from './product.model';
import { ISize } from './size.model';

export interface ICart {
  id?: number;
  amount?: number;
  description?: string;
  username?: string;
  product?: IProduct | null;
  size?: string;
}

export class Cart implements ICart {
  constructor(
    public id?: number,
    public amount?: number,
    public description?: string,
    public username?: string,
    public product?: IProduct | null,
    public size?: string
  ) {
    (this.id = id),
      (this.amount = amount),
      (this.description = description),
      (this.username = username),
      (this.product = product),
      (this.size = size);
  }
}
