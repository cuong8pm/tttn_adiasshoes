import { IProduct } from 'app/shared/model/product.model';

export interface IProductDetails {
  id?: number;
  description?: string;
  product?: IProduct;
}

export class ProductDetails implements IProductDetails {
  constructor(public id?: number, public description?: string, public product?: IProduct) {}
}
