export interface IDetailProduct {
  id?: number;
}

export class DetailProduct implements IDetailProduct {
  constructor(public id?: number) {}
}
