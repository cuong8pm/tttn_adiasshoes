import { IOrders } from 'app/shared/model/orders.model';
import { IListCart } from 'app/shared/model/list-cart.model';

export interface IOrderProduct {
  id?: number;
  description?: string;
  orders?: IOrders;
  listCart?: IListCart;
}

export class OrderProduct implements IOrderProduct {
  constructor(public id?: number, public description?: string, public orders?: IOrders, public listCart?: IListCart) {}
}
