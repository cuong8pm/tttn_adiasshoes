export interface IGrossProduct {
  state: string;
  year1998: number;
  year2001: number;
  year2004: number;
}

export class GrossProduct implements IGrossProduct {
  constructor(public state: string, public year1998: number, public year2001: number, public year2004: number) {}
}
