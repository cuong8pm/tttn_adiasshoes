import { IListCart } from 'app/shared/model/list-cart.model';
import { Moment } from 'moment';

export interface IOrderNow {
  id?: number;
  name?: string;
  phone?: string;
  address?: string;
  orderDate?: Moment;
  deliveryDate?: Moment;
  orderConfirm?: boolean;
  delivered?: boolean;
  status?: boolean;
  totalOrderPrice?: number;
  listCart?: IListCart;
}

export class OrderNow implements IOrderNow {
  constructor(
    public id?: number,
    public name?: string,
    public phone?: string,
    public address?: string,
    public orderDate?: Moment,
    public deliveryDate?: Moment,
    public orderConfirm?: boolean,
    public delivered?: boolean,
    public status?: boolean,
    public totalOrderPrice?: number,
    public listCart?: IListCart
  ) {
    this.orderConfirm = this.orderConfirm || false;
    this.delivered = this.delivered || false;
    this.status = this.status || false;
  }
}
