import { IAmount } from 'app/shared/model/amount.model';

export interface IProductImportHistory {
  id?: number;
  description?: string;
  amount?: IAmount;
}

export class ProductImportHistory implements IProductImportHistory {
  constructor(public id?: number, public description?: string, public amount?: IAmount) {}
}
