export interface IUserReview {
  firstName?: string;
  lastName?: string;
  review?: string;
}

export class UserReview implements IUserReview {
  constructor(public firstName?: string, public lastName?: string, public review?: string) {}
}
