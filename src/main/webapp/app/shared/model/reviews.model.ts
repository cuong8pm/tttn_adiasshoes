import { IOrderProduct } from 'app/shared/model/order-product.model';

export interface IReviews {
  id?: number;
  rating?: number;
  decription?: string;
  orderProduct?: IOrderProduct;
}

export class Reviews implements IReviews {
  constructor(public id?: number, public rating?: number, public decription?: string, public orderProduct?: IOrderProduct) {}
}
