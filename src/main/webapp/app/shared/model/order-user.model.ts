import { IOrders } from './orders.model';
import { ITrackOrder } from './track-order.model';
import { IOrderProduct } from './order-product.model';
import { IListCart } from './list-cart.model';

export interface IOrderUser {
  orders?: IOrders;
  trackOrder?: ITrackOrder;
  listCarts?: IListCart[];
}

export class OrderUser implements IOrderUser {
  constructor(public orders?: IOrders, public trackOrder?: ITrackOrder, public listCart?: IListCart[]) {}
}
