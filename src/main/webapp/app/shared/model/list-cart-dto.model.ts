import { IUser } from 'app/core/user/user.model';
import { IProduct } from 'app/shared/model/product.model';
import { ISize } from 'app/shared/model/size.model';

export interface IListCartDTO {
  id?: number;
  amount?: number;
  description?: string;
  user?: IUser;
  product?: IProduct;
  size?: ISize;
  totalPrice?: number;
}

export class ListCartDTO implements IListCartDTO {
  constructor(
    public id?: number,
    public amount?: number,
    public description?: string,
    public user?: IUser,
    public product?: IProduct,
    public size?: ISize,
    public totalPrice?: number
  ) {}
}
