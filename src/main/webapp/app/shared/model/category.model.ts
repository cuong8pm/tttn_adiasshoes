export interface ICategory {
  id?: number;
  name?: string;
  decription?: string;
}

export class Category implements ICategory {
  constructor(public id?: number, public name?: string, public decription?: string) {}
}
