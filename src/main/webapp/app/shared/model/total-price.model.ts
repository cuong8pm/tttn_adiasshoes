export interface ITotalPrice {
  totalProductPrice?: number;
  totalOrderPrice?: number;
}

export class totalPrice implements ITotalPrice {
  constructor(public totalProductPrice?: number, public totalOrderPrice?: number) {}
}
