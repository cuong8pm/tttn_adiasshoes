export interface ITrackOrderUser {
  id?: number;
}

export class TrackOrderUser implements ITrackOrderUser {
  constructor(public id?: number) {}
}
