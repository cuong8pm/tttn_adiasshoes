import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';

export interface IOrders {
  id?: number;
  confirm?: boolean;
  descripstion?: string;
  totalOrderPrice?: number;
  orderDate?: Moment;
  deliveryDateFrom?: Moment;
  deliveryDateTo?: Moment;
  user?: IUser;
}

export class Orders implements IOrders {
  constructor(
    public id?: number,
    public confirm?: boolean,
    public descripstion?: string,
    public totalOrderPrice?: number,
    public orderDate?: Moment,
    public deliveryDateFrom?: Moment,
    public deliveryDateTo?: Moment,
    public user?: IUser
  ) {
    this.confirm = this.confirm || false;
  }
}
