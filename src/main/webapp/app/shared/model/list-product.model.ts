export interface IListProduct {
  id?: number;
}

export class ListProduct implements IListProduct {
  constructor(public id?: number) {}
}
