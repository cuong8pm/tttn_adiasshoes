export interface ISize {
  id?: number;
  name?: string;
  descripstion?: string;
}

export class Size implements ISize {
  constructor(public id?: number, public name?: string, public descripstion?: string) {}
}
