import { IOrders } from 'app/shared/model/orders.model';

export interface ITrackOrder {
  id?: number;
  orderConfirm?: boolean;
  waittingProduct?: boolean;
  shipping?: boolean;
  delivered?: boolean;
  status?: boolean;
  orders?: IOrders;
}

export class TrackOrder implements ITrackOrder {
  constructor(
    public id?: number,
    public orderConfirm?: boolean,
    public waittingProduct?: boolean,
    public shipping?: boolean,
    public delivered?: boolean,
    public status?: boolean,
    public orders?: IOrders
  ) {
    this.orderConfirm = this.orderConfirm || false;
    this.waittingProduct = this.waittingProduct || false;
    this.shipping = this.shipping || false;
    this.delivered = this.delivered || false;
    this.status = this.status || false;
  }
}
