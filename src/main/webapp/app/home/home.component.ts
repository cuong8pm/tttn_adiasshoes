import { Component, OnInit, OnDestroy } from '@angular/core';
import {Observable, Subscription} from 'rxjs';

import { LoginModalService } from 'app/core/login/login-modal.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import {IProduct} from "app/shared/model/product.model";
import {ProductService} from "app/entities/product/product.service";
import {JhiDataUtils} from "ng-jhipster";
import {HttpHeaders, HttpResponse} from "@angular/common/http";

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  account: Account | null = null;
  authSubscription?: Subscription;
  productsHot?: IProduct[];
  productsLike?: IProduct[];

  constructor(private accountService: AccountService, private loginModalService: LoginModalService,protected productService: ProductService,protected dataUtils: JhiDataUtils,) {}

  ngOnInit(): void {
    this.authSubscription = this.accountService.getAuthenticationState().subscribe(account => (this.account = account));
    this.productService
      .queryProductHot({
      })
      .subscribe(
        (res: HttpResponse<IProduct[]>) => this.onSuccessProductHot(res.body, res.headers),
      );
    this.productService
      .queryProductLike({
      })
      .subscribe(
        (res: HttpResponse<IProduct[]>) => this.onSuccessProductLike(res.body, res.headers),
      );

  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  ngOnDestroy(): void {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }
  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType = '', base64String: string): void {
    return this.dataUtils.openFile(contentType, base64String);
  }
  protected onSuccessProductHot(data: IProduct[] | null, headers: HttpHeaders): void {
    this.productsHot = data || [];
  }
  protected onSuccessProductLike(data: IProduct[] | null, headers: HttpHeaders): void {
    this.productsLike = data || [];
  }
}
