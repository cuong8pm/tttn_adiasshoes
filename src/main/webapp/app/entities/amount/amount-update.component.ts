import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAmount, Amount } from 'app/shared/model/amount.model';
import { AmountService } from './amount.service';
import { IProduct } from 'app/shared/model/product.model';
import { ProductService } from 'app/entities/product/product.service';
import { ISize } from 'app/shared/model/size.model';
import { SizeService } from 'app/entities/size/size.service';

type SelectableEntity = IProduct | ISize;

@Component({
  selector: 'jhi-amount-update',
  templateUrl: './amount-update.component.html',
})
export class AmountUpdateComponent implements OnInit {
  isSaving = false;
  products: IProduct[] = [];
  sizes: ISize[] = [];

  editForm = this.fb.group({
    id: [],
    total: [],
    inventory: [],
    description: [],
    soldProduct: [],
    product: [],
    size: [],
  });

  constructor(
    protected amountService: AmountService,
    protected productService: ProductService,
    protected sizeService: SizeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ amount }) => {
      this.updateForm(amount);

      this.productService.query().subscribe((res: HttpResponse<IProduct[]>) => (this.products = res.body || []));

      this.sizeService.query().subscribe((res: HttpResponse<ISize[]>) => (this.sizes = res.body || []));
    });
  }

  updateForm(amount: IAmount): void {
    this.editForm.patchValue({
      id: amount.id,
      total: amount.total,
      inventory: amount.inventory,
      description: amount.description,
      soldProduct: amount.soldProduct,
      product: amount.product,
      size: amount.size,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const amount = this.createFromForm();
    if (amount.id !== undefined) {
      this.subscribeToSaveResponse(this.amountService.update(amount));
    } else {
      this.subscribeToSaveResponse(this.amountService.create(amount));
    }
  }

  private createFromForm(): IAmount {
    return {
      ...new Amount(),
      id: this.editForm.get(['id'])!.value,
      total: this.editForm.get(['total'])!.value,
      inventory: this.editForm.get(['inventory'])!.value,
      description: this.editForm.get(['description'])!.value,
      soldProduct: this.editForm.get(['soldProduct'])!.value,
      product: this.editForm.get(['product'])!.value,
      size: this.editForm.get(['size'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAmount>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
