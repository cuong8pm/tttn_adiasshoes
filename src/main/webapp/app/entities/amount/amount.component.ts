import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, ParamMap, Router, Data } from '@angular/router';
import { Subscription, combineLatest } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAmount } from 'app/shared/model/amount.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { AmountService } from './amount.service';
import { AmountDeleteDialogComponent } from './amount-delete-dialog.component';
import { ProductImportHistoryService } from '../product-import-history/product-import-history.service';
import { IProductImportHistory } from '../../shared/model/product-import-history.model';
import { IReviews } from '../../shared/model/reviews.model';
import { ProductService } from '../product/product.service';
import { IProduct } from '../../shared/model/product.model';
import { ListProductService } from '../list-product/list-product.service';

@Component({
  selector: 'jhi-amount',
  templateUrl: './amount.component.html',
  styleUrls: ['amount.scss'],
})
export class AmountComponent implements OnInit, OnDestroy {
  amounts?: IAmount[] | null = null;
  products: IProduct[] | null = null;
  productImportHistories: IProductImportHistory[] | null = null;
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = 7;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  productId? = 1;
  name?: string;
  tabIndex?: number = 1;

  constructor(
    protected amountService: AmountService,
    protected activatedRoute: ActivatedRoute,
    protected productImportHistoryService: ProductImportHistoryService,
    protected productService: ProductService,
    protected router: Router,
    protected listProductService: ListProductService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadPage(productId?: number, page?: number, dontNavigate?: boolean): void {
    const pageToLoad: number = page || this.page || 1;

    this.amountService
      .query({
        productId: productId,
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<IAmount[]>) => (this.amounts = res.body));
  }

  ngOnInit(): void {
    this.productService.query({}).subscribe((res: HttpResponse<IProduct[]>) => (this.products = res.body));
    this.amountService.currentData.subscribe(data => (this.productId = data));
    this.amountService.query({ productId: this.productId }).subscribe((res: HttpResponse<IAmount[]>) => (this.amounts = res.body));
    this.registerChangeInAmounts();
  }

  protected handleNavigation(productId?: number): void {
    combineLatest(this.activatedRoute.data, this.activatedRoute.queryParamMap, (data: Data, params: ParamMap) => {
      const page = params.get('page');
      const pageNumber = page !== null ? +page : 1;
      const sort = (params.get('sort') ?? data['defaultSort']).split(',');
      const predicate = sort[0];
      const ascending = sort[1] === 'asc';
      if (pageNumber !== this.page || predicate !== this.predicate || ascending !== this.ascending) {
        this.predicate = predicate;
        this.ascending = ascending;
        this.productId = Number(productId);
        this.loadPage(this.productId, pageNumber, true);
      }
    }).subscribe();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAmount): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAmounts(): void {
    this.eventSubscriber = this.eventManager.subscribe('amountListModification', () => this.loadPage());
  }

  delete(amount: IAmount): void {
    const modalRef = this.modalService.open(AmountDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.amount = amount;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IAmount[] | null, headers: HttpHeaders, page: number, navigate: boolean, productId?: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    if (navigate) {
      this.router.navigate(['/amount'], {
        queryParams: {
          productId: productId,
          page: this.page,
          size: this.itemsPerPage,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
        },
      });
    }
    this.amounts = data || [];
    this.ngbPaginationPage = this.page;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page ?? 1;
  }

  loadproduct(id: number | undefined): void {
    this.productId = id;
    this.tabIndex = id;
    this.amountService.changProducId(id);
    this.amountService
      .query({
        productId: id,
      })
      .subscribe((res: HttpResponse<IAmount[]>) => (this.amounts = res.body));
  }

  loadHistory(id: number | undefined): void {
    this.productImportHistoryService
      .query({ amountId: id })
      .subscribe((res: HttpResponse<IProductImportHistory[]>) => (this.productImportHistories = res.body));
  }
  changeLoadProduct(newName: any): void {
    this.name = newName;
    this.listProductService.querySearchTree({ name: this.name }).subscribe((res: HttpResponse<IProduct[]>) => (this.products = res.body));
  }
}
