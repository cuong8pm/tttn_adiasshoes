import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IOrderTracks } from 'app/shared/model/order-tracks.model';

type EntityResponseType = HttpResponse<IOrderTracks>;
type EntityArrayResponseType = HttpResponse<IOrderTracks[]>;

@Injectable({ providedIn: 'root' })
export class OrderTracksService {
  public resourceUrl = SERVER_API_URL + 'api/order-tracks';

  constructor(protected http: HttpClient) {}

  create(orderTracks: IOrderTracks): Observable<EntityResponseType> {
    return this.http.post<IOrderTracks>(this.resourceUrl, orderTracks, { observe: 'response' });
  }

  update(orderTracks: IOrderTracks): Observable<EntityResponseType> {
    return this.http.put<IOrderTracks>(this.resourceUrl, orderTracks, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IOrderTracks>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IOrderTracks[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
