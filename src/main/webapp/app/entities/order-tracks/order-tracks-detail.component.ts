import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOrderTracks } from 'app/shared/model/order-tracks.model';

@Component({
  selector: 'jhi-order-tracks-detail',
  templateUrl: './order-tracks-detail.component.html',
})
export class OrderTracksDetailComponent implements OnInit {
  orderTracks: IOrderTracks | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ orderTracks }) => (this.orderTracks = orderTracks));
  }

  previousState(): void {
    window.history.back();
  }
}
