import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IOrderTracks, OrderTracks } from 'app/shared/model/order-tracks.model';
import { OrderTracksService } from './order-tracks.service';

@Component({
  selector: 'jhi-order-tracks-update',
  templateUrl: './order-tracks-update.component.html',
})
export class OrderTracksUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
  });

  constructor(protected orderTracksService: OrderTracksService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ orderTracks }) => {
      this.updateForm(orderTracks);
    });
  }

  updateForm(orderTracks: IOrderTracks): void {
    this.editForm.patchValue({
      id: orderTracks.id,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const orderTracks = this.createFromForm();
    if (orderTracks.id !== undefined) {
      this.subscribeToSaveResponse(this.orderTracksService.update(orderTracks));
    } else {
      this.subscribeToSaveResponse(this.orderTracksService.create(orderTracks));
    }
  }

  private createFromForm(): IOrderTracks {
    return {
      ...new OrderTracks(),
      id: this.editForm.get(['id'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrderTracks>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
