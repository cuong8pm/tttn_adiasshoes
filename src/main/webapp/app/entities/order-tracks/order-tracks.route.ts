import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IOrderTracks, OrderTracks } from 'app/shared/model/order-tracks.model';
import { OrderTracksService } from './order-tracks.service';
import { OrderTracksComponent } from './order-tracks.component';
import { OrderTracksDetailComponent } from './order-tracks-detail.component';
import { OrderTracksUpdateComponent } from './order-tracks-update.component';

@Injectable({ providedIn: 'root' })
export class OrderTracksResolve implements Resolve<IOrderTracks> {
  constructor(private service: OrderTracksService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOrderTracks> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((orderTracks: HttpResponse<OrderTracks>) => {
          if (orderTracks.body) {
            return of(orderTracks.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new OrderTracks());
  }
}

export const orderTracksRoute: Routes = [
  {
    path: '',
    component: OrderTracksComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'hauiAdidasShoesApp.orderTracks.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: OrderTracksDetailComponent,
    resolve: {
      orderTracks: OrderTracksResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.orderTracks.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: OrderTracksUpdateComponent,
    resolve: {
      orderTracks: OrderTracksResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.orderTracks.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: OrderTracksUpdateComponent,
    resolve: {
      orderTracks: OrderTracksResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.orderTracks.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
