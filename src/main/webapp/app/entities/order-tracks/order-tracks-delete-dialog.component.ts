import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOrderTracks } from 'app/shared/model/order-tracks.model';
import { OrderTracksService } from './order-tracks.service';

@Component({
  templateUrl: './order-tracks-delete-dialog.component.html',
})
export class OrderTracksDeleteDialogComponent {
  orderTracks?: IOrderTracks;

  constructor(
    protected orderTracksService: OrderTracksService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.orderTracksService.delete(id).subscribe(() => {
      this.eventManager.broadcast('orderTracksListModification');
      this.activeModal.close();
    });
  }
}
