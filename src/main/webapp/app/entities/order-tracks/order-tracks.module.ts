import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HauiAdidasShoesSharedModule } from 'app/shared/shared.module';
import { OrderTracksComponent } from './order-tracks.component';
import { OrderTracksDetailComponent } from './order-tracks-detail.component';
import { OrderTracksUpdateComponent } from './order-tracks-update.component';
import { OrderTracksDeleteDialogComponent } from './order-tracks-delete-dialog.component';
import { orderTracksRoute } from './order-tracks.route';

@NgModule({
  imports: [HauiAdidasShoesSharedModule, RouterModule.forChild(orderTracksRoute)],
  declarations: [OrderTracksComponent, OrderTracksDetailComponent, OrderTracksUpdateComponent, OrderTracksDeleteDialogComponent],
  entryComponents: [OrderTracksDeleteDialogComponent],
})
export class HauiAdidasShoesOrderTracksModule {}
