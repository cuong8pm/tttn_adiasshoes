import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, ParamMap, Router, Data } from '@angular/router';
import { Subscription, combineLatest } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IListProduct } from 'app/shared/model/list-product.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ListProductService } from './list-product.service';
import { ListProductDeleteDialogComponent } from './list-product-delete-dialog.component';
import { ProductService } from '../product/product.service';
import { IProduct } from '../../shared/model/product.model';
import { ICategory } from '../../shared/model/category.model';
import { CategoryService } from '../category/category.service';

@Component({
  selector: 'jhi-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['list-product.scss'],
})
export class ListProductComponent implements OnInit, OnDestroy {
  listProducts?: IProduct[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  type!: number | undefined;
  name!: string;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  nameProduct!: string;
  typeProduct!: string;
  toPrice!: string;
  fromPrice!: string;
  isCheckedTrue = true;
  isCheckedFalse = false;
  categories?: ICategory[];

  constructor(
    protected listProductService: ListProductService,
    protected productService: ProductService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected categoryService: CategoryService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadPage(page?: number, dontNavigate?: boolean): void {
    const pageToLoad: number = page || this.page || 1;

    this.listProductService
      .query({
        type: this.type,
        name: this.name,
        toPrice: this.toPrice,
        formPrice: this.fromPrice,
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe(
        (res: HttpResponse<IListProduct[]>) => this.onSuccess(res.body, res.headers, this.type, this.name, pageToLoad, !dontNavigate),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.categoryService.query().subscribe((res: HttpResponse<ICategory[]>) => (this.categories = res.body || []));
    this.handleNavigation();
    this.registerChangeInListProducts();
  }

  protected handleNavigation(): void {
    combineLatest(this.activatedRoute.data, this.activatedRoute.queryParamMap, (data: Data, params: ParamMap) => {
      const page = params.get('page');
      const type = params.get('type');
      const name = params.get('name');
      const pageNumber = page !== null ? +page : 1;
      const sort = (params.get('sort') ?? data['defaultSort']).split(',');
      const predicate = sort[0];
      const ascending = sort[1] === 'asc';
      this.predicate = 'price';
      this.ascending = ascending;
      this.type = Number(type);
      this.name = String(name);
      this.loadPage(pageNumber, true);
    }).subscribe();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IListProduct): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInListProducts(): void {
    this.eventSubscriber = this.eventManager.subscribe('listProductListModification', () => this.loadPage());
  }

  delete(listProduct: IListProduct): void {
    const modalRef = this.modalService.open(ListProductDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.listProduct = listProduct;
  }

  sort(): string[] {
    if (this.isCheckedFalse === true) {
      const result = [this.predicate + ',' + 'desc'];
      return result;
    } else {
      const result = [this.predicate + ',' + 'asc'];
      return result;
    }
  }

  protected onSuccess(
    data: IProduct[] | null,
    headers: HttpHeaders,
    type: number | undefined,
    name: string,
    page: number,
    navigate: boolean
  ): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.type = type;
    this.name = name;
    if (navigate) {
      this.router.navigate(['/list-product'], {
        queryParams: {
          type: this.type,
          name: this.name,
          toPrice: this.toPrice,
          fromPrice: this.fromPrice,
          page: this.page,
          size: this.itemsPerPage,
          sort: this.predicate + ',' + this.ascending,
        },
      });
    }
    this.listProducts = data || [];
    this.ngbPaginationPage = this.page;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page ?? 1;
  }

  public checkPrice(): void {
    if (this.isCheckedTrue === true) {
      this.isCheckedTrue = false;
      this.isCheckedFalse = true;
    } else {
      this.isCheckedTrue = true;
      this.isCheckedFalse = false;
    }
  }

  public searchFrom(): void {
    const type = this.typeProduct;
    const name = this.nameProduct;
    const toPrice = this.toPrice;
    const fromPrice = this.fromPrice;

    this.router.navigate(['/list-product'], { queryParams: { type: type, name: name, toPrice: toPrice, fromPrice: fromPrice } });
  }

  selectChangeHandler(event: any): void {
    this.typeProduct = event.target.value;
  }
  replace(price: number | undefined): String {
    const aaa = new String(price);
    return aaa.replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }
}
