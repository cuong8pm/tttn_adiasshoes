import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HauiAdidasShoesSharedModule } from 'app/shared/shared.module';
import { ListProductComponent } from './list-product.component';
import { ListProductDetailComponent } from './list-product-detail.component';
import { ListProductUpdateComponent } from './list-product-update.component';
import { ListProductDeleteDialogComponent } from './list-product-delete-dialog.component';
import { listProductRoute } from './list-product.route';

@NgModule({
  imports: [HauiAdidasShoesSharedModule, RouterModule.forChild(listProductRoute)],
  declarations: [ListProductComponent, ListProductDetailComponent, ListProductUpdateComponent, ListProductDeleteDialogComponent],
  entryComponents: [ListProductDeleteDialogComponent],
})
export class HauiAdidasShoesListProductModule {}
