import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IListProduct, ListProduct } from 'app/shared/model/list-product.model';
import { ListProductService } from './list-product.service';
import { ListProductComponent } from './list-product.component';
import { ListProductDetailComponent } from './list-product-detail.component';
import { ListProductUpdateComponent } from './list-product-update.component';

@Injectable({ providedIn: 'root' })
export class ListProductResolve implements Resolve<IListProduct> {
  constructor(private service: ListProductService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IListProduct> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((listProduct: HttpResponse<ListProduct>) => {
          if (listProduct.body) {
            return of(listProduct.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ListProduct());
  }
}

export const listProductRoute: Routes = [
  {
    path: '',
    component: ListProductComponent,
    data: {
      defaultSort: 'id,asc',
      pageTitle: 'hauiAdidasShoesApp.listProduct.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ListProductDetailComponent,
    resolve: {
      listProduct: ListProductResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.listProduct.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ListProductUpdateComponent,
    resolve: {
      listProduct: ListProductResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.listProduct.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ListProductUpdateComponent,
    resolve: {
      listProduct: ListProductResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.listProduct.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
