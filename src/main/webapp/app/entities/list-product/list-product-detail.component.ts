import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IListProduct } from 'app/shared/model/list-product.model';

@Component({
  selector: 'jhi-list-product-detail',
  templateUrl: './list-product-detail.component.html',
})
export class ListProductDetailComponent implements OnInit {
  listProduct: IListProduct | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ listProduct }) => (this.listProduct = listProduct));
  }

  previousState(): void {
    window.history.back();
  }
}
