import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IListProduct, ListProduct } from 'app/shared/model/list-product.model';
import { ListProductService } from './list-product.service';

@Component({
  selector: 'jhi-list-product-update',
  templateUrl: './list-product-update.component.html',
})
export class ListProductUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
  });

  constructor(protected listProductService: ListProductService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ listProduct }) => {
      this.updateForm(listProduct);
    });
  }

  updateForm(listProduct: IListProduct): void {
    this.editForm.patchValue({
      id: listProduct.id,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const listProduct = this.createFromForm();
    if (listProduct.id !== undefined) {
      this.subscribeToSaveResponse(this.listProductService.update(listProduct));
    } else {
      this.subscribeToSaveResponse(this.listProductService.create(listProduct));
    }
  }

  private createFromForm(): IListProduct {
    return {
      ...new ListProduct(),
      id: this.editForm.get(['id'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IListProduct>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
