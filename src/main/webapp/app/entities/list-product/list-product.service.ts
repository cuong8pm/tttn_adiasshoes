import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IListProduct } from 'app/shared/model/list-product.model';

type EntityResponseType = HttpResponse<IListProduct>;
type EntityArrayResponseType = HttpResponse<IListProduct[]>;

@Injectable({ providedIn: 'root' })
export class ListProductService {
  public resourceUrl = SERVER_API_URL + 'api/list-products';

  constructor(protected http: HttpClient) {}

  create(listProduct: IListProduct): Observable<EntityResponseType> {
    return this.http.post<IListProduct>(this.resourceUrl, listProduct, { observe: 'response' });
  }

  update(listProduct: IListProduct): Observable<EntityResponseType> {
    return this.http.put<IListProduct>(this.resourceUrl, listProduct, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IListProduct>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IListProduct[]>(this.resourceUrl, { params: options, observe: 'response' });
  }
  querySearchTree(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IListProduct[]>(`${this.resourceUrl}/search-tree`, { params: options, observe: 'response' });
  }
  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
