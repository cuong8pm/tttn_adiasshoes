import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IListProduct } from 'app/shared/model/list-product.model';
import { ListProductService } from './list-product.service';

@Component({
  templateUrl: './list-product-delete-dialog.component.html',
})
export class ListProductDeleteDialogComponent {
  listProduct?: IListProduct;

  constructor(
    protected listProductService: ListProductService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.listProductService.delete(id).subscribe(() => {
      this.eventManager.broadcast('listProductListModification');
      this.activeModal.close();
    });
  }
}
