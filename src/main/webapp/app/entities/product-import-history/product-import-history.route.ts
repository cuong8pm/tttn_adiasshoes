import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IProductImportHistory, ProductImportHistory } from 'app/shared/model/product-import-history.model';
import { ProductImportHistoryService } from './product-import-history.service';
import { ProductImportHistoryComponent } from './product-import-history.component';
import { ProductImportHistoryDetailComponent } from './product-import-history-detail.component';
import { ProductImportHistoryUpdateComponent } from './product-import-history-update.component';

@Injectable({ providedIn: 'root' })
export class ProductImportHistoryResolve implements Resolve<IProductImportHistory> {
  constructor(private service: ProductImportHistoryService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IProductImportHistory> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((productImportHistory: HttpResponse<ProductImportHistory>) => {
          if (productImportHistory.body) {
            return of(productImportHistory.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ProductImportHistory());
  }
}

export const productImportHistoryRoute: Routes = [
  {
    path: '',
    component: ProductImportHistoryComponent,
    data: {
      defaultSort: 'id,asc',
      pageTitle: 'hauiAdidasShoesApp.productImportHistory.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ProductImportHistoryDetailComponent,
    resolve: {
      productImportHistory: ProductImportHistoryResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.productImportHistory.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ProductImportHistoryUpdateComponent,
    resolve: {
      productImportHistory: ProductImportHistoryResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.productImportHistory.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ProductImportHistoryUpdateComponent,
    resolve: {
      productImportHistory: ProductImportHistoryResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.productImportHistory.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
