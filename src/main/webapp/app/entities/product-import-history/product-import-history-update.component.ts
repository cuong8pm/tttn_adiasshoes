import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IProductImportHistory, ProductImportHistory } from 'app/shared/model/product-import-history.model';
import { ProductImportHistoryService } from './product-import-history.service';
import { IAmount } from 'app/shared/model/amount.model';
import { AmountService } from 'app/entities/amount/amount.service';

@Component({
  selector: 'jhi-product-import-history-update',
  templateUrl: './product-import-history-update.component.html',
})
export class ProductImportHistoryUpdateComponent implements OnInit {
  isSaving = false;
  amounts: IAmount[] = [];

  editForm = this.fb.group({
    id: [],
    description: [],
    amount: [],
  });

  constructor(
    protected productImportHistoryService: ProductImportHistoryService,
    protected amountService: AmountService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ productImportHistory }) => {
      this.updateForm(productImportHistory);

      this.amountService.query().subscribe((res: HttpResponse<IAmount[]>) => (this.amounts = res.body || []));
    });
  }

  updateForm(productImportHistory: IProductImportHistory): void {
    this.editForm.patchValue({
      id: productImportHistory.id,
      description: productImportHistory.description,
      amount: productImportHistory.amount,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const productImportHistory = this.createFromForm();
    if (productImportHistory.id !== undefined) {
      this.subscribeToSaveResponse(this.productImportHistoryService.update(productImportHistory));
    } else {
      this.subscribeToSaveResponse(this.productImportHistoryService.create(productImportHistory));
    }
  }

  private createFromForm(): IProductImportHistory {
    return {
      ...new ProductImportHistory(),
      id: this.editForm.get(['id'])!.value,
      description: this.editForm.get(['description'])!.value,
      amount: this.editForm.get(['amount'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProductImportHistory>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IAmount): any {
    return item.id;
  }
}
