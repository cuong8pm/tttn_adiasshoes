import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, ParamMap, Router, Data } from '@angular/router';
import { Subscription, combineLatest } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IProductImportHistory } from 'app/shared/model/product-import-history.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ProductImportHistoryService } from './product-import-history.service';
import { ProductImportHistoryDeleteDialogComponent } from './product-import-history-delete-dialog.component';

import { IPay } from 'app/shared/model/pay.model';

import { ListCartComponent } from '../list-cart/list-cart.component';
import { ListCartService } from '../list-cart/list-cart.service';
import { DetailProductService } from '../detail-product/detail-product.service';
import { Account } from '../../core/user/account.model';
import { AccountService } from '../../core/auth/account.service';
import { IListCart } from '../../shared/model/list-cart.model';
import { ISize } from '../../shared/model/size.model';
import { IListCartDTO } from '../../shared/model/list-cart-dto.model';
import { ITotalPrice } from '../../shared/model/total-price.model';
import { IOrders } from '../../shared/model/orders.model';
import { PayService } from '../pay/pay.service';
import { PayDeleteDialogComponent } from '../pay/pay-delete-dialog.component';
import { IUser } from '../../core/user/user.model';

@Component({
  selector: 'jhi-product-import-history',
  templateUrl: './product-import-history.component.html',
  styleUrls: ['pay1.scss'],
})
export class ProductImportHistoryComponent implements OnInit, OnDestroy {
  productImportHistories?: IProductImportHistory[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  pays?: IPay[];
  cart!: any;
  username?: string;
  aaccount!: Account;
  listCarts!: IListCartDTO[];
  totalPrice: ITotalPrice | null = null;
  messageOrder!: string;
  user?: IUser | null = null;
  name?: string = '';
  phone?: string = '';
  address?: string = '';

  constructor(
    protected productImportHistoryService: ProductImportHistoryService,
    protected activatedRoute: ActivatedRoute,
    protected detailProductService: DetailProductService,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected listCartService: ListCartService,
    protected payService: PayService,
    private accountService: AccountService
  ) {}

  ngOnInit(): void {
    this.detailProductService.currentData.subscribe(data => (this.cart = data));
    this.productImportHistoryService.currentData.subscribe(data => (this.username = data));
    this.payService.getUser({ username: this.username }).subscribe((res: HttpResponse<IUser | undefined>) => (this.user = res.body));
    this.payService
      .getListProduct(this.cart)
      .subscribe(
        (res: HttpResponse<IListCartDTO[]>) => (
          (this.listCarts = res.body || []),
          this.payService.getTotalPrice(this.listCarts).subscribe((res: HttpResponse<ITotalPrice>) => (this.totalPrice = res.body))
        )
      );
    // }
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IPay): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  delete(pay: IPay): void {
    const modalRef = this.modalService.open(PayDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.pay = pay;
  }

  onKeyName(event: any): void {
    this.name = event.target.value;
  }
  onKeyPhone(event: any): void {
    this.phone = event.target.value;
  }
  onKeyAddress(event: any): void {
    this.address = event.target.value;
  }

  createOrder(): void {
    let check = false;
    if (this.user) {
      this.payService
        .createOrderUserNow(this.listCarts, { username: this.username })
        .subscribe((res: HttpResponse<boolean>) => (check = res.body || false));
    } else {
      this.payService
        .createOrderNow(this.listCarts, { name: this.name, phone: this.phone, address: this.address })
        .subscribe((res: HttpResponse<boolean>) => (check = res.body || false));
    }
  }
  replace(price: number | undefined): String {
    const aaa = new String(price);
    return aaa.replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }
}
