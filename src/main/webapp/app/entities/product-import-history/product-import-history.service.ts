import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IProductImportHistory } from 'app/shared/model/product-import-history.model';

type EntityResponseType = HttpResponse<IProductImportHistory>;
type EntityArrayResponseType = HttpResponse<IProductImportHistory[]>;

@Injectable({ providedIn: 'root' })
export class ProductImportHistoryService {
  public resourceUrl = SERVER_API_URL + 'api/product-import-histories';

  private dataUsername = new BehaviorSubject<any>(undefined);
  currentData = this.dataUsername.asObservable();

  constructor(protected http: HttpClient) {}

  changUsername(data: any): void {
    this.dataUsername.next(data);
  }

  create(productImportHistory: IProductImportHistory): Observable<EntityResponseType> {
    return this.http.post<IProductImportHistory>(this.resourceUrl, productImportHistory, { observe: 'response' });
  }

  update(productImportHistory: IProductImportHistory): Observable<EntityResponseType> {
    return this.http.put<IProductImportHistory>(this.resourceUrl, productImportHistory, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IProductImportHistory>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IProductImportHistory[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
