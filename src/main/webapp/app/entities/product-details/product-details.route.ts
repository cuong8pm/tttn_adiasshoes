import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IProductDetails, ProductDetails } from 'app/shared/model/product-details.model';
import { ProductDetailsService } from './product-details.service';
import { ProductDetailsComponent } from './product-details.component';
import { ProductDetailsDetailComponent } from './product-details-detail.component';
import { ProductDetailsUpdateComponent } from './product-details-update.component';

@Injectable({ providedIn: 'root' })
export class ProductDetailsResolve implements Resolve<IProductDetails> {
  constructor(private service: ProductDetailsService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IProductDetails> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((productDetails: HttpResponse<ProductDetails>) => {
          if (productDetails.body) {
            return of(productDetails.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ProductDetails());
  }
}

export const productDetailsRoute: Routes = [
  {
    path: '',
    component: ProductDetailsComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'hauiAdidasShoesApp.productDetails.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ProductDetailsDetailComponent,
    resolve: {
      productDetails: ProductDetailsResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.productDetails.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ProductDetailsUpdateComponent,
    resolve: {
      productDetails: ProductDetailsResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.productDetails.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ProductDetailsUpdateComponent,
    resolve: {
      productDetails: ProductDetailsResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.productDetails.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
