import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HauiAdidasShoesSharedModule } from 'app/shared/shared.module';
import { ProductDetailsComponent } from './product-details.component';
import { ProductDetailsDetailComponent } from './product-details-detail.component';
import { ProductDetailsUpdateComponent } from './product-details-update.component';
import { ProductDetailsDeleteDialogComponent } from './product-details-delete-dialog.component';
import { productDetailsRoute } from './product-details.route';

@NgModule({
  imports: [HauiAdidasShoesSharedModule, RouterModule.forChild(productDetailsRoute)],
  declarations: [
    ProductDetailsComponent,
    ProductDetailsDetailComponent,
    ProductDetailsUpdateComponent,
    ProductDetailsDeleteDialogComponent,
  ],
  entryComponents: [ProductDetailsDeleteDialogComponent],
})
export class HauiAdidasShoesProductDetailsModule {}
