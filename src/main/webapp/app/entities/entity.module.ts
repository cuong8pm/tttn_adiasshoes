import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'category',
        loadChildren: () => import('./category/category.module').then(m => m.HauiAdidasShoesCategoryModule),
      },
      {
        path: 'product',
        loadChildren: () => import('./product/product.module').then(m => m.HauiAdidasShoesProductModule),
      },
      {
        path: 'product-image',
        loadChildren: () => import('./product-image/product-image.module').then(m => m.HauiAdidasShoesProductImageModule),
      },
      {
        path: 'product-details',
        loadChildren: () => import('./product-details/product-details.module').then(m => m.HauiAdidasShoesProductDetailsModule),
      },
      {
        path: 'size',
        loadChildren: () => import('./size/size.module').then(m => m.HauiAdidasShoesSizeModule),
      },
      {
        path: 'amount',
        loadChildren: () => import('./amount/amount.module').then(m => m.HauiAdidasShoesAmountModule),
      },
      {
        path: 'product-import-history',
        loadChildren: () =>
          import('./product-import-history/product-import-history.module').then(m => m.HauiAdidasShoesProductImportHistoryModule),
      },
      {
        path: 'orders',
        loadChildren: () => import('./orders/orders.module').then(m => m.HauiAdidasShoesOrdersModule),
      },
      {
        path: 'track-order',
        loadChildren: () => import('./track-order/track-order.module').then(m => m.HauiAdidasShoesTrackOrderModule),
      },
      {
        path: 'order-product',
        loadChildren: () => import('./order-product/order-product.module').then(m => m.HauiAdidasShoesOrderProductModule),
      },
      {
        path: 'reviews',
        loadChildren: () => import('./reviews/reviews.module').then(m => m.HauiAdidasShoesReviewsModule),
      },
      {
        path: 'list-product',
        loadChildren: () => import('./list-product/list-product.module').then(m => m.HauiAdidasShoesListProductModule),
      },
      {
        path: 'detail-product',
        loadChildren: () => import('./detail-product/detail-product.module').then(m => m.HauiAdidasShoesDetailProductModule),
      },
      {
        path: 'list-cart',
        loadChildren: () => import('./list-cart/list-cart.module').then(m => m.HauiAdidasShoesListCartModule),
      },
      {
        path: 'pay',
        loadChildren: () => import('./pay/pay.module').then(m => m.HauiAdidasShoesPayModule),
      },
      {
        path: 'order-tracks',
        loadChildren: () => import('./order-tracks/order-tracks.module').then(m => m.HauiAdidasShoesOrderTracksModule),
      },
      {
        path: 'track-order-user',
        loadChildren: () => import('./track-order-user/track-order-user.module').then(m => m.HauiAdidasShoesTrackOrderUserModule),
      },
      {
        path: 'order-now',
        loadChildren: () => import('./order-now/order-now.module').then(m => m.HauiAdidasShoesOrderNowModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class HauiAdidasShoesEntityModule {}
