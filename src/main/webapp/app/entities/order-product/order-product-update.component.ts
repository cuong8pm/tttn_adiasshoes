import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IOrderProduct, OrderProduct } from 'app/shared/model/order-product.model';
import { OrderProductService } from './order-product.service';
import { IOrders } from 'app/shared/model/orders.model';
import { OrdersService } from 'app/entities/orders/orders.service';
import { IListCart } from 'app/shared/model/list-cart.model';
import { ListCartService } from 'app/entities/list-cart/list-cart.service';

type SelectableEntity = IOrders | IListCart;

@Component({
  selector: 'jhi-order-product-update',
  templateUrl: './order-product-update.component.html',
})
export class OrderProductUpdateComponent implements OnInit {
  isSaving = false;
  orders: IOrders[] = [];
  listcarts: IListCart[] = [];

  editForm = this.fb.group({
    id: [],
    description: [],
    orders: [],
    listCart: [],
  });

  constructor(
    protected orderProductService: OrderProductService,
    protected ordersService: OrdersService,
    protected listCartService: ListCartService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ orderProduct }) => {
      this.updateForm(orderProduct);

      this.ordersService.query().subscribe((res: HttpResponse<IOrders[]>) => (this.orders = res.body || []));

      this.listCartService.query().subscribe((res: HttpResponse<IListCart[]>) => (this.listcarts = res.body || []));
    });
  }

  updateForm(orderProduct: IOrderProduct): void {
    this.editForm.patchValue({
      id: orderProduct.id,
      description: orderProduct.description,
      orders: orderProduct.orders,
      listCart: orderProduct.listCart,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const orderProduct = this.createFromForm();
    if (orderProduct.id !== undefined) {
      this.subscribeToSaveResponse(this.orderProductService.update(orderProduct));
    } else {
      this.subscribeToSaveResponse(this.orderProductService.create(orderProduct));
    }
  }

  private createFromForm(): IOrderProduct {
    return {
      ...new OrderProduct(),
      id: this.editForm.get(['id'])!.value,
      description: this.editForm.get(['description'])!.value,
      orders: this.editForm.get(['orders'])!.value,
      listCart: this.editForm.get(['listCart'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrderProduct>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
