import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IListCart, ListCart } from 'app/shared/model/list-cart.model';
import { ListCartService } from './list-cart.service';
import { ListCartComponent } from './list-cart.component';
import { ListCartDetailComponent } from './list-cart-detail.component';
import { ListCartUpdateComponent } from './list-cart-update.component';

@Injectable({ providedIn: 'root' })
export class ListCartResolve implements Resolve<IListCart> {
  constructor(private service: ListCartService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IListCart> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((listCart: HttpResponse<ListCart>) => {
          if (listCart.body) {
            return of(listCart.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ListCart());
  }
}

export const listCartRoute: Routes = [
  {
    path: '',
    component: ListCartComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'hauiAdidasShoesApp.listCart.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ListCartDetailComponent,
    resolve: {
      listCart: ListCartResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.listCart.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ListCartUpdateComponent,
    resolve: {
      listCart: ListCartResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.listCart.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ListCartUpdateComponent,
    resolve: {
      listCart: ListCartResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.listCart.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
