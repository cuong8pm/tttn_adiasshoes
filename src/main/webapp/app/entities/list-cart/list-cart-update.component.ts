import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IListCart, ListCart } from 'app/shared/model/list-cart.model';
import { ListCartService } from './list-cart.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IProduct } from 'app/shared/model/product.model';
import { ProductService } from 'app/entities/product/product.service';
import { ISize } from 'app/shared/model/size.model';
import { SizeService } from 'app/entities/size/size.service';

type SelectableEntity = IUser | IProduct | ISize;

@Component({
  selector: 'jhi-list-cart-update',
  templateUrl: './list-cart-update.component.html',
})
export class ListCartUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];
  products: IProduct[] = [];
  sizes: ISize[] = [];

  editForm = this.fb.group({
    id: [],
    amount: [],
    description: [],
    user: [],
    product: [],
    size: [],
  });

  constructor(
    protected listCartService: ListCartService,
    protected userService: UserService,
    protected productService: ProductService,
    protected sizeService: SizeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ listCart }) => {
      this.updateForm(listCart);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));

      this.productService.query().subscribe((res: HttpResponse<IProduct[]>) => (this.products = res.body || []));

      this.sizeService.query().subscribe((res: HttpResponse<ISize[]>) => (this.sizes = res.body || []));
    });
  }

  updateForm(listCart: IListCart): void {
    this.editForm.patchValue({
      id: listCart.id,
      amount: listCart.amount,
      description: listCart.description,
      user: listCart.user,
      product: listCart.product,
      size: listCart.size,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const listCart = this.createFromForm();
    if (listCart.id !== undefined) {
      this.subscribeToSaveResponse(this.listCartService.update(listCart));
    } else {
      this.subscribeToSaveResponse(this.listCartService.create(listCart));
    }
  }

  private createFromForm(): IListCart {
    return {
      ...new ListCart(),
      id: this.editForm.get(['id'])!.value,
      amount: this.editForm.get(['amount'])!.value,
      description: this.editForm.get(['description'])!.value,
      user: this.editForm.get(['user'])!.value,
      product: this.editForm.get(['product'])!.value,
      size: this.editForm.get(['size'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IListCart>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
