import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, ParamMap, Router, Data } from '@angular/router';
import { Subscription, combineLatest } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { IListCart } from 'app/shared/model/list-cart.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ListCartService } from './list-cart.service';
import { ListCartDeleteDialogComponent } from './list-cart-delete-dialog.component';
import { IAmount } from '../../shared/model/amount.model';
import { ISize } from '../../shared/model/size.model';
import { SizeService } from '../size/size.service';
import { DetailProductService } from '../detail-product/detail-product.service';
import { numberOfBytes } from 'ng-jhipster/directive/number-of-bytes';
import { AmountService } from '../amount/amount.service';
import { Account } from '../../core/user/account.model';
import { AccountService } from '../../core/auth/account.service';

@Component({
  selector: 'jhi-list-cart',
  templateUrl: './list-cart.component.html',
  styleUrls: ['list-cart.scss'],
})
export class ListCartComponent implements OnInit, OnDestroy {
  listCarts?: IListCart[];
  listCart?: IListCart | any[];
  amounts?: IAmount[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  username: string | null = null;
  sizetest!: string;
  sizes: ISize[] = [];
  amount!: number;
  form!: FormGroup;
  account!: Account;

  constructor(
    protected listCartService: ListCartService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    private accountService: AccountService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected sizeService: SizeService,
    protected detailProductService: DetailProductService,
    protected amountService: AmountService,
    private fb: FormBuilder
  ) {
    this.form = this.fb.group({
      checkArray: this.fb.array([]),
    });
  }

  loadPage(page?: number, dontNavigate?: boolean): void {
    const pageToLoad: number = page || this.page || 1;

    this.listCartService
      .query({
        username: this.username,
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe(
        (res: HttpResponse<IListCart[]>) => this.onSuccess(res.body, res.headers, pageToLoad, !dontNavigate),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.accountService.identity().subscribe(account => {
      if (account) {
        this.account = account;
        this.username = this.account.login;
      }
    });
    this.handleNavigation();
    this.registerChangeInListCarts();
    this.sizeService.query().subscribe((res: HttpResponse<ISize[]>) => (this.sizes = res.body || []));
    this.amountService.queryAll().subscribe((res: HttpResponse<IAmount[]>) => (this.amounts = res.body || []));
  }

  protected handleNavigation(): void {
    combineLatest(this.activatedRoute.data, this.activatedRoute.queryParamMap, (data: Data, params: ParamMap) => {
      const page = params.get('page');
      const pageNumber = page !== null ? +page : 1;
      const sort = (params.get('sort') ?? data['defaultSort']).split(',');
      const predicate = sort[0];
      const ascending = sort[1] === 'asc';
      if (pageNumber !== this.page || predicate !== this.predicate || ascending !== this.ascending) {
        this.predicate = predicate;
        this.ascending = ascending;
        this.loadPage(pageNumber, true);
      }
    }).subscribe();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IListCart): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInListCarts(): void {
    this.eventSubscriber = this.eventManager.subscribe('listCartListModification', () => this.loadPage());
  }

  delete(listCart: IListCart): void {
    const modalRef = this.modalService.open(ListCartDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.listCart = listCart;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IListCart[] | null, headers: HttpHeaders, page: number, navigate: boolean): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    if (navigate) {
      this.router.navigate(['/list-cart'], {
        queryParams: {
          username: this.username,
          page: this.page,
          size: this.itemsPerPage,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
        },
      });
    }
    this.listCarts = data || [];
    this.ngbPaginationPage = this.page;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page ?? 1;
  }

  selectChangeHandler(event: any): void {
    this.sizetest = event.target.value;
  }
  updateSize(id: number | undefined): void {
    const idc = Number(id);
    this.listCartService.updateSize(idc, this.sizetest).subscribe();
    this.listCartService
      .query({
        username: this.username,
        page: 0,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe(
        (res: HttpResponse<IListCart[]>) => this.onSuccess(res.body, res.headers, 1, true),
        () => this.onError()
      );
  }
  public minus(id: number | undefined): void {
    const idc = Number(id);
    this.listCartService.updateAmount(idc, -1).subscribe();
    this.listCartService
      .query({
        username: this.username,
        page: 0,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe(
        (res: HttpResponse<IListCart[]>) => this.onSuccess(res.body, res.headers, 1, true),
        () => this.onError()
      );
  }
  public plus(id: number | undefined): void {
    const idc = Number(id);
    this.listCartService.updateAmount(idc, 1).subscribe();
    this.listCartService
      .query({
        username: this.username,
        page: 0,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe(
        (res: HttpResponse<IListCart[]>) => this.onSuccess(res.body, res.headers, 1, true),
        () => this.onError()
      );
  }

  onCheckboxChange(e: any): void {
    const checkArray: FormArray = this.form.get('checkArray') as FormArray;

    if (e.target.checked) {
      checkArray.push(new FormControl(e.target.value));
    } else {
      const index = checkArray.controls.findIndex(x => x.value === e.target.value);
      checkArray.removeAt(index);
    }
  }

  submit(): void {
    this.listCartService.listCartProduct(this.form.value);
    this.router.navigate(['/pay'], { queryParams: {} });
  }
  replace(price: number | undefined): String {
    const aaa = new String(price);
    return aaa.replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }
}
