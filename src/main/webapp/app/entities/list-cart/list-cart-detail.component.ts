import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IListCart } from 'app/shared/model/list-cart.model';

@Component({
  selector: 'jhi-list-cart-detail',
  templateUrl: './list-cart-detail.component.html',
})
export class ListCartDetailComponent implements OnInit {
  listCart: IListCart | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ listCart }) => (this.listCart = listCart));
  }

  previousState(): void {
    window.history.back();
  }
}
