import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IListCart } from 'app/shared/model/list-cart.model';
import { Cart, ICart } from 'app/shared/model/cart.model';
import { IProduct } from '../../shared/model/product.model';

type EntityResponseType = HttpResponse<IListCart>;
type EntityArrayResponseType = HttpResponse<IListCart[]>;

@Injectable({ providedIn: 'root' })
export class ListCartService {
  public resourceUrl = SERVER_API_URL + 'api/list-carts';

  private dataListCart = new BehaviorSubject<any>('abc');
  currentData = this.dataListCart.asObservable();

  constructor(protected http: HttpClient) {}

  listCartProduct(data: any): void {
    this.dataListCart.next(data);
  }

  create(listCart: IListCart | undefined): Observable<EntityResponseType> {
    return this.http.post<IListCart>(this.resourceUrl, listCart, { observe: 'response' });
  }
  createCart(cart: Cart | undefined): Observable<EntityResponseType> {
    return this.http.post<IListCart>(`${this.resourceUrl}/create`, cart, { observe: 'response' });
  }
  update(listCart: IListCart): Observable<EntityResponseType> {
    return this.http.put<IListCart>(this.resourceUrl, listCart, { observe: 'response' });
  }
  updateSize(id: number, size: string): Observable<HttpResponse<{}>> {
    return this.http.put(`${this.resourceUrl}/${id}/update-size`, size, { observe: 'response' });
  }
  updateAmount(id: number, count: number): Observable<HttpResponse<{}>> {
    return this.http.put(`${this.resourceUrl}/${id}/update-amount`, count, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IListCart>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IListCart[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getTotalCarts(req?: any): Observable<HttpResponse<Number | undefined>> {
    const options = createRequestOption(req);
    return this.http.get<Number | undefined>(`${this.resourceUrl}/total-carts`, { params: options, observe: 'response' });
  }
}
