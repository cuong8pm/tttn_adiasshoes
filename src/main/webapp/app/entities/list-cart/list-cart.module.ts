import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HauiAdidasShoesSharedModule } from 'app/shared/shared.module';
import { ListCartComponent } from './list-cart.component';
import { ListCartDetailComponent } from './list-cart-detail.component';
import { ListCartUpdateComponent } from './list-cart-update.component';
import { ListCartDeleteDialogComponent } from './list-cart-delete-dialog.component';
import { listCartRoute } from './list-cart.route';

@NgModule({
  imports: [HauiAdidasShoesSharedModule, RouterModule.forChild(listCartRoute)],
  declarations: [ListCartComponent, ListCartDetailComponent, ListCartUpdateComponent, ListCartDeleteDialogComponent],
  entryComponents: [ListCartDeleteDialogComponent],
})
export class HauiAdidasShoesListCartModule {}
