import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IListCart } from 'app/shared/model/list-cart.model';
import { ListCartService } from './list-cart.service';

@Component({
  templateUrl: './list-cart-delete-dialog.component.html',
})
export class ListCartDeleteDialogComponent {
  listCart?: IListCart;

  constructor(protected listCartService: ListCartService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.listCartService.delete(id).subscribe(() => {
      this.eventManager.broadcast('listCartListModification');
      this.activeModal.close();
    });
  }
}
