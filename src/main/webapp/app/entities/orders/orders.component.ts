import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, ParamMap, Router, Data } from '@angular/router';
import { Subscription, combineLatest } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IOrders } from 'app/shared/model/orders.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { OrdersService } from './orders.service';
import { OrdersDeleteDialogComponent } from './orders-delete-dialog.component';
import { TrackOrderComponent } from '../track-order/track-order.component';
import { OrderNow } from '../../shared/model/order-now.model';
import { Moment } from 'moment';

@Component({
  selector: 'jhi-orders',
  templateUrl: './orders.component.html',
})
export class OrdersComponent implements OnInit, OnDestroy {
  orders?: IOrders[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  confirm = '1';
  name: string = '0';
  orderDate?: string = '0';

  constructor(
    protected ordersService: OrdersService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadPage(page?: number, dontNavigate?: boolean): void {
    const pageToLoad: number = page || this.page || 1;

    this.ordersService
      .query({
        name: this.name,
        orderDate: this.orderDate,
        confirm: this.confirm,
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe(
        (res: HttpResponse<IOrders[]>) => this.onSuccess(res.body, res.headers, pageToLoad, !dontNavigate),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.handleNavigation();
    this.registerChangeInOrders();
  }

  protected handleNavigation(): void {
    combineLatest(this.activatedRoute.data, this.activatedRoute.queryParamMap, (data: Data, params: ParamMap) => {
      const page = params.get('page');
      const confirm = params.get('confirm');
      const pageNumber = page !== null ? +page : 1;
      const sort = (params.get('sort') ?? data['defaultSort']).split(',');
      const predicate = sort[0];
      const ascending = sort[1] === 'asc';
      if (confirm !== null) this.confirm = confirm;
      if (pageNumber !== this.page || predicate !== this.predicate || ascending !== this.ascending) {
        this.predicate = predicate;
        this.ascending = ascending;
        this.loadPage(pageNumber, true);
      }
    }).subscribe();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IOrders): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInOrders(): void {
    this.eventSubscriber = this.eventManager.subscribe('ordersListModification', () => this.loadPage());
  }

  delete(orders: IOrders): void {
    const modalRef = this.modalService.open(OrdersDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.orders = orders;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IOrders[] | null, headers: HttpHeaders, page: number, navigate: boolean): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    if (navigate) {
      this.router.navigate(['/orders'], {
        queryParams: {
          name: this.name,
          orderDate: this.orderDate,
          confirm: this.confirm,
          page: this.page,
          size: this.itemsPerPage,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
        },
      });
    }
    this.orders = data || [];
    this.ngbPaginationPage = this.page;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page ?? 1;
  }

  showTrackOrder(id: number | undefined): void {
    this.ordersService.changProducId(id);
    const modalRef = this.modalService.open(TrackOrderComponent, { size: 'xl', backdrop: 'static' });
  }
  setOrderConfirm(orderNow: OrderNow, isActivated: boolean): void {
    this.ordersService.update({ ...orderNow, confirm: isActivated }).subscribe(() => this.loadPage());
  }
  replace(price: number | undefined): String {
    const aaa = new String(price);
    return aaa.replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }

  optionAll(): void {
    this.confirm = '2';
    this.loadPage();
  }

  optionConfirmed(): void {
    this.confirm = '1';
    this.loadPage();
  }

  optionUnconfirmed(): void {
    this.confirm = '0';
    this.loadPage();
  }
  onKeyName(event: any): void {
    this.name = event.target.value;
  }
  onKeyDate(event: any): void {
    this.orderDate = event.target.value;
  }
  search(): void {
    this.loadPage();
  }
}
