import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, ParamMap, Router, Data } from '@angular/router';
import { Subscription, combineLatest } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IReviews } from 'app/shared/model/reviews.model';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ReviewsService } from './reviews.service';
import { ReviewsDeleteDialogComponent } from './reviews-delete-dialog.component';
import { IStatisticalNumber } from '../../shared/model/statisticalNumber.model';
import { IListCartDTO } from '../../shared/model/list-cart-dto.model';
import { ITotalPrice } from '../../shared/model/total-price.model';
import { ISize } from '../../shared/model/size.model';
import { GrossProduct, IGrossProduct } from '../../shared/model/grossProduct.model';
import DataSource, { DataSourceOptions } from 'devextreme/data/data_source';

@Component({
  selector: 'jhi-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['review.scss'],
})
export class ReviewsComponent implements OnInit, OnDestroy {
  reviews?: IReviews[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  statisticalNumberModel: IStatisticalNumber | null = null;
  grossProductData: IGrossProduct[] = [];

  constructor(
    protected reviewsService: ReviewsService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.reviewsService
      .queryStatisticalNumber()
      .subscribe((res: HttpResponse<IStatisticalNumber>) => (this.statisticalNumberModel = res.body));
    this.reviewsService.queryChart().subscribe((res: HttpResponse<IGrossProduct[] | null>) => {
      if (res.body) this.grossProductData = res.body;
    });
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }
  replace(price: number | undefined): String {
    const aaa = new String(price);
    return aaa.replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }

  orderNow(): void {
    this.router.navigate(['/order-now'], { queryParams: { confirm: 0 } });
  }

  orderUser(): void {
    this.router.navigate(['/orders'], { queryParams: { confirm: 0 } });
  }
  onPointClick(e: any): void {
    e.target.select();
  }
}
