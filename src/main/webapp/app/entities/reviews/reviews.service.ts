import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IReviews } from 'app/shared/model/reviews.model';
import { IStatisticalNumber } from '../../shared/model/statisticalNumber.model';
import { IGrossProduct } from '../../shared/model/grossProduct.model';

type EntityResponseType = HttpResponse<IReviews>;
type EntityArrayResponseType = HttpResponse<IReviews[]>;

@Injectable({ providedIn: 'root' })
export class ReviewsService {
  public resourceUrl = SERVER_API_URL + 'api/reviews';

  constructor(protected http: HttpClient) {}

  create(reviews: IReviews): Observable<EntityResponseType> {
    return this.http.post<IReviews>(this.resourceUrl, reviews, { observe: 'response' });
  }

  update(reviews: IReviews): Observable<EntityResponseType> {
    return this.http.put<IReviews>(this.resourceUrl, reviews, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IReviews>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IReviews[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  queryStatisticalNumber(): Observable<HttpResponse<IStatisticalNumber>> {
    return this.http.get<IStatisticalNumber>(`${this.resourceUrl}/statistical-number`, { observe: 'response' });
  }

  queryChart(): Observable<HttpResponse<IGrossProduct[]>> {
    return this.http.get<IGrossProduct[]>(`${this.resourceUrl}/statistical`, { observe: 'response' });
  }
}
