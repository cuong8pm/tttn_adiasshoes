import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IReviews, Reviews } from 'app/shared/model/reviews.model';
import { ReviewsService } from './reviews.service';
import { IOrderProduct } from 'app/shared/model/order-product.model';
import { OrderProductService } from 'app/entities/order-product/order-product.service';

@Component({
  selector: 'jhi-reviews-update',
  templateUrl: './reviews-update.component.html',
})
export class ReviewsUpdateComponent implements OnInit {
  isSaving = false;
  orderproducts: IOrderProduct[] = [];

  editForm = this.fb.group({
    id: [],
    rating: [],
    decription: [],
    orderProduct: [],
  });

  constructor(
    protected reviewsService: ReviewsService,
    protected orderProductService: OrderProductService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ reviews }) => {
      this.updateForm(reviews);

      this.orderProductService
        .query({ 'reviewsId.specified': 'false' })
        .pipe(
          map((res: HttpResponse<IOrderProduct[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IOrderProduct[]) => {
          if (!reviews.orderProduct || !reviews.orderProduct.id) {
            this.orderproducts = resBody;
          } else {
            this.orderProductService
              .find(reviews.orderProduct.id)
              .pipe(
                map((subRes: HttpResponse<IOrderProduct>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IOrderProduct[]) => (this.orderproducts = concatRes));
          }
        });
    });
  }

  updateForm(reviews: IReviews): void {
    this.editForm.patchValue({
      id: reviews.id,
      rating: reviews.rating,
      decription: reviews.decription,
      orderProduct: reviews.orderProduct,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const reviews = this.createFromForm();
    if (reviews.id !== undefined) {
      this.subscribeToSaveResponse(this.reviewsService.update(reviews));
    } else {
      this.subscribeToSaveResponse(this.reviewsService.create(reviews));
    }
  }

  private createFromForm(): IReviews {
    return {
      ...new Reviews(),
      id: this.editForm.get(['id'])!.value,
      rating: this.editForm.get(['rating'])!.value,
      decription: this.editForm.get(['decription'])!.value,
      orderProduct: this.editForm.get(['orderProduct'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IReviews>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IOrderProduct): any {
    return item.id;
  }
}
