import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, ParamMap, Router, Data } from '@angular/router';
import { Subscription, combineLatest } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IOrderNow, OrderNow } from 'app/shared/model/order-now.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { OrderNowService } from './order-now.service';
import { OrderNowDeleteDialogComponent } from './order-now-delete-dialog.component';
import { User } from '../../core/user/user.model';
import { Moment } from 'moment';

@Component({
  selector: 'jhi-order-now',
  templateUrl: './order-now.component.html',
})
export class OrderNowComponent implements OnInit, OnDestroy {
  orderNows?: IOrderNow[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  confirm = '1';
  name: string = '';
  orderDate?: Moment;

  constructor(
    protected orderNowService: OrderNowService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadPage(page?: number, dontNavigate?: boolean): void {
    const pageToLoad: number = page || this.page || 1;

    this.orderNowService
      .query({
        confirm: this.confirm,
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe(
        (res: HttpResponse<IOrderNow[]>) => this.onSuccess(res.body, res.headers, pageToLoad, !dontNavigate),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.handleNavigation();
    this.registerChangeInOrderNows();
  }

  protected handleNavigation(): void {
    combineLatest(this.activatedRoute.data, this.activatedRoute.queryParamMap, (data: Data, params: ParamMap) => {
      const page = params.get('page');
      const confirm = params.get('confirm');
      const pageNumber = page !== null ? +page : 1;
      const sort = (params.get('sort') ?? data['defaultSort']).split(',');
      const predicate = sort[0];
      const ascending = sort[1] === 'asc';
      if (confirm !== null) this.confirm = confirm;
      if (pageNumber !== this.page || predicate !== this.predicate || ascending !== this.ascending) {
        this.predicate = predicate;
        this.ascending = ascending;
        this.loadPage(pageNumber, true);
      }
    }).subscribe();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IOrderNow): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInOrderNows(): void {
    this.eventSubscriber = this.eventManager.subscribe('orderNowListModification', () => this.loadPage());
  }

  delete(orderNow: IOrderNow): void {
    const modalRef = this.modalService.open(OrderNowDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.orderNow = orderNow;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IOrderNow[] | null, headers: HttpHeaders, page: number, navigate: boolean): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    if (navigate) {
      this.router.navigate(['/order-now'], {
        queryParams: {
          confirm: this.confirm,
          page: this.page,
          size: this.itemsPerPage,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
        },
      });
    }
    this.orderNows = data || [];
    this.ngbPaginationPage = this.page;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page ?? 1;
  }

  setConfirm(orderNow: OrderNow, isActivated: boolean): void {
    this.orderNowService.update({ ...orderNow, orderConfirm: isActivated }).subscribe(() => this.loadPage());
  }
  setDelivered(orderNow: OrderNow, isActivated: boolean): void {
    this.orderNowService.update({ ...orderNow, delivered: isActivated }).subscribe(() => this.loadPage());
  }
  setStatus(orderNow: OrderNow, isActivated: boolean): void {
    this.orderNowService.update({ ...orderNow, status: isActivated }).subscribe(() => this.loadPage());
  }
  replace(price: number | undefined): String {
    const aaa = new String(price);
    return aaa.replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }

  optionAll(): void {
    this.confirm = '2';
    this.loadPage();
  }

  optionConfirmed(): void {
    this.confirm = '1';
    this.loadPage();
  }

  optionUnconfirmed(): void {
    this.confirm = '0';
    this.loadPage();
  }
  onKeyName(event: any): void {
    this.name = event.target.value;
  }
  onKeyDate(event: any): void {
    this.orderDate = event.target.value;
  }
  search(): void {
    this.loadPage();
  }
}
