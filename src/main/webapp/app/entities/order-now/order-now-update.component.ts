import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IOrderNow, OrderNow } from 'app/shared/model/order-now.model';
import { OrderNowService } from './order-now.service';
import { IListCart } from 'app/shared/model/list-cart.model';
import { ListCartService } from 'app/entities/list-cart/list-cart.service';

@Component({
  selector: 'jhi-order-now-update',
  templateUrl: './order-now-update.component.html',
})
export class OrderNowUpdateComponent implements OnInit {
  isSaving = false;
  listcarts: IListCart[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    phone: [],
    address: [],
    orderConfirm: [],
    delivered: [],
    status: [],
    totalOrderPrice: [],
    listCart: [],
  });

  constructor(
    protected orderNowService: OrderNowService,
    protected listCartService: ListCartService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ orderNow }) => {
      this.updateForm(orderNow);

      this.listCartService.query().subscribe((res: HttpResponse<IListCart[]>) => (this.listcarts = res.body || []));
    });
  }

  updateForm(orderNow: IOrderNow): void {
    this.editForm.patchValue({
      id: orderNow.id,
      name: orderNow.name,
      phone: orderNow.phone,
      address: orderNow.address,
      orderConfirm: orderNow.orderConfirm,
      delivered: orderNow.delivered,
      status: orderNow.status,
      totalOrderPrice: orderNow.totalOrderPrice,
      listCart: orderNow.listCart,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const orderNow = this.createFromForm();
    if (orderNow.id !== undefined) {
      this.subscribeToSaveResponse(this.orderNowService.update(orderNow));
    } else {
      this.subscribeToSaveResponse(this.orderNowService.create(orderNow));
    }
  }

  private createFromForm(): IOrderNow {
    return {
      ...new OrderNow(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      phone: this.editForm.get(['phone'])!.value,
      address: this.editForm.get(['address'])!.value,
      orderConfirm: this.editForm.get(['orderConfirm'])!.value,
      delivered: this.editForm.get(['delivered'])!.value,
      status: this.editForm.get(['status'])!.value,
      totalOrderPrice: this.editForm.get(['totalOrderPrice'])!.value,
      listCart: this.editForm.get(['listCart'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrderNow>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IListCart): any {
    return item.id;
  }
}
