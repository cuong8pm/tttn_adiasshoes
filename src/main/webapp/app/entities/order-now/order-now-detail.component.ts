import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOrderNow } from 'app/shared/model/order-now.model';

@Component({
  selector: 'jhi-order-now-detail',
  templateUrl: './order-now-detail.component.html',
})
export class OrderNowDetailComponent implements OnInit {
  orderNow: IOrderNow | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ orderNow }) => (this.orderNow = orderNow));
  }

  previousState(): void {
    window.history.back();
  }
}
