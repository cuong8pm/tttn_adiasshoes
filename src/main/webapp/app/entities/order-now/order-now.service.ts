import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IOrderNow } from 'app/shared/model/order-now.model';

type EntityResponseType = HttpResponse<IOrderNow>;
type EntityArrayResponseType = HttpResponse<IOrderNow[]>;

@Injectable({ providedIn: 'root' })
export class OrderNowService {
  public resourceUrl = SERVER_API_URL + 'api/order-nows';

  constructor(protected http: HttpClient) {}

  create(orderNow: IOrderNow): Observable<EntityResponseType> {
    return this.http.post<IOrderNow>(this.resourceUrl, orderNow, { observe: 'response' });
  }

  update(orderNow: IOrderNow): Observable<EntityResponseType> {
    return this.http.put<IOrderNow>(this.resourceUrl, orderNow, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IOrderNow>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IOrderNow[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
