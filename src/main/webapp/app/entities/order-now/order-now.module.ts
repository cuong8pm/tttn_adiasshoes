import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HauiAdidasShoesSharedModule } from 'app/shared/shared.module';
import { OrderNowComponent } from './order-now.component';
import { OrderNowDetailComponent } from './order-now-detail.component';
import { OrderNowUpdateComponent } from './order-now-update.component';
import { OrderNowDeleteDialogComponent } from './order-now-delete-dialog.component';
import { orderNowRoute } from './order-now.route';

@NgModule({
  imports: [HauiAdidasShoesSharedModule, RouterModule.forChild(orderNowRoute)],
  declarations: [OrderNowComponent, OrderNowDetailComponent, OrderNowUpdateComponent, OrderNowDeleteDialogComponent],
  entryComponents: [OrderNowDeleteDialogComponent],
})
export class HauiAdidasShoesOrderNowModule {}
