import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IOrderNow, OrderNow } from 'app/shared/model/order-now.model';
import { OrderNowService } from './order-now.service';
import { OrderNowComponent } from './order-now.component';
import { OrderNowDetailComponent } from './order-now-detail.component';
import { OrderNowUpdateComponent } from './order-now-update.component';

@Injectable({ providedIn: 'root' })
export class OrderNowResolve implements Resolve<IOrderNow> {
  constructor(private service: OrderNowService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOrderNow> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((orderNow: HttpResponse<OrderNow>) => {
          if (orderNow.body) {
            return of(orderNow.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new OrderNow());
  }
}

export const orderNowRoute: Routes = [
  {
    path: '',
    component: OrderNowComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'hauiAdidasShoesApp.orderNow.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: OrderNowDetailComponent,
    resolve: {
      orderNow: OrderNowResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.orderNow.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: OrderNowUpdateComponent,
    resolve: {
      orderNow: OrderNowResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.orderNow.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: OrderNowUpdateComponent,
    resolve: {
      orderNow: OrderNowResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.orderNow.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
