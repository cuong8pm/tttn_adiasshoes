import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOrderNow } from 'app/shared/model/order-now.model';
import { OrderNowService } from './order-now.service';

@Component({
  templateUrl: './order-now-delete-dialog.component.html',
})
export class OrderNowDeleteDialogComponent {
  orderNow?: IOrderNow;

  constructor(protected orderNowService: OrderNowService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.orderNowService.delete(id).subscribe(() => {
      this.eventManager.broadcast('orderNowListModification');
      this.activeModal.close();
    });
  }
}
