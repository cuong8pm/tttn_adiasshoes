import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IDetailProduct } from 'app/shared/model/detail-product.model';
import { IProductDetails } from '../../shared/model/product-details.model';
import { IProductImage, ProductImage } from '../../shared/model/product-image.model';
import { Amount, IAmount } from '../../shared/model/amount.model';
import { IUserReview, UserReview } from '../../shared/model/review-product.model';

type EntityResponseType = HttpResponse<IDetailProduct>;
type EntityArrayResponseType = HttpResponse<IDetailProduct[]>;

@Injectable({ providedIn: 'root' })
export class DetailProductService {
  public resourceUrl = SERVER_API_URL + 'api/detail-products';

  private dataProduct = new BehaviorSubject<any>('abc');
  currentData = this.dataProduct.asObservable();

  constructor(protected http: HttpClient) {}

  changProduct(data: any): void {
    this.dataProduct.next(data);
  }

  create(detailProduct: IDetailProduct): Observable<EntityResponseType> {
    return this.http.post<IDetailProduct>(this.resourceUrl, detailProduct, { observe: 'response' });
  }

  update(detailProduct: IDetailProduct): Observable<EntityResponseType> {
    return this.http.put<IDetailProduct>(this.resourceUrl, detailProduct, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDetailProduct>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDetailProduct[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findAllImageById(id: number): Observable<EntityArrayResponseType> {
    return this.http.get<IProductImage[]>(`${this.resourceUrl}/${id}/images`, { observe: 'response' });
  }

  findAmountByProductIdAndSize(id: number, size: string): Observable<EntityResponseType> {
    return this.http.get<IAmount>(`${this.resourceUrl}/${id}/amount?size=${size}`, { observe: 'response' });
  }

  findReviewByProductId(id: number): Observable<EntityArrayResponseType> {
    return this.http.get<any[]>(`${this.resourceUrl}/${id}/review`, { observe: 'response' });
  }
}
