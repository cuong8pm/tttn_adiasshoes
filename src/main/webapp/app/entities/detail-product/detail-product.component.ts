import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, ParamMap, Router, Data } from '@angular/router';
import { Subscription, combineLatest } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IDetailProduct } from 'app/shared/model/detail-product.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { DetailProductService } from './detail-product.service';
import { DetailProductDeleteDialogComponent } from './detail-product-delete-dialog.component';
import { IProduct } from '../../shared/model/product.model';
import { ProductService } from '../product/product.service';
import { ICategory } from '../../shared/model/category.model';
import { ISize } from '../../shared/model/size.model';
import { SizeService } from '../size/size.service';
import { ProductDetailsService } from '../product-details/product-details.service';
import { IProductDetails } from '../../shared/model/product-details.model';
import { IProductImage, ProductImage } from '../../shared/model/product-image.model';
import { FormBuilder } from '@angular/forms';
import { Amount, IAmount } from '../../shared/model/amount.model';
import { ListCartService } from '../list-cart/list-cart.service';
import { IListCart } from '../../shared/model/list-cart.model';
import { AccountService } from '../../core/auth/account.service';
import { Account } from '../../core/user/account.model';
import { Cart, ICart } from '../../shared/model/cart.model';
import { ProductImportHistoryService } from '../product-import-history/product-import-history.service';

@Component({
  selector: 'jhi-detail-product',
  templateUrl: './detail-product.component.html',
  styleUrls: ['detail-product.scss'],
})
export class DetailProductComponent implements OnInit, OnDestroy {
  detailProducts?: IDetailProduct[];
  product: IProduct | null = null;
  sizes: ISize[] = [];
  productDetails?: IProductDetails[];
  Imagedata?: IProductImage[];
  reviewUser?: any[];
  listCart?: IListCart | null = null;
  amounts: IAmount | null = null;
  cart?: Cart;
  account!: Account;
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  id!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  amount = 1;
  size?: string;
  sizetest!: string;
  username!: string;
  totalCart?: Number | any[];

  constructor(
    protected detailProductService: DetailProductService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected productImportHistoryService: ProductImportHistoryService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    private productService: ProductService,
    protected sizeService: SizeService,
    protected productDetailsService: ProductDetailsService,
    protected listCartService: ListCartService,
    private accountService: AccountService,
    private fb: FormBuilder
  ) {}

  loadPage(page?: number, dontNavigate?: boolean): void {
    const pageToLoad: number = page || this.page || 1;
    this.detailProductService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe(
        (res: HttpResponse<IDetailProduct[]>) => this.onSuccess(res.body, res.headers, pageToLoad, !dontNavigate),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.handleNavigation();
    this.activatedRoute.data.subscribe(({ detailProduct }) => (this.id = detailProduct.id));
    this.productService.findById(this.id).subscribe((res: HttpResponse<IProduct>) => (this.product = res.body));
    this.detailProductService
      .findAllImageById(this.id)
      .subscribe((res: HttpResponse<IProductImage[]>) => (this.Imagedata = res.body || []));
    this.detailProductService
      .findReviewByProductId(this.id)
      .subscribe((res: HttpResponse<IProductImage[]>) => (this.reviewUser = res.body || []));
    this.sizeService.query().subscribe((res: HttpResponse<ISize[]>) => (this.sizes = res.body || []));
    this.amountProduct();
    this.productDetailsService
      .findAllById(this.id)
      .subscribe((res: HttpResponse<IProductDetails[]>) => (this.productDetails = res.body || []));
    this.registerChangeInDetailProducts();
  }

  protected handleNavigation(): void {
    combineLatest(this.activatedRoute.data, this.activatedRoute.queryParamMap, (data: Data, params: ParamMap) => {
      const id = params.get('id');
      const page = params.get('page');
      const pageNumber = page !== null ? +page : 1;
      const sort = (params.get('sort') ?? data['defaultSort']).split(',');
      const predicate = sort[0];
      const ascending = sort[1] === 'asc';
      if (pageNumber !== this.page || predicate !== this.predicate || ascending !== this.ascending) {
        this.predicate = predicate;
        this.ascending = ascending;
        this.loadPage(pageNumber, true);
      }
    }).subscribe();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IDetailProduct): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInDetailProducts(): void {
    this.eventSubscriber = this.eventManager.subscribe('detailProductListModification', () => this.loadPage());
  }

  delete(detailProduct: IDetailProduct): void {
    const modalRef = this.modalService.open(DetailProductDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.detailProduct = detailProduct;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IDetailProduct[] | null, headers: HttpHeaders, page: number, navigate: boolean): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    if (navigate) {
      this.router.navigate(['/detail-product'], {
        queryParams: {
          page: this.page,
          size: this.itemsPerPage,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
        },
      });
    }
    this.detailProducts = data || [];
    this.ngbPaginationPage = this.page;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page ?? 1;
  }
  protected onSuccessProduct(data: IProduct | null, headers: HttpHeaders): void {
    this.product = data;
  }
  public minus(): void {
    if (this.amount > 1) {
      this.amount = this.amount - 1;
    }
  }
  public plus(): void {
    this.amount = this.amount + 1;
  }
  public amountProduct(): void {
    const sizes = this.sizetest;
    const id = this.id;
    if (sizes === undefined)
      this.detailProductService.findAmountByProductIdAndSize(id, '44').subscribe((res: HttpResponse<IAmount>) => (this.amounts = res.body));
    else
      this.detailProductService
        .findAmountByProductIdAndSize(id, sizes)
        .subscribe((res: HttpResponse<IAmount>) => (this.amounts = res.body));
  }

  selectChangeHandler(event: any): void {
    this.sizetest = event.target.value;
  }

  addCart(): void {
    this.accountService.identity().subscribe(account => {
      if (account) {
        this.account = account;
        this.username = this.account.login;
      }
    });
    this.cart = new Cart(undefined, this.amount, 'abc', this.username, this.product, this.sizetest);

    this.listCartService.createCart(this.cart).subscribe((res: HttpResponse<IListCart>) => (this.listCart = res.body));
  }
  submit(): void {
    this.productImportHistoryService.currentData.subscribe(data => (this.username = data));
    this.cart = new Cart(undefined, this.amount, 'abc', this.username, this.product, this.sizetest);
    this.detailProductService.changProduct(this.cart);
    this.router.navigate(['/product-import-history'], { queryParams: {} });
  }
  replace(price: number | undefined): String {
    const aaa = new String(price);
    return aaa.replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }
}
