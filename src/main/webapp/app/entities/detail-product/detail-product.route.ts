import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IDetailProduct, DetailProduct } from 'app/shared/model/detail-product.model';
import { DetailProductService } from './detail-product.service';
import { DetailProductComponent } from './detail-product.component';
import { DetailProductDetailComponent } from './detail-product-detail.component';
import { DetailProductUpdateComponent } from './detail-product-update.component';
import { ProductService } from '../product/product.service';

@Injectable({ providedIn: 'root' })
export class DetailProductResolve implements Resolve<IDetailProduct> {
  constructor(private service: DetailProductService, private router: Router, private productService: ProductService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDetailProduct> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.productService.findById(id).pipe(
        flatMap((detailProduct: HttpResponse<DetailProduct>) => {
          if (detailProduct.body) {
            return of(detailProduct.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new DetailProduct());
  }
}

export const detailProductRoute: Routes = [
  {
    path: ':id',
    component: DetailProductComponent,
    resolve: {
      detailProduct: DetailProductResolve,
    },
    data: {
      pageTitle: 'hauiAdidasShoesApp.detailProduct.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DetailProductDetailComponent,
    resolve: {
      detailProduct: DetailProductResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.detailProduct.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DetailProductUpdateComponent,
    resolve: {
      detailProduct: DetailProductResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.detailProduct.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DetailProductUpdateComponent,
    resolve: {
      detailProduct: DetailProductResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.detailProduct.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
