import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDetailProduct } from 'app/shared/model/detail-product.model';

@Component({
  selector: 'jhi-detail-product-detail',
  templateUrl: './detail-product-detail.component.html',
})
export class DetailProductDetailComponent implements OnInit {
  detailProduct: IDetailProduct | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ detailProduct }) => (this.detailProduct = detailProduct));
  }

  previousState(): void {
    window.history.back();
  }
}
