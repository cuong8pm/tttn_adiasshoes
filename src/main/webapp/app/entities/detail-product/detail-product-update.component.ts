import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IDetailProduct, DetailProduct } from 'app/shared/model/detail-product.model';
import { DetailProductService } from './detail-product.service';

@Component({
  selector: 'jhi-detail-product-update',
  templateUrl: './detail-product-update.component.html',
})
export class DetailProductUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
  });

  constructor(protected detailProductService: DetailProductService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ detailProduct }) => {
      this.updateForm(detailProduct);
    });
  }

  updateForm(detailProduct: IDetailProduct): void {
    this.editForm.patchValue({
      id: detailProduct.id,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const detailProduct = this.createFromForm();
    if (detailProduct.id !== undefined) {
      this.subscribeToSaveResponse(this.detailProductService.update(detailProduct));
    } else {
      this.subscribeToSaveResponse(this.detailProductService.create(detailProduct));
    }
  }

  private createFromForm(): IDetailProduct {
    return {
      ...new DetailProduct(),
      id: this.editForm.get(['id'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDetailProduct>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
