import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HauiAdidasShoesSharedModule } from 'app/shared/shared.module';
import { DetailProductComponent } from './detail-product.component';
import { DetailProductDetailComponent } from './detail-product-detail.component';
import { DetailProductUpdateComponent } from './detail-product-update.component';
import { DetailProductDeleteDialogComponent } from './detail-product-delete-dialog.component';
import { detailProductRoute } from './detail-product.route';

@NgModule({
  imports: [HauiAdidasShoesSharedModule, RouterModule.forChild(detailProductRoute)],
  declarations: [DetailProductComponent, DetailProductDetailComponent, DetailProductUpdateComponent, DetailProductDeleteDialogComponent],
  entryComponents: [DetailProductDeleteDialogComponent],
})
export class HauiAdidasShoesDetailProductModule {}
