import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDetailProduct } from 'app/shared/model/detail-product.model';
import { DetailProductService } from './detail-product.service';

@Component({
  templateUrl: './detail-product-delete-dialog.component.html',
})
export class DetailProductDeleteDialogComponent {
  detailProduct?: IDetailProduct;

  constructor(
    protected detailProductService: DetailProductService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.detailProductService.delete(id).subscribe(() => {
      this.eventManager.broadcast('detailProductListModification');
      this.activeModal.close();
    });
  }
}
