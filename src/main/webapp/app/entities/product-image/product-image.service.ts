import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IProductImage } from 'app/shared/model/product-image.model';

type EntityResponseType = HttpResponse<IProductImage>;
type EntityArrayResponseType = HttpResponse<IProductImage[]>;

@Injectable({ providedIn: 'root' })
export class ProductImageService {
  public resourceUrl = SERVER_API_URL + 'api/product-images';

  private productID = new BehaviorSubject<number | undefined>(1);
  currentData = this.productID.asObservable();

  constructor(protected http: HttpClient) {}

  changProducId(data: number | undefined): void {
    this.productID.next(data);
  }

  create(productImage: IProductImage, req?: any): Observable<EntityResponseType> {
    const options = createRequestOption(req);
    return this.http.post<IProductImage>(this.resourceUrl, productImage, { params: options, observe: 'response' });
  }

  update(productImage: IProductImage, req?: any): Observable<EntityResponseType> {
    const options = createRequestOption(req);
    return this.http.put<IProductImage>(this.resourceUrl, productImage, { params: options, observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IProductImage>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IProductImage[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
