import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, ParamMap, Router, Data } from '@angular/router';
import { Subscription, combineLatest } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IProductImage } from 'app/shared/model/product-image.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ProductImageService } from './product-image.service';
import { ProductImageDeleteDialogComponent } from './product-image-delete-dialog.component';
import { IProductDetails } from '../../shared/model/product-details.model';
import { ProductDetailsService } from '../product-details/product-details.service';
import { IReviews } from '../../shared/model/reviews.model';
import { ReviewsService } from '../reviews/reviews.service';
import { IProduct } from '../../shared/model/product.model';
import { ProductService } from '../product/product.service';
import { ProductDetailsDeleteDialogComponent } from '../product-details/product-details-delete-dialog.component';
import { ListProductService } from '../list-product/list-product.service';

@Component({
  selector: 'jhi-product-image',
  templateUrl: './product-image.component.html',
  styleUrls: ['product-detail.scss'],
})
export class ProductImageComponent implements OnInit, OnDestroy {
  productImages?: IProductImage[] | null = null;
  products: IProduct[] | null = null;
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = 7;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  productDetails: IProductDetails[] | null = null;
  reviews: IReviews[] | null = null;
  id?: number;
  test = 0;
  name?: string;
  tabIndex?: Number = 1;

  constructor(
    protected productImageService: ProductImageService,
    protected activatedRoute: ActivatedRoute,
    protected productService: ProductService,
    protected dataUtils: JhiDataUtils,
    protected router: Router,
    protected listProductService: ListProductService,
    protected reviewsService: ReviewsService,
    protected productDetailsService: ProductDetailsService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.productService.query({}).subscribe((res: HttpResponse<IProduct[]>) => (this.products = res.body));

    this.productImageService.currentData.subscribe(data => (this.id = data));
    this.productImageService.query({ id: this.id }).subscribe(
      (res: HttpResponse<IProductImage[]>) => (this.productImages = res.body),
      () => this.onError()
    );
    this.productDetailsService.query({ id: this.id }).subscribe((res: HttpResponse<IProductDetails[]>) => (this.productDetails = res.body));
    this.reviewsService.query({ id: this.id }).subscribe((res: HttpResponse<IReviews[]>) => (this.reviews = res.body));
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IProductImage): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  STT(): number {
    this.test = this.test + 1;
    return this.test;
  }
  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType = '', base64String: string): void {
    return this.dataUtils.openFile(contentType, base64String);
  }

  delete(productImage: IProductImage): void {
    const modalRef = this.modalService.open(ProductImageDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.productImage = productImage;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page ?? 1;
  }

  loadProduct(id: number | undefined): void {
    this.id = id;
    this.tabIndex = id;
    this.productImageService.changProducId(id);
    this.productImageService.query({ id: this.id }).subscribe(
      (res: HttpResponse<IProductImage[]>) => (this.productImages = res.body),
      () => this.onError()
    );
    this.productDetailsService.query({ id: id }).subscribe((res: HttpResponse<IProductDetails[]>) => (this.productDetails = res.body));
    this.reviewsService.query({ id: id }).subscribe((res: HttpResponse<IReviews[]>) => (this.reviews = res.body));
  }

  createProductImage(): void {
    this.router.navigate(['/product-image/new'], {
      queryParams: { productId: this.id },
    });
  }
  createProductDes(): void {
    this.router.navigate(['/product-details/new'], {
      queryParams: { productId: this.id },
    });
  }

  deleteProductDetail(productDetails: IProductDetails): void {
    const modalRef = this.modalService.open(ProductDetailsDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.productDetails = productDetails;
  }
  changeLoadProduct(newName: any): void {
    this.name = newName;
    this.listProductService.querySearchTree({ name: this.name }).subscribe((res: HttpResponse<IProduct[]>) => (this.products = res.body));
  }
}
