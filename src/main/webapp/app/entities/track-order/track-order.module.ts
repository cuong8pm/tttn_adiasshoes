import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HauiAdidasShoesSharedModule } from 'app/shared/shared.module';
import { TrackOrderComponent } from './track-order.component';
import { TrackOrderDetailComponent } from './track-order-detail.component';
import { TrackOrderUpdateComponent } from './track-order-update.component';
import { TrackOrderDeleteDialogComponent } from './track-order-delete-dialog.component';
import { trackOrderRoute } from './track-order.route';

@NgModule({
  imports: [HauiAdidasShoesSharedModule, RouterModule.forChild(trackOrderRoute)],
  declarations: [TrackOrderComponent, TrackOrderDetailComponent, TrackOrderUpdateComponent, TrackOrderDeleteDialogComponent],
  entryComponents: [TrackOrderDeleteDialogComponent],
})
export class HauiAdidasShoesTrackOrderModule {}
