import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, ParamMap, Router, Data } from '@angular/router';
import { Subscription, combineLatest } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITrackOrder } from 'app/shared/model/track-order.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { TrackOrderService } from './track-order.service';
import { TrackOrderDeleteDialogComponent } from './track-order-delete-dialog.component';
import { OrdersService } from '../orders/orders.service';
import { IOrders } from '../../shared/model/orders.model';
import { IOrderUser } from '../../shared/model/order-user.model';
import { TrackOrderUserService } from '../track-order-user/track-order-user.service';

@Component({
  selector: 'jhi-track-order',
  templateUrl: './track-order.component.html',
  styleUrls: ['track-order.scss'],
})
export class TrackOrderComponent implements OnInit, OnDestroy {
  trackOrders?: ITrackOrder[] | null = null;
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = 10;
  page!: number;
  orderId?: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  orderUser: IOrderUser | null = null;

  constructor(
    protected trackOrderService: TrackOrderService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected ordersService: OrdersService,
    protected trackOrderUserService: TrackOrderUserService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    public activeModal: NgbActiveModal
  ) {}

  loadPage(page?: number, dontNavigate?: boolean): void {
    const pageToLoad: number = page || this.page || 1;

    this.trackOrderService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe(
        (res: HttpResponse<ITrackOrder[]>) => this.onSuccess(res.body, res.headers, pageToLoad, !dontNavigate),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.ordersService.currentData.subscribe(data => (this.orderId = data));
    this.trackOrderService.query({ orderId: this.orderId }).subscribe((res: HttpResponse<ITrackOrder[]>) => (this.trackOrders = res.body));
    this.trackOrderUserService
      .getOrder({ orderId: this.orderId })
      .subscribe((res: HttpResponse<IOrderUser>) => (this.orderUser = res.body));
  }

  protected handleNavigation(): void {
    combineLatest(this.activatedRoute.data, this.activatedRoute.queryParamMap, (data: Data, params: ParamMap) => {
      const page = params.get('page');
      const pageNumber = page !== null ? +page : 1;
      const sort = (params.get('sort') ?? data['defaultSort']).split(',');
      const predicate = sort[0];
      const ascending = sort[1] === 'asc';
      if (pageNumber !== this.page || predicate !== this.predicate || ascending !== this.ascending) {
        this.predicate = predicate;
        this.ascending = ascending;
        this.loadPage(pageNumber, true);
      }
    }).subscribe();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITrackOrder): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInTrackOrders(): void {
    this.eventSubscriber = this.eventManager.subscribe('trackOrderListModification', () => this.loadPage());
  }

  delete(trackOrder: ITrackOrder): void {
    const modalRef = this.modalService.open(TrackOrderDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.trackOrder = trackOrder;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: ITrackOrder[] | null, headers: HttpHeaders, page: number, navigate: boolean): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    if (navigate) {
      this.router.navigate(['/track-order'], {
        queryParams: {
          page: this.page,
          size: this.itemsPerPage,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
        },
      });
    }
    this.trackOrders = data || [];
    this.ngbPaginationPage = this.page;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page ?? 1;
  }

  closeCom(): void {
    this.activeModal.close();
  }

  setOrderConfirm(trackOrder: ITrackOrder, confirm: boolean): void {
    this.trackOrderService
      .update({ ...trackOrder, orderConfirm: confirm })
      .subscribe(() =>
        this.trackOrderService
          .query({ orderId: this.orderId })
          .subscribe((res: HttpResponse<ITrackOrder[]>) => (this.trackOrders = res.body))
      );
  }
  setWaittingProduct(trackOrder: ITrackOrder, confirm: boolean): void {
    this.trackOrderService
      .update({ ...trackOrder, waittingProduct: confirm })
      .subscribe(() =>
        this.trackOrderService
          .query({ orderId: this.orderId })
          .subscribe((res: HttpResponse<ITrackOrder[]>) => (this.trackOrders = res.body))
      );
  }
  setShipping(trackOrder: ITrackOrder, confirm: boolean): void {
    this.trackOrderService
      .update({ ...trackOrder, shipping: confirm })
      .subscribe(() =>
        this.trackOrderService
          .query({ orderId: this.orderId })
          .subscribe((res: HttpResponse<ITrackOrder[]>) => (this.trackOrders = res.body))
      );
  }
  setDelivered(trackOrder: ITrackOrder, confirm: boolean): void {
    this.trackOrderService
      .update({ ...trackOrder, delivered: confirm })
      .subscribe(() =>
        this.trackOrderService
          .query({ orderId: this.orderId })
          .subscribe((res: HttpResponse<ITrackOrder[]>) => (this.trackOrders = res.body))
      );
  }
  setStatus(trackOrder: ITrackOrder, confirm: boolean): void {
    this.trackOrderService
      .update({ ...trackOrder, status: confirm })
      .subscribe(() =>
        this.trackOrderService
          .query({ orderId: this.orderId })
          .subscribe((res: HttpResponse<ITrackOrder[]>) => (this.trackOrders = res.body))
      );
  }
}
