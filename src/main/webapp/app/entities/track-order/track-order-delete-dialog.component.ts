import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITrackOrder } from 'app/shared/model/track-order.model';
import { TrackOrderService } from './track-order.service';

@Component({
  templateUrl: './track-order-delete-dialog.component.html',
})
export class TrackOrderDeleteDialogComponent {
  trackOrder?: ITrackOrder;

  constructor(
    protected trackOrderService: TrackOrderService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.trackOrderService.delete(id).subscribe(() => {
      this.eventManager.broadcast('trackOrderListModification');
      this.activeModal.close();
    });
  }
}
