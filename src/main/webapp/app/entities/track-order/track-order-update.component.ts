import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ITrackOrder, TrackOrder } from 'app/shared/model/track-order.model';
import { TrackOrderService } from './track-order.service';
import { IOrders } from 'app/shared/model/orders.model';
import { OrdersService } from 'app/entities/orders/orders.service';

@Component({
  selector: 'jhi-track-order-update',
  templateUrl: './track-order-update.component.html',
})
export class TrackOrderUpdateComponent implements OnInit {
  isSaving = false;
  orders: IOrders[] = [];

  editForm = this.fb.group({
    id: [],
    orderConfirm: [],
    waittingProduct: [],
    shipping: [],
    delivered: [],
    status: [],
    orders: [],
  });

  constructor(
    protected trackOrderService: TrackOrderService,
    protected ordersService: OrdersService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ trackOrder }) => {
      this.updateForm(trackOrder);

      this.ordersService
        .query({ 'trackOrderId.specified': 'false' })
        .pipe(
          map((res: HttpResponse<IOrders[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IOrders[]) => {
          if (!trackOrder.orders || !trackOrder.orders.id) {
            this.orders = resBody;
          } else {
            this.ordersService
              .find(trackOrder.orders.id)
              .pipe(
                map((subRes: HttpResponse<IOrders>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IOrders[]) => (this.orders = concatRes));
          }
        });
    });
  }

  updateForm(trackOrder: ITrackOrder): void {
    this.editForm.patchValue({
      id: trackOrder.id,
      orderConfirm: trackOrder.orderConfirm,
      waittingProduct: trackOrder.waittingProduct,
      shipping: trackOrder.shipping,
      delivered: trackOrder.delivered,
      status: trackOrder.status,
      orders: trackOrder.orders,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const trackOrder = this.createFromForm();
    if (trackOrder.id !== undefined) {
      this.subscribeToSaveResponse(this.trackOrderService.update(trackOrder));
    } else {
      this.subscribeToSaveResponse(this.trackOrderService.create(trackOrder));
    }
  }

  private createFromForm(): ITrackOrder {
    return {
      ...new TrackOrder(),
      id: this.editForm.get(['id'])!.value,
      orderConfirm: this.editForm.get(['orderConfirm'])!.value,
      waittingProduct: this.editForm.get(['waittingProduct'])!.value,
      shipping: this.editForm.get(['shipping'])!.value,
      delivered: this.editForm.get(['delivered'])!.value,
      status: this.editForm.get(['status'])!.value,
      orders: this.editForm.get(['orders'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITrackOrder>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IOrders): any {
    return item.id;
  }
}
