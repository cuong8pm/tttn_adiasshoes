import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITrackOrder, TrackOrder } from 'app/shared/model/track-order.model';
import { TrackOrderService } from './track-order.service';
import { TrackOrderComponent } from './track-order.component';
import { TrackOrderDetailComponent } from './track-order-detail.component';
import { TrackOrderUpdateComponent } from './track-order-update.component';

@Injectable({ providedIn: 'root' })
export class TrackOrderResolve implements Resolve<ITrackOrder> {
  constructor(private service: TrackOrderService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITrackOrder> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((trackOrder: HttpResponse<TrackOrder>) => {
          if (trackOrder.body) {
            return of(trackOrder.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TrackOrder());
  }
}

export const trackOrderRoute: Routes = [
  {
    path: '',
    component: TrackOrderComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'hauiAdidasShoesApp.trackOrder.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TrackOrderDetailComponent,
    resolve: {
      trackOrder: TrackOrderResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.trackOrder.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TrackOrderUpdateComponent,
    resolve: {
      trackOrder: TrackOrderResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.trackOrder.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TrackOrderUpdateComponent,
    resolve: {
      trackOrder: TrackOrderResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.trackOrder.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
