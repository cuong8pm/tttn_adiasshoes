import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITrackOrder } from 'app/shared/model/track-order.model';

type EntityResponseType = HttpResponse<ITrackOrder>;
type EntityArrayResponseType = HttpResponse<ITrackOrder[]>;

@Injectable({ providedIn: 'root' })
export class TrackOrderService {
  public resourceUrl = SERVER_API_URL + 'api/track-orders';

  constructor(protected http: HttpClient) {}

  create(trackOrder: ITrackOrder): Observable<EntityResponseType> {
    return this.http.post<ITrackOrder>(this.resourceUrl, trackOrder, { observe: 'response' });
  }

  update(trackOrder: ITrackOrder): Observable<EntityResponseType> {
    return this.http.put<ITrackOrder>(this.resourceUrl, trackOrder, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITrackOrder>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITrackOrder[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
