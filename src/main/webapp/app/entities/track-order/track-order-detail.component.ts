import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITrackOrder } from 'app/shared/model/track-order.model';

@Component({
  selector: 'jhi-track-order-detail',
  templateUrl: './track-order-detail.component.html',
})
export class TrackOrderDetailComponent implements OnInit {
  trackOrder: ITrackOrder | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ trackOrder }) => (this.trackOrder = trackOrder));
  }

  previousState(): void {
    window.history.back();
  }
}
