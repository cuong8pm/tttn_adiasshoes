import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISize } from 'app/shared/model/size.model';
import { SizeService } from './size.service';

@Component({
  templateUrl: './size-delete-dialog.component.html',
})
export class SizeDeleteDialogComponent {
  size?: ISize;

  constructor(protected sizeService: SizeService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.sizeService.delete(id).subscribe(() => {
      this.eventManager.broadcast('sizeListModification');
      this.activeModal.close();
    });
  }
}
