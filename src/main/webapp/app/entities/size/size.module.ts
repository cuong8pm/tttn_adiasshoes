import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HauiAdidasShoesSharedModule } from 'app/shared/shared.module';
import { SizeComponent } from './size.component';
import { SizeDetailComponent } from './size-detail.component';
import { SizeUpdateComponent } from './size-update.component';
import { SizeDeleteDialogComponent } from './size-delete-dialog.component';
import { sizeRoute } from './size.route';

@NgModule({
  imports: [HauiAdidasShoesSharedModule, RouterModule.forChild(sizeRoute)],
  declarations: [SizeComponent, SizeDetailComponent, SizeUpdateComponent, SizeDeleteDialogComponent],
  entryComponents: [SizeDeleteDialogComponent],
})
export class HauiAdidasShoesSizeModule {}
