import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, ParamMap, Router, Data } from '@angular/router';
import { Subscription, combineLatest } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ITrackOrderUser } from 'app/shared/model/track-order-user.model';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { TrackOrderUserService } from './track-order-user.service';
import { TrackOrderUserDeleteDialogComponent } from './track-order-user-delete-dialog.component';
import { Account } from '../../core/user/account.model';
import { AccountService } from '../../core/auth/account.service';
import { ITotalPrice } from '../../shared/model/total-price.model';
import { IOrderUser } from '../../shared/model/order-user.model';
import { ITrackOrder, TrackOrder } from '../../shared/model/track-order.model';
import { IProduct } from '../../shared/model/product.model';

@Component({
  selector: 'jhi-track-order-user',
  templateUrl: './track-order-user.component.html',
  styleUrls: ['track-order-user.scss'],
})
export class TrackOrderUserComponent implements OnInit, OnDestroy {
  trackOrderUsers?: ITrackOrderUser[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  tabIndex = 1;
  account!: Account;
  orderUsers: IOrderUser[] | null = null;
  textReview?: string = '';

  constructor(
    protected trackOrderUserService: TrackOrderUserService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    private accountService: AccountService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.accountService.identity().subscribe(account => {
      if (account) {
        this.account = account;
      }
    });
    this.trackOrderUserService
      .getAllOrder({ username: this.account.login, track: this.tabIndex })
      .subscribe((res: HttpResponse<IOrderUser[]>) => (this.orderUsers = res.body));
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITrackOrderUser): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  tabClick(val: any): void {
    this.tabIndex = val;
    this.trackOrderUserService
      .getAllOrder({ username: this.account.login, track: this.tabIndex })
      .subscribe((res: HttpResponse<IOrderUser[]>) => (this.orderUsers = res.body));
  }

  cancelOrder(trackOrder: TrackOrder | undefined): void {
    this.tabIndex = 5;
    this.trackOrderUserService
      .update(trackOrder)
      .subscribe((res: HttpResponse<ITrackOrder>) =>
        this.trackOrderUserService
          .getAllOrder({ username: this.account.login, track: this.tabIndex })
          .subscribe((res: HttpResponse<IOrderUser[]>) => (this.orderUsers = res.body))
      );
  }
  replace(price: number | undefined): String {
    const aaa = new String(price);
    return aaa.replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }
  onKey(event: any) {
    this.textReview = event.target.value;
  }

  review(id?: number, listCartId?: number): void {
    this.trackOrderUserService
      .createReview(id, this.textReview, { listCartId })
      .subscribe((res: HttpResponse<boolean>) =>
        this.trackOrderUserService
          .getAllOrder({ username: this.account.login, track: 4 })
          .subscribe((res: HttpResponse<IOrderUser[]>) => (this.orderUsers = res.body))
      );
  }
}
