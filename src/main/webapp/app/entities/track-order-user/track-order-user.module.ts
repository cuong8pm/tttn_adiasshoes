import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HauiAdidasShoesSharedModule } from 'app/shared/shared.module';
import { TrackOrderUserComponent } from './track-order-user.component';
import { TrackOrderUserDetailComponent } from './track-order-user-detail.component';
import { TrackOrderUserUpdateComponent } from './track-order-user-update.component';
import { TrackOrderUserDeleteDialogComponent } from './track-order-user-delete-dialog.component';
import { trackOrderUserRoute } from './track-order-user.route';
import { MatTabsModule } from '@angular/material/tabs';

@NgModule({
  imports: [HauiAdidasShoesSharedModule, RouterModule.forChild(trackOrderUserRoute), MatTabsModule],
  declarations: [
    TrackOrderUserComponent,
    TrackOrderUserDetailComponent,
    TrackOrderUserUpdateComponent,
    TrackOrderUserDeleteDialogComponent,
  ],
  entryComponents: [TrackOrderUserDeleteDialogComponent],
})
export class HauiAdidasShoesTrackOrderUserModule {}
