import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITrackOrderUser } from 'app/shared/model/track-order-user.model';
import { IOrderUser } from '../../shared/model/order-user.model';
import { ITotalPrice } from '../../shared/model/total-price.model';
import { ITrackOrder } from '../../shared/model/track-order.model';

type EntityResponseType = HttpResponse<ITrackOrderUser>;
type EntityArrayResponseType = HttpResponse<ITrackOrderUser[]>;

@Injectable({ providedIn: 'root' })
export class TrackOrderUserService {
  public resourceUrl = SERVER_API_URL + 'api/track-order-users';

  constructor(protected http: HttpClient) {}

  create(trackOrderUser: ITrackOrderUser): Observable<EntityResponseType> {
    return this.http.post<ITrackOrderUser>(this.resourceUrl, trackOrderUser, { observe: 'response' });
  }

  update(trackOrder: ITrackOrder | undefined): Observable<EntityResponseType> {
    return this.http.put<ITrackOrder>(this.resourceUrl, trackOrder, { observe: 'response' });
  }

  find(id: number | undefined): Observable<EntityResponseType> {
    return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITrackOrderUser[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
  getAllOrder(req?: any): Observable<HttpResponse<IOrderUser[]>> {
    const options = createRequestOption(req);
    return this.http.get<IOrderUser[]>(`${this.resourceUrl}/all-order`, { params: options, observe: 'response' });
  }

  getOrder(req?: any): Observable<HttpResponse<IOrderUser>> {
    const options = createRequestOption(req);
    return this.http.get<IOrderUser>(`${this.resourceUrl}/order-detail`, { params: options, observe: 'response' });
  }

  createReview(id?: number, textReview?: string, req?: any) {
    const options = createRequestOption(req);
    return this.http.post<boolean>(`${this.resourceUrl}/${id}/create-review`, textReview, { params: options, observe: 'response' });
  }
}
