import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITrackOrderUser } from 'app/shared/model/track-order-user.model';

@Component({
  selector: 'jhi-track-order-user-detail',
  templateUrl: './track-order-user-detail.component.html',
})
export class TrackOrderUserDetailComponent implements OnInit {
  trackOrderUser: ITrackOrderUser | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ trackOrderUser }) => (this.trackOrderUser = trackOrderUser));
  }

  previousState(): void {
    window.history.back();
  }
}
