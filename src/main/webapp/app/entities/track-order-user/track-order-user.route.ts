import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITrackOrderUser, TrackOrderUser } from 'app/shared/model/track-order-user.model';
import { TrackOrderUserService } from './track-order-user.service';
import { TrackOrderUserComponent } from './track-order-user.component';
import { TrackOrderUserDetailComponent } from './track-order-user-detail.component';
import { TrackOrderUserUpdateComponent } from './track-order-user-update.component';

@Injectable({ providedIn: 'root' })
export class TrackOrderUserResolve implements Resolve<ITrackOrderUser> {
  constructor(private service: TrackOrderUserService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITrackOrderUser> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((trackOrderUser: HttpResponse<TrackOrderUser>) => {
          if (trackOrderUser.body) {
            return of(trackOrderUser.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TrackOrderUser());
  }
}

export const trackOrderUserRoute: Routes = [
  {
    path: '',
    component: TrackOrderUserComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'hauiAdidasShoesApp.trackOrderUser.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TrackOrderUserDetailComponent,
    resolve: {
      trackOrderUser: TrackOrderUserResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.trackOrderUser.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TrackOrderUserUpdateComponent,
    resolve: {
      trackOrderUser: TrackOrderUserResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.trackOrderUser.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TrackOrderUserUpdateComponent,
    resolve: {
      trackOrderUser: TrackOrderUserResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'hauiAdidasShoesApp.trackOrderUser.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
