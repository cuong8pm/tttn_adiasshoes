import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ITrackOrderUser, TrackOrderUser } from 'app/shared/model/track-order-user.model';
import { TrackOrderUserService } from './track-order-user.service';

@Component({
  selector: 'jhi-track-order-user-update',
  templateUrl: './track-order-user-update.component.html',
})
export class TrackOrderUserUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
  });

  constructor(protected trackOrderUserService: TrackOrderUserService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ trackOrderUser }) => {
      this.updateForm(trackOrderUser);
    });
  }

  updateForm(trackOrderUser: ITrackOrderUser): void {
    this.editForm.patchValue({
      id: trackOrderUser.id,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const trackOrderUser = this.createFromForm();
    if (trackOrderUser.id !== undefined) {
      this.subscribeToSaveResponse(this.trackOrderUserService.update(trackOrderUser));
    } else {
      this.subscribeToSaveResponse(this.trackOrderUserService.create(trackOrderUser));
    }
  }

  private createFromForm(): ITrackOrderUser {
    return {
      ...new TrackOrderUser(),
      id: this.editForm.get(['id'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITrackOrderUser>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
