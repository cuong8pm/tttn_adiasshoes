import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITrackOrderUser } from 'app/shared/model/track-order-user.model';
import { TrackOrderUserService } from './track-order-user.service';

@Component({
  templateUrl: './track-order-user-delete-dialog.component.html',
})
export class TrackOrderUserDeleteDialogComponent {
  trackOrderUser?: ITrackOrderUser;

  constructor(
    protected trackOrderUserService: TrackOrderUserService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.trackOrderUserService.delete(id).subscribe(() => {
      this.eventManager.broadcast('trackOrderUserListModification');
      this.activeModal.close();
    });
  }
}
