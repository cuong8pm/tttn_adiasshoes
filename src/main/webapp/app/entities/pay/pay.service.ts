import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IPay } from 'app/shared/model/pay.model';
import { IListCart } from '../../shared/model/list-cart.model';
import { ITotalPrice } from '../../shared/model/total-price.model';
import { IListCartDTO } from '../../shared/model/list-cart-dto.model';
import { IOrders } from '../../shared/model/orders.model';
import { IUser } from '../../core/user/user.model';

type EntityResponseType = HttpResponse<IPay>;
type EntityArrayResponseType = HttpResponse<IPay[]>;

@Injectable({ providedIn: 'root' })
export class PayService {
  public resourceUrl = SERVER_API_URL + 'api/pays';

  constructor(protected http: HttpClient) {}

  create(pay: IPay): Observable<EntityResponseType> {
    return this.http.post<IPay>(this.resourceUrl, pay, { observe: 'response' });
  }

  update(pay: IPay): Observable<EntityResponseType> {
    return this.http.put<IPay>(this.resourceUrl, pay, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPay>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPay[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  createOrder(listID: IListCartDTO[], req?: any): Observable<HttpResponse<boolean>> {
    const options = createRequestOption(req);
    return this.http.post<boolean>(`${this.resourceUrl}/create-order`, listID, { params: options, observe: 'response' });
  }

  createOrderUserNow(listID: IListCartDTO[], req?: any): Observable<HttpResponse<boolean>> {
    const options = createRequestOption(req);
    return this.http.post<boolean>(`${this.resourceUrl}/create-order-user-now`, listID, { params: options, observe: 'response' });
  }

  createOrderNow(listID: IListCartDTO[], req?: any): Observable<HttpResponse<boolean>> {
    const options = createRequestOption(req);
    return this.http.post<boolean>(`${this.resourceUrl}/create-order-now`, listID, { params: options, observe: 'response' });
  }

  getListCart(listID: any, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.post<IListCartDTO[]>(`${this.resourceUrl}/list-cart`, listID, { params: options, observe: 'response' });
  }
  getListProduct(listID: any, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.post<IListCartDTO[]>(`${this.resourceUrl}/list-product`, listID, { params: options, observe: 'response' });
  }

  getTotalPrice(listCart: IListCartDTO[]): Observable<HttpResponse<ITotalPrice>> {
    return this.http.post<ITotalPrice>(`${this.resourceUrl}/list-cart-total`, listCart, { observe: 'response' });
  }

  getUser(req?: any): Observable<HttpResponse<IUser | undefined>> {
    const options = createRequestOption(req);
    return this.http.get<IUser | undefined>(`${this.resourceUrl}/get-user`, { params: options, observe: 'response' });
  }
}
