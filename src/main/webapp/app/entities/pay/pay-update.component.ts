import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IPay, Pay } from 'app/shared/model/pay.model';
import { PayService } from './pay.service';

@Component({
  selector: 'jhi-pay-update',
  templateUrl: './pay-update.component.html',
})
export class PayUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
  });

  constructor(protected payService: PayService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ pay }) => {
      this.updateForm(pay);
    });
  }

  updateForm(pay: IPay): void {
    this.editForm.patchValue({
      id: pay.id,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const pay = this.createFromForm();
    if (pay.id !== undefined) {
      this.subscribeToSaveResponse(this.payService.update(pay));
    } else {
      this.subscribeToSaveResponse(this.payService.create(pay));
    }
  }

  private createFromForm(): IPay {
    return {
      ...new Pay(),
      id: this.editForm.get(['id'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPay>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
