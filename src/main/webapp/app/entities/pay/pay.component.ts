import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, ParamMap, Router, Data } from '@angular/router';
import { Subscription, combineLatest } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPay } from 'app/shared/model/pay.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { PayService } from './pay.service';
import { PayDeleteDialogComponent } from './pay-delete-dialog.component';
import { ListCartComponent } from '../list-cart/list-cart.component';
import { ListCartService } from '../list-cart/list-cart.service';
import { Account } from '../../core/user/account.model';
import { AccountService } from '../../core/auth/account.service';
import { IListCart } from '../../shared/model/list-cart.model';
import { ISize } from '../../shared/model/size.model';
import { IListCartDTO } from '../../shared/model/list-cart-dto.model';
import { ITotalPrice } from '../../shared/model/total-price.model';
import { IOrders } from '../../shared/model/orders.model';

@Component({
  selector: 'jhi-pay',
  templateUrl: './pay.component.html',
  styleUrls: ['pay.scss'],
})
export class PayComponent implements OnInit, OnDestroy {
  pays?: IPay[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  listID!: any;
  account!: Account;
  listCarts!: IListCartDTO[];
  totalPrice: ITotalPrice | null = null;
  messageOrder!: string;

  constructor(
    protected listCartService: ListCartService,
    protected payService: PayService,
    private accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.listCartService.currentData.subscribe(data => (this.listID = data));
    // if (this.listID === null) {
    this.accountService.identity().subscribe(account => {
      if (account) {
        this.account = account;
      }
    });
    this.payService
      .getListCart(this.listID, { username: this.account.login })
      .subscribe(
        (res: HttpResponse<IListCartDTO[]>) => (
          (this.listCarts = res.body || []),
          this.payService.getTotalPrice(this.listCarts).subscribe((res: HttpResponse<ITotalPrice>) => (this.totalPrice = res.body))
        )
      );
    // }
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IPay): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  delete(pay: IPay): void {
    const modalRef = this.modalService.open(PayDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.pay = pay;
  }

  createOrder(): void {
    let check = false;
    this.payService
      .createOrder(this.listCarts, { username: this.account.login })
      .subscribe((res: HttpResponse<boolean>) => (check = res.body || false));
    if ((check = true)) {
      this.messageOrder = 'abc';
    } else {
      this.messageOrder = '123';
    }
  }
  replace(price: number | undefined): String {
    const aaa = new String(price);
    return aaa.replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }
}
