import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HauiAdidasShoesSharedModule } from 'app/shared/shared.module';
import { PayComponent } from './pay.component';
import { PayDetailComponent } from './pay-detail.component';
import { PayUpdateComponent } from './pay-update.component';
import { PayDeleteDialogComponent } from './pay-delete-dialog.component';
import { payRoute } from './pay.route';

@NgModule({
  imports: [HauiAdidasShoesSharedModule, RouterModule.forChild(payRoute)],
  declarations: [PayComponent, PayDetailComponent, PayUpdateComponent, PayDeleteDialogComponent],
  entryComponents: [PayDeleteDialogComponent],
})
export class HauiAdidasShoesPayModule {}
