package com.haui.adidasshoes.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import org.hibernate.cache.jcache.ConfigSettings;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.info.GitProperties;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import io.github.jhipster.config.cache.PrefixedKeyGenerator;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {
    private GitProperties gitProperties;
    private BuildProperties buildProperties;
    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache = jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, com.haui.adidasshoes.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, com.haui.adidasshoes.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, com.haui.adidasshoes.domain.User.class.getName());
            createCache(cm, com.haui.adidasshoes.domain.Authority.class.getName());
            createCache(cm, com.haui.adidasshoes.domain.User.class.getName() + ".authorities");
            createCache(cm, com.haui.adidasshoes.domain.Category.class.getName());
            createCache(cm, com.haui.adidasshoes.domain.Product.class.getName());
            createCache(cm, com.haui.adidasshoes.domain.ProductImage.class.getName());
            createCache(cm, com.haui.adidasshoes.domain.ProductDetails.class.getName());
            createCache(cm, com.haui.adidasshoes.domain.Size.class.getName());
            createCache(cm, com.haui.adidasshoes.domain.Amount.class.getName());
            createCache(cm, com.haui.adidasshoes.domain.ProductImportHistory.class.getName());
            createCache(cm, com.haui.adidasshoes.domain.Orders.class.getName());
            createCache(cm, com.haui.adidasshoes.domain.TrackOrder.class.getName());
            createCache(cm, com.haui.adidasshoes.domain.OrderProduct.class.getName());
            createCache(cm, com.haui.adidasshoes.domain.Reviews.class.getName());
            createCache(cm, com.haui.adidasshoes.domain.ListProduct.class.getName());
            createCache(cm, com.haui.adidasshoes.domain.DetailProduct.class.getName());
            createCache(cm, com.haui.adidasshoes.domain.ListCart.class.getName());
            createCache(cm, com.haui.adidasshoes.domain.Pay.class.getName());
            createCache(cm, com.haui.adidasshoes.domain.OrderTracks.class.getName());
            createCache(cm, com.haui.adidasshoes.domain.TrackOrderUser.class.getName());
            createCache(cm, com.haui.adidasshoes.domain.OrderNow.class.getName());
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache == null) {
            cm.createCache(cacheName, jcacheConfiguration);
        }
    }

    @Autowired(required = false)
    public void setGitProperties(GitProperties gitProperties) {
        this.gitProperties = gitProperties;
    }

    @Autowired(required = false)
    public void setBuildProperties(BuildProperties buildProperties) {
        this.buildProperties = buildProperties;
    }

    @Bean
    public KeyGenerator keyGenerator() {
        return new PrefixedKeyGenerator(this.gitProperties, this.buildProperties);
    }
}
