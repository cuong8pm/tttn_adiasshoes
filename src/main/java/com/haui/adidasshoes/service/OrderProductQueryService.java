package com.haui.adidasshoes.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.haui.adidasshoes.domain.OrderProduct;
import com.haui.adidasshoes.domain.*; // for static metamodels
import com.haui.adidasshoes.repository.OrderProductRepository;
import com.haui.adidasshoes.service.dto.OrderProductCriteria;

/**
 * Service for executing complex queries for {@link OrderProduct} entities in the database.
 * The main input is a {@link OrderProductCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link OrderProduct} or a {@link Page} of {@link OrderProduct} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OrderProductQueryService extends QueryService<OrderProduct> {

    private final Logger log = LoggerFactory.getLogger(OrderProductQueryService.class);

    private final OrderProductRepository orderProductRepository;

    public OrderProductQueryService(OrderProductRepository orderProductRepository) {
        this.orderProductRepository = orderProductRepository;
    }

    /**
     * Return a {@link List} of {@link OrderProduct} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<OrderProduct> findByCriteria(OrderProductCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<OrderProduct> specification = createSpecification(criteria);
        return orderProductRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link OrderProduct} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<OrderProduct> findByCriteria(OrderProductCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<OrderProduct> specification = createSpecification(criteria);
        return orderProductRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(OrderProductCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<OrderProduct> specification = createSpecification(criteria);
        return orderProductRepository.count(specification);
    }

    /**
     * Function to convert {@link OrderProductCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<OrderProduct> createSpecification(OrderProductCriteria criteria) {
        Specification<OrderProduct> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), OrderProduct_.id));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), OrderProduct_.description));
            }
            if (criteria.getOrdersId() != null) {
                specification = specification.and(buildSpecification(criteria.getOrdersId(),
                    root -> root.join(OrderProduct_.orders, JoinType.LEFT).get(Orders_.id)));
            }
            if (criteria.getListCartId() != null) {
                specification = specification.and(buildSpecification(criteria.getListCartId(),
                    root -> root.join(OrderProduct_.listCart, JoinType.LEFT).get(ListCart_.id)));
            }
        }
        return specification;
    }
}
