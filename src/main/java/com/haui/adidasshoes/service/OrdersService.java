package com.haui.adidasshoes.service;

import com.haui.adidasshoes.domain.*;
import com.haui.adidasshoes.repository.*;
import com.haui.adidasshoes.service.dto.ListCartDTO;
import com.haui.adidasshoes.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Orders}.
 */
@Service
@Transactional
public class OrdersService {

    private final Logger log = LoggerFactory.getLogger(OrdersService.class);

    private final OrdersRepository ordersRepository;

    private static final String ENTITY_NAME = "amount";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ListCartRepository listCartRepository;

    @Autowired
    private AmountRepository amountRepository;

    @Autowired
    private OrderProductRepository orderProductRepository;

    @Autowired
    private  TrackOrderRepository trackOrderRepository;

    public OrdersService(OrdersRepository ordersRepository) {
        this.ordersRepository = ordersRepository;
    }

    public Orders save(Orders orders) {
        log.debug("Request to save Orders : {}", orders);
        if(orders.isConfirm()){
            TrackOrder trackOrder = trackOrderRepository.findByOrderID(orders.getId());
            trackOrder.setOrderConfirm(true);
            trackOrderRepository.save(trackOrder);
        }else {
            TrackOrder trackOrder = trackOrderRepository.findByOrderID(orders.getId());
            trackOrder.setOrderConfirm(false);
            trackOrderRepository.save(trackOrder);
        }
        return ordersRepository.save(orders);
    }

    @Transactional(readOnly = true)
    public Page<Orders> findAll(Pageable pageable) {
        log.debug("Request to get all Orders");
        return ordersRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Optional<Orders> findOne(Long id) {
        log.debug("Request to get Orders : {}", id);
        return ordersRepository.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete Orders : {}", id);
        ordersRepository.deleteById(id);
    }

    public Orders saveOrder(String username, List<ListCartDTO> listCartDTOS) {
        User user = userRepository.findOneByUsername(username);
        LocalDate date = LocalDate.now();
        Double totalOrderPrice = (double) 0;
        for (ListCartDTO item: listCartDTOS) {
            totalOrderPrice+=item.getTotalPrice();
            Amount amount = amountRepository.findByProductIdAndSize(item.getProduct().getId(),item.getSize().getName());
            if(amount==null || item.getAmount() > amount.getInventory()){
                throw new BadRequestAlertException("Sản phẩm đã hết hàng", ENTITY_NAME, "outofproduct");
            }
        }
        totalOrderPrice= totalOrderPrice + 30000;
        //add order
        Orders orders = new Orders();
        orders.setUser(user);
        orders.setConfirm(false);
        orders.setDescripstion("Add ngay "+date);
        orders.setTotalOrderPrice(totalOrderPrice);
        orders.setOrderDate(date);
        orders.setDeliveryDateFrom(date.plusDays(2));
        orders.setDeliveryDateTo(date.plusDays(4));
        Orders newOrder = ordersRepository.save(orders);

        //chuyen List<listcartDTO> -> List<listcart>
        List<ListCart> listCarts = new ArrayList<ListCart>();
        for (ListCartDTO item: listCartDTOS) {
            ListCart listCart = new ListCart();
            listCart.setId(item.getId());
            listCart.setProduct(item.getProduct());
            listCart.setSize(item.getSize());
            listCart.setCheckReview(false);
            listCart.setAmount(item.getAmount());
            listCart.setUser(user);
            listCart.setDescription("cc");
            listCarts.add(listCart);
            //xoa cart khoi gio hang
            ListCart listCart1 = listCartRepository.findOneByID(item.getId());
            listCart1.setCheckDisplay(false);
            listCartRepository.save(listCart1);
        }

        //add orderProduct
        for (ListCart item: listCarts) {
            OrderProduct orderProduct = new OrderProduct();
            orderProduct.setOrders(newOrder);
            orderProduct.setDescription("Add ngay "+date);
            orderProduct.setListCart(item);
            orderProductRepository.save(orderProduct);
            //tru san pham khi da khach hang thanh toan
            Amount amount= amountRepository.findByProductIdAndSize(item.getProduct().getId(),item.getSize().getName());
            amount.setInventory(amount.getInventory()-item.getAmount());
            amount.setSoldProduct(amount.getSoldProduct()+item.getAmount());
            amountRepository.save(amount);
        }

        //add trackOrder
        TrackOrder trackOrder = new TrackOrder();
        trackOrder.setOrders(newOrder);
        trackOrder.setStatus(true);
        trackOrder.setOrderConfirm(false);
        trackOrder.setWaittingProduct(false);
        trackOrder.setWaittingProduct(false);
        trackOrder.setShipping(false);
        trackOrder.setDelivered(false);
        trackOrderRepository.save(trackOrder);

        return newOrder;
    }

    public List<Orders> findByUsernameAndOrderConfirm(String username) {
        return ordersRepository.findByUsernameAndOrderConfirm(username);
    }

    public List<Orders> findByUsernameAndWaittingProduct(String username) {
        return ordersRepository.findByUsernameAndWaittingProduct(username);
    }

    public List<Orders> findByUsernameAndShipping(String username) {
        return ordersRepository.findByUsernameAndShipping(username);
    }

    public List<Orders> findByUsernameAnddelivered(String username) {
        return ordersRepository.findByUsernameAnddelivered(username);
    }

    public List<Orders> findByUsernameAndStatus(String username) {
        return ordersRepository.findByUsernameAndStatus(username);
    }

    public Orders saveOrderNow(String username, List<ListCartDTO> listCartDTOS) {
        User user = userRepository.findOneByUsername(username);
        LocalDate date = LocalDate.now();
        Double totalOrderPrice = (double) 0;
        for (ListCartDTO item: listCartDTOS) {
            totalOrderPrice+=item.getTotalPrice();
            Amount amount = amountRepository.findByProductIdAndSize(item.getProduct().getId(),item.getSize().getName());
            if( amount == null || item.getAmount() > amount.getInventory()){
                throw new BadRequestAlertException("Sản phẩm đã hết hàng", ENTITY_NAME, "outofproduct");
            }
        }
        totalOrderPrice= totalOrderPrice + 30000;
        //add order
        Orders orders = new Orders();
        orders.setUser(user);
        orders.setConfirm(false);
        orders.setDescripstion("Add ngay "+date);
        orders.setTotalOrderPrice(totalOrderPrice);
        orders.setOrderDate(date);
        orders.setDeliveryDateFrom(date.plusDays(2));
        orders.setDeliveryDateTo(date.plusDays(4));
        Orders newOrder = ordersRepository.save(orders);

        //chuyen List<listcartDTO> -> List<listcart>
        List<ListCart> listCarts = new ArrayList<ListCart>();
        for (ListCartDTO item: listCartDTOS) {
            ListCart listCart = new ListCart();
            listCart.setId(item.getId());
            listCart.setProduct(item.getProduct());
            listCart.setSize(item.getSize());
            listCart.setAmount(item.getAmount());
            listCart.setCheckReview(false);
            listCart.setUser(user);
            listCart.setDescription("cc");
            listCarts.add(listCart);
            listCart = listCartRepository.save(listCart);
            //xoa cart khoi gio hang
            ListCart listCart1 = listCartRepository.findOneByID(listCart.getId());
            listCart1.setCheckDisplay(false);
            listCartRepository.save(listCart1);
        }

        //add orderProduct
        for (ListCart item: listCarts) {
            OrderProduct orderProduct = new OrderProduct();
            orderProduct.setOrders(newOrder);
            orderProduct.setDescription("Add ngay "+date);
            orderProduct.setListCart(item);
            orderProductRepository.save(orderProduct);
            //tru san pham khi da khach hang thanh toan
            Amount amount= amountRepository.findByProductIdAndSize(item.getProduct().getId(),item.getSize().getName());
            amount.setInventory(amount.getInventory()-item.getAmount());
            amount.setSoldProduct(amount.getSoldProduct()+item.getAmount());
            amountRepository.save(amount);
        }

        //add trackOrder
        TrackOrder trackOrder = new TrackOrder();
        trackOrder.setOrders(newOrder);
        trackOrder.setStatus(true);
        trackOrder.setOrderConfirm(false);
        trackOrder.setWaittingProduct(false);
        trackOrder.setWaittingProduct(false);
        trackOrder.setShipping(false);
        trackOrder.setDelivered(false);
        trackOrderRepository.save(trackOrder);

        return newOrder;
    }
}
