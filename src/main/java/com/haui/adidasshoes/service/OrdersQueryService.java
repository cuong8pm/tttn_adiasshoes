package com.haui.adidasshoes.service;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.criteria.JoinType;

import com.haui.adidasshoes.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.haui.adidasshoes.domain.Orders;
import com.haui.adidasshoes.domain.*; // for static metamodels
import com.haui.adidasshoes.repository.OrdersRepository;
import com.haui.adidasshoes.service.dto.OrdersCriteria;

/**
 * Service for executing complex queries for {@link Orders} entities in the database.
 * The main input is a {@link OrdersCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Orders} or a {@link Page} of {@link Orders} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OrdersQueryService extends QueryService<Orders> {

    private static final String ENTITY_NAME = "ORDER";
    private final Logger log = LoggerFactory.getLogger(OrdersQueryService.class);

    private final OrdersRepository ordersRepository;

    public OrdersQueryService(OrdersRepository ordersRepository) {
        this.ordersRepository = ordersRepository;
    }

    /**
     * Return a {@link List} of {@link Orders} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Orders> findByCriteria(OrdersCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Orders> specification = createSpecification(criteria);
        return ordersRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Orders} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Orders> findByCriteria(OrdersCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Orders> specification = createSpecification(criteria);
        return ordersRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(OrdersCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Orders> specification = createSpecification(criteria);
        return ordersRepository.count(specification);
    }

    /**
     * Function to convert {@link OrdersCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Orders> createSpecification(OrdersCriteria criteria) {
        Specification<Orders> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Orders_.id));
            }
            if (criteria.getConfirm() != null) {
                specification = specification.and(buildSpecification(criteria.getConfirm(), Orders_.confirm));
            }
            if (criteria.getDescripstion() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescripstion(), Orders_.descripstion));
            }
            if (criteria.getTotalOrderPrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalOrderPrice(), Orders_.totalOrderPrice));
            }
            if (criteria.getOrderDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getOrderDate(), Orders_.orderDate));
            }
            if (criteria.getDeliveryDateFrom() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDeliveryDateFrom(), Orders_.deliveryDateFrom));
            }
            if (criteria.getDeliveryDateTo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDeliveryDateTo(), Orders_.deliveryDateTo));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(Orders_.user, JoinType.LEFT).get(User_.id)));
            }
        }
        return specification;
    }

    public Page<Orders> findByConfirm(String id, String orderDate,String confirm, Pageable pageable) {

        Long idOrder = Long.parseLong("0");

        try {
            idOrder = Long.parseLong(id);
        }catch (Exception e){
            throw new BadRequestAlertException("Id khong dung", ENTITY_NAME, "outofproduct");
        }
        if(idOrder !=0){
            return ordersRepository.getOrderById(idOrder,pageable);
        }

        if(!orderDate.equals("0")) {
            try {
                LocalDate a = LocalDate.parse(orderDate);
                return ordersRepository.getOrderByOrderDate(a,pageable);
            } catch (Exception e) {
                throw new BadRequestAlertException("Ngay khong dung dinh dang", ENTITY_NAME, "outofproduct");
            }
        }

        if(confirm.equals("1")){
            return ordersRepository.getallOrderNowConfirm(pageable);
        }else if (confirm.equals("0")){
            return ordersRepository.getallOrderNowNotConfirm(pageable);
        }else {
            return ordersRepository.getall(pageable);
        }

    }
}
