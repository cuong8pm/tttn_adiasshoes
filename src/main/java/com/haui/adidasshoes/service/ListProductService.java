package com.haui.adidasshoes.service;

import com.haui.adidasshoes.domain.ListProduct;
import com.haui.adidasshoes.domain.Product;
import com.haui.adidasshoes.repository.ListProductRepository;
import com.haui.adidasshoes.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ListProduct}.
 */
@Service
@Transactional
public class ListProductService {

    private final Logger log = LoggerFactory.getLogger(ListProductService.class);

    @Autowired
    private ProductRepository productRepository;

    private final ListProductRepository listProductRepository;

    public ListProductService(ListProductRepository listProductRepository) {
        this.listProductRepository = listProductRepository;
    }

    public ListProduct save(ListProduct listProduct) {
        log.debug("Request to save ListProduct : {}", listProduct);
        return listProductRepository.save(listProduct);
    }

    @Transactional(readOnly = true)
    public Page<ListProduct> findAll(Pageable pageable) {
        log.debug("Request to get all ListProducts");
        return listProductRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Page<Product> findAllWithPage(Long type,String name,String toPrice,String fromPrice, Pageable pageable) {
        log.debug("Request to get all ListProducts");
        if(type==0)
            type=null;
        if(name.equals("null")){
            name=null;
        }
        Double newToPrice,newFromDouble;
        try {
            newToPrice =  Double.parseDouble(toPrice);
        }catch (Exception e){
            newToPrice=null;
        };
        try {
            newFromDouble =  Double.parseDouble(fromPrice);
        }catch (Exception e){
            newFromDouble=null;
        };
        return listProductRepository.findAllWithPage(type,name,newToPrice,newFromDouble,pageable);
    }


    @Transactional(readOnly = true)
    public Optional<ListProduct> findOne(Long id) {
        log.debug("Request to get ListProduct : {}", id);
        return listProductRepository.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete ListProduct : {}", id);
        listProductRepository.deleteById(id);
    }

    public List<Product> findAllByName(String name) {
        return productRepository.findAllByName(name);
    }
}
