package com.haui.adidasshoes.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.haui.adidasshoes.domain.Amount;
import com.haui.adidasshoes.domain.*; // for static metamodels
import com.haui.adidasshoes.repository.AmountRepository;
import com.haui.adidasshoes.service.dto.AmountCriteria;

/**
 * Service for executing complex queries for {@link Amount} entities in the database.
 * The main input is a {@link AmountCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Amount} or a {@link Page} of {@link Amount} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AmountQueryService extends QueryService<Amount> {

    private final Logger log = LoggerFactory.getLogger(AmountQueryService.class);

    private final AmountRepository amountRepository;

    public AmountQueryService(AmountRepository amountRepository) {
        this.amountRepository = amountRepository;
    }

    /**
     * Return a {@link List} of {@link Amount} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Amount> findByCriteria(AmountCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Amount> specification = createSpecification(criteria);
        return amountRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Amount} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Amount> findByProductId(Long productID, Pageable page) {
        log.debug("find by criteria : {}, page: {}",page);
        return amountRepository.findAllByProductId(productID,page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AmountCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Amount> specification = createSpecification(criteria);
        return amountRepository.count(specification);
    }

    /**
     * Function to convert {@link AmountCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Amount> createSpecification(AmountCriteria criteria) {
        Specification<Amount> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Amount_.id));
            }
            if (criteria.getTotal() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotal(), Amount_.total));
            }
            if (criteria.getInventory() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getInventory(), Amount_.inventory));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Amount_.description));
            }
            if (criteria.getSoldProduct() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoldProduct(), Amount_.soldProduct));
            }
            if (criteria.getProductId() != null) {
                specification = specification.and(buildSpecification(criteria.getProductId(),
                    root -> root.join(Amount_.product, JoinType.LEFT).get(Product_.id)));
            }
            if (criteria.getSizeId() != null) {
                specification = specification.and(buildSpecification(criteria.getSizeId(),
                    root -> root.join(Amount_.size, JoinType.LEFT).get(Size_.id)));
            }
        }
        return specification;
    }
}
