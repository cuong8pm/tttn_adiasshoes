package com.haui.adidasshoes.service;

import com.haui.adidasshoes.domain.ListCart;
import com.haui.adidasshoes.domain.OrderProduct;
import com.haui.adidasshoes.domain.ProductDetails;
import com.haui.adidasshoes.domain.Reviews;
import com.haui.adidasshoes.repository.*;
import com.haui.adidasshoes.service.dto.Statistical;
import com.haui.adidasshoes.service.dto.StatisticalDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;

/**
 * Service Implementation for managing {@link Reviews}.
 */
@Service
@Transactional
public class ReviewsService {

    private final Logger log = LoggerFactory.getLogger(ReviewsService.class);

    @Autowired
    private OrderProductRepository orderProductRepository;

    @Autowired
    private ListCartRepository listCartRepository;

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private OrderNowRepository orderNowRepository;

    private final ReviewsRepository reviewsRepository;

    public ReviewsService(ReviewsRepository reviewsRepository) {
        this.reviewsRepository = reviewsRepository;
    }

    public Reviews save(Reviews reviews) {
        log.debug("Request to save Reviews : {}", reviews);
        return reviewsRepository.save(reviews);
    }

    @Transactional(readOnly = true)
    public Page<Reviews> findAll(Pageable pageable) {
        log.debug("Request to get all Reviews");
        return reviewsRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Optional<Reviews> findOne(Long id) {
        log.debug("Request to get Reviews : {}", id);
        return reviewsRepository.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete Reviews : {}", id);
        reviewsRepository.deleteById(id);
    }

    public List<Reviews> findAllById(Long id) {
        return reviewsRepository.findAllById(id);
    }

    public Boolean createReview(Long orderId,Long listCartID, String textReview) {
        Reviews reviews = new Reviews();
        reviews.setDecription(textReview);
        OrderProduct orderProduct = orderProductRepository.findByOrderID(orderId,listCartID);
        reviews.setOrderProduct(orderProduct);
        Reviews reviews1 = reviewsRepository.save(reviews);

        //update bien check review sang true
        ListCart listCart = listCartRepository.findOneByID(listCartID);
        listCart.setCheckReview(true);
        listCartRepository.save(listCart);

        return  true;
    }

    public Map<String, Integer> getStatisticalNumber() {
        Map<String, Integer> result = new HashMap<String, Integer>();

        //get order new
        Integer orderNewNumber=0;
        orderNewNumber = ordersRepository.getOrderNew();
        result.put("orderNew",orderNewNumber);

        //get order now new
        Integer orderNowNewNumber=0;
        orderNowNewNumber = orderNowRepository.getOrderNowNew();
        result.put("orderNowNew",orderNowNewNumber);

        //get revenue 1 thang
        Integer revenue=0;
        Integer revenueUserOrder=0;
        Integer revenueOrderNow=0;
        revenueUserOrder = ordersRepository.getRevenueInOneDay();
        revenueOrderNow = orderNowRepository.getRevenueInMonth();
        if(revenueUserOrder==null) revenueUserOrder=0;
        if(revenueOrderNow==null) revenueOrderNow=0;
        revenue=revenueUserOrder+revenueOrderNow;
        result.put("revenue",revenue);

        //get product 1 thang
        Integer productNumber=0;
        Integer productNumberOrderUser=0;
        Integer productNumberNow=0;
        productNumberOrderUser = ordersRepository.getProductNumberInMonth();
        productNumberNow = orderNowRepository.getProductNumberNowInMonth();
        if(productNumberOrderUser==null) productNumberOrderUser=0;
        if(productNumberNow==null) productNumberNow=0;
        productNumber = productNumberNow + productNumberOrderUser;
        result.put("product",productNumber);

        return  result;
    }

    public List<StatisticalDTO> getStatistical() {
        List<StatisticalDTO> statisticalList = ordersRepository.getStatistical();

        return statisticalList;
    }
}
