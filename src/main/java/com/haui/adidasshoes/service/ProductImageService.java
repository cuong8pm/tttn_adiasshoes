package com.haui.adidasshoes.service;

import com.haui.adidasshoes.domain.ProductImage;
import com.haui.adidasshoes.repository.ProductImageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ProductImage}.
 */
@Service
@Transactional
public class ProductImageService {

    private final Logger log = LoggerFactory.getLogger(ProductImageService.class);

    private final ProductImageRepository productImageRepository;

    public ProductImageService(ProductImageRepository productImageRepository) {
        this.productImageRepository = productImageRepository;
    }

    /**
     * Save a productImage.
     *
     * @param productImage the entity to save.
     * @return the persisted entity.
     */
    public ProductImage save(ProductImage productImage) {
        log.debug("Request to save ProductImage : {}", productImage);
        return productImageRepository.save(productImage);
    }

    /**
     * Get all the productImages.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ProductImage> findAll(Pageable pageable) {
        log.debug("Request to get all ProductImages");
        return productImageRepository.findAll(pageable);
    }


    /**
     * Get one productImage by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ProductImage> findOne(Long id) {
        log.debug("Request to get ProductImage : {}", id);
        return productImageRepository.findById(id);
    }

    /**
     * Delete the productImage by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ProductImage : {}", id);
        productImageRepository.deleteById(id);
    }

    public List<ProductImage> findAllById(Long id) {
        return productImageRepository.findAllById(id);
    }
}
