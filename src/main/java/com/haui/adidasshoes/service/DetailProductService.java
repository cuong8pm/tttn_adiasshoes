package com.haui.adidasshoes.service;

import com.haui.adidasshoes.domain.DetailProduct;
import com.haui.adidasshoes.repository.DetailProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link DetailProduct}.
 */
@Service
@Transactional
public class DetailProductService {

    private final Logger log = LoggerFactory.getLogger(DetailProductService.class);

    private final DetailProductRepository detailProductRepository;

    public DetailProductService(DetailProductRepository detailProductRepository) {
        this.detailProductRepository = detailProductRepository;
    }

    /**
     * Save a detailProduct.
     *
     * @param detailProduct the entity to save.
     * @return the persisted entity.
     */
    public DetailProduct save(DetailProduct detailProduct) {
        log.debug("Request to save DetailProduct : {}", detailProduct);
        return detailProductRepository.save(detailProduct);
    }

    /**
     * Get all the detailProducts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<DetailProduct> findAll(Pageable pageable) {
        log.debug("Request to get all DetailProducts");
        return detailProductRepository.findAll(pageable);
    }


    /**
     * Get one detailProduct by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<DetailProduct> findOne(Long id) {
        log.debug("Request to get DetailProduct : {}", id);
        return detailProductRepository.findById(id);
    }

    /**
     * Delete the detailProduct by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete DetailProduct : {}", id);
        detailProductRepository.deleteById(id);
    }
}
