package com.haui.adidasshoes.service;

import com.haui.adidasshoes.domain.Amount;
import com.haui.adidasshoes.domain.ProductImportHistory;
import com.haui.adidasshoes.repository.AmountRepository;
import com.haui.adidasshoes.repository.ProductImportHistoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AmountService {

    private final Logger log = LoggerFactory.getLogger(AmountService.class);

    @Autowired
    private  ProductImportHistoryRepository productImportHistoryRepository;

    private final AmountRepository amountRepository;

    public AmountService(AmountRepository amountRepository) {
        this.amountRepository = amountRepository;
    }

    public Amount save(Amount amount) {
        log.debug("Request to save Amount : {}", amount);
        return amountRepository.save(amount);
    }

    public Amount update(Amount amount){
        Amount oldAmount = amountRepository.findOneById(amount.getId());
        if(oldAmount.getTotal() > amount.getTotal()){
            int minusAmount = oldAmount.getTotal()-amount.getTotal();

            amount.setInventory(oldAmount.getInventory()-minusAmount);

            ProductImportHistory productImportHistory = new ProductImportHistory();
            productImportHistory.setAmount(oldAmount);
            productImportHistory.setDescription("Trừ "+minusAmount+" sản phẩm");
            productImportHistoryRepository.save(productImportHistory);
        }else {
            int plusAmount = amount.getTotal()-oldAmount.getTotal();

            amount.setInventory(oldAmount.getInventory()+plusAmount);

            ProductImportHistory productImportHistory = new ProductImportHistory();
            productImportHistory.setAmount(oldAmount);
            productImportHistory.setDescription("Cộng "+plusAmount+" sản phẩm");
            productImportHistoryRepository.save(productImportHistory);

        }
        return amountRepository.save(amount);
    }

    @Transactional(readOnly = true)
    public Page<Amount> findAll(Pageable pageable) {
        log.debug("Request to get all Amounts");
        return amountRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Optional<Amount> findOne(Long id) {
        log.debug("Request to get Amount : {}", id);
        return amountRepository.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete Amount : {}", id);
        amountRepository.deleteById(id);
    }
    public Amount findByProductIdAndSize(Long id, String size) {
        return amountRepository.findByProductIdAndSize(id,size);
    }

    public List<Amount> findAllAmount() {
        return amountRepository.findAll();
    }
}
