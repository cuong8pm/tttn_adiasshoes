package com.haui.adidasshoes.service;

import com.haui.adidasshoes.domain.OrderTracks;
import com.haui.adidasshoes.repository.OrderTracksRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link OrderTracks}.
 */
@Service
@Transactional
public class OrderTracksService {

    private final Logger log = LoggerFactory.getLogger(OrderTracksService.class);

    private final OrderTracksRepository orderTracksRepository;

    public OrderTracksService(OrderTracksRepository orderTracksRepository) {
        this.orderTracksRepository = orderTracksRepository;
    }

    /**
     * Save a orderTracks.
     *
     * @param orderTracks the entity to save.
     * @return the persisted entity.
     */
    public OrderTracks save(OrderTracks orderTracks) {
        log.debug("Request to save OrderTracks : {}", orderTracks);
        return orderTracksRepository.save(orderTracks);
    }

    /**
     * Get all the orderTracks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<OrderTracks> findAll(Pageable pageable) {
        log.debug("Request to get all OrderTracks");
        return orderTracksRepository.findAll(pageable);
    }


    /**
     * Get one orderTracks by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<OrderTracks> findOne(Long id) {
        log.debug("Request to get OrderTracks : {}", id);
        return orderTracksRepository.findById(id);
    }

    /**
     * Delete the orderTracks by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete OrderTracks : {}", id);
        orderTracksRepository.deleteById(id);
    }
}
