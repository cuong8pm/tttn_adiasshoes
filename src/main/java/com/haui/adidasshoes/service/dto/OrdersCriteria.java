package com.haui.adidasshoes.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.haui.adidasshoes.domain.Orders} entity. This class is used
 * in {@link com.haui.adidasshoes.web.rest.OrdersResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /orders?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class OrdersCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter confirm;

    private StringFilter descripstion;

    private DoubleFilter totalOrderPrice;

    private LocalDateFilter orderDate;

    private LocalDateFilter deliveryDateFrom;

    private LocalDateFilter deliveryDateTo;

    private LongFilter userId;

    public OrdersCriteria() {
    }

    public OrdersCriteria(OrdersCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.confirm = other.confirm == null ? null : other.confirm.copy();
        this.descripstion = other.descripstion == null ? null : other.descripstion.copy();
        this.totalOrderPrice = other.totalOrderPrice == null ? null : other.totalOrderPrice.copy();
        this.orderDate = other.orderDate == null ? null : other.orderDate.copy();
        this.deliveryDateFrom = other.deliveryDateFrom == null ? null : other.deliveryDateFrom.copy();
        this.deliveryDateTo = other.deliveryDateTo == null ? null : other.deliveryDateTo.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
    }

    @Override
    public OrdersCriteria copy() {
        return new OrdersCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getConfirm() {
        return confirm;
    }

    public void setConfirm(BooleanFilter confirm) {
        this.confirm = confirm;
    }

    public StringFilter getDescripstion() {
        return descripstion;
    }

    public void setDescripstion(StringFilter descripstion) {
        this.descripstion = descripstion;
    }

    public DoubleFilter getTotalOrderPrice() {
        return totalOrderPrice;
    }

    public void setTotalOrderPrice(DoubleFilter totalOrderPrice) {
        this.totalOrderPrice = totalOrderPrice;
    }

    public LocalDateFilter getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateFilter orderDate) {
        this.orderDate = orderDate;
    }

    public LocalDateFilter getDeliveryDateFrom() {
        return deliveryDateFrom;
    }

    public void setDeliveryDateFrom(LocalDateFilter deliveryDateFrom) {
        this.deliveryDateFrom = deliveryDateFrom;
    }

    public LocalDateFilter getDeliveryDateTo() {
        return deliveryDateTo;
    }

    public void setDeliveryDateTo(LocalDateFilter deliveryDateTo) {
        this.deliveryDateTo = deliveryDateTo;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final OrdersCriteria that = (OrdersCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(confirm, that.confirm) &&
            Objects.equals(descripstion, that.descripstion) &&
            Objects.equals(totalOrderPrice, that.totalOrderPrice) &&
            Objects.equals(orderDate, that.orderDate) &&
            Objects.equals(deliveryDateFrom, that.deliveryDateFrom) &&
            Objects.equals(deliveryDateTo, that.deliveryDateTo) &&
            Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        confirm,
        descripstion,
        totalOrderPrice,
        orderDate,
        deliveryDateFrom,
        deliveryDateTo,
        userId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OrdersCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (confirm != null ? "confirm=" + confirm + ", " : "") +
                (descripstion != null ? "descripstion=" + descripstion + ", " : "") +
                (totalOrderPrice != null ? "totalOrderPrice=" + totalOrderPrice + ", " : "") +
                (orderDate != null ? "orderDate=" + orderDate + ", " : "") +
                (deliveryDateFrom != null ? "deliveryDateFrom=" + deliveryDateFrom + ", " : "") +
                (deliveryDateTo != null ? "deliveryDateTo=" + deliveryDateTo + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
            "}";
    }

}
