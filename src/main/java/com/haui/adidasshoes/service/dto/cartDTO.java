package com.haui.adidasshoes.service.dto;

import com.haui.adidasshoes.domain.Product;

public class cartDTO {
    private Long id;
    private Integer amount;
    private Boolean checkReview;
    private Boolean checkDisplay;
    private String description;
    private String username;
    private Product product;
    private String size;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Boolean getCheckReview() {
        return checkReview;
    }

    public void setCheckReview(Boolean checkReview) {
        this.checkReview = checkReview;
    }

    public Boolean getCheckDisplay() {
        return checkDisplay;
    }

    public void setCheckDisplay(Boolean checkDisplay) {
        this.checkDisplay = checkDisplay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
