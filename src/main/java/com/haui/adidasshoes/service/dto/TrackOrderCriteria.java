package com.haui.adidasshoes.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.haui.adidasshoes.domain.TrackOrder} entity. This class is used
 * in {@link com.haui.adidasshoes.web.rest.TrackOrderResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /track-orders?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TrackOrderCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter orderConfirm;

    private BooleanFilter waittingProduct;

    private BooleanFilter shipping;

    private BooleanFilter delivered;

    private BooleanFilter status;

    private LongFilter ordersId;

    public TrackOrderCriteria() {
    }

    public TrackOrderCriteria(TrackOrderCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.orderConfirm = other.orderConfirm == null ? null : other.orderConfirm.copy();
        this.waittingProduct = other.waittingProduct == null ? null : other.waittingProduct.copy();
        this.shipping = other.shipping == null ? null : other.shipping.copy();
        this.delivered = other.delivered == null ? null : other.delivered.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.ordersId = other.ordersId == null ? null : other.ordersId.copy();
    }

    @Override
    public TrackOrderCriteria copy() {
        return new TrackOrderCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getOrderConfirm() {
        return orderConfirm;
    }

    public void setOrderConfirm(BooleanFilter orderConfirm) {
        this.orderConfirm = orderConfirm;
    }

    public BooleanFilter getWaittingProduct() {
        return waittingProduct;
    }

    public void setWaittingProduct(BooleanFilter waittingProduct) {
        this.waittingProduct = waittingProduct;
    }

    public BooleanFilter getShipping() {
        return shipping;
    }

    public void setShipping(BooleanFilter shipping) {
        this.shipping = shipping;
    }

    public BooleanFilter getDelivered() {
        return delivered;
    }

    public void setDelivered(BooleanFilter delivered) {
        this.delivered = delivered;
    }

    public BooleanFilter getStatus() {
        return status;
    }

    public void setStatus(BooleanFilter status) {
        this.status = status;
    }

    public LongFilter getOrdersId() {
        return ordersId;
    }

    public void setOrdersId(LongFilter ordersId) {
        this.ordersId = ordersId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TrackOrderCriteria that = (TrackOrderCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(orderConfirm, that.orderConfirm) &&
            Objects.equals(waittingProduct, that.waittingProduct) &&
            Objects.equals(shipping, that.shipping) &&
            Objects.equals(delivered, that.delivered) &&
            Objects.equals(status, that.status) &&
            Objects.equals(ordersId, that.ordersId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        orderConfirm,
        waittingProduct,
        shipping,
        delivered,
        status,
        ordersId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TrackOrderCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (orderConfirm != null ? "orderConfirm=" + orderConfirm + ", " : "") +
                (waittingProduct != null ? "waittingProduct=" + waittingProduct + ", " : "") +
                (shipping != null ? "shipping=" + shipping + ", " : "") +
                (delivered != null ? "delivered=" + delivered + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (ordersId != null ? "ordersId=" + ordersId + ", " : "") +
            "}";
    }

}
