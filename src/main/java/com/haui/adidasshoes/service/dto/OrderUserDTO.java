package com.haui.adidasshoes.service.dto;

import com.haui.adidasshoes.domain.ListCart;
import com.haui.adidasshoes.domain.OrderProduct;
import com.haui.adidasshoes.domain.Orders;
import com.haui.adidasshoes.domain.TrackOrder;

import java.util.List;

public class OrderUserDTO {
    private Orders orders;
    private TrackOrder trackOrder;
    private List<ListCart> listCarts;

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    public TrackOrder getTrackOrder() {
        return trackOrder;
    }

    public void setTrackOrder(TrackOrder trackOrder) {
        this.trackOrder = trackOrder;
    }

    public List<ListCart> getListCarts() {
        return listCarts;
    }

    public void setListCarts(List<ListCart> listCarts) {
        this.listCarts = listCarts;
    }
}
