package com.haui.adidasshoes.service.dto;

import java.time.LocalDate;

public interface StatisticalDTO {
    String getState();
    Double getYear1998();
    Double getYear2001();
    Double getYear2004();
}
