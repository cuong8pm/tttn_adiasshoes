package com.haui.adidasshoes.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.haui.adidasshoes.domain.Reviews} entity. This class is used
 * in {@link com.haui.adidasshoes.web.rest.ReviewsResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /reviews?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ReviewsCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter rating;

    private StringFilter decription;

    private LongFilter orderProductId;

    public ReviewsCriteria() {
    }

    public ReviewsCriteria(ReviewsCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.rating = other.rating == null ? null : other.rating.copy();
        this.decription = other.decription == null ? null : other.decription.copy();
        this.orderProductId = other.orderProductId == null ? null : other.orderProductId.copy();
    }

    @Override
    public ReviewsCriteria copy() {
        return new ReviewsCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getRating() {
        return rating;
    }

    public void setRating(IntegerFilter rating) {
        this.rating = rating;
    }

    public StringFilter getDecription() {
        return decription;
    }

    public void setDecription(StringFilter decription) {
        this.decription = decription;
    }

    public LongFilter getOrderProductId() {
        return orderProductId;
    }

    public void setOrderProductId(LongFilter orderProductId) {
        this.orderProductId = orderProductId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ReviewsCriteria that = (ReviewsCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(rating, that.rating) &&
            Objects.equals(decription, that.decription) &&
            Objects.equals(orderProductId, that.orderProductId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        rating,
        decription,
        orderProductId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ReviewsCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (rating != null ? "rating=" + rating + ", " : "") +
                (decription != null ? "decription=" + decription + ", " : "") +
                (orderProductId != null ? "orderProductId=" + orderProductId + ", " : "") +
            "}";
    }

}
