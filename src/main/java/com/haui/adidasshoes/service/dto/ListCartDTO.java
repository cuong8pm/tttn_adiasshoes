package com.haui.adidasshoes.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.haui.adidasshoes.domain.Product;
import com.haui.adidasshoes.domain.Size;
import com.haui.adidasshoes.domain.User;

import javax.persistence.*;

public class ListCartDTO {
    private Long id;

    private Integer amount;

    private String description;

    private User user;

    private Product product;

    private Size size;

    private double totalPrice;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }
}
