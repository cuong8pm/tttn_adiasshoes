package com.haui.adidasshoes.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.haui.adidasshoes.domain.Amount} entity. This class is used
 * in {@link com.haui.adidasshoes.web.rest.AmountResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /amounts?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AmountCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter total;

    private IntegerFilter inventory;

    private StringFilter description;

    private IntegerFilter soldProduct;

    private LongFilter productId;

    private LongFilter sizeId;

    public AmountCriteria() {
    }

    public AmountCriteria(AmountCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.total = other.total == null ? null : other.total.copy();
        this.inventory = other.inventory == null ? null : other.inventory.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.soldProduct = other.soldProduct == null ? null : other.soldProduct.copy();
        this.productId = other.productId == null ? null : other.productId.copy();
        this.sizeId = other.sizeId == null ? null : other.sizeId.copy();
    }

    @Override
    public AmountCriteria copy() {
        return new AmountCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getTotal() {
        return total;
    }

    public void setTotal(IntegerFilter total) {
        this.total = total;
    }

    public IntegerFilter getInventory() {
        return inventory;
    }

    public void setInventory(IntegerFilter inventory) {
        this.inventory = inventory;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public IntegerFilter getSoldProduct() {
        return soldProduct;
    }

    public void setSoldProduct(IntegerFilter soldProduct) {
        this.soldProduct = soldProduct;
    }

    public LongFilter getProductId() {
        return productId;
    }

    public void setProductId(LongFilter productId) {
        this.productId = productId;
    }

    public LongFilter getSizeId() {
        return sizeId;
    }

    public void setSizeId(LongFilter sizeId) {
        this.sizeId = sizeId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AmountCriteria that = (AmountCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(total, that.total) &&
            Objects.equals(inventory, that.inventory) &&
            Objects.equals(description, that.description) &&
            Objects.equals(soldProduct, that.soldProduct) &&
            Objects.equals(productId, that.productId) &&
            Objects.equals(sizeId, that.sizeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        total,
        inventory,
        description,
        soldProduct,
        productId,
        sizeId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AmountCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (total != null ? "total=" + total + ", " : "") +
                (inventory != null ? "inventory=" + inventory + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (soldProduct != null ? "soldProduct=" + soldProduct + ", " : "") +
                (productId != null ? "productId=" + productId + ", " : "") +
                (sizeId != null ? "sizeId=" + sizeId + ", " : "") +
            "}";
    }

}
