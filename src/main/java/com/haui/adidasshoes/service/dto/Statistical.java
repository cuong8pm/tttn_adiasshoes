package com.haui.adidasshoes.service.dto;

import com.haui.adidasshoes.domain.Orders;
import com.haui.adidasshoes.domain.TrackOrder;

import java.time.LocalDate;

public class Statistical {
    private LocalDate state;
    private Double year1998;
    private Double year2001;
    private Double year2004;

    public LocalDate getState() {
        return state;
    }

    public void setState(LocalDate state) {
        this.state = state;
    }

    public Double getYear1998() {
        return year1998;
    }

    public void setYear1998(Double year1998) {
        this.year1998 = year1998;
    }

    public Double getYear2001() {
        return year2001;
    }

    public void setYear2001(Double year2001) {
        this.year2001 = year2001;
    }

    public Double getYear2004() {
        return year2004;
    }

    public void setYear2004(Double year2004) {
        this.year2004 = year2004;
    }
}
