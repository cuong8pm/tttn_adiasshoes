package com.haui.adidasshoes.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.haui.adidasshoes.domain.OrderProduct} entity. This class is used
 * in {@link com.haui.adidasshoes.web.rest.OrderProductResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /order-products?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class OrderProductCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter description;

    private LongFilter ordersId;

    private LongFilter listCartId;

    public OrderProductCriteria() {
    }

    public OrderProductCriteria(OrderProductCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.ordersId = other.ordersId == null ? null : other.ordersId.copy();
        this.listCartId = other.listCartId == null ? null : other.listCartId.copy();
    }

    @Override
    public OrderProductCriteria copy() {
        return new OrderProductCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public LongFilter getOrdersId() {
        return ordersId;
    }

    public void setOrdersId(LongFilter ordersId) {
        this.ordersId = ordersId;
    }

    public LongFilter getListCartId() {
        return listCartId;
    }

    public void setListCartId(LongFilter listCartId) {
        this.listCartId = listCartId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final OrderProductCriteria that = (OrderProductCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(description, that.description) &&
            Objects.equals(ordersId, that.ordersId) &&
            Objects.equals(listCartId, that.listCartId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        description,
        ordersId,
        listCartId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OrderProductCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (ordersId != null ? "ordersId=" + ordersId + ", " : "") +
                (listCartId != null ? "listCartId=" + listCartId + ", " : "") +
            "}";
    }

}
