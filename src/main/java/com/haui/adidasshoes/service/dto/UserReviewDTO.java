package com.haui.adidasshoes.service.dto;

public interface UserReviewDTO {
    String getFirstName();
    String getLastName();
    String getReview();
}
