package com.haui.adidasshoes.service;

import com.haui.adidasshoes.domain.OrderProduct;
import com.haui.adidasshoes.repository.OrderProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link OrderProduct}.
 */
@Service
@Transactional
public class OrderProductService {

    private final Logger log = LoggerFactory.getLogger(OrderProductService.class);

    private final OrderProductRepository orderProductRepository;

    public OrderProductService(OrderProductRepository orderProductRepository) {
        this.orderProductRepository = orderProductRepository;
    }

    public OrderProduct save(OrderProduct orderProduct) {
        log.debug("Request to save OrderProduct : {}", orderProduct);
        return orderProductRepository.save(orderProduct);
    }

    @Transactional(readOnly = true)
    public Page<OrderProduct> findAll(Pageable pageable) {
        log.debug("Request to get all OrderProducts");
        return orderProductRepository.findAll(pageable);
    }


    @Transactional(readOnly = true)
    public Optional<OrderProduct> findOne(Long id) {
        log.debug("Request to get OrderProduct : {}", id);
        return orderProductRepository.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete OrderProduct : {}", id);
        orderProductRepository.deleteById(id);
    }
}
