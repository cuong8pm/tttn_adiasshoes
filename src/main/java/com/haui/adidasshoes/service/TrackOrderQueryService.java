package com.haui.adidasshoes.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.haui.adidasshoes.domain.TrackOrder;
import com.haui.adidasshoes.domain.*; // for static metamodels
import com.haui.adidasshoes.repository.TrackOrderRepository;
import com.haui.adidasshoes.service.dto.TrackOrderCriteria;

/**
 * Service for executing complex queries for {@link TrackOrder} entities in the database.
 * The main input is a {@link TrackOrderCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TrackOrder} or a {@link Page} of {@link TrackOrder} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TrackOrderQueryService extends QueryService<TrackOrder> {

    private final Logger log = LoggerFactory.getLogger(TrackOrderQueryService.class);

    private final TrackOrderRepository trackOrderRepository;

    public TrackOrderQueryService(TrackOrderRepository trackOrderRepository) {
        this.trackOrderRepository = trackOrderRepository;
    }

    /**
     * Return a {@link List} of {@link TrackOrder} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TrackOrder> findByCriteria(TrackOrderCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TrackOrder> specification = createSpecification(criteria);
        return trackOrderRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link TrackOrder} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TrackOrder> findByCriteria(TrackOrderCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TrackOrder> specification = createSpecification(criteria);
        return trackOrderRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TrackOrderCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TrackOrder> specification = createSpecification(criteria);
        return trackOrderRepository.count(specification);
    }

    /**
     * Function to convert {@link TrackOrderCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TrackOrder> createSpecification(TrackOrderCriteria criteria) {
        Specification<TrackOrder> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TrackOrder_.id));
            }
            if (criteria.getOrderConfirm() != null) {
                specification = specification.and(buildSpecification(criteria.getOrderConfirm(), TrackOrder_.orderConfirm));
            }
            if (criteria.getWaittingProduct() != null) {
                specification = specification.and(buildSpecification(criteria.getWaittingProduct(), TrackOrder_.waittingProduct));
            }
            if (criteria.getShipping() != null) {
                specification = specification.and(buildSpecification(criteria.getShipping(), TrackOrder_.shipping));
            }
            if (criteria.getDelivered() != null) {
                specification = specification.and(buildSpecification(criteria.getDelivered(), TrackOrder_.delivered));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getStatus(), TrackOrder_.status));
            }
            if (criteria.getOrdersId() != null) {
                specification = specification.and(buildSpecification(criteria.getOrdersId(),
                    root -> root.join(TrackOrder_.orders, JoinType.LEFT).get(Orders_.id)));
            }
        }
        return specification;
    }

    public List<TrackOrder> findByOrderId(Long orderId) {
        return trackOrderRepository.findAllByOrderID(orderId);
    }
}
