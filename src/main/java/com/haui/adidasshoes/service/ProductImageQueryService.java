package com.haui.adidasshoes.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.haui.adidasshoes.domain.ProductImage;
import com.haui.adidasshoes.domain.*; // for static metamodels
import com.haui.adidasshoes.repository.ProductImageRepository;
import com.haui.adidasshoes.service.dto.ProductImageCriteria;

/**
 * Service for executing complex queries for {@link ProductImage} entities in the database.
 * The main input is a {@link ProductImageCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProductImage} or a {@link Page} of {@link ProductImage} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProductImageQueryService extends QueryService<ProductImage> {

    private final Logger log = LoggerFactory.getLogger(ProductImageQueryService.class);

    private final ProductImageRepository productImageRepository;

    public ProductImageQueryService(ProductImageRepository productImageRepository) {
        this.productImageRepository = productImageRepository;
    }

    /**
     * Return a {@link List} of {@link ProductImage} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProductImage> findByCriteria(ProductImageCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProductImage> specification = createSpecification(criteria);
        return productImageRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link ProductImage} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProductImage> findByCriteria(ProductImageCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProductImage> specification = createSpecification(criteria);
        return productImageRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProductImageCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProductImage> specification = createSpecification(criteria);
        return productImageRepository.count(specification);
    }

    /**
     * Function to convert {@link ProductImageCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ProductImage> createSpecification(ProductImageCriteria criteria) {
        Specification<ProductImage> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ProductImage_.id));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), ProductImage_.description));
            }
            if (criteria.getProductId() != null) {
                specification = specification.and(buildSpecification(criteria.getProductId(),
                    root -> root.join(ProductImage_.product, JoinType.LEFT).get(Product_.id)));
            }
        }
        return specification;
    }

    public Page<ProductImage> findById(Long id, Pageable pageable) {
        return productImageRepository.findAll(id,pageable);
    }
}
