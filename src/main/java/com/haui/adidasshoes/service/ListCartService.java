package com.haui.adidasshoes.service;

import com.haui.adidasshoes.domain.ListCart;
import com.haui.adidasshoes.domain.Size;
import com.haui.adidasshoes.domain.User;
import com.haui.adidasshoes.repository.ListCartRepository;
import com.haui.adidasshoes.service.dto.cartDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ListCart}.
 */
@Service
@Transactional
public class ListCartService {

    private final Logger log = LoggerFactory.getLogger(ListCartService.class);

    private final ListCartRepository listCartRepository;

    @Autowired
    private  SizeService sizeService;

    @Autowired
    private  UserService userService;

    public ListCartService(ListCartRepository listCartRepository) {
        this.listCartRepository = listCartRepository;
    }

    public ListCart save(ListCart listCart) {
        log.debug("Request to save ListCart : {}", listCart);
        return listCartRepository.save(listCart);
    }

    @Transactional(readOnly = true)
    public Page<ListCart> findAll(String username,Pageable pageable) {
        log.debug("Request to get all ListCarts");
        return listCartRepository.findAllByUsername(username,pageable);
    }

    @Transactional(readOnly = true)
    public Optional<ListCart> findOne(Long id) {
        log.debug("Request to get ListCart : {}", id);
        return listCartRepository.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete ListCart : {}", id);
        listCartRepository.deleteById(id);
    }

    public ListCart findOneById(Long id) {
        return listCartRepository.findOneByID(id);
    }

    public void updateaMount(Long id, float count) {
        ListCart listCart = findOneById(id);
        if(count == 1){
            listCart.setAmount(listCart.getAmount()+1);
        }
        if(count == -1 && listCart.getAmount()>1){
            listCart.setAmount(listCart.getAmount()-1);
        }
        save(listCart);
    }

    public ListCart creareCart(cartDTO cart) {
        Size size = new Size();
        if(cart.getSize()==null){
             size = sizeService.findOneBySize("44");
        }else {
             size = sizeService.findOneBySize(cart.getSize());
        }
        User user = userService.findByUsername(cart.getUsername());
        ListCart listCart = new ListCart();
        listCart.setAmount(cart.getAmount());
        listCart.setCheckReview(false);
        listCart.setCheckDisplay(true);
        listCart.setSize(size);
        listCart.setUser(user);
        listCart.setProduct(cart.getProduct());
        listCart.setDescription(cart.getDescription());

        return listCart;
    }

    public List<ListCart> findAllByOrderID(Long id) {
        return listCartRepository.findAllByOrderID(id);
    }

    public Integer getTotalCartsByUsername(String username) {
        return listCartRepository.getTotalCartsByUsername(username);
    }
}
