package com.haui.adidasshoes.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.haui.adidasshoes.domain.Reviews;
import com.haui.adidasshoes.domain.*; // for static metamodels
import com.haui.adidasshoes.repository.ReviewsRepository;
import com.haui.adidasshoes.service.dto.ReviewsCriteria;

/**
 * Service for executing complex queries for {@link Reviews} entities in the database.
 * The main input is a {@link ReviewsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Reviews} or a {@link Page} of {@link Reviews} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ReviewsQueryService extends QueryService<Reviews> {

    private final Logger log = LoggerFactory.getLogger(ReviewsQueryService.class);

    private final ReviewsRepository reviewsRepository;

    public ReviewsQueryService(ReviewsRepository reviewsRepository) {
        this.reviewsRepository = reviewsRepository;
    }

    @Transactional(readOnly = true)
    public List<Reviews> findByCriteria(ReviewsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Reviews> specification = createSpecification(criteria);
        return reviewsRepository.findAll(specification);
    }

    @Transactional(readOnly = true)
    public Page<Reviews> findByCriteria(ReviewsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Reviews> specification = createSpecification(criteria);
        return reviewsRepository.findAll(specification, page);
    }

    @Transactional(readOnly = true)
    public long countByCriteria(ReviewsCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Reviews> specification = createSpecification(criteria);
        return reviewsRepository.count(specification);
    }

    protected Specification<Reviews> createSpecification(ReviewsCriteria criteria) {
        Specification<Reviews> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Reviews_.id));
            }
            if (criteria.getRating() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRating(), Reviews_.rating));
            }
            if (criteria.getDecription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDecription(), Reviews_.decription));
            }
            if (criteria.getOrderProductId() != null) {
                specification = specification.and(buildSpecification(criteria.getOrderProductId(),
                    root -> root.join(Reviews_.orderProduct, JoinType.LEFT).get(OrderProduct_.id)));
            }
        }
        return specification;
    }

    public List<Reviews> findAllById(Long id) {
        return reviewsRepository.findAllByProductId(id);
    }
}
