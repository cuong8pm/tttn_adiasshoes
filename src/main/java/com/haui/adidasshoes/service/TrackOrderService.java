package com.haui.adidasshoes.service;

import com.haui.adidasshoes.domain.TrackOrder;
import com.haui.adidasshoes.repository.TrackOrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link TrackOrder}.
 */
@Service
@Transactional
public class TrackOrderService {

    private final Logger log = LoggerFactory.getLogger(TrackOrderService.class);

    private final TrackOrderRepository trackOrderRepository;

    public TrackOrderService(TrackOrderRepository trackOrderRepository) {
        this.trackOrderRepository = trackOrderRepository;
    }

    public TrackOrder save(TrackOrder trackOrder) {
        log.debug("Request to save TrackOrder : {}", trackOrder);
        return trackOrderRepository.save(trackOrder);
    }

    @Transactional(readOnly = true)
    public Page<TrackOrder> findAll(Pageable pageable) {
        log.debug("Request to get all TrackOrders");
        return trackOrderRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Optional<TrackOrder> findOne(Long id) {
        log.debug("Request to get TrackOrder : {}", id);
        return trackOrderRepository.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete TrackOrder : {}", id);
        trackOrderRepository.deleteById(id);
    }

    public TrackOrder findOneById(Long trackOrderId) {
        TrackOrder trackOrder = trackOrderRepository.findOneById(trackOrderId);
        trackOrder.setStatus(false);
        trackOrderRepository.save(trackOrder);
        return  trackOrder;
    }
}
