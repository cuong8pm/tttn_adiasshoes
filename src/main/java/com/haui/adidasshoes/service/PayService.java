package com.haui.adidasshoes.service;

import com.haui.adidasshoes.domain.*;
import com.haui.adidasshoes.repository.*;
import com.haui.adidasshoes.service.dto.ListCartDTO;
import com.haui.adidasshoes.service.dto.OrderUserDTO;
import com.haui.adidasshoes.service.dto.cartDTO;
import com.haui.adidasshoes.web.rest.errors.BadRequestAlertException;
import liquibase.pro.packaged.D;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;

/**
 * Service Implementation for managing {@link Pay}.
 */
@Service
@Transactional
public class PayService {

    private final Logger log = LoggerFactory.getLogger(PayService.class);
    private static final String ENTITY_NAME = "amount";

    @Autowired
    private ListCartService listCartService;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private OrderNowRepository orderNowRepository;

    @Autowired
    private ListCartRepository listCartRepository;

    @Autowired
    private AmountRepository amountRepository;

    @Autowired
    private SizeService sizeService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private  TrackOrderRepository trackOrderRepository;

    @Autowired
    private  OrderProductRepository orderProductRepository;

    private final PayRepository payRepository;

    public PayService(PayRepository payRepository) {
        this.payRepository = payRepository;
    }

    public List<ListCartDTO> getListCarts(Map<String, Long[]> idListCarts) {
        List<ListCartDTO> listCartsDTO = new ArrayList<ListCartDTO>();
        Long[] ids = idListCarts.get("checkArray");
        for (Long id:ids
             ) {
            ListCart listCart = listCartService.findOneById(id);

            ListCartDTO listCartDTO = new ListCartDTO();
            listCartDTO.setId(listCart.getId());
            listCartDTO.setAmount(listCart.getAmount());
            listCartDTO.setSize(listCart.getSize());
            listCartDTO.setProduct(listCart.getProduct());
            listCartDTO.setDescription(listCart.getDescription());
            listCartDTO.setUser(listCart.getUser());
            listCartDTO.setTotalPrice(listCart.getAmount()*listCart.getProduct().getPrice());
            listCartsDTO.add(listCartDTO);
        }
        return  listCartsDTO;
    }

    public Pay save(Pay pay) {
        log.debug("Request to save Pay : {}", pay);
        return payRepository.save(pay);
    }

    @Transactional(readOnly = true)
    public Page<Pay> findAll(Pageable pageable) {
        log.debug("Request to get all Pays");
        return payRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Optional<Pay> findOne(Long id) {
        log.debug("Request to get Pay : {}", id);
        return payRepository.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete Pay : {}", id);
        payRepository.deleteById(id);
    }

    public Map<String, Double> getTotalPrice(List<ListCartDTO> listCartDTO) {
        Map<String, Double> totalPrice = new HashMap<String, Double>();
        double totalProductPrice=0;
        for (ListCartDTO item: listCartDTO) {
            totalProductPrice += item.getTotalPrice();
        }
        totalPrice.put("totalProductPrice",totalProductPrice);
        totalPrice.put("totalOrderPrice",totalProductPrice+30000);
        return totalPrice;
    }

    public List<OrderUserDTO> getAllOrder(String username, Integer track) {
        List<OrderUserDTO> orderUserDTO = new ArrayList<OrderUserDTO>();
        List<Orders> orders = getListOrderByStatusTrackOrder(username,track);
        for (Orders item: orders) {
            OrderUserDTO orderUserDTO1 = new OrderUserDTO();
            orderUserDTO1.setOrders(item);

            TrackOrder trackOrder = trackOrderRepository.findByOrderID(item.getId());
            orderUserDTO1.setTrackOrder(trackOrder);

            List<ListCart> listCarts = listCartService.findAllByOrderID(item.getId());
            orderUserDTO1.setListCarts(listCarts);
            orderUserDTO.add(orderUserDTO1);
        }
        return orderUserDTO;
    }

    public List<Orders> getListOrderByStatusTrackOrder(String username, Integer track){
        List<Orders> orders = new ArrayList<Orders>();
        if(track==1){
            orders = ordersService.findByUsernameAndOrderConfirm(username);
        }
        if(track==2){
            orders = ordersService.findByUsernameAndWaittingProduct(username);
        }
        if(track==3){
            orders = ordersService.findByUsernameAndShipping(username);
        }
        if(track==4){
            orders = ordersService.findByUsernameAnddelivered(username);
        }
        if(track==5){
            orders = ordersService.findByUsernameAndStatus(username);
        }
        return orders;
    }

    public OrderUserDTO getOrder(Long orderId) {
        OrderUserDTO orderUserDTO = new OrderUserDTO();
        Orders orders = ordersRepository.findOneById(orderId);
        orderUserDTO.setOrders(orders);
        List<ListCart> listCarts = listCartService.findAllByOrderID(orders.getId());
        orderUserDTO.setListCarts(listCarts);
        return orderUserDTO;
    }

    public List<ListCartDTO> getListProduct(cartDTO cart) {
        List<ListCartDTO> listCartDTOS = new ArrayList<ListCartDTO>();

        //tao listcart
        Size size = new Size();
        if(cart.getSize()==null){
            size = sizeService.findOneBySize("44");
        }else {
            size = sizeService.findOneBySize(cart.getSize());
        }
        User user = userService.findByUsername(cart.getUsername());
        ListCart listCart = new ListCart();
        listCart.setAmount(cart.getAmount());
        listCart.setCheckReview(false);
        listCart.setCheckDisplay(true);
        listCart.setSize(size);
        listCart.setUser(user);
        listCart.setProduct(cart.getProduct());
        listCart.setDescription(cart.getDescription());

        //tao listcartDTO
        ListCartDTO listCartDTO = new ListCartDTO();
        listCartDTO.setId(listCart.getId());
        listCartDTO.setAmount(listCart.getAmount());
        listCartDTO.setSize(listCart.getSize());
        listCartDTO.setProduct(listCart.getProduct());
        listCartDTO.setDescription(listCart.getDescription());
        listCartDTO.setUser(listCart.getUser());
        listCartDTO.setTotalPrice(listCart.getAmount()*listCart.getProduct().getPrice());
        listCartDTOS.add(listCartDTO);


        return  listCartDTOS;
    }

    public User getUser(String username) {
        return userRepository.findOneByUsername(username);
    }

    public OrderNow createOrderNow(String name, String phone, String address, List<ListCartDTO> listCartDTOS) {

        Double totalOrderPrice = (double) 0;
        for (ListCartDTO item: listCartDTOS) {
            totalOrderPrice+=item.getTotalPrice();
            Amount amount = amountRepository.findByProductIdAndSize(item.getProduct().getId(),item.getSize().getName());
            if(amount == null || item.getAmount() > amount.getInventory() ){
                throw new BadRequestAlertException("Sản phẩm đã hết hàng", ENTITY_NAME, "outofproduct");
            }
        }
        totalOrderPrice = totalOrderPrice + 30000;

        OrderNow orderNow = new OrderNow();
        orderNow.setOrderConfirm(false);
        orderNow.setDelivered(false);
        orderNow.setStatus(true);
        orderNow.setName(name);
        orderNow.setAddress(address);
        orderNow.setPhone(phone);
        orderNow.setOrderDate(LocalDate.now());
        orderNow.setDeliveryDate(LocalDate.now().plusDays(2));
        orderNow.setTotalOrderPrice(totalOrderPrice);

        for (ListCartDTO item: listCartDTOS) {

            ListCart listCart = new ListCart();
            listCart.setId(null);
            listCart.setProduct(item.getProduct());
            listCart.setSize(item.getSize());
            listCart.setAmount(item.getAmount());
            listCart.setCheckReview(false);
            listCart.setDescription("cc");
            listCart = listCartRepository.save(listCart);
            orderNow.setListCart(listCart);

            //xoa cart khoi gio hang
            ListCart listCart1 = listCartRepository.findOneByID(listCart.getId());
            listCart1.setCheckDisplay(false);
            listCartRepository.save(listCart1);
            //tru sp ton khi khach hàng mua hang
            Amount amount= amountRepository.findByProductIdAndSize(item.getProduct().getId(),item.getSize().getName());
            amount.setInventory(amount.getInventory()-item.getAmount());
            amount.setSoldProduct(amount.getSoldProduct()+item.getAmount());
            amountRepository.save(amount);
        }

        return orderNowRepository.save(orderNow);
    }
}
