package com.haui.adidasshoes.service;

import com.haui.adidasshoes.domain.TrackOrderUser;
import com.haui.adidasshoes.repository.TrackOrderUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link TrackOrderUser}.
 */
@Service
@Transactional
public class TrackOrderUserService {

    private final Logger log = LoggerFactory.getLogger(TrackOrderUserService.class);

    private final TrackOrderUserRepository trackOrderUserRepository;

    public TrackOrderUserService(TrackOrderUserRepository trackOrderUserRepository) {
        this.trackOrderUserRepository = trackOrderUserRepository;
    }

    /**
     * Save a trackOrderUser.
     *
     * @param trackOrderUser the entity to save.
     * @return the persisted entity.
     */
    public TrackOrderUser save(TrackOrderUser trackOrderUser) {
        log.debug("Request to save TrackOrderUser : {}", trackOrderUser);
        return trackOrderUserRepository.save(trackOrderUser);
    }

    /**
     * Get all the trackOrderUsers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TrackOrderUser> findAll(Pageable pageable) {
        log.debug("Request to get all TrackOrderUsers");
        return trackOrderUserRepository.findAll(pageable);
    }


    /**
     * Get one trackOrderUser by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TrackOrderUser> findOne(Long id) {
        log.debug("Request to get TrackOrderUser : {}", id);
        return trackOrderUserRepository.findById(id);
    }

    /**
     * Delete the trackOrderUser by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TrackOrderUser : {}", id);
        trackOrderUserRepository.deleteById(id);
    }
}
