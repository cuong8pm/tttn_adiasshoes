package com.haui.adidasshoes.service;

import com.haui.adidasshoes.domain.OrderNow;
import com.haui.adidasshoes.repository.OrderNowRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link OrderNow}.
 */
@Service
@Transactional
public class OrderNowService {

    private final Logger log = LoggerFactory.getLogger(OrderNowService.class);

    private final OrderNowRepository orderNowRepository;

    public OrderNowService(OrderNowRepository orderNowRepository) {
        this.orderNowRepository = orderNowRepository;
    }

    /**
     * Save a orderNow.
     *
     * @param orderNow the entity to save.
     * @return the persisted entity.
     */
    public OrderNow save(OrderNow orderNow) {
        log.debug("Request to save OrderNow : {}", orderNow);
        return orderNowRepository.save(orderNow);
    }

    /**
     * Get all the orderNows.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<OrderNow> findAll(Pageable pageable) {
        log.debug("Request to get all OrderNows");
        return orderNowRepository.findAll(pageable);
    }


    /**
     * Get one orderNow by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<OrderNow> findOne(Long id) {
        log.debug("Request to get OrderNow : {}", id);
        return orderNowRepository.findById(id);
    }

    /**
     * Delete the orderNow by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete OrderNow : {}", id);
        orderNowRepository.deleteById(id);
    }

    public Page<OrderNow> getALL(String confirm, Pageable pageable) {
        if(confirm.equals("1")){
            return orderNowRepository.getallOrderNowConfirm(pageable);
        }else if (confirm.equals("0")){
            return orderNowRepository.getallOrderNowNotConfirm(pageable);
        }else {
            return orderNowRepository.getall(pageable);
        }
    }
}
