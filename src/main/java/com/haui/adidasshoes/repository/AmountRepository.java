package com.haui.adidasshoes.repository;

import com.haui.adidasshoes.domain.Amount;

import com.haui.adidasshoes.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Amount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AmountRepository extends JpaRepository<Amount, Long>, JpaSpecificationExecutor<Amount> {

    @Query(value = "select a from Amount a inner join Product p on a.product.id=p.id inner join Size s on a.size.id= s.id " +
        "WHERE p.id= :id and s.name= :size")
    Amount findByProductIdAndSize(@Param("id") Long id, @Param("size") String size );

    @Query(value = "select a from Amount a inner join Product p on a.product.id=p.id "+
        "WHERE p.id= :id ")
    Page<Amount> findAllByProductId(@Param("id") Long productID, Pageable page);

    Amount findOneById(@Param("id") Long id);
}
