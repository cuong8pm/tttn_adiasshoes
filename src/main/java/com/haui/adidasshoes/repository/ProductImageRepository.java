package com.haui.adidasshoes.repository;

import com.haui.adidasshoes.domain.ProductImage;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the ProductImage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductImageRepository extends JpaRepository<ProductImage, Long>, JpaSpecificationExecutor<ProductImage> {
    @Query(value = "select pi from  ProductImage pi where pi.product.id = :id")
    List<ProductImage> findAllById(@Param("id") Long id);

    @Query(value = "select pi from  ProductImage pi where pi.product.id = :id")
    Page<ProductImage> findAll(@Param("id") Long id, Pageable pageable);
}
