package com.haui.adidasshoes.repository;

import com.haui.adidasshoes.domain.TrackOrderUser;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the TrackOrderUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TrackOrderUserRepository extends JpaRepository<TrackOrderUser, Long> {
}
