package com.haui.adidasshoes.repository;

import com.haui.adidasshoes.domain.ListCart;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the ListCart entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ListCartRepository extends JpaRepository<ListCart, Long> {

    @Query("select listCart from ListCart listCart where listCart.user.login = ?#{principal.username}")
    List<ListCart> findByUserIsCurrentUser();

    @Query(value = "select lc from ListCart lc where lc.user.login = :username")
    Page<ListCart> findAllByUsername(@Param("username") String username, Pageable pageable);

    @Query(value = "select lc from ListCart lc where lc.id = :id")
    ListCart findOneByID(@Param("id")Long id);

    @Query(value = "select lc from ListCart lc inner join OrderProduct  op on lc.id = op.listCart.id " +
        "inner join Orders  o on op.orders.id = o.id where o.id = :id")
    List<ListCart> findAllByOrderID(@Param("id")Long id);

    @Query(value = "select count(lc) from ListCart lc inner join User u on lc.user.login=u.login " +
        "where u.login = :username  and lc.checkDisplay = true")
    Integer getTotalCartsByUsername(@Param("username")String username);
}
