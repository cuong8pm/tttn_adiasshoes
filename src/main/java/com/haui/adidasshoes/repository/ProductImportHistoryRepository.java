package com.haui.adidasshoes.repository;

import com.haui.adidasshoes.domain.ProductImportHistory;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the ProductImportHistory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductImportHistoryRepository extends JpaRepository<ProductImportHistory, Long>, JpaSpecificationExecutor<ProductImportHistory> {

    @Query(value = "select pih from ProductImportHistory pih where pih.amount.id =:amountId")
    List<ProductImportHistory> findAllByAmountId(@Param("amountId") Long amountId);
}
