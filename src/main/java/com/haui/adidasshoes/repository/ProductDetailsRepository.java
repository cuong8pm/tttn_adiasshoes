package com.haui.adidasshoes.repository;

import com.haui.adidasshoes.domain.ProductDetails;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the ProductDetails entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductDetailsRepository extends JpaRepository<ProductDetails, Long>, JpaSpecificationExecutor<ProductDetails> {

    @Query(value = "select dp from ProductDetails dp where dp.product.id = :id")
   List<ProductDetails> findAllById(@Param("id") Long id);
}
