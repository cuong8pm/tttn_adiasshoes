package com.haui.adidasshoes.repository;

import com.haui.adidasshoes.domain.Orders;

import com.haui.adidasshoes.service.dto.Statistical;
import com.haui.adidasshoes.service.dto.StatisticalDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.List;

/**
 * Spring Data  repository for the Orders entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrdersRepository extends JpaRepository<Orders, Long>, JpaSpecificationExecutor<Orders> {

    @Query("select orders from Orders orders where orders.user.login = ?#{principal.username}")
    List<Orders> findByUserIsCurrentUser();

    @Query("select o from Orders  o inner join TrackOrder to on o.id = to.orders.id" +
        " inner join User u on o.user.id = u.id where to.waittingProduct = false and to.status = true and u.login= :username")
    List<Orders> findByUsernameAndOrderConfirm(@Param("username") String username);

    @Query("select o from Orders  o inner join TrackOrder to on o.id = to.orders.id" +
        " inner join User u on o.user.id = u.id where to.waittingProduct = true and to.status = true and to.shipping = false and u.login= :username")
    List<Orders> findByUsernameAndWaittingProduct(@Param("username")String username);

    @Query("select o from Orders  o inner join TrackOrder to on o.id = to.orders.id" +
        " inner join User u on o.user.id = u.id where to.shipping = true and to.status = true and to.delivered = false and u.login= :username")
    List<Orders> findByUsernameAndShipping(@Param("username")String username);

    @Query("select o from Orders  o inner join TrackOrder to on o.id = to.orders.id" +
        " inner join User u on o.user.id = u.id where to.delivered = true and to.status = true and u.login= :username")
    List<Orders> findByUsernameAnddelivered(@Param("username")String username);

    @Query("select o from Orders  o inner join TrackOrder to on o.id = to.orders.id" +
        " inner join User u on o.user.id = u.id where to.status = false and u.login= :username")
    List<Orders> findByUsernameAndStatus(@Param("username")String username);

    Orders findOneById(@Param("orderId") Long orderId);


    @Query("select count(o) from Orders o where  o.confirm = false ")
    Integer getOrderNew();

    @Query(value = "select  sum(o.total_order_price)\n" +
        "        from orders o \n" +
        "        join track_order T on o.id = t.orders_id\n" +
        "        where t.delivered = true \n" +
        "        and t.status = true \n" +
        "        and month(o.delivery_date_to) = month(now()) " , nativeQuery = true)
    Integer getRevenueInOneDay();

    @Query(value = "select  sum(lc.amount)\n" +
        "from orders o \n" +
        "join track_order T on o.id = t.orders_id\n" +
        "join order_product op on o.id = op.orders_id\n" +
        "join list_cart lc on op.list_cart_id = lc.id\n" +
        "where t.delivered = true \n" +
        "and t.status = true \n" +
        "and month(o.delivery_date_to) = month(now()) " , nativeQuery = true)
    Integer getProductNumberInMonth();

    @Query(value = "select o from Orders o where o.confirm = true")
    Page<Orders> getallOrderNowConfirm(Pageable pageable);

    @Query(value = "select o from Orders o where o.confirm = false ")
    Page<Orders> getallOrderNowNotConfirm(Pageable pageable);

    @Query(value = "select o from Orders o")
    Page<Orders> getall(Pageable pageable);

    @Query(value = " select   order_date as state  ,  sum(total_order_price) as year2004 , sum(hangthanhcong) as year1998,sum(hangtralai) as year2001\n" +
        "from (\n" +
        "select DATE_ADD(date(now()), INTERVAL -7 DAY) order_date, 0 total_order_price, 0 hangthanhcong , 0 hangtralai union all \n" +
        "select DATE_ADD(date(now()), INTERVAL -6 DAY) order_date, 0 total_order_price, 0 hangthanhcong , 0 hangtralai union all \n" +
        "select DATE_ADD(date(now()), INTERVAL -5 DAY) order_date, 0 total_order_price, 0 hangthanhcong , 0 hangtralai union all \n" +
        "select DATE_ADD(date(now()), INTERVAL -4 DAY) order_date, 0 total_order_price, 0 hangthanhcong , 0 hangtralai union all \n" +
        "select DATE_ADD(date(now()), INTERVAL -3 DAY) order_date, 0 total_order_price, 0 hangthanhcong , 0 hangtralai union all \n" +
        "select DATE_ADD(date(now()), INTERVAL -2 DAY) order_date, 0 total_order_price, 0 hangthanhcong , 0 hangtralai union all \n" +
        "select DATE_ADD(date(now()), INTERVAL -1 DAY) order_date, 0 total_order_price, 0 hangthanhcong , 0 hangtralai union all \n" +
        "select DATE_ADD(date(now()), INTERVAL -0 DAY) order_date, 0 total_order_price, 0 hangthanhcong , 0 hangtralai union all\n" +
        "SELECT order_date , case when track_order.status=true and track_order.delivered=true  then total_order_price else 0 end total_order_price ,\n" +
        "case when track_order.status=true and track_order.delivered=true  then  amount else 0 end hangthanhcong, \n" +
        "case when track_order.status=false then  amount else 0 end hangtralai \n" +
        "FROM orders inner join track_order on orders.id = track_order.orders_id \n" +
        "inner join order_product on orders.id = order_product.orders_id \n" +
        "inner join list_cart on order_product.list_cart_id = list_cart.id \n" +
        "where orders.order_date >= DATE_ADD(date(now()), INTERVAL -7 DAY) and orders.order_date <= date(now()) union all\n" +
        "SELECT order_date , case when order_now.status=true and order_now.delivered=true  then total_order_price else 0 end total_order_price ,\n" +
        "case when order_now.status=true and order_now.delivered=true  then  amount else 0 end hangthanhcong, \n" +
        "case when order_now.status=false then  amount else 0 end hangtralai \n" +
        "FROM order_now inner join list_cart on order_now.list_cart_id = list_cart.id \n" +
        "where order_now.order_date >= DATE_ADD(date(now()), INTERVAL -7 DAY) and order_now.order_date <= date(now())\n" +
        ")a group by a.order_date\n" , nativeQuery = true)
    List<StatisticalDTO> getStatistical();

    @Query(value = "select o from Orders o where o.id = :idOrder")
    Page<Orders> getOrderById(@Param("idOrder") Long idOrder, Pageable pageable);

    @Query(value = "select o from Orders o where o.orderDate = :a")
    Page<Orders> getOrderByOrderDate(@Param("a") LocalDate a, Pageable pageable);
}
