package com.haui.adidasshoes.repository;

import com.haui.adidasshoes.domain.DetailProduct;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the DetailProduct entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DetailProductRepository extends JpaRepository<DetailProduct, Long> {
}
