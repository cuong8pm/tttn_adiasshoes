package com.haui.adidasshoes.repository;

import com.haui.adidasshoes.domain.Size;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Size entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SizeRepository extends JpaRepository<Size, Long>, JpaSpecificationExecutor<Size> {
   @Query(value = "select s from Size s where s.name= :size")
    Size findOneByName(@Param("size") String size);
}
