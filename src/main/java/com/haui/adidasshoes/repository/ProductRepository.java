package com.haui.adidasshoes.repository;

import com.haui.adidasshoes.domain.Product;

import com.haui.adidasshoes.service.dto.UserReviewDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.print.attribute.standard.MediaSize;
import java.util.List;

/**
 * Spring Data  repository for the Product entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductRepository extends JpaRepository<Product, Long>, JpaSpecificationExecutor<Product> {

    @Query(value ="select p from Product p inner join Amount a on p.id = a.product.id group by p.id order by a.soldProduct desc ")
    List<Product> findAllBySoldProduct();

    Product findOneById(Long id);

    @Query(value = "select u.firstName as firstName,u.lastName as lastName,r.decription as review from Product p inner join ListCart lc on p.id= lc.product.id "+
        "inner join OrderProduct op on lc.id = op.listCart.id " +
        "inner join Reviews r on op.id = r.orderProduct.id inner join Orders o on op.orders.id = o.id " +
        "inner join User u on o.user.id = u.id where p.id =:id ")
    List<UserReviewDTO> findProductReviewByProductId(@Param("id") Long id);

    @Query(value ="select p from Product p  WHERE p.name like %:name%")
    List<Product> findAllByName(@Param("name") String name);

    @Query(value ="select p from Product p  WHERE p.name like %:name%")
    Page<Product> findByName(@Param("name") String name, Pageable pageable);
}
