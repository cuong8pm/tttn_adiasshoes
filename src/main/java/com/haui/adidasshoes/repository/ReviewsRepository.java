package com.haui.adidasshoes.repository;

import com.haui.adidasshoes.domain.Reviews;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Reviews entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReviewsRepository extends JpaRepository<Reviews, Long>, JpaSpecificationExecutor<Reviews> {
    List<Reviews> findAllById(@Param("id") Long id);

    @Query(value = "select r from Product p inner join ListCart lc on p.id= lc.product.id "+
        "inner join OrderProduct op on lc.id = op.listCart.id " +
        "inner join Reviews r on op.id = r.orderProduct.id inner join Orders o on op.orders.id = o.id " +
        "inner join User u on o.user.id = u.id where p.id =:id ")
    List<Reviews> findAllByProductId(@Param("id") Long id);
}
