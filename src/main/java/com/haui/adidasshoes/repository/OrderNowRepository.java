package com.haui.adidasshoes.repository;

import com.haui.adidasshoes.domain.OrderNow;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the OrderNow entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderNowRepository extends JpaRepository<OrderNow, Long> {

    @Query(value = "select count(onn) from OrderNow onn where onn.orderConfirm = false ")
    Integer getOrderNowNew();

    @Query(value = "select  sum(lc.amount)\n" +
        "from order_now o \n" +
        "join list_cart lc on o.list_cart_id = lc.id\n" +
        "where o.delivered = true \n" +
        "and o.status = true \n" +
        "and month(o.order_date) = month(now()) " , nativeQuery = true)
    Integer getProductNumberNowInMonth();


    @Query(value = "select  sum(o.total_order_price)\n" +
        "from order_now o \n" +
        "where o.delivered = true \n" +
        "and o.status = true \n" +
        "and month(o.order_date) = month(now()) " , nativeQuery = true)
    Integer getRevenueInMonth();

    @Query(value = "select odn from OrderNow odn where odn.orderConfirm = true")
    Page<OrderNow> getallOrderNowConfirm(Pageable pageable);

    @Query(value = "select odn from OrderNow odn where odn.orderConfirm = false ")
    Page<OrderNow> getallOrderNowNotConfirm(Pageable pageable);

    @Query(value = "select odn from OrderNow odn")
    Page<OrderNow> getall(Pageable pageable);
}
