package com.haui.adidasshoes.repository;

import com.haui.adidasshoes.domain.ListProduct;

import com.haui.adidasshoes.domain.Product;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data  repository for the ListProduct entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ListProductRepository extends JpaRepository<ListProduct, Long> {

    @Query(value = "select p from Product p where (:type is null or p.category.id = :type) " +
        "AND (:name is null or p.name like %:name%) " +
        "AND (:toPrice is null or p.price < :toPrice) " +
        "AND (:fromPrice is null or p.price > :fromPrice)")
    Page<Product> findAllWithPage(@Param("type") Long type,@Param("name") String name,@Param("toPrice") Double toPrice,@Param("fromPrice") Double fromPrice, Pageable pageable);
}
