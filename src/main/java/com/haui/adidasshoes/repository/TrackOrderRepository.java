package com.haui.adidasshoes.repository;

import com.haui.adidasshoes.domain.TrackOrder;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the TrackOrder entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TrackOrderRepository extends JpaRepository<TrackOrder, Long>, JpaSpecificationExecutor<TrackOrder> {

    @Query(value = "select to from TrackOrder to where to.orders.id = :id")
    TrackOrder findByOrderID(@Param("id") Long id);

    @Query(value = "select to from TrackOrder to where to.orders.id = :orderId")
    List<TrackOrder> findAllByOrderID(@Param("orderId") Long orderId);

    TrackOrder findOneById(Long trackOrderId);
}
