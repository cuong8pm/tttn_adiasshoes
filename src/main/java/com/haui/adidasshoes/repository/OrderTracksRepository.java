package com.haui.adidasshoes.repository;

import com.haui.adidasshoes.domain.OrderTracks;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the OrderTracks entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderTracksRepository extends JpaRepository<OrderTracks, Long> {
}
