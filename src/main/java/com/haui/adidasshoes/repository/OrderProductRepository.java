package com.haui.adidasshoes.repository;

import com.haui.adidasshoes.domain.OrderProduct;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the OrderProduct entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderProductRepository extends JpaRepository<OrderProduct, Long>, JpaSpecificationExecutor<OrderProduct> {

    @Query(value = "select op from OrderProduct op where op.orders.id= :id")
    List<OrderProduct> findAllByOrderID(@Param("id")Long id);

    @Query(value = "select op from OrderProduct op where op.orders.id= :orderId and op.listCart.id = :listCartId")
    OrderProduct findByOrderID(@Param("orderId") Long orderId,@Param("listCartId") Long listCartId);
}
