package com.haui.adidasshoes.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Size.
 */
@Entity
@Table(name = "size")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Size implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "descripstion")
    private String descripstion;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Size name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescripstion() {
        return descripstion;
    }

    public Size descripstion(String descripstion) {
        this.descripstion = descripstion;
        return this;
    }

    public void setDescripstion(String descripstion) {
        this.descripstion = descripstion;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Size)) {
            return false;
        }
        return id != null && id.equals(((Size) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Size{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", descripstion='" + getDescripstion() + "'" +
            "}";
    }
}
