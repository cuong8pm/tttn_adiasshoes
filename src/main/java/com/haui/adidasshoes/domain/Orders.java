package com.haui.adidasshoes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Orders.
 */
@Entity
@Table(name = "orders")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Orders implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "confirm")
    private Boolean confirm;

    @Column(name = "descripstion")
    private String descripstion;

    @Column(name = "total_order_price")
    private Double totalOrderPrice;

    @Column(name = "order_date")
    private LocalDate orderDate;

    @Column(name = "delivery_date_from")
    private LocalDate deliveryDateFrom;

    @Column(name = "delivery_date_to")
    private LocalDate deliveryDateTo;

    @ManyToOne
    @JsonIgnoreProperties(value = "orders", allowSetters = true)
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isConfirm() {
        return confirm;
    }

    public Orders confirm(Boolean confirm) {
        this.confirm = confirm;
        return this;
    }

    public void setConfirm(Boolean confirm) {
        this.confirm = confirm;
    }

    public String getDescripstion() {
        return descripstion;
    }

    public Orders descripstion(String descripstion) {
        this.descripstion = descripstion;
        return this;
    }

    public void setDescripstion(String descripstion) {
        this.descripstion = descripstion;
    }

    public Double getTotalOrderPrice() {
        return totalOrderPrice;
    }

    public Orders totalOrderPrice(Double totalOrderPrice) {
        this.totalOrderPrice = totalOrderPrice;
        return this;
    }

    public void setTotalOrderPrice(Double totalOrderPrice) {
        this.totalOrderPrice = totalOrderPrice;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public Orders orderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
        return this;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public LocalDate getDeliveryDateFrom() {
        return deliveryDateFrom;
    }

    public Orders deliveryDateFrom(LocalDate deliveryDateFrom) {
        this.deliveryDateFrom = deliveryDateFrom;
        return this;
    }

    public void setDeliveryDateFrom(LocalDate deliveryDateFrom) {
        this.deliveryDateFrom = deliveryDateFrom;
    }

    public LocalDate getDeliveryDateTo() {
        return deliveryDateTo;
    }

    public Orders deliveryDateTo(LocalDate deliveryDateTo) {
        this.deliveryDateTo = deliveryDateTo;
        return this;
    }

    public void setDeliveryDateTo(LocalDate deliveryDateTo) {
        this.deliveryDateTo = deliveryDateTo;
    }

    public User getUser() {
        return user;
    }

    public Orders user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Orders)) {
            return false;
        }
        return id != null && id.equals(((Orders) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Orders{" +
            "id=" + getId() +
            ", confirm='" + isConfirm() + "'" +
            ", descripstion='" + getDescripstion() + "'" +
            ", totalOrderPrice=" + getTotalOrderPrice() +
            ", orderDate='" + getOrderDate() + "'" +
            ", deliveryDateFrom='" + getDeliveryDateFrom() + "'" +
            ", deliveryDateTo='" + getDeliveryDateTo() + "'" +
            "}";
    }
}
