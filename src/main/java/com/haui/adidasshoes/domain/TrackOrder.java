package com.haui.adidasshoes.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A TrackOrder.
 */
@Entity
@Table(name = "track_order")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TrackOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "order_confirm")
    private Boolean orderConfirm;

    @Column(name = "waitting_product")
    private Boolean waittingProduct;

    @Column(name = "shipping")
    private Boolean shipping;

    @Column(name = "delivered")
    private Boolean delivered;

    @Column(name = "status")
    private Boolean status;

    @OneToOne
    @JoinColumn(unique = true)
    private Orders orders;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isOrderConfirm() {
        return orderConfirm;
    }

    public TrackOrder orderConfirm(Boolean orderConfirm) {
        this.orderConfirm = orderConfirm;
        return this;
    }

    public void setOrderConfirm(Boolean orderConfirm) {
        this.orderConfirm = orderConfirm;
    }

    public Boolean isWaittingProduct() {
        return waittingProduct;
    }

    public TrackOrder waittingProduct(Boolean waittingProduct) {
        this.waittingProduct = waittingProduct;
        return this;
    }

    public void setWaittingProduct(Boolean waittingProduct) {
        this.waittingProduct = waittingProduct;
    }

    public Boolean isShipping() {
        return shipping;
    }

    public TrackOrder shipping(Boolean shipping) {
        this.shipping = shipping;
        return this;
    }

    public void setShipping(Boolean shipping) {
        this.shipping = shipping;
    }

    public Boolean isDelivered() {
        return delivered;
    }

    public TrackOrder delivered(Boolean delivered) {
        this.delivered = delivered;
        return this;
    }

    public void setDelivered(Boolean delivered) {
        this.delivered = delivered;
    }

    public Boolean isStatus() {
        return status;
    }

    public TrackOrder status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Orders getOrders() {
        return orders;
    }

    public TrackOrder orders(Orders orders) {
        this.orders = orders;
        return this;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TrackOrder)) {
            return false;
        }
        return id != null && id.equals(((TrackOrder) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TrackOrder{" +
            "id=" + getId() +
            ", orderConfirm='" + isOrderConfirm() + "'" +
            ", waittingProduct='" + isWaittingProduct() + "'" +
            ", shipping='" + isShipping() + "'" +
            ", delivered='" + isDelivered() + "'" +
            ", status='" + isStatus() + "'" +
            "}";
    }
}
