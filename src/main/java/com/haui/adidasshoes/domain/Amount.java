package com.haui.adidasshoes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Amount.
 */
@Entity
@Table(name = "amount")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Amount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "total")
    private Integer total;

    @Column(name = "inventory")
    private Integer inventory;

    @Column(name = "description")
    private String description;

    @Column(name = "sold_product")
    private Integer soldProduct;

    @ManyToOne
    @JsonIgnoreProperties(value = "amounts", allowSetters = true)
    private Product product;

    @ManyToOne
    @JsonIgnoreProperties(value = "amounts", allowSetters = true)
    private Size size;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTotal() {
        return total;
    }

    public Amount total(Integer total) {
        this.total = total;
        return this;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getInventory() {
        return inventory;
    }

    public Amount inventory(Integer inventory) {
        this.inventory = inventory;
        return this;
    }

    public void setInventory(Integer inventory) {
        this.inventory = inventory;
    }

    public String getDescription() {
        return description;
    }

    public Amount description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSoldProduct() {
        return soldProduct;
    }

    public Amount soldProduct(Integer soldProduct) {
        this.soldProduct = soldProduct;
        return this;
    }

    public void setSoldProduct(Integer soldProduct) {
        this.soldProduct = soldProduct;
    }

    public Product getProduct() {
        return product;
    }

    public Amount product(Product product) {
        this.product = product;
        return this;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Size getSize() {
        return size;
    }

    public Amount size(Size size) {
        this.size = size;
        return this;
    }

    public void setSize(Size size) {
        this.size = size;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Amount)) {
            return false;
        }
        return id != null && id.equals(((Amount) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Amount{" +
            "id=" + getId() +
            ", total=" + getTotal() +
            ", inventory=" + getInventory() +
            ", description='" + getDescription() + "'" +
            ", soldProduct=" + getSoldProduct() +
            "}";
    }
}
