package com.haui.adidasshoes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A ListCart.
 */
@Entity
@Table(name = "list_cart")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ListCart implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "amount")
    private Integer amount;

    @Column(name = "check_review")
    private Boolean checkReview;

    @Column(name = "check_display")
    private Boolean checkDisplay;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JsonIgnoreProperties(value = "listCarts", allowSetters = true)
    private User user;

    @ManyToOne
    @JsonIgnoreProperties(value = "listCarts", allowSetters = true)
    private Product product;

    @ManyToOne
    @JsonIgnoreProperties(value = "listCarts", allowSetters = true)
    private Size size;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public ListCart amount(Integer amount) {
        this.amount = amount;
        return this;
    }

    public Boolean getCheckDisplay() {
        return checkDisplay;
    }

    public void setCheckDisplay(Boolean checkDisplay) {
        this.checkDisplay = checkDisplay;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public ListCart description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public ListCart user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Product getProduct() {
        return product;
    }

    public ListCart product(Product product) {
        this.product = product;
        return this;
    }
    public Boolean getCheckReview() {
        return checkReview;
    }

    public void setCheckReview(Boolean checkRewiew) {
        this.checkReview = checkRewiew;
    }
    public void setProduct(Product product) {
        this.product = product;
    }

    public Size getSize() {
        return size;
    }

    public ListCart size(Size size) {
        this.size = size;
        return this;
    }

    public void setSize(Size size) {
        this.size = size;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ListCart)) {
            return false;
        }
        return id != null && id.equals(((ListCart) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ListCart{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
