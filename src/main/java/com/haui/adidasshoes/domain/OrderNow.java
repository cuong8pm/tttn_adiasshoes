package com.haui.adidasshoes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A OrderNow.
 */
@Entity
@Table(name = "order_now")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class OrderNow implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "phone")
    private String phone;

    @Column(name = "address")
    private String address;

    @Column(name = "order_confirm")
    private Boolean orderConfirm;

    @Column(name = "delivered")
    private Boolean delivered;

    @Column(name = "order_date")
    private LocalDate orderDate;

    @Column(name = "delivery_date")
    private LocalDate deliveryDate;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "total_order_price")
    private Double totalOrderPrice;

    @ManyToOne
    @JsonIgnoreProperties(value = "orderNows", allowSetters = true)
    private ListCart listCart;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public OrderNow name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public OrderNow phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public OrderNow address(String address) {
        this.address = address;
        return this;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public LocalDate getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(LocalDate deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean isOrderConfirm() {
        return orderConfirm;
    }

    public OrderNow orderConfirm(Boolean orderConfirm) {
        this.orderConfirm = orderConfirm;
        return this;
    }

    public void setOrderConfirm(Boolean orderConfirm) {
        this.orderConfirm = orderConfirm;
    }

    public Boolean isDelivered() {
        return delivered;
    }

    public OrderNow delivered(Boolean delivered) {
        this.delivered = delivered;
        return this;
    }

    public void setDelivered(Boolean delivered) {
        this.delivered = delivered;
    }

    public Boolean isStatus() {
        return status;
    }

    public OrderNow status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Double getTotalOrderPrice() {
        return totalOrderPrice;
    }

    public OrderNow totalOrderPrice(Double totalOrderPrice) {
        this.totalOrderPrice = totalOrderPrice;
        return this;
    }

    public void setTotalOrderPrice(Double totalOrderPrice) {
        this.totalOrderPrice = totalOrderPrice;
    }

    public ListCart getListCart() {
        return listCart;
    }

    public OrderNow listCart(ListCart listCart) {
        this.listCart = listCart;
        return this;
    }

    public void setListCart(ListCart listCart) {
        this.listCart = listCart;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrderNow)) {
            return false;
        }
        return id != null && id.equals(((OrderNow) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OrderNow{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", phone='" + getPhone() + "'" +
            ", address='" + getAddress() + "'" +
            ", orderConfirm='" + isOrderConfirm() + "'" +
            ", delivered='" + isDelivered() + "'" +
            ", status='" + isStatus() + "'" +
            ", totalOrderPrice=" + getTotalOrderPrice() +
            "}";
    }
}
