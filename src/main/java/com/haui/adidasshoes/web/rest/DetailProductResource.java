package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.domain.*;
import com.haui.adidasshoes.service.*;
import com.haui.adidasshoes.service.dto.UserDTO;
import com.haui.adidasshoes.service.dto.UserReviewDTO;
import com.haui.adidasshoes.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link com.haui.adidasshoes.domain.DetailProduct}.
 */
@RestController
@RequestMapping("/api")
public class DetailProductResource {

    private final Logger log = LoggerFactory.getLogger(DetailProductResource.class);

    private static final String ENTITY_NAME = "detailProduct";

    @Autowired
    private ProductDetailsService productDetailsService;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DetailProductService detailProductService;

    @Autowired
    private ReviewsService reviewsService;

    @Autowired
    private ProductImageService productImageService;

    @Autowired
    private  AmountService amountService;

    @Autowired
    private ProductService productService;

    public DetailProductResource(DetailProductService detailProductService) {
        this.detailProductService = detailProductService;
    }

    @GetMapping("/detail-products/{id}/specs")
    public ResponseEntity<List<ProductDetails>> getAllDetailProductsById(@PathVariable Long id) {
        List<ProductDetails> list = productDetailsService.findAllById(id);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/detail-products/{id}/comments")
    public ResponseEntity<List<Reviews>> getAllCommentProductsById(@PathVariable Long id) {
        List<Reviews> list = reviewsService.findAllById(id);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/detail-products/{id}/images")
    public ResponseEntity<List<ProductImage>> getImageProductsById(@PathVariable Long id) {
        List<ProductImage> list =  productImageService.findAllById(id);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/detail-products/{id}/amount")
    public ResponseEntity<Amount> getInventoryProductsByIdAndSize(@PathVariable Long id,
                                                                  @RequestParam(name = "size",required = false,defaultValue = "") String size) {
        Amount amount =  amountService.findByProductIdAndSize(id,size);
        return ResponseEntity.ok().body(amount);
    }

    @GetMapping("/detail-products/{id}/review")
    public ResponseEntity<List<UserReviewDTO>> getReviewByProductId(@PathVariable Long id) {
        List<UserReviewDTO> list = productService.findProductReviewByProductId(id);
        return ResponseEntity.ok().body(list);
    }

    @PostMapping("/detail-products")
    public ResponseEntity<DetailProduct> createDetailProduct(@RequestBody DetailProduct detailProduct) throws URISyntaxException {
        log.debug("REST request to save DetailProduct : {}", detailProduct);
        if (detailProduct.getId() != null) {
            throw new BadRequestAlertException("A new detailProduct cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DetailProduct result = detailProductService.save(detailProduct);
        return ResponseEntity.created(new URI("/api/detail-products/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PutMapping("/detail-products")
    public ResponseEntity<DetailProduct> updateDetailProduct(@RequestBody DetailProduct detailProduct) throws URISyntaxException {
        log.debug("REST request to update DetailProduct : {}", detailProduct);
        if (detailProduct.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DetailProduct result = detailProductService.save(detailProduct);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, detailProduct.getId().toString()))
            .body(result);
    }

    @GetMapping("/detail-products")
    public ResponseEntity<List<DetailProduct>> getAllDetailProducts(Pageable pageable) {
        log.debug("REST request to get a page of DetailProducts");
        Page<DetailProduct> page = detailProductService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/detail-products/{id}")
    public ResponseEntity<DetailProduct> getDetailProduct(@PathVariable Long id) {
        log.debug("REST request to get DetailProduct : {}", id);
        Optional<DetailProduct> detailProduct = detailProductService.findOne(id);
        return ResponseUtil.wrapOrNotFound(detailProduct);
    }

    @DeleteMapping("/detail-products/{id}")
    public ResponseEntity<Void> deleteDetailProduct(@PathVariable Long id) {
        log.debug("REST request to delete DetailProduct : {}", id);
        detailProductService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
