package com.haui.adidasshoes.web.rest.Message;

import io.github.jhipster.web.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 *
 */
public class Header {

    private static final Logger log = LoggerFactory.getLogger(HeaderUtil.class);

    private Header() {
    }

    public static HttpHeaders createAlert(String applicationName, String message, String param) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-" + applicationName + "-alert", message);

//        try {
//            headers.add("X-" + applicationName + "-params", URLEncoder.encode(param, StandardCharsets.UTF_8.toString()));
//        } catch (UnsupportedEncodingException var5) {
//        }

        return headers;
    }
    public static HttpHeaders createEntityCreationAlert(String applicationName, boolean enableTranslation, String entityName, String param) {
        String message = "The product has been added to cart";
        return createAlert(applicationName, message, param);
    }

}
