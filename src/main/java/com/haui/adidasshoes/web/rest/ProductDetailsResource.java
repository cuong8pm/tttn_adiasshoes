package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.domain.Product;
import com.haui.adidasshoes.domain.ProductDetails;
import com.haui.adidasshoes.repository.ProductRepository;
import com.haui.adidasshoes.service.ProductDetailsService;
import com.haui.adidasshoes.web.rest.errors.BadRequestAlertException;
import com.haui.adidasshoes.service.dto.ProductDetailsCriteria;
import com.haui.adidasshoes.service.ProductDetailsQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.haui.adidasshoes.domain.ProductDetails}.
 */
@RestController
@RequestMapping("/api")
public class ProductDetailsResource {

    private final Logger log = LoggerFactory.getLogger(ProductDetailsResource.class);

    private static final String ENTITY_NAME = "productDetails";

    @Autowired
    private ProductRepository productRepository;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductDetailsService productDetailsService;

    private final ProductDetailsQueryService productDetailsQueryService;

    public ProductDetailsResource(ProductDetailsService productDetailsService, ProductDetailsQueryService productDetailsQueryService) {
        this.productDetailsService = productDetailsService;
        this.productDetailsQueryService = productDetailsQueryService;
    }

    @PostMapping("/product-details")
    public ResponseEntity<ProductDetails> createProductDetails(@RequestBody ProductDetails productDetails,
                                                               @RequestParam(value = "productId") Long productId) throws URISyntaxException {
        log.debug("REST request to save ProductDetails : {}", productDetails);
        if (productDetails.getId() != null) {
            throw new BadRequestAlertException("A new productDetails cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Product product = productRepository.findOneById(productId);
        productDetails.setProduct(product);
        ProductDetails result = productDetailsService.save(productDetails);
        return ResponseEntity.created(new URI("/api/product-details/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PutMapping("/product-details")
    public ResponseEntity<ProductDetails> updateProductDetails(@RequestBody ProductDetails productDetails) throws URISyntaxException {
        log.debug("REST request to update ProductDetails : {}", productDetails);
        if (productDetails.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductDetails result = productDetailsService.save(productDetails);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, productDetails.getId().toString()))
            .body(result);
    }

    @GetMapping("/product-details")
    public ResponseEntity<List<ProductDetails>> getAllProductDetails(@RequestParam(value = "id") Long id) {
        List<ProductDetails> list = productDetailsQueryService.findAllById(id);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/product-details/count")
    public ResponseEntity<Long> countProductDetails(ProductDetailsCriteria criteria) {
        log.debug("REST request to count ProductDetails by criteria: {}", criteria);
        return ResponseEntity.ok().body(productDetailsQueryService.countByCriteria(criteria));
    }


    @GetMapping("/product-details/{id}")
    public ResponseEntity<ProductDetails> getProductDetails(@PathVariable Long id) {
        log.debug("REST request to get ProductDetails : {}", id);
        Optional<ProductDetails> productDetails = productDetailsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(productDetails);
    }


    @DeleteMapping("/product-details/{id}")
    public ResponseEntity<Void> deleteProductDetails(@PathVariable Long id) {
        log.debug("REST request to delete ProductDetails : {}", id);
        productDetailsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
