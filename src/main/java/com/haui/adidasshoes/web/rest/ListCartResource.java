package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.domain.ListCart;
import com.haui.adidasshoes.domain.Size;
import com.haui.adidasshoes.domain.User;
import com.haui.adidasshoes.service.ListCartService;
import com.haui.adidasshoes.service.SizeService;
import com.haui.adidasshoes.service.UserService;
import com.haui.adidasshoes.service.dto.cartDTO;
import com.haui.adidasshoes.web.rest.Message.Header;
import com.haui.adidasshoes.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ListCartResource {

    private final Logger log = LoggerFactory.getLogger(ListCartResource.class);

    private static final String ENTITY_NAME = "listCart";

    @Autowired
    private  SizeService sizeService;

    @Autowired
    private  UserService userService;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ListCartService listCartService;

    public ListCartResource(ListCartService listCartService) {
        this.listCartService = listCartService;
    }


    @PostMapping("/list-carts")
    public ResponseEntity<ListCart> createListCart(@RequestBody ListCart listCart) throws URISyntaxException {
        log.debug("REST request to save ListCart : {}", listCart);
        if (listCart.getId() != null) {
            throw new BadRequestAlertException("A new listCart cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ListCart result = listCartService.save(listCart);
        return ResponseEntity.created(new URI("/api/list-carts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PostMapping("/list-carts/create")
    public ResponseEntity<ListCart> createCart(@RequestBody cartDTO cart) throws URISyntaxException {
        ListCart listCart = listCartService.creareCart(cart);
        ListCart result = listCartService.save(listCart);
        return ResponseEntity.created(new URI("/api/list-carts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PutMapping("/list-carts")
    public ResponseEntity<ListCart> updateListCart(@RequestBody ListCart listCart) throws URISyntaxException {
        log.debug("REST request to update ListCart : {}", listCart);
        if (listCart.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ListCart result = listCartService.save(listCart);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, listCart.getId().toString()))
            .body(result);
    }
    @PutMapping("/list-carts/{id}/update-size")
    public ResponseEntity<ListCart> updateListCartSize(@RequestBody String size,
                                                       @PathVariable Long id) throws URISyntaxException {
        Size size1 = sizeService.findOneBySize(size);
        ListCart listCart = listCartService.findOneById(id);
        listCart.setSize(size1);
        ListCart result = listCartService.save(listCart);
        return ResponseEntity.noContent().build();
    }
    @PutMapping("/list-carts/{id}/update-amount")
    public ResponseEntity<ListCart> updateListCartAmount(@RequestBody float count,
                                                       @PathVariable Long id) throws URISyntaxException {
        listCartService.updateaMount(id,count);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/list-carts/total-carts")
    public ResponseEntity<Integer> getTotalCarts(@RequestParam(name = "username",required = false) String username) {
        Integer result = listCartService.getTotalCartsByUsername(username);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/list-carts")
    public ResponseEntity<List<ListCart>> getAllListCarts(@RequestParam(name = "username",required = false) String username, Pageable pageable) {
        log.debug("REST request to get a page of ListCarts");
        Page<ListCart> page = listCartService.findAll(username,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/list-carts/{id}")
    public ResponseEntity<ListCart> getListCart(@PathVariable Long id) {
        log.debug("REST request to get ListCart : {}", id);
        Optional<ListCart> listCart = listCartService.findOne(id);
        return ResponseUtil.wrapOrNotFound(listCart);
    }

    @DeleteMapping("/list-carts/{id}")
    public ResponseEntity<Void> deleteListCart(@PathVariable Long id) {
        log.debug("REST request to delete ListCart : {}", id);
        listCartService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
