package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.domain.TrackOrder;
import com.haui.adidasshoes.service.TrackOrderService;
import com.haui.adidasshoes.web.rest.errors.BadRequestAlertException;
import com.haui.adidasshoes.service.dto.TrackOrderCriteria;
import com.haui.adidasshoes.service.TrackOrderQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.haui.adidasshoes.domain.TrackOrder}.
 */
@RestController
@RequestMapping("/api")
public class TrackOrderResource {

    private final Logger log = LoggerFactory.getLogger(TrackOrderResource.class);

    private static final String ENTITY_NAME = "trackOrder";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TrackOrderService trackOrderService;

    private final TrackOrderQueryService trackOrderQueryService;

    public TrackOrderResource(TrackOrderService trackOrderService, TrackOrderQueryService trackOrderQueryService) {
        this.trackOrderService = trackOrderService;
        this.trackOrderQueryService = trackOrderQueryService;
    }


    @PostMapping("/track-orders")
    public ResponseEntity<TrackOrder> createTrackOrder(@RequestBody TrackOrder trackOrder) throws URISyntaxException {
        log.debug("REST request to save TrackOrder : {}", trackOrder);
        if (trackOrder.getId() != null) {
            throw new BadRequestAlertException("A new trackOrder cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TrackOrder result = trackOrderService.save(trackOrder);
        return ResponseEntity.created(new URI("/api/track-orders/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PutMapping("/track-orders")
    public ResponseEntity<TrackOrder> updateTrackOrder(@RequestBody TrackOrder trackOrder) throws URISyntaxException {
        log.debug("REST request to update TrackOrder : {}", trackOrder);
        if (trackOrder.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TrackOrder result = trackOrderService.save(trackOrder);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, trackOrder.getId().toString()))
            .body(result);
    }

    @GetMapping("/track-orders")
    public ResponseEntity<List<TrackOrder>> getAllTrackOrders(@RequestParam(value = "orderId",required = false) Long orderId) {
        List<TrackOrder> page = trackOrderQueryService.findByOrderId(orderId);
        return ResponseEntity.ok().body(page);
    }


    @GetMapping("/track-orders/count")
    public ResponseEntity<Long> countTrackOrders(TrackOrderCriteria criteria) {
        log.debug("REST request to count TrackOrders by criteria: {}", criteria);
        return ResponseEntity.ok().body(trackOrderQueryService.countByCriteria(criteria));
    }


    @GetMapping("/track-orders/{id}")
    public ResponseEntity<TrackOrder> getTrackOrder(@PathVariable Long id) {
        log.debug("REST request to get TrackOrder : {}", id);
        Optional<TrackOrder> trackOrder = trackOrderService.findOne(id);
        return ResponseUtil.wrapOrNotFound(trackOrder);
    }


    @DeleteMapping("/track-orders/{id}")
    public ResponseEntity<Void> deleteTrackOrder(@PathVariable Long id) {
        log.debug("REST request to delete TrackOrder : {}", id);
        trackOrderService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
