package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.domain.Product;
import com.haui.adidasshoes.domain.ProductImage;
import com.haui.adidasshoes.repository.ProductRepository;
import com.haui.adidasshoes.service.ProductImageService;
import com.haui.adidasshoes.web.rest.errors.BadRequestAlertException;
import com.haui.adidasshoes.service.dto.ProductImageCriteria;
import com.haui.adidasshoes.service.ProductImageQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.haui.adidasshoes.domain.ProductImage}.
 */
@RestController
@RequestMapping("/api")
public class ProductImageResource {

    private final Logger log = LoggerFactory.getLogger(ProductImageResource.class);

    private static final String ENTITY_NAME = "productImage";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    @Autowired
    private  ProductRepository productRepository;

    private final ProductImageService productImageService;

    private final ProductImageQueryService productImageQueryService;

    public ProductImageResource(ProductImageService productImageService, ProductImageQueryService productImageQueryService) {
        this.productImageService = productImageService;
        this.productImageQueryService = productImageQueryService;
    }

    @PostMapping("/product-images")
    public ResponseEntity<ProductImage> createProductImage(@RequestBody ProductImage productImage,
                                                           @RequestParam(value = "productId") Long productId) throws URISyntaxException {
        log.debug("REST request to save ProductImage : {}", productImage);
        if (productImage.getId() != null) {
            throw new BadRequestAlertException("A new productImage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Product product = productRepository.findOneById(productId);
        productImage.setProduct(product);
        ProductImage result = productImageService.save(productImage);
        return ResponseEntity.created(new URI("/api/product-images/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PutMapping("/product-images")
    public ResponseEntity<ProductImage> updateProductImage(@RequestBody ProductImage productImage) throws URISyntaxException {
        log.debug("REST request to update ProductImage : {}", productImage);
        if (productImage.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductImage result = productImageService.save(productImage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, productImage.getId().toString()))
            .body(result);
    }

    @GetMapping("/product-images")
    public ResponseEntity<List<ProductImage>> getAllProductImages(@RequestParam(value = "id") Long id, Pageable pageable) {
        Page<ProductImage> page = productImageQueryService.findById(id, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/product-images/count")
    public ResponseEntity<Long> countProductImages(ProductImageCriteria criteria) {
        log.debug("REST request to count ProductImages by criteria: {}", criteria);
        return ResponseEntity.ok().body(productImageQueryService.countByCriteria(criteria));
    }

    @GetMapping("/product-images/{id}")
    public ResponseEntity<ProductImage> getProductImage(@PathVariable Long id) {
        log.debug("REST request to get ProductImage : {}", id);
        Optional<ProductImage> productImage = productImageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(productImage);
    }

    @DeleteMapping("/product-images/{id}")
    public ResponseEntity<Void> deleteProductImage(@PathVariable Long id) {
        log.debug("REST request to delete ProductImage : {}", id);
        productImageService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
