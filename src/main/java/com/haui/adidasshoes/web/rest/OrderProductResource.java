package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.domain.OrderProduct;
import com.haui.adidasshoes.service.OrderProductService;
import com.haui.adidasshoes.web.rest.errors.BadRequestAlertException;
import com.haui.adidasshoes.service.dto.OrderProductCriteria;
import com.haui.adidasshoes.service.OrderProductQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.haui.adidasshoes.domain.OrderProduct}.
 */
@RestController
@RequestMapping("/api")
public class OrderProductResource {

    private final Logger log = LoggerFactory.getLogger(OrderProductResource.class);

    private static final String ENTITY_NAME = "orderProduct";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrderProductService orderProductService;

    private final OrderProductQueryService orderProductQueryService;

    public OrderProductResource(OrderProductService orderProductService, OrderProductQueryService orderProductQueryService) {
        this.orderProductService = orderProductService;
        this.orderProductQueryService = orderProductQueryService;
    }

    @PostMapping("/order-products")
    public ResponseEntity<OrderProduct> createOrderProduct(@RequestBody OrderProduct orderProduct) throws URISyntaxException {
        log.debug("REST request to save OrderProduct : {}", orderProduct);
        if (orderProduct.getId() != null) {
            throw new BadRequestAlertException("A new orderProduct cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrderProduct result = orderProductService.save(orderProduct);
        return ResponseEntity.created(new URI("/api/order-products/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PutMapping("/order-products")
    public ResponseEntity<OrderProduct> updateOrderProduct(@RequestBody OrderProduct orderProduct) throws URISyntaxException {
        log.debug("REST request to update OrderProduct : {}", orderProduct);
        if (orderProduct.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OrderProduct result = orderProductService.save(orderProduct);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, orderProduct.getId().toString()))
            .body(result);
    }

    @GetMapping("/order-products")
    public ResponseEntity<List<OrderProduct>> getAllOrderProducts(OrderProductCriteria criteria, Pageable pageable) {
        log.debug("REST request to get OrderProducts by criteria: {}", criteria);
        Page<OrderProduct> page = orderProductQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/order-products/count")
    public ResponseEntity<Long> countOrderProducts(OrderProductCriteria criteria) {
        log.debug("REST request to count OrderProducts by criteria: {}", criteria);
        return ResponseEntity.ok().body(orderProductQueryService.countByCriteria(criteria));
    }

    @GetMapping("/order-products/{id}")
    public ResponseEntity<OrderProduct> getOrderProduct(@PathVariable Long id) {
        log.debug("REST request to get OrderProduct : {}", id);
        Optional<OrderProduct> orderProduct = orderProductService.findOne(id);
        return ResponseUtil.wrapOrNotFound(orderProduct);
    }

    @DeleteMapping("/order-products/{id}")
    public ResponseEntity<Void> deleteOrderProduct(@PathVariable Long id) {
        log.debug("REST request to delete OrderProduct : {}", id);
        orderProductService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
