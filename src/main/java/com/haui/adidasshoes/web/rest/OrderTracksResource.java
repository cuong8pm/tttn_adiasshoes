package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.domain.OrderTracks;
import com.haui.adidasshoes.service.OrderTracksService;
import com.haui.adidasshoes.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class OrderTracksResource {

    private final Logger log = LoggerFactory.getLogger(OrderTracksResource.class);

    private static final String ENTITY_NAME = "orderTracks";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrderTracksService orderTracksService;

    public OrderTracksResource(OrderTracksService orderTracksService) {
        this.orderTracksService = orderTracksService;
    }

    @PostMapping("/order-tracks")
    public ResponseEntity<OrderTracks> createOrderTracks(@RequestBody OrderTracks orderTracks) throws URISyntaxException {
        log.debug("REST request to save OrderTracks : {}", orderTracks);
        if (orderTracks.getId() != null) {
            throw new BadRequestAlertException("A new orderTracks cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrderTracks result = orderTracksService.save(orderTracks);
        return ResponseEntity.created(new URI("/api/order-tracks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PutMapping("/order-tracks")
    public ResponseEntity<OrderTracks> updateOrderTracks(@RequestBody OrderTracks orderTracks) throws URISyntaxException {
        log.debug("REST request to update OrderTracks : {}", orderTracks);
        if (orderTracks.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OrderTracks result = orderTracksService.save(orderTracks);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, orderTracks.getId().toString()))
            .body(result);
    }

    @GetMapping("/order-tracks")
    public ResponseEntity<List<OrderTracks>> getAllOrderTracks(Pageable pageable) {
        log.debug("REST request to get a page of OrderTracks");
        Page<OrderTracks> page = orderTracksService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/order-tracks/{id}")
    public ResponseEntity<OrderTracks> getOrderTracks(@PathVariable Long id) {
        log.debug("REST request to get OrderTracks : {}", id);
        Optional<OrderTracks> orderTracks = orderTracksService.findOne(id);
        return ResponseUtil.wrapOrNotFound(orderTracks);
    }

    @DeleteMapping("/order-tracks/{id}")
    public ResponseEntity<Void> deleteOrderTracks(@PathVariable Long id) {
        log.debug("REST request to delete OrderTracks : {}", id);
        orderTracksService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
