package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.domain.ListProduct;
import com.haui.adidasshoes.domain.Product;
import com.haui.adidasshoes.service.ListProductService;
import com.haui.adidasshoes.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.haui.adidasshoes.domain.ListProduct}.
 */
@RestController
@RequestMapping("/api")
public class ListProductResource {

    private final Logger log = LoggerFactory.getLogger(ListProductResource.class);

    private static final String ENTITY_NAME = "listProduct";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ListProductService listProductService;

    public ListProductResource(ListProductService listProductService) {
        this.listProductService = listProductService;
    }

    @GetMapping("/list-products")
    public ResponseEntity<List<Product>> getAllListProductsWithPage(@RequestParam(name = "type",required = false) Long type,
                                                                    @RequestParam(name = "name",required = false) String name,
                                                                    @RequestParam(name = "toPrice",required = false) String toPrice,
                                                                    @RequestParam(name = "formPrice",required = false) String fromPrice,
                                                                    Pageable pageable) {
        Page<Product> page = listProductService.findAllWithPage(type,name,toPrice,fromPrice,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/list-products/search-tree")
    public ResponseEntity<List<Product>> getAllListProductsByName(@RequestParam(name = "name",defaultValue = "",required = false) String name) {
        List<Product> page = listProductService.findAllByName(name);
        return ResponseEntity.ok().body(page);
    }

    @PostMapping("/list-products")
    public ResponseEntity<ListProduct> createListProduct(@RequestBody ListProduct listProduct) throws URISyntaxException {
        if (listProduct.getId() != null) {
            throw new BadRequestAlertException("A new listProduct cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ListProduct result = listProductService.save(listProduct);
        return ResponseEntity.created(new URI("/api/list-products/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PutMapping("/list-products")
    public ResponseEntity<ListProduct> updateListProduct(@RequestBody ListProduct listProduct) throws URISyntaxException {
        if (listProduct.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ListProduct result = listProductService.save(listProduct);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, listProduct.getId().toString()))
            .body(result);
    }

    @GetMapping("/list-productss")
    public ResponseEntity<List<ListProduct>> getAllListProducts(Pageable pageable) {
        log.debug("REST request to get a page of ListProducts");
        Page<ListProduct> page = listProductService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/list-products/{id}")
    public ResponseEntity<ListProduct> getListProduct(@PathVariable Long id) {
        log.debug("REST request to get ListProduct : {}", id);
        Optional<ListProduct> listProduct = listProductService.findOne(id);
        return ResponseUtil.wrapOrNotFound(listProduct);
    }

    @DeleteMapping("/list-products/{id}")
    public ResponseEntity<Void> deleteListProduct(@PathVariable Long id) {
        log.debug("REST request to delete ListProduct : {}", id);
        listProductService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
