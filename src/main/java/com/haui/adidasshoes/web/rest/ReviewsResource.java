package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.domain.Reviews;
import com.haui.adidasshoes.service.ReviewsService;
import com.haui.adidasshoes.service.dto.Statistical;
import com.haui.adidasshoes.service.dto.StatisticalDTO;
import com.haui.adidasshoes.web.rest.errors.BadRequestAlertException;
import com.haui.adidasshoes.service.dto.ReviewsCriteria;
import com.haui.adidasshoes.service.ReviewsQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link com.haui.adidasshoes.domain.Reviews}.
 */
@RestController
@RequestMapping("/api")
public class ReviewsResource {

    private final Logger log = LoggerFactory.getLogger(ReviewsResource.class);

    private static final String ENTITY_NAME = "reviews";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ReviewsService reviewsService;

    private final ReviewsQueryService reviewsQueryService;

    public ReviewsResource(ReviewsService reviewsService, ReviewsQueryService reviewsQueryService) {
        this.reviewsService = reviewsService;
        this.reviewsQueryService = reviewsQueryService;
    }

    @GetMapping("/reviews/statistical")
    public ResponseEntity<List<StatisticalDTO>> getStatistical() {
        List<StatisticalDTO> result = reviewsService.getStatistical();
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/reviews/statistical-number")
    public ResponseEntity<Map<String,Integer>> getStatisticalNumber() {
        Map<String,Integer> result = reviewsService.getStatisticalNumber();
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/reviews")
    public ResponseEntity<Reviews> createReviews(@RequestBody Reviews reviews) throws URISyntaxException {
        log.debug("REST request to save Reviews : {}", reviews);
        if (reviews.getId() != null) {
            throw new BadRequestAlertException("A new reviews cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Reviews result = reviewsService.save(reviews);
        return ResponseEntity.created(new URI("/api/reviews/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PutMapping("/reviews")
    public ResponseEntity<Reviews> updateReviews(@RequestBody Reviews reviews) throws URISyntaxException {
        log.debug("REST request to update Reviews : {}", reviews);
        if (reviews.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Reviews result = reviewsService.save(reviews);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, reviews.getId().toString()))
            .body(result);
    }


    @GetMapping("/reviews")
    public ResponseEntity<List<Reviews>> getAllReviews(@RequestParam(value = "id") Long id) {
        List<Reviews> list = reviewsQueryService.findAllById(id);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/reviews/count")
    public ResponseEntity<Long> countReviews(ReviewsCriteria criteria) {
        log.debug("REST request to count Reviews by criteria: {}", criteria);
        return ResponseEntity.ok().body(reviewsQueryService.countByCriteria(criteria));
    }

    @GetMapping("/reviews/{id}")
    public ResponseEntity<Reviews> getReviews(@PathVariable Long id) {
        log.debug("REST request to get Reviews : {}", id);
        Optional<Reviews> reviews = reviewsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(reviews);
    }

    @DeleteMapping("/reviews/{id}")
    public ResponseEntity<Void> deleteReviews(@PathVariable Long id) {
        log.debug("REST request to delete Reviews : {}", id);
        reviewsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
