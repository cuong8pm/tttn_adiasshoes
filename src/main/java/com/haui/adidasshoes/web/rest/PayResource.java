package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.domain.*;
import com.haui.adidasshoes.service.OrdersService;
import com.haui.adidasshoes.service.PayService;
import com.haui.adidasshoes.service.dto.ListCartDTO;
import com.haui.adidasshoes.service.dto.OrderUserDTO;
import com.haui.adidasshoes.service.dto.cartDTO;
import com.haui.adidasshoes.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

/**
 * REST controller for managing {@link com.haui.adidasshoes.domain.Pay}.
 */
@RestController
@RequestMapping("/api")
public class PayResource {

    private final Logger log = LoggerFactory.getLogger(PayResource.class);

    private static final String ENTITY_NAME = "pay";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    @Autowired
    private OrdersService ordersService;

    private final PayService payService;

    public PayResource(PayService payService) {
        this.payService = payService;
    }

    @GetMapping("/track-order-users/all-order")
    public ResponseEntity<List<OrderUserDTO>> getAllOrder(@RequestParam(name = "username") String username,
                                                          @RequestParam(name = "track") Integer track) {
        List<OrderUserDTO> orderUserDTOList = payService.getAllOrder(username,track);
        return ResponseEntity.ok().body(orderUserDTOList);
    }

    @GetMapping("/track-order-users/order-detail")
    public ResponseEntity<OrderUserDTO> getOrder(@RequestParam(name = "orderId") Long orderId) {
        OrderUserDTO orderUserDTO = payService.getOrder(orderId);
        return ResponseEntity.ok().body(orderUserDTO);
    }

    @PutMapping("/pays")
    public ResponseEntity<Pay> updatePay(@RequestBody Pay pay) throws URISyntaxException {
        log.debug("REST request to update Pay : {}", pay);
        if (pay.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Pay result = payService.save(pay);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, pay.getId().toString()))
            .body(result);
    }

    @GetMapping("/pays")
    public ResponseEntity<List<Pay>> getAllPays(Pageable pageable) {
        log.debug("REST request to get a page of Pays");
        Page<Pay> page = payService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/pays/{id}")
    public ResponseEntity<Pay> getPay(@PathVariable Long id) {
        log.debug("REST request to get Pay : {}", id);
        Optional<Pay> pay = payService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pay);
    }

    @PostMapping("/pays/list-cart-total")
    public ResponseEntity<Map<String,Double>> getListCartTotal(@RequestBody List<ListCartDTO> ListCartDTO){
        Map<String,Double> totalPrice = payService.getTotalPrice(ListCartDTO);
        return ResponseEntity.ok().body(totalPrice);
    }

    @PostMapping("/pays/list-cart")
    public ResponseEntity<List<ListCartDTO>> getListCart(@RequestParam(name = "username") String username,
                                                         @RequestBody Map<String,Long[]> IDListCarts){
        List<ListCartDTO> listCartsDTO = payService.getListCarts(IDListCarts);
        return ResponseEntity.ok().body(listCartsDTO);
    }

    @PostMapping("/pays/list-product")
    public ResponseEntity<List<ListCartDTO>> getListProduct(@RequestBody cartDTO cart){
        List<ListCartDTO> listCartsDTO = payService.getListProduct(cart);
        return ResponseEntity.ok().body(listCartsDTO);
    }

    @GetMapping("/pays/get-user")
    public ResponseEntity<User> getUser(@RequestParam(name = "username") String username) {
        User user = payService.getUser(username);
        return ResponseEntity.ok().body(user);
    }

    @PostMapping("/pays/create-order")
    public ResponseEntity<Orders> createOrder(@RequestParam(name = "username") String username,
                                               @RequestBody(required = false) List<ListCartDTO> listCartDTOS) throws URISyntaxException {
        Orders result = ordersService.saveOrder(username,listCartDTOS);
        return ResponseEntity.status(HttpStatus.CREATED).body(result);
    }

    @PostMapping("/pays/create-order-user-now")
    public ResponseEntity<Orders> createOrderUserNow(@RequestParam(name = "username") String username,
                                              @RequestBody(required = false) List<ListCartDTO> listCartDTOS) throws URISyntaxException {
        Orders result = ordersService.saveOrderNow(username,listCartDTOS);
        return ResponseEntity.status(HttpStatus.CREATED).body(result);
    }

    @PostMapping("/pays/create-order-now")
    public ResponseEntity<OrderNow> createOrderNow(@RequestParam(name = "name") String name,
                                                 @RequestParam(name = "phone") String phone,
                                                 @RequestParam(name = "address") String address,
                                                 @RequestBody(required = false) List<ListCartDTO> listCartDTOS) throws URISyntaxException {
        OrderNow orderNow = payService.createOrderNow(name,phone,address,listCartDTOS);
        return ResponseEntity.created(new URI("/api/pays/" + orderNow.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, orderNow.getId().toString()))
            .body(orderNow);
    }

    @DeleteMapping("/pays/{id}")
    public ResponseEntity<Void> deletePay(@PathVariable Long id) {
        log.debug("REST request to delete Pay : {}", id);
        payService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
