package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.domain.OrderNow;
import com.haui.adidasshoes.service.OrderNowService;
import com.haui.adidasshoes.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.haui.adidasshoes.domain.OrderNow}.
 */
@RestController
@RequestMapping("/api")
public class OrderNowResource {

    private final Logger log = LoggerFactory.getLogger(OrderNowResource.class);

    private static final String ENTITY_NAME = "orderNow";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrderNowService orderNowService;

    public OrderNowResource(OrderNowService orderNowService) {
        this.orderNowService = orderNowService;
    }


    @PostMapping("/order-nows")
    public ResponseEntity<OrderNow> createOrderNow(@RequestBody OrderNow orderNow) throws URISyntaxException {
        log.debug("REST request to save OrderNow : {}", orderNow);
        if (orderNow.getId() != null) {
            throw new BadRequestAlertException("A new orderNow cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrderNow result = orderNowService.save(orderNow);
        return ResponseEntity.created(new URI("/api/order-nows/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PutMapping("/order-nows")
    public ResponseEntity<OrderNow> updateOrderNow(@RequestBody OrderNow orderNow) throws URISyntaxException {
        log.debug("REST request to update OrderNow : {}", orderNow);
        if (orderNow.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OrderNow result = orderNowService.save(orderNow);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, orderNow.getId().toString()))
            .body(result);
    }

    @GetMapping("/order-nows")
    public ResponseEntity<List<OrderNow>> getAllOrderNows(@RequestParam(name="confirm",required = false) String confirm,
                                                          Pageable pageable) {
        log.debug("REST request to get a page of OrderNows");
        Page<OrderNow> page = orderNowService.getALL(confirm,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/order-nows/{id}")
    public ResponseEntity<OrderNow> getOrderNow(@PathVariable Long id) {
        log.debug("REST request to get OrderNow : {}", id);
        Optional<OrderNow> orderNow = orderNowService.findOne(id);
        return ResponseUtil.wrapOrNotFound(orderNow);
    }

    @DeleteMapping("/order-nows/{id}")
    public ResponseEntity<Void> deleteOrderNow(@PathVariable Long id) {
        log.debug("REST request to delete OrderNow : {}", id);
        orderNowService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
