package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.domain.Size;
import com.haui.adidasshoes.service.SizeService;
import com.haui.adidasshoes.web.rest.errors.BadRequestAlertException;
import com.haui.adidasshoes.service.dto.SizeCriteria;
import com.haui.adidasshoes.service.SizeQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.haui.adidasshoes.domain.Size}.
 */
@RestController
@RequestMapping("/api")
public class SizeResource {

    private final Logger log = LoggerFactory.getLogger(SizeResource.class);

    private static final String ENTITY_NAME = "size";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SizeService sizeService;

    private final SizeQueryService sizeQueryService;

    public SizeResource(SizeService sizeService, SizeQueryService sizeQueryService) {
        this.sizeService = sizeService;
        this.sizeQueryService = sizeQueryService;
    }


    @PostMapping("/sizes")
    public ResponseEntity<Size> createSize(@RequestBody Size size) throws URISyntaxException {
        log.debug("REST request to save Size : {}", size);
        if (size.getId() != null) {
            throw new BadRequestAlertException("A new size cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Size result = sizeService.save(size);
        return ResponseEntity.created(new URI("/api/sizes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }


    @PutMapping("/sizes")
    public ResponseEntity<Size> updateSize(@RequestBody Size size) throws URISyntaxException {
        log.debug("REST request to update Size : {}", size);
        if (size.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Size result = sizeService.save(size);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, size.getId().toString()))
            .body(result);
    }


    @GetMapping("/sizes")
    public ResponseEntity<List<Size>> getAllSizes(SizeCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Sizes by criteria: {}", criteria);
        Page<Size> page = sizeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    @GetMapping("/sizes/count")
    public ResponseEntity<Long> countSizes(SizeCriteria criteria) {
        log.debug("REST request to count Sizes by criteria: {}", criteria);
        return ResponseEntity.ok().body(sizeQueryService.countByCriteria(criteria));
    }


    @GetMapping("/sizes/{id}")
    public ResponseEntity<Size> getSize(@PathVariable Long id) {
        log.debug("REST request to get Size : {}", id);
        Optional<Size> size = sizeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(size);
    }


    @DeleteMapping("/sizes/{id}")
    public ResponseEntity<Void> deleteSize(@PathVariable Long id) {
        log.debug("REST request to delete Size : {}", id);
        sizeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
