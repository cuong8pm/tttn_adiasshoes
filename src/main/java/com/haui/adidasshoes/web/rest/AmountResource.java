package com.haui.adidasshoes.web.rest;

import com.haui.adidasshoes.domain.Amount;
import com.haui.adidasshoes.service.AmountService;
import com.haui.adidasshoes.web.rest.errors.BadRequestAlertException;
import com.haui.adidasshoes.service.dto.AmountCriteria;
import com.haui.adidasshoes.service.AmountQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.haui.adidasshoes.domain.Amount}.
 */
@RestController
@RequestMapping("/api")
public class AmountResource {

    private final Logger log = LoggerFactory.getLogger(AmountResource.class);

    private static final String ENTITY_NAME = "amount";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AmountService amountService;

    private final AmountQueryService amountQueryService;

    public AmountResource(AmountService amountService, AmountQueryService amountQueryService) {
        this.amountService = amountService;
        this.amountQueryService = amountQueryService;
    }

    @PostMapping("/amounts")
    public ResponseEntity<Amount> createAmount(@RequestBody Amount amount) throws URISyntaxException {
        log.debug("REST request to save Amount : {}", amount);
        if (amount.getId() != null) {
            throw new BadRequestAlertException("A new amount cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Amount result = amountService.save(amount);
        return ResponseEntity.created(new URI("/api/amounts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PutMapping("/amounts")
    public ResponseEntity<Amount> updateAmount(@RequestBody Amount amount) throws URISyntaxException {
        log.debug("REST request to update Amount : {}", amount);
        if (amount.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Amount result = amountService.update(amount);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, amount.getId().toString()))
            .body(result);
    }

    @GetMapping("/amounts")
    public ResponseEntity<List<Amount>> getAllAmounts(@RequestParam(value = "productId",required = false) Long productId,
                                                      Pageable pageable) {
        Page<Amount> page = amountQueryService.findByProductId(productId,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    @GetMapping("/amounts-all")
    public ResponseEntity<List<Amount>> getAllAmountss() {
        List<Amount> list = amountService.findAllAmount();
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/amounts/count")
    public ResponseEntity<Long> countAmounts(AmountCriteria criteria) {
        log.debug("REST request to count Amounts by criteria: {}", criteria);
        return ResponseEntity.ok().body(amountQueryService.countByCriteria(criteria));
    }

    @GetMapping("/amounts/{id}")
    public ResponseEntity<Amount> getAmount(@PathVariable Long id) {
        log.debug("REST request to get Amount : {}", id);
        Optional<Amount> amount = amountService.findOne(id);
        return ResponseUtil.wrapOrNotFound(amount);
    }

    @DeleteMapping("/amounts/{id}")
    public ResponseEntity<Void> deleteAmount(@PathVariable Long id) {
        log.debug("REST request to delete Amount : {}", id);
        amountService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
